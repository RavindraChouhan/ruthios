//
//  Extensions.swift
//  Ruth
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import Foundation

extension UIColor {
    class func hexStringToColor(hex: String) -> UIColor {
        return UIColor.hexStr(Str: hex, alpha: 1)
    }
    class private func hexStr(Str: String, alpha: CGFloat) -> UIColor {
        let hexStr = Str.replacingOccurrences(of: "#", with: "")
        let scanner = Scanner(string: hexStr)
        var color: UInt32 = 0
        if scanner.scanHexInt32(&color) {
            let r = CGFloat((color & 0xFF0000) >> 16) / 255.0
            let g = CGFloat((color & 0x00FF00) >> 8) / 255.0
            let b = CGFloat(color & 0x0000FF) / 255.0
            return UIColor(red:r,green:g,blue:b,alpha:alpha)
        } else {
            print("invalid hex string", terminator: "")
            return UIColor.white;
        }
    }
}
extension Date {
    
    // MARK:- offset From (use for get counter alert time)
    func offsetFrom(date: Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        //        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: );
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: self, to: date)
        let strSecond = NSString(format: "%02d",difference.second!) as String
        let strMinutes = NSString(format: "%02d:",difference.minute!) as String
        let strHours = NSString(format: "%02d:",difference.hour!) as String //"\(difference.hour ?? 0):" //+  minutes
        if strHours != "00:"
        {
            return strHours+strMinutes+strSecond
        }else{
            return strMinutes+strSecond
        }
    }
    
    // MARK:- add Component in date
    static func addComponent(component : Calendar.Component,  n: Int, date :Date) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: component, value: n, to: date)!
    }
}
extension NSAttributedStringKey {
    static let myName = NSAttributedStringKey(rawValue: "myCustomAttributeKey")
}
extension UIViewController {
    func containsOnlyLetters(input: String) -> Bool {
        for chr in input {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    }
    func setTabBarVisible(visible:Bool, animated:Bool) {
        if let control = Util.util.tabControl.view.viewWithTag(999) as? CustomTabBar {
            Util.util.tabControl.tabBar.isHidden = true
            if visible {
                UIView.animate(withDuration: 0.4) {
                    if(DeviceType.iPhoneX){
                        control.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 95, width: UIScreen.main.bounds.width, height: 115)
                    } else {
                        control.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - control.frame.size.height, width: UIScreen.main.bounds.width, height: control.frame.size.height)
                    }
                    return
                }
            } else {
                UIView.animate(withDuration: 0.4) {
                    control.frame = CGRect(x: 0, y: 1000, width: UIScreen.main.bounds.width, height: control.frame.size.height)
                    return
                }
            }
        }
    }
    
    typealias AlertHandlerWithOption = (_ buttonIdx: Int) -> Swift.Void
    func showAlert( title : String? = "" , msg: String? , alertTypeOk:Bool = true ,  btnPositiveTitle : String? = "Ok",  btnNegativeTitle : String? = "Cancel",  callBack:AlertHandlerWithOption?) {
        let alert = UIAlertController(title: title, message: msg!, preferredStyle: .alert)
        if !alertTypeOk {
            alert.addAction(UIAlertAction(title: btnPositiveTitle!, style: .default, handler: { (action) in
                //   self.view.frame = CGRect(x: 0, y: 64, width: sWidth, height:  sHeight)
                if callBack != nil{
                    callBack!(1)
                }
            }))
        }
        alert.addAction(UIAlertAction(title: btnNegativeTitle!, style: .default, handler: { (action) in
            //  self.view.frame = CGRect(x: 0, y: 64, width: sWidth, height:  sHeight)
            if callBack != nil{
                callBack!(0)
            }
        }))
        // self.view.frame = CGRect(x: 0, y: 64, width: sWidth, height:  sHeight)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func addTapGestureToView() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideBoards))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideBoards() {
        self.view.endEditing(true)
    }

    func addInvaildDeviceNotificationObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(invaildDevice), name: AppNotifications.notificationUpdateInvalidDevice, object: nil)
    }
    
    //    let isShowInvaildDevice = false
    @objc func invaildDevice() {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: AppNotifications.notificationUpdateInvalidDevice, object: nil)
        UserNew.currentUser.logout()
        let controllerArray = self.navigationController?.childViewControllers
        if  let controller = controllerArray!.last
        {
            if !(controller is LoginController) {
                let stroryBord = UIStoryboard(name: "Main", bundle: nil)
                let landingViewController = stroryBord.instantiateViewController(withIdentifier: "LandingViewController") as! LandingViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                var addWindow: UIWindow!
                if let window = appDelegate.window
                {
                    addWindow = window
                }else{
                    addWindow = UIWindow(frame: UIScreen.main.bounds)
                }
                appDelegate.window = addWindow
                let nav1 = UINavigationController(rootViewController: landingViewController)
                appDelegate.window?.rootViewController = nav1
                appDelegate.window?.makeKeyAndVisible()
            }
        }
    }
    
    func makeTransprentNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
         UINavigationBar.appearance().backgroundColor = .clear
        //self.navigationController?.navigationBar.isTranslucent = true
    }
    // MARK:- For Add left button in navigation Controller with UIButton
    func addLeftbuttomWithController(withCustomView btn: UIButton) {
        let leftBtn = UIBarButtonItem(customView: btn)
        self.navigationItem.leftBarButtonItem = leftBtn
    }
    func addLeftViewController(withCustomView view: UIView) {
        let leftView = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftView
    }
    // MARK:- For Add Right button in navigation Controller with UIButton
    func addRightbuttomWithController(withCustomView btn: UIView) {
        let rightBtn = UIBarButtonItem(customView: btn)
        self.navigationItem.rightBarButtonItem = rightBtn
    }
    
    func showAlertViewWithAlertView(viewAlert:UIView, WithBlackTransperentView viewblackTransperent: UIView ) {
        if isAlertShow {
            UIView.animate(withDuration: 0.5, animations: {
                viewAlert.transform = CGAffineTransform.identity
                viewAlert.transform = CGAffineTransform.init(scaleX: 0, y: 0)
                viewblackTransperent.isHidden = true
            }, completion: { (BOOL) in
                viewAlert.isHidden = true
                isAlertShow = false
            })
        }
        if (!isAlertShow) {
            viewAlert.transform = CGAffineTransform.init(scaleX: 0, y: 0)
            viewAlert.isHidden = false
            UIView.animate(withDuration: 0.5, animations: {
                viewAlert.transform = CGAffineTransform.identity
                viewblackTransperent.isHidden = false
            }, completion: { (BOOL) in
                isAlertShow = true
            })
        }
    }
    func setupNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.backgroundColor = .black
        self.navigationController?.navigationBar.tintColor = .white
//        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    public func showToast(message : String) {
        let yPos: CGFloat = DeviceType.iPhoneX ? 150 : 100
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-yPos, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont.systemFont(ofSize: 13)   
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds = true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func getNumberWithCode(mobile: String) -> String {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist")
            let CallingCodes = NSArray(contentsOfFile: path!) as! [[String: String]]
            let countryData = CallingCodes.filter { $0["code"] == countryCode }
            var cCode = countryData[0]["dial_code"]!
            cCode = "+852"
            //if cCode == "+852" { }
            let aString = cCode + mobile
            return aString.replacingOccurrences(of: "+", with: "")
        }
        else { return "" }
    }
    func socialLoginWith(socailID:String, userName: String, imageURL: String, deviceToken: String,deviceType:String ,accountType: String, email: String? = nil) {
        Util.util.showHUD()
        NetworkClient.shared.loginWithSocialID(username: userName, socialId: socailID, image: imageURL, type: accountType, emailID: email, deviceToken: deviceToken) { (user, error) -> (Void) in
            Util.util.hideHUD()
            guard error == nil else {
                self.showAlertMessage(message: error ?? "")
                return
            }
            if !UserNew.currentUser.isNewUser {
               objAppDelegate.checkLoginInAppLaunch()
            } else {
                guard let tutorial = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.inviteView.rawValue) as? InviteController else { return }
                let navControl = UINavigationController(rootViewController: tutorial)
                self.present(navControl, animated: true, completion: nil)
            }
        }
    }
    
//    // MARK: Load HomeController
//    func loadHomeController() {
//        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        self.tabControl = storyboard.instantiateViewController(withIdentifier: "tabControl") as! UITabBarController
//        self.navigationController?.pushViewController(self.tabControl, animated: true)
//        self.setUpTabBar()
//        UINavigationBar.appearance().tintColor = UIColor.white
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size:  18.0)!]
//        NotificationCenter.default.addObserver(self, selector: #selector(self.ChangeTabBar(notification:)), name: AppNotifications.notificationChangeTabBar, object: nil)
//    }
//
//    // MARK: Change TabBar notification
//    @objc func ChangeTabBar(notification:Notification)  {
//        let num:NSNumber = notification.object as! NSNumber
//        let index:NSInteger = num.intValue
//        //log(msg: "index == \(index)")
//        self.tabControl.selectedIndex = index
//        Util.util.tabControl = self.tabControl
//        //        UserDefaults.standard.set(false, forKey: "isAlbumLoaded")
//        //        UserDefaults.standard.synchronize()
//    }
//
//    // MARK: Setup TabBar UI
//    func setUpTabBar() {
//        let customtab:CustomTabBar = Bundle.main.loadNibNamed("CustomTabBar", owner: self, options: nil)?[0] as! CustomTabBar
//        if(DeviceType.iPhoneX){
//            customtab.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 116, width: UIScreen.main.bounds.width, height: 116)
//        }else{
//            customtab.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - customtab.frame.size.height, width: UIScreen.main.bounds.width, height: customtab.frame.size.height)
//        }
//        customtab.tag = 999
//        //        customtab.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleWidth]
//        self.tabControl.view.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleWidth]
//        self.tabControl.view.addSubview(customtab)
//        Util.util.tabControl = self.tabControl
//        Util.util.tabControl.selectedIndex = 0
//    }
    
    
    
    
    
    func calculateAge(dob : String) -> (year :Int, month : Int, day : Int){
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yyyy"
//        df.dateFormat = "MMM, dd, yyyy"
        let date = df.date(from: dob)
        guard let val = date else {
            return (0, 0, 0)
        }
        var years = 0
        var months = 0
        var days = 0
        
        let cal = Calendar.current
        years = cal.component(.year, from: Date()) -  cal.component(.year, from: val)
        
        let currMonth = cal.component(.month, from: Date())
        let birthMonth = cal.component(.month, from: val)
        
        //get difference between current month and birthMonth
        months = currMonth - birthMonth
        if months < 0 {
            years = years - 1
            months = 12 - birthMonth + currMonth
            if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val){
                months = months - 1
            }
        }
        else if months == 0 && cal.component(.day, from: Date()) < cal.component(.day, from: val) {
            years = years - 1
            months = 11
        }
        
        //Calculate the days
        if cal.component(.day, from: Date()) > cal.component(.day, from: val){
            days = cal.component(.day, from: Date()) - cal.component(.day, from: val)
        }
        else if cal.component(.day, from: Date()) < cal.component(.day, from: val) {
            let today = cal.component(.day, from: Date())
            let date = cal.date(byAdding: .month, value: -1, to: Date(), wrappingComponents: true)
            
            days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
        }
        else {
            days = 0
            if months == 12 {
                years = years + 1
                months = 0
            }
        }
        return (years, months, days)
    }
}

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        return NSPredicate(format:"SELF MATCHES %@", emailRegEx).evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        let pwdRegEx = "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$"        
        return NSPredicate(format:"SELF MATCHES %@", pwdRegEx).evaluate(with: self)
    }
    static func chanage24To12Hrs(_ strTime: String ) -> String {
        var temp = strTime
        if strTime == "24:00"{
            temp = "00:00"
        }
        let dateformat = DateFormatter()
        dateformat.dateFormat = "HH:mm"
        let dateTime = dateformat.date(from: temp)
        dateformat.dateFormat = "h:m a"
        let strDate = dateformat.string(from: dateTime!)
        rLog("Convrted Time = \(strDate)")
        return strDate
    }
    func checkStringCharCount() -> [String:Any] {
        let letters = CharacterSet.letters
        let digits = CharacterSet.decimalDigits
        
        var letterCount = 0
        var digitCount = 0
        
        for uni in self.unicodeScalars {
            if letters.contains(uni) {
                letterCount += 1
            } else if digits.contains(uni) {
                digitCount += 1
            }
        }
        let dict = ["letter" : letterCount, "digit" : digitCount]
        return dict
    }
    
    func trim() -> String {
        let trimStr:String = self.trimmingCharacters(in: NSCharacterSet.newlines)
        return trimStr.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func gethight(_ text: String, with font: UIFont, withLable width: Int) -> CGFloat {
        let maximumLabelSize = CGSize(width: CGFloat(width), height:CGFloat(UINT32_MAX))
        let textRect: CGRect = text.boundingRect(with: maximumLabelSize, options: ([.usesLineFragmentOrigin, .usesFontLeading]), attributes: [NSAttributedStringKey.font: font], context: nil)
        return textRect.size.height
    }
    
    func getFormetedPhoneNumber() -> String {
        let tmp1 = self.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
        let tmp2 = tmp1.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
        let tmp3 = tmp2.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
        let strNo = tmp3.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
        return strNo
    }
    
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func base64Decoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
   static func randomString(_ n: Int) -> String
    {
        let a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        
        var s = ""
        
        for _ in 0..<n
        {
            let r = Int(arc4random_uniform(UInt32(a.count)))
            
            s += String(a[a.index(a.startIndex, offsetBy: r)])
        }
        
        return s
    }
}

extension UITextField: UITextFieldDelegate {
    func addDoneButtonPickerView()  {
        let doneToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 40))
        doneToolbar.backgroundColor = .white
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 65, height: 30)
        button.setTitle("Done".localized(), for: .normal)
        let color = UIColor.hexStringToColor(hex: "00A90C")
        button.setTitleColor(color, for: .normal)
        button.backgroundColor = UIColor .clear
        button.addTarget(self, action: #selector(self.actionDone(btn:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(self.actionDone(btn:)), for: .touchUpOutside)
        let doneButton: UIBarButtonItem = UIBarButtonItem(customView: button)
        let negativeSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpace.width = -10.0
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        doneToolbar.setItems([flexSpace, doneButton, negativeSpace], animated: false)
        self.inputAccessoryView = doneToolbar
    }
    
    @IBAction func actionDone(btn: UIButton) {
        NotificationCenter.default.post(name: AppNotifications.notificationAgeSelected, object: nil)
        self.resignFirstResponder()
    }
    
    func addToolbarOnTextfield()  {
        let doneToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 40))
        doneToolbar.backgroundColor = .white
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 65, height: 30)
        button.setTitle("Done".localized(), for: .normal)
        let color = UIColor.hexStringToColor(hex: "00A90C")
        button.setTitleColor(color, for: .normal)
        button.backgroundColor = UIColor .clear
        button.addTarget(self, action: #selector(self.hideKeyboards), for: .touchUpInside)
        button.addTarget(self, action: #selector(self.hideKeyboards), for: .touchUpOutside)
        let doneButton: UIBarButtonItem = UIBarButtonItem(customView: button)
        let negativeSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = -10.0
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        doneToolbar.setItems([flexSpace, doneButton, negativeSpace], animated: false)
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func hideKeyboards() {
        self.resignFirstResponder()
    }
    
    func setPlaceholderColor(_ placeholder : String) {
        self.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                                 attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
    }
    
    func toggleSecureEntry() {
        let wasFirstResponder = isFirstResponder
        if wasFirstResponder { resignFirstResponder() }
        isSecureTextEntry = !isSecureTextEntry
        if wasFirstResponder { becomeFirstResponder() }
    }
}

extension UILabel {
    func labelHeight() -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        label.sizeToFit()
        return label.frame.height
    }
}

extension UITableView {
    
    // MARK:-  Add Footer View in UITableView
    func addFooterView(withFooterHight hight : CGFloat) {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: hight))
        viewFooter.backgroundColor = .clear
        self.tableFooterView = viewFooter
    }
    // MARK:-  Remove Top Extra Padding
    func removeTopExtraPadding() {
        self.tableHeaderView = nil
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0.001))
        headerView.backgroundColor = .clear
        self.tableHeaderView = headerView
    }
    // MARK:-  Remove Bottom Extra Padding
    func removeBottomExtraPadding() {
        self.tableFooterView = nil
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0.001))
        headerView.backgroundColor = .clear
        self.tableFooterView = headerView
    }
    
}

extension UIView {
    func addDropShadowToView(){
        self.layer.masksToBounds =  false
        self.layer.shadowColor = UIColor.lightGray.cgColor;
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)//CGSizeMake(1.0, 1.0)
        self.layer.shadowOpacity = 0.8
    }
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func setCornerRadius(_ radius: CGFloat) {
        self.layer.masksToBounds =  true
        self.layer.cornerRadius =  radius
    }
    
    func setCircle() {
        self.layer.cornerRadius =  self.frame.width/2
        self.layer.masksToBounds = true
    }

    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func prasentViewAnimationWithFrame(rect:CGRect, completion: ((Bool) -> Swift.Void)? = nil ) {
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.70, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.allowUserInteraction, animations: {
            self.frame = rect
        }) { (isDone) in
            completion!(isDone)
        }
    }
    
    func fadeinAndFadeOutAnimationWithBool(isShow:Bool, completion: ((Bool) -> Swift.Void)? = nil ) {
        UIView.animate(withDuration: 0.4, animations: {
            self.alpha = isShow ? 1 : 0 //rect
        }) { (isDone) in
            completion!(isDone)
        }
    }
   
}

