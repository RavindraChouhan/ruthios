//
//  RuthConstant.swift
//  Ruth
//
//  Created by mac on 15/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import Foundation
import UIKit

let objAppDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
let DeviceiOS = "iOS"
// Testing
//let BaseURL = "http://192.168.1.195:5000" // testing
//let SocketUrl = "http://192.168.1.195:1232"
//let RTMPBaseURL = "rtmp://192.168.1.195:1935/live/"
// Live
    let BaseURL = "http://52.14.252.73:3005"
    let RTMPBaseURL = "rtmp://52.14.252.73:4000/live/"
    let SocketUrl = "http://52.14.252.73:5000"

let GoogleClientId = "1065771002577-k3jfvnf2deuaikj13ia1cmecs4qams1s.apps.googleusercontent.com"

// For check user location
var currentLatitude = 22.28552
var currentLongitude = 114.15769
var isVaildCountry = false
let defaultCountry = "Hong Kong"
var currentCountry = "Hong Kong"

let userMobileBroadcastViews = "/user/mobile/broadcast/views/"
let userMobileBroadcastGifts = "/user/mobile/broadcast/gifts/"
let broadcastsUpdateCustomOrder = "/broadcasts/update-custom-order"
let broadcastsCustomOrder = "/broadcasts/custom-order/"
let broadcastsInvites = "/broadcasts/invites/"
let broadcastsInviteUser = "/broadcasts/invite-user"
let BroadcastApplication = "/user/mobile/broadcast/application"
let broadcastsViewer = "broadcasts/viewer"

let userMobileUpdate = "/user/mobile/update"
let channelsMobileUpdate = "/channels/mobile/update"
let followUsersHome = "/user/followUsers"


// Social login type
enum LoginType: String {
    case instagram = "instagram"
    case facebook = "facebook"
    case google = "googleplus"
}
enum StickerType: String {
    //case all = "All"
    case basic = "Basic"
    case scary = "Scary"
    case cute = "Cute"
    case funny = "Funny"
    case strange = "Strange"
}
var DE_BUG = false

// OTP type
enum OTPType: String {
    case registration = "registration"
    case forgot = "forgot"
}
public func rLog(_ msg: Any) {
    #if DEBUG
    //        print( "\n>>>______________________________________\n")
    print( "\(msg) \n")
    //        print( "______________________________________<<<<\n")
    
    #endif
}
enum StickerFilterType {
    case positive
    case negative
    case spcorder
    //case filtered
}
struct INSTAGRAM_IDS {
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    static let INSTAGRAM_CLIENT_ID  = "5c51e07bfd674c04a726891c088956b7"
    static let INSTAGRAM_CLIENTSERCRET = "6d2e3fc14be74dc49b1cdf34db8e58e1"
    static let INSTAGRAM_REDIRECT_URI = "http://appone.hk"
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
}
struct INSTAGRAM {
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    static let INSTAGRAM_CLIENT_ID  = "507ea1125d804fac9958d949634686c2"
    static let INSTAGRAM_CLIENTSERCRET = "556548580da144adbb3db25055544236"
    static let INSTAGRAM_REDIRECT_URI = "http://52.54.83.73/instagram/deep/redirect"
    static let INSTAGRAM_ACCESS_TOKEN =  "54d9b9f9d02944de8b4384dff24a87d6"
}
struct AWS {
    private init() {}
    static var S3BucketName = "ruthapp"
    static var accessKey    = "QUtJQUlGMklJQldVVk41UkNaVlE="
    static var secretKey    = "V2oweWNjRU9SdHBrdnZMNk1wTVhURDgwb09zVm1hYlF0VDdCVXh0Tw=="
    static var picturePath = "coverImage/"
}
struct ScreenSize {
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
struct DeviceType {
    static let iPhone4 =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let iPhone5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let iPhone6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let iPhone6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let iPhoneX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
}

struct AppNotifications {
    static let notificationAgeSelected = Notification.Name("NotificationAgeSelected")
    static let notificationShowTabBar = Notification.Name("Notification_ShowTabBar")
    static let notificationHideTabBar = Notification.Name("Notification_HideTabBar")
    
    static let notificationInternetConnection = Notification.Name("notificationInternetConnection")
    static let notificationChangeLangugage = Notification.Name("ChangeLangugage")
    static let notificationChangeTabBar = Notification.Name("notificationChangeTabBar")
    static let notificationUserProfile = Notification.Name("notificationUserProfile")
    static let notificationUpdateInvalidDevice = Notification.Name("UpdateInvalidDevice")
    static let notificationMap = Notification.Name("notificationMap")
    static let notificationRuthStory = Notification.Name("notificationRuthStory")
    static let notificationRuthVid = Notification.Name("notificationRuthVid")
    static let notificationRuthKnows = Notification.Name("notificationRuthKnows")
    static let notificationBible = Notification.Name("notificationBible")
    static let notificationVIPShare = Notification.Name("notificationVIPShare")
    static let notificationLoadPlayerViewController = Notification.Name("LoadPlayerViewController")
      static let notificationMoreButton = Notification.Name("notificationMoreButton")
    //notificationUpdateInvalidDevice
}
