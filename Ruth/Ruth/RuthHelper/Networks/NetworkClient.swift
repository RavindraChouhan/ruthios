//
//  NetworkClient.swift
//  Ruth
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import Alamofire
import CodableAlamofire

class NetworkClient: NSObject {
    
    static var shared = NetworkClient()
    
    var baseURL: String
    let decoder: JSONDecoder
    let sessionManager = SessionManager.default
    internal var authToken = UserDefaults.standard.value(forKey: "authToken") as? String
    internal var token: String? = nil
    internal var headers: HTTPHeaders  {
        
        if let userToken = authToken {
            self.token = userToken
        }
        guard let token = token else {
            return [:]
        }
        return [
            "Authorization": token
        ]
    }
    
    init(domain: String = BaseURL, decoder: JSONDecoder = JSONDecoder()) {
        self.baseURL = domain
        self.decoder = decoder
    }
    
    func getRequest(_ url: URLConvertible, parameters: Parameters? = nil) -> DataRequest {
           return sessionManager.request("\(baseURL)/\(url)", method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers)
    }
    func deleteRequest(_ url: URLConvertible, parameters: Parameters? = nil) -> DataRequest {
        return sessionManager.request("\(baseURL)/\(url)", method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers)
    }
    
    func postRequest(_ url: URLConvertible, parameters: Parameters? = nil) -> DataRequest {
        return sessionManager.request("\(baseURL)/\(url)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers)
    }
    
    func putRequest(_ url: URLConvertible, parameters: Parameters? = nil) -> DataRequest {
        return sessionManager.request("\(baseURL)/\(url)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers)
    }
}
