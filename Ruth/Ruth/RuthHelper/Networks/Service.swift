//
//  Service.swift
//  PostCard
//
//  Created by Mac on 21/04/17.
//  Copyright © 2017 Linkites. All rights reserved.
//

import Foundation
import Alamofire

struct Service {
    
    static let service = Service()
    
    func getAuthHeaders() -> HTTPHeaders {
        let tokan = UserDefaults.standard.value(forKey: "authToken") as! String
        if(UserNew.currentUser.isLogin) {
            let headers: HTTPHeaders = [
                "Authorization": tokan
            ]
             print("headers**\(headers)")
            return headers
        }
        else {
            let headers: HTTPHeaders = [
                "Accept": "application/json"
            ]
            return headers
        }
    }
    func get(url:String,completion:@escaping (_ result: Any) -> Void) {
            Alamofire.request(url, headers:self.getAuthHeaders()).responseJSON { response in
                if let result = response.result.value {
                    if result is [String : Any]{
                        let dict = result as! [String : Any]
                        if let message = dict["message"] as? String{
                            if message == "Invalid device login" {
                                Util.util.hideHUD()
                                NotificationCenter.default.post(name: AppNotifications.notificationUpdateInvalidDevice, object: nil)
                            }else{
                                completion(result)
                            }
                        }else{
                            completion(result)
                        }
                    }
                }else if let result = response.result.error {
                    completion(result as Any)
                }
            }
    }
    
    //post data to service
    func post(url:String, parameters:Parameters,completion:@escaping (_ result: Any) -> Void) {
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: self.getAuthHeaders()).responseJSON { (response) in
            if let result = response.result.value {
                if result is [String : Any]{
                    let dict = result as! [String : Any]
                    if let message = dict["message"] as? String{
                        if message == "Invalid device login" {
                            Util.util.hideHUD()
                            NotificationCenter.default.post(name: AppNotifications.notificationUpdateInvalidDevice, object: nil)
                        }else{
                            completion(result)
                        }
                    }else{
                        completion(result)
                    }
                }
            }else if let result = response.result.error {
                completion(result as Any)
            }
        }
    }
    
    //put data to service
    func put(url:String, parameters:Parameters,completion:@escaping (_ result: Any) -> Void) {
        
        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: self.getAuthHeaders()).responseJSON { (response) in
            if let result = response.result.value {
                if result is [String : Any]{
                    let dict = result as! [String : Any]
                    if let message = dict["message"] as? String{
                        if message == "Invalid device login" {
                            Util.util.hideHUD()
                            NotificationCenter.default.post(name: AppNotifications.notificationUpdateInvalidDevice, object: nil)
                        }else{
                            completion(result)
                        }
                    }else{
                        completion(result)
                    }
                }
            }else if let result = response.result.error {
                completion(result as Any)
            }
        }
    }
    //delete data to service
    func del(url:String, parameters:Parameters,completion:@escaping (_ result: AnyObject) -> Void) {
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if let result = response.result.value {
                let JSON = result
                completion(JSON as AnyObject)
            }
            if let result = response.result.error {
                completion(result as AnyObject)
            }
        }
    }
}
