//
//  Constants.swift
//  PostCard
//
//  Created by Mac on 21/04/17.
//  Copyright © 2017 Linkites. All rights reserved.
//

import Foundation

let objAppDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate

struct Config {
    
    static var serverUrl_sty = "http://app.stylepic.com/"
    static var serverUrl = "https://dash.mytoch.com/api/v1/" //"https://mytoch.com/api/v5/mobile/"
    static var iTunesAppID = "id1118065704"
    static var kClientID = "603831085794-cc4uh0isaaimq1hhlgqi0bvsunfvl509.apps.googleusercontent.com"
    static var AppID = "Vk34ELr9e-EyA_8MUnl"
    static var ImagePathUrl = "https://mytoch.com/"
    static var logo_url = "https://mytoch.com/img/gifLogo.png"
    static var BundleID = "com.vogueme.toch"
    static var parseAppID = "jzhg10E4VF9lJS0Y22E2z5pnWhKlFBJDAkuAFkbi"
    static var parseClientKey = "lfVQnPqcft0RWAckfE4Y6BVrMrZzoh1Ds85M1Gx5"
    static var AnalyticWriteKey = "iUYezHRBKGY1Utz3piTjizCOaygLAyw9"
    static var Fyber_APPID = "875faf9cf8d9ea9a3e881d1ca241e115"
    static var Flurry_APPID = "P5TT526DZFBY2DT7QRT7"
    static var GROUPON_AFFILIATE_ID = "201951"
    static var GROUPON_API = "https://partner-int-api.groupon.com/deals.json?country_code="
    static var Instagram_CLIENT_ID = "cc48f100853e4c76b46f1c610e338022"
    static var Instagram_SECRET = "f5b6b3b77b464fc7874b476bc12bf6e8"
    static var Facebook_PERMISSIONS = ["public_profile" , "email"]
    static var PICTURE_BUCKET = "getglance"
    static var QuoteFeed = "http://www.iheartquotes.com/api/v1/random?format=json"
    static var AstroFeed = "http://www.findyourfate.com/rss/horoscope-astrology-feed.asp?mode=view&todate="
    static var iTunesAppURL = "https://itunes.apple.com/us/app/toch-watch-discover-shop/id1147640817?ls=1&mt=8"
    static var BASEURL = "http://app.stylepic.com/"
    static var InstagramAPP_ID = "5cfff21ae7704780b834a39975892fc6"
    static var MailChimp_AppID = "0ec6acea60f9fd28fd94e39ab2b3385f-us9"
    static var MailChimp_ListID = "9b260a9694"

}

struct Colors {
    static var PinkColor = "EC268F"
}
struct Sharing_Menus {
    static var email = 1
    static var facebook = 2  //FACEBOOK = 2
    static var message = 3 //MESSAGE = 3
    static var whatsapp = 4//WHATSAPP = 4
}
struct Sharing_Type {
    static var video_sha = 1//VIDEO_SHARING = 1
    static var product_sha = 2//PRODUCT_SHARING = 2
    static var app_sha = 3 //APP_SHARING = 3
    static var referral_sha = 4 //REFERRAL_SHARING = 4
}
enum AnimationDirection  : String{
    case AnimationDirectionNone
    case AnimationDirectionRight
    case AnimationDirectionLeft
    case AnimationDirectionTop
    case AnimationDirectionBottom
}


enum ProductType : String{
    case actor
    case product
    case overlayitem
    case location
    case other
    case emotion
    case activite
}

struct WebPageURL {
    static var AboutUs  = "https://mytoch.com/aboutus.html"
    static var PrivayPolicy = "https://mytoch.com/privacy.html"
    static var Blog = "https://blog.mytoch.com/"
    static var TermsCondition = "https://mytoch.com/terms_condition.html"
}
struct AppNotifications {
    static let notificationReloadDataAfterReconectToInternet = Notification.Name("Connectivity")
    static let kNtificationShowTabbar = Notification.Name("ShowTabBar")
    static let kNtificationHideTabbar = Notification.Name("HideTabBar")
    static let notificationReloadCategories = Notification.Name("ReloadCategories")
    static let notificationGotoHOME = Notification.Name("notificationGotoHOME")
    static let notificationReloadCategoryVideos = Notification.Name("notificationReloadCategoryVideos")
    static let notificationPageFeaturedDataViewData = Notification.Name("notificationPageFeaturedDataViewData")
    static let notificationProductData = Notification.Name("notificationProductData")
}

