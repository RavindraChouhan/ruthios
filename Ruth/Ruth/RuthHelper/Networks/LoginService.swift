//
//  LoginService.swift
//  Ruth
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import Foundation
import Alamofire

typealias SocailResponseHandler = (User?, String?) -> (Void)
typealias InstagramHandler = (InstaData?, String?) -> (Void)
typealias OtpHandler = (Bool?, String?) -> (Void)


protocol LoginService {
    func loginWithSocialID(username: String, socialId: String, image: String, type: String, emailID: String?, deviceToken: String, onResult: @escaping SocailResponseHandler)
    func loginWithInstagram(authToken: String, deviceToken: String, onResult: @escaping InstagramHandler)
    func sendOtp(mobile: String, username: String?, type: String, onResult: @escaping OtpHandler)
    func verifyOtp(mobile: String, otpCode: String, onResult: @escaping OtpHandler)
    func registerByMobile(username: String, mobile: String, email: String, password: String, onResult: @escaping SocailResponseHandler)
    func loginWithMobileNumber(mobile: String, password: String,deviceToken: String, onResult: @escaping SocailResponseHandler)
    func forgotPassword(mobile: String, onResult: @escaping OtpHandler)
    func resetPassword(token: String, password: String, onResult: @escaping OtpHandler)
}

extension NetworkClient: LoginService {
 
    func loginWithSocialID(username: String, socialId: String, image: String, type: String, emailID: String?, deviceToken: String, onResult: @escaping SocailResponseHandler) {
        var deviceToken = ""
        if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
            deviceToken = temp as! String
        }
        var parameters = [String:Any]()
        rLog(parameters)
        if emailID == nil {
            parameters = [
                "lng": currentLongitude,
                "lat": currentLatitude,
                "country": currentCountry,
                "deviceType": DeviceiOS,
                "accountType": type,
                "name": username,
                "socialID": socialId,
                "deviceToken": deviceToken,
                "photoUrl": image
            ]
        }
        else {
            parameters = [
                "lng": currentLongitude,
                "lat": currentLatitude,
                "country": currentCountry,
                "deviceType": DeviceiOS,
                "accountType": type,
                "deviceToken": deviceToken,
                "name": username,
                "socialID": socialId,
                "photoUrl": image,
                "email": emailID ?? ""
            ]
        }
        
        print("PARAM parameters == \(parameters)")
        
        sessionManager.request("\(baseURL)/user/socialSignUp", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseDecodableObject { (response:DataResponse<SignupResponse>) in
            
            if let error = response.result.error {
                onResult(nil, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    
                    if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                        if strLang == "zh-Hant"{
                            onResult(nil, response.result.value?.zh_Hant)
                        } else{
                            onResult(nil, response.result.value?.zh_Hans)
                        }
                    } else {
                        onResult(nil, response.result.value?.message)
                    }
                    return
                }
            }
            mainUser = response.result.value?.data
                        self.token = response.result.value?.token
                        UserNew.currentUser.authToken = (response.result.value?.token)!
            UserNew.currentUser.isLogin = true
            UserDefaults.standard.set(true, forKey: "isLogin")
            UserDefaults.standard.set(UserNew.currentUser.authToken, forKey: "authToken")
            UserDefaults.standard.synchronize()
            UserNew.currentUser.saveUserData(mainUser!)
            onResult(response.result.value?.data, nil)
        }
    }
    
    func loginWithInstagram(authToken: String, deviceToken: String, onResult: @escaping InstagramHandler) {
        
        let authUrl = "https://api.instagram.com/v1/users/self/?access_token=\(authToken)"
        
        sessionManager.request(authUrl, method: .get, encoding: JSONEncoding.default, headers: nil).responseDecodableObject { (response:DataResponse<InstagramResponse>) in
            
            if let error = response.result.error {
                onResult(nil, error.localizedDescription)
                return
            }
            if let code = response.result.value?.meta?.code {
                if code == 400 {
                    onResult(nil, response.result.value?.meta?.errorMessage)
                    return
                }
            }
            onResult(response.result.value?.data, nil)
        }
    }
    
    func sendOtp(mobile: String, username: String?, type: String, onResult: @escaping OtpHandler) {
        var parameters = [String:Any]()
        if username == nil {
            parameters = [
                "mobileNo": mobile,
                "type": type
            ]
        }
        else {
            parameters = [
                "mobileNo": mobile,
                "userName": username ?? "",
                "type": type
            ]
        }
        
        print("parameters == \(parameters)")
        sessionManager.request("\(baseURL)/user/sendOTP", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseDecodableObject { (response:DataResponse<OtpResponse>) in
            
            if let error = response.result.error {
                onResult(false, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                        if strLang == "zh-Hant"{
                            onResult(false, response.result.value?.zh_Hant)
                        } else{
                            onResult(false, response.result.value?.zh_Hans)
                        }
                    } else {
                        onResult(true, response.result.value?.message)
                    }
                    return
                }
            }
            onResult(true, response.result.value?.token)
        }
    }
    
    func verifyOtp(mobile: String, otpCode: String, onResult: @escaping OtpHandler) {
        let parameters: Parameters = [
            "mobileNo": mobile,
            "otp": otpCode
        ]
        print("PAram == \(parameters)")
        sessionManager.request("\(baseURL)/user/verifyOTP", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseDecodableObject { (response:DataResponse<OtpResponse>) in
            
            if let error = response.result.error {
                onResult(false, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                        if strLang == "zh-Hant"{
                            onResult(false, response.result.value?.zh_Hant)
                        } else{
                            onResult(false, response.result.value?.zh_Hans)
                        }
                    } else {
                        onResult(true, response.result.value?.message)
                    }
                    return
                }
            }
            onResult(true, response.result.value?.message)
        }
    }
    
    func registerByMobile(username: String, mobile: String, email: String, password: String, onResult: @escaping SocailResponseHandler) {
        
        let parameters: Parameters = [
            "lng": currentLongitude,
            "lat": currentLatitude,
            "country": currentCountry,
            "deviceType": DeviceiOS,
            "userName": username,
            "mobileNo": mobile,
            "email": email,
            "password": password
        ]
        
        print("signup == \(parameters)")
        sessionManager.request("\(baseURL)/user/mobile/signup", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseDecodableObject { (response:DataResponse<SignupResponse>) in
            
            if let error = response.result.error {
                onResult(nil, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                        if strLang == "zh-Hant"{
                            onResult(nil, response.result.value?.zh_Hant)
                        } else{
                            onResult(nil, response.result.value?.zh_Hans)
                        }
                    } else {
                        onResult(nil, response.result.value?.message)
                    }
                    return
                }
            }
            mainUser = response.result.value?.data
            self.token = response.result.value?.token
            UserNew.currentUser.authToken = (response.result.value?.token)!
            UserNew.currentUser.isLogin = true
            UserDefaults.standard.set(true, forKey: "isLogin")
            UserDefaults.standard.set(UserNew.currentUser.authToken, forKey: "authToken")
            UserDefaults.standard.synchronize()
            UserNew.currentUser.saveUserData(mainUser!)
            onResult(response.result.value?.data, nil)
        }
    }
    
    func loginWithMobileNumber(mobile: String, password: String, deviceToken: String, onResult: @escaping SocailResponseHandler) {
        var deviceToken = ""
        if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
            deviceToken = temp as! String
        }
        let parameters: Parameters = [
            "mobileNo": mobile,
            "password": password,
            "deviceType": DeviceiOS,
//            "deviceToken": deviceToken
            "deviceToken": deviceToken
        ]
        print("login parameters *** \(parameters)")
        sessionManager.request("\(baseURL)/user/mobile/login", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseDecodableObject { (response:DataResponse<SignupResponse>) in
            
            if let error = response.result.error {
                onResult(nil, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                        if strLang == "zh-Hant"{
                            onResult(nil, response.result.value?.zh_Hant)
                        } else{
                            onResult(nil, response.result.value?.zh_Hans)
                        }
                    } else {
                        onResult(nil, response.result.value?.message)
                    }
                    return
                }
            }
            if response.result.value?.status == nil {
                if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                    if strLang == "zh-Hant"{
                        onResult(nil, response.result.value?.zh_Hant)
                    } else{
                        onResult(nil, response.result.value?.zh_Hans)
                    }
                } else {
                    onResult(nil, response.result.value?.message)
                }
                return
            }
            mainUser = response.result.value?.data
            self.token = response.result.value?.token
            UserNew.currentUser.authToken = (response.result.value?.token)!
            UserNew.currentUser.isLogin = true
            UserDefaults.standard.set(true, forKey: "isLogin")
            UserDefaults.standard.set((response.result.value?.token)!, forKey: "authToken")
            UserDefaults.standard.synchronize()
            UserNew.currentUser.saveUserData(mainUser!)
            onResult(response.result.value?.data, nil)
        }
    }
    
    func forgotPassword(mobile: String, onResult: @escaping OtpHandler) {
        let parameters: Parameters = [
            "mobileNo": mobile
        ]
        print("PAram == \(parameters)")
        sessionManager.request("\(baseURL)/user/mobile/forgotPassword", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseDecodableObject { (response:DataResponse<OtpResponse>) in
            
            if let error = response.result.error {
                onResult(false, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                        if strLang == "zh-Hant"{
                            onResult(nil, response.result.value?.zh_Hant)
                        } else{
                            onResult(nil, response.result.value?.zh_Hans)
                        }
                    } else {
                        onResult(nil, response.result.value?.message)
                    }
                    return
                }
            }
            onResult(true, response.result.value?.token)
        }
    }
    
    func resetPassword(token: String, password: String, onResult: @escaping OtpHandler) {
        
        let parameters: Parameters = [
            "token": token,
            "password": password
        ]
        print("PAram == \(parameters)")
        sessionManager.request("\(baseURL)/user/mobile/resetPassword", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseDecodableObject { (response:DataResponse<OtpResponse>) in
            
            if let error = response.result.error {
                onResult(false, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                        if strLang == "zh-Hant"{
                            onResult(nil, response.result.value?.zh_Hant)
                        } else{
                            onResult(nil, response.result.value?.zh_Hans)
                        }
                    } else {
                        onResult(nil, response.result.value?.message)
                    }
                    return
                }
                
            }
             onResult(true, response.result.value?.token)
        }
       
    }
    
    func getUserProfile(onResult: @escaping SocailResponseHandler){
        let uu = UserNew.currentUser
        let url = "\(baseURL)/user/mobile/me/" + uu.id
        
        rLog("getUserProfile URL === \(url)")
        
        sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseDecodableObject { (response:DataResponse<SignupResponse>) in
            
            if let error = response.result.error {
                onResult(nil, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    if let message = response.result.value?.message{
                        if message == "Invalid device login" {
                            Util.util.hideHUD()
                            NotificationCenter.default.post(name: AppNotifications.notificationUpdateInvalidDevice, object: nil)
                             return
                        }
                    }
                    
                    if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
                        if strLang == "zh-Hant"{
                            onResult(nil, response.result.value?.zh_Hant)
                        } else{
                            onResult(nil, response.result.value?.zh_Hans)
                        }
                    } else {
                        onResult(nil, response.result.value?.message)
                    }
                    return
                }
            }
            mainUser = response.result.value?.data
            UserNew.currentUser.saveUserData(mainUser!)
            onResult(response.result.value?.data, nil)
        }
        }
}
/*
extension NetworkClient: LoginService {
    func logout() {
//        self.token = nil
//        mainUser = nil
//        UserDefaults.standard.set(false, forKey: "isLogin")
//        UserDefaults.standard.set(nil, forKey: "token")
//        UserDefaults.standard.synchronize()
    }
}

extension String {
    func stringByRemovingAll(characters: [Character]) -> String {
        return String(self.filter({ !characters.contains($0) }))
    }
}
*/
