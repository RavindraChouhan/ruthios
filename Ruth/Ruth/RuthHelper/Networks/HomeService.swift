//
//  HomeService.swift
//  Ruth
//
//  Created by mac on 17/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import Foundation
import Alamofire

typealias FollowUserHandler = ([FollowUsers]?, String?) -> (Void)

protocol HomeService {
    func getFollowUser(onResult : @escaping FollowUserHandler)
}

extension NetworkClient : HomeService {
    
    func getFollowUser(onResult: @escaping FollowUserHandler) {
        
        let request = self.getRequest("/user/followUsers")
        request.responseDecodableObject(decoder: self.decoder) { (response : DataResponse<AllFollowUser>) in
            if let error = response.result.error {
                onResult(nil, error.localizedDescription)
                return
            }
            if let success = response.result.value?.status {
                if !success {
                    onResult(nil, response.result.value?.message)
                    return
                }
            }
            onResult(response.result.value?.data, nil)
        }
    }
    
    
    
}
