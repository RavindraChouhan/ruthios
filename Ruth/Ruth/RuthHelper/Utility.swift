//
//  Utility.swift
//  Ruth
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation
import NVActivityIndicatorView
import AWSS3

class NetworkConnectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
extension UIViewController: NVActivityIndicatorViewable {}
class Util:NSObject, NVActivityIndicatorViewable, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    typealias SelectedImage = (_ pickedImg : UIImage)->Swift.Void
    var selectedImage : SelectedImage?
    static let util = Util()
    var pickedImg : UIImage!
    var controller : UIViewController!
    var picker:UIImagePickerController?=UIImagePickerController()
    var tabControl: UITabBarController!
    var dictMsg = [String : Any]()
    public class func isLocationEnabled() -> Bool {
        return !(!CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .restricted)
    }
    
    func showHUD() {
        let control = UIViewController()
        control.addInvaildDeviceNotificationObserver()
        let size = CGSize(width: 35, height: 35)
        control.startAnimating(size, message: "", messageFont: UIFont(name: "Poppins-Medium", size:  14.0), type: .ballSpinFadeLoader, color: UIColor.hexStringToColor(hex: "00F920"))
    }
    
    func readJsonFile() {
        if let path = Bundle.main.path(forResource: "msg", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [String : Any]{
                    self.dictMsg = jsonResult
                    print(self.dictMsg)
                }
            } catch {
                // handle error
            }
        }
    }
    func getMsgWithKey(_ key : String) -> String {
        var msg = "Something went wrong".localized()
        if let msgTemp = self.dictMsg[key] as? [String : Any]{
            if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String
            {
               msg = msgTemp[strLang] as! String
            }else{
                msg = msgTemp["zh-Hant"] as! String
            }
        }
        return msg
    }
     func getFormatedDateWithRequiredFormat( strFormat: String ,withDate strDate : String)-> String {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "dd/MM/yyyy" //'T'HH:mm:ss.SSSZ
        let date = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = strFormat //"MMM d, yyyy" ; //"dd-MM-yyyy HH:mm:ss"
        dateFormatter.locale = tempLocale // reset the locale --> but no need here
        let dateString = dateFormatter.string(from:date!)
        return dateString
    }
    func getMsgWithDict(_ dictMsg : [String : Any]) -> String {
        var msg = "Something went wrong".localized()
        if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String
        {
            msg = dictMsg[strLang] as! String
        }else{
            msg = dictMsg["data"] as! String
        }
        return msg
    }
    func hideHUD() {
        let control = UIViewController()
        control.stopAnimating()
    }
    
    func changeChineseMonth(_ month: String) -> String{
    var strMonth = month
        strMonth = strMonth.replacingOccurrences(of: "January", with: "一月")
        strMonth = strMonth.replacingOccurrences(of: "February", with: "二月")
        strMonth = strMonth.replacingOccurrences(of: "March", with: "三月")
        strMonth = strMonth.replacingOccurrences(of: "April", with: "四月")
        strMonth = strMonth.replacingOccurrences(of: "May", with: "五月")
        strMonth = strMonth.replacingOccurrences(of: "June", with: "六月")
        strMonth = strMonth.replacingOccurrences(of: "July", with: "七月")
        strMonth = strMonth.replacingOccurrences(of: "August", with: "八月")
        strMonth = strMonth.replacingOccurrences(of: "September", with: "九月")
        strMonth = strMonth.replacingOccurrences(of: "October", with: "十月")
        strMonth = strMonth.replacingOccurrences(of: "November", with: "十一月")
        strMonth = strMonth.replacingOccurrences(of: "December", with: "十二月")
        return strMonth
    }
    func changeChineseDay(_ day: String) -> String{
        var strDay = day
        switch strDay {
//        case "0":
//            strDay = strDay.replacingOccurrences(of: "0", with: "〇")
        case "1":
            strDay = strDay.replacingOccurrences(of: "1", with: "一")
        case "2":
            strDay = strDay.replacingOccurrences(of: "2", with: "二")
        case "3":
            strDay = strDay.replacingOccurrences(of: "3", with: "三")
        case "4":
            strDay = strDay.replacingOccurrences(of: "4", with: "四")
        case "5":
            strDay = strDay.replacingOccurrences(of: "5", with: "五")
        case "6":
            strDay = strDay.replacingOccurrences(of: "6", with: "六")
        case "7":
            strDay = strDay.replacingOccurrences(of: "7", with: "七")
        case "8":
            strDay = strDay.replacingOccurrences(of: "8", with: "八")
        case "9":
            strDay = strDay.replacingOccurrences(of: "9", with: "九")
        case "10":
            strDay = strDay.replacingOccurrences(of: "10", with: "十")
        case "11":
            strDay = strDay.replacingOccurrences(of: "11", with: "十一")
        case "12":
            strDay = strDay.replacingOccurrences(of: "12", with: "十二")
        case "13":
            strDay = strDay.replacingOccurrences(of: "13", with: "十三")
        case "14":
            strDay = strDay.replacingOccurrences(of: "14", with: "十四")
        case "15":
            strDay = strDay.replacingOccurrences(of: "15", with: "十五")
        case "16":
            strDay = strDay.replacingOccurrences(of: "16", with: "十六")
        case "17":
            strDay = strDay.replacingOccurrences(of: "17", with: "十七")
        case "18":
            strDay = strDay.replacingOccurrences(of: "18", with: "十八")
        case "19":
            strDay = strDay.replacingOccurrences(of: "19", with: "十九")
        case "20":
            strDay = strDay.replacingOccurrences(of: "20", with: "二十")
        case "21":
            strDay = strDay.replacingOccurrences(of: "21", with: "二十一")
        case "22":
            strDay = strDay.replacingOccurrences(of: "22", with: "二十二")
        case "23":
            strDay = strDay.replacingOccurrences(of: "23", with: "二十三")
        case "24":
            strDay = strDay.replacingOccurrences(of: "24", with: "二十四")
        case "25":
            strDay = strDay.replacingOccurrences(of: "25", with: "二十五")
        case "26":
            strDay = strDay.replacingOccurrences(of: "26", with: "二十六")
        case "27":
            strDay = strDay.replacingOccurrences(of: "27", with: "二十七")
        case "28":
            strDay = strDay.replacingOccurrences(of: "28", with: "二十八")
        case "29":
            strDay = strDay.replacingOccurrences(of: "29", with: "二十九")
        case "30":
            strDay = strDay.replacingOccurrences(of: "30", with: "三十")
        case "31":
            strDay = strDay.replacingOccurrences(of: "31", with: "三十一")
        default:
            break
        }
        return strDay
    }
    func changeChineseYear(_ year: String) -> String{
        var strYear = year
        strYear = strYear.replacingOccurrences(of: "0", with: "〇")
        strYear = strYear.replacingOccurrences(of: "1", with: "一")
        strYear = strYear.replacingOccurrences(of: "2", with: "二")
        strYear = strYear.replacingOccurrences(of: "3", with: "三")
        strYear = strYear.replacingOccurrences(of: "4", with: "四")
        strYear = strYear.replacingOccurrences(of: "5", with: "五")
        strYear = strYear.replacingOccurrences(of: "6", with: "六")
        strYear = strYear.replacingOccurrences(of: "7", with: "七")
        strYear = strYear.replacingOccurrences(of: "8", with: "八")
        strYear = strYear.replacingOccurrences(of: "9", with: "九")
        return strYear
    }

    struct TimeParser {
        
        private static let calendar = Calendar(identifier: .gregorian)
        
        private static let hourMinuteSecondFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm:ss"
            return formatter
        }()
        
        private static let minuteSecondFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "mm:ss"
            return formatter
        }()
        
        static func string(from duration: TimeInterval) -> String {
            
            guard duration > 0 else { return "00:00" }
            
            let maximumDuration: TimeInterval = 23*3600 + 59*60 + 59
            guard duration <= maximumDuration else { return "23:59:59" }
            
            let hours = floor(duration / 3600)
            let minutesAndSeconds = duration.truncatingRemainder(dividingBy: 3600)
            let minutes = floor(minutesAndSeconds / 60)
            let seconds = floor(minutesAndSeconds.truncatingRemainder(dividingBy: 60))
            
            var comps = DateComponents()
            comps.calendar = calendar
            comps.day = 1
            comps.month = 1
            comps.year = 2016
            comps.hour = Int(hours)
            comps.minute = Int(minutes)
            comps.second = Int(seconds)
            
            guard let date = comps.date else { return "00:00" }
            
            if hours > 0 {
                let result = hourMinuteSecondFormatter.string(from: date)
                return result
                
            } else {
                let result = minuteSecondFormatter.string(from: date)
                return result
            }
        }
        
        private init() {}
    }
    
    var isOpenCameraView = false
    
    func openCamera(_ isCamera: Bool, _ inViewController : UIViewController? = nil, callback : @escaping SelectedImage)
    {
        if isOpenCameraView
        {
            return
        }
        if self.controller != nil
        {
            self.controller = nil
        }
        self.selectedImage = callback
        if inViewController != nil {
            self.controller = inViewController
        }
        
        isOpenCameraView = true
        picker?.delegate = self
        if isCamera {
            if(UIImagePickerController .isSourceTypeAvailable(.camera)){
                picker!.allowsEditing = false
                picker!.sourceType = .camera
                picker!.cameraCaptureMode = .photo
                if self.controller != nil {
                    self.controller.present(picker!, animated: true, completion: nil)
                }else{
                    self.tabControl.present(picker!, animated: true, completion: nil)
                }
            }else{
                let strMsg = "This device has no Camera".localized()
                if self.controller != nil {
                    self.controller.showMessageTitle("", strMsg, nil, optionHandler: nil)
                }else{
                    self.tabControl.showMessageTitle("", strMsg, nil, optionHandler: nil)
                }
            }
        }else{
            if(UIImagePickerController .isSourceTypeAvailable(.photoLibrary)){
                picker!.allowsEditing = false
                picker!.sourceType = .photoLibrary
                if self.controller != nil {
                    self.controller.present(picker!, animated: true, completion: nil)
                }else{
                    self.tabControl.present(picker!, animated: true, completion: nil)
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.isOpenCameraView = false
        if self.controller != nil {
            self.controller.dismiss(animated: true, completion: nil)
        }else{
            self.tabControl.dismiss(animated: true, completion: nil)
        }
   }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // imgPlant.contentMode = .scaleAspectFit
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            // Use editedImage Here
            self.pickedImg = editedImage.wxCompress()
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // Use originalImage Here
            self.pickedImg = originalImage.wxCompress()
        }
        if self.selectedImage != nil{
            self.selectedImage!(self.pickedImg)
        }
        //self.addAnalytics(dict: ["name" : "imagesTaken"]) { (isDone) in }
//        let stroryBord = UIStoryboard(name: "Main", bundle: nil)
//        let cameraViewController = stroryBord.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
//        cameraViewController.img = self.imgPlant
//        cameraViewController.tabControl = self.tabControl
//        let navi = UINavigationController(rootViewController: cameraViewController)
        self.isOpenCameraView = false
        picker.dismiss(animated: true)
        
        // imagesSubmitted
        
        //self.tabControl.present(navi, animated: true) {
//        }
        // self.tabControl.present(navi, animated: true, completion: nil)
    }
    
    func uploadImageToAWS(img: UIImage, completion:@escaping (_ result: [String:Any]) -> Void) {
        // file url
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("photo.jpeg")
        let imageData = UIImageJPEGRepresentation(img, 0.7)
        fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)
        let fileUrl = NSURL(fileURLWithPath: path)
        
        // File name
        let imgName = mainUser?.id ?? ""
        let currentDate = Date()
        let datefar = DateFormatter()
        datefar.dateFormat = "dd-MM-yyyy_hh_mm_ss"
        let dateKey = datefar.string(from: currentDate)
        
        let uploadKey = AWS.picturePath+"\(imgName)\(dateKey)"+".jpg"
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.bucket = AWS.S3BucketName
        uploadRequest.key = uploadKey
        
        uploadRequest.contentType = "image/jpeg"
        uploadRequest.body = (fileUrl as URL?)!
        uploadRequest.acl = .publicRead
        
        uploadRequest.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
                let amountUploaded = Int(totalBytesSent) // To show the updating data status in label.
                let fileSize = Int(totalBytesExpectedToSend)
                print("Image fileSize**\(fileSize) upload finished**\(amountUploaded)")
            })
        }
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.mainThread()) { (task:AWSTask) -> Any? in
            if let error = task.error {
                print("***Photo upload failed with error:: (\(error.localizedDescription))")
                let dict: [String:Any] = ["success": false, "awsURl":""]
                completion(dict)
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let s3url = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                let imageS3Url = String(describing: s3url!)
                print("**Photo Uploaded to:: \(imageS3Url)")
                let dict: [String:Any] = ["status": true, "awsURl":imageS3Url]
                completion(dict)
            }
            
            //            if task.result != nil {
            //                print(("******* Image UploadFinished *******"))
            //                let s3url = AWS.picturePath + uploadRequest.key!
            //                let fileS3Url = String(describing: s3url)
            //                print("\n **Image File Uploaded to:: \(fileS3Url)")
            //                let dict: [String:Any] = ["success": true, "awsURl":fileS3Url]
            //                completion(dict)
            //            }
            return nil
        }
    }
    
    func createBroadcast(parameters:[String : Any], completion:@escaping (_ result: [String:Any]) -> Void) {
        let Url = BaseURL + "/user/mobile/broadcast"
        Service.service.post(url: Url, parameters: parameters, completion: { (result ) in
            rLog(" broadcast  ==\(result) ==== Url $$$$  = \(Url) ")
            Util.util.hideHUD()
            if result is [String : Any] {
                let response = result as! [String:Any]
                rLog("user/mobile/broadcast broadcast  ==\(response)")
                
                if response["status"] as! Bool {
                    completion(response)
//                    let broadID = ""
//                    UserNew.currentUser.lbroadcastId = broadID
                    
                }else{
                    completion(response)
                }
            }else if result is Error  {
                let error = result as! Error
                 rLog(error)
                var strMsg = "Something went wrong".localized()
                strMsg = error.localizedDescription
                
                if strMsg == "The Internet connection appears to be offline."
                {
                    strMsg = "No internet connection".localized()
                }
                //self.controller.showAlertMessage(message: strMsg)
                var response = [String : Any]()
                response["message"] =  strMsg
                response["zh_Hans"]  =  strMsg
                response["zh_Hant"]  =  strMsg
                response["status"] =  false
                completion(response)
            }
        })
    }
    func userMobileUpdateFollow(parametes: [String:Any], completion:@escaping (_ success: Any) -> Void) {
        let url = BaseURL + userMobileUpdate
        Service().post(url: url, parameters: parametes) { (result) in
            if result is [String : Any] {
                let response = result as! [String : Any]
                rLog(" userMobileUpdateFollow  ==\(response)")
                if (response["status"] as! Bool) {
                }else{
                    completion(response)
                }
            }else {
                // //print("error \(result)");
                completion(result)
            }
        }
    }
    func getApiMsg(_ dict : [String : Any]) -> String {
        var strMsg = ""
        if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String {
            if strLang == "zh-Hant"{
                strMsg = dict["zh_Hant"] as! String
                
            }else if strLang == "zh-Hans"{
                strMsg = dict["zh_Hans"] as! String
            }else{
                strMsg = dict["message"] as! String
            }
        } else {
           strMsg = dict["message"] as! String
        }
        return strMsg
    }
    func getMaskOptionF() -> [MaskOption] {
        var arrayMaskOption = [MaskOption]()
        let MaskOption1 =  MaskOption()
        MaskOption1.title = "All"
        MaskOption1.zhHans = "全部"
        MaskOption1.zhHant = "全部"
        MaskOption1.isSelected = true
        let MaskOption2 =  MaskOption()
        MaskOption2.title = "classic"
        MaskOption2.zhHans = "經典"
        MaskOption2.zhHant = "經典"
        let MaskOption3 =  MaskOption()
        MaskOption3.title = "horrible"
        MaskOption3.zhHans = "可怕"
        MaskOption3.zhHant = "可怕"
        let MaskOption4 =  MaskOption()
        MaskOption4.title = "Cute"
        MaskOption4.zhHans = "可愛"
        MaskOption4.zhHant = "可愛"
        let MaskOption5 =  MaskOption()
        MaskOption5.title = "Interesting"
        MaskOption5.zhHans = "有趣"
        MaskOption5.zhHant = "有趣"
        let MaskOption6 =  MaskOption()
        MaskOption6.title = "Strange"
        MaskOption6.zhHans = "奇怪"
        MaskOption6.zhHant = "奇怪"
        arrayMaskOption.append(MaskOption1)
        arrayMaskOption.append(MaskOption2)
        arrayMaskOption.append(MaskOption3)
        arrayMaskOption.append(MaskOption4)
        arrayMaskOption.append(MaskOption5)
        arrayMaskOption.append(MaskOption6)
        return arrayMaskOption
    }
    
    func getStickerOptionForGift() -> [StickerOption] {
        var arrayStickerOption = [StickerOption]()
        let stickerOption1 =  StickerOption()
        stickerOption1.title = "All"
        stickerOption1.zhHans = "全部"
        stickerOption1.zhHant = "全部"
        stickerOption1.isSelected = true
        
        let stickerOption3 =  StickerOption()
        stickerOption3.title = "Basic"
        stickerOption3.zhHans = "基本"
        stickerOption3.zhHant = "基本"
    
        let stickerOption4 =  StickerOption()
        stickerOption4.title = "Scary"
        stickerOption4.zhHans = "恐怖"
        stickerOption4.zhHant = "恐怖"
        
        let stickerOption5 =  StickerOption()
        stickerOption5.title = "Funny"
        stickerOption5.zhHans = "有趣"
        stickerOption5.zhHant = "有趣"
        
        let stickerOption6 =  StickerOption()
        stickerOption6.title = "Strange"
        stickerOption6.zhHans = "奇怪"
        stickerOption6.zhHant = "奇怪"
        
        let stickerOption2 =  StickerOption()
        stickerOption2.title = "Cute"
        stickerOption2.zhHans = "可愛"
        stickerOption2.zhHant = "可愛"

        arrayStickerOption.append(stickerOption1)
        arrayStickerOption.append(stickerOption2)
        arrayStickerOption.append(stickerOption3)
        arrayStickerOption.append(stickerOption4)
        arrayStickerOption.append(stickerOption5)
        arrayStickerOption.append(stickerOption6)
        return arrayStickerOption
    }
    
    func getStickerOptionForwWhistle() -> [StickerOption] {
        var arrayStickerOption = [StickerOption]()
        let stickerOption1 =  StickerOption()
        stickerOption1.title = "All"
        stickerOption1.zhHans = "全部"
        stickerOption1.zhHant = "全部"
        stickerOption1.isSelected = true
        
        let stickerOption2 =  StickerOption()
        stickerOption2.title = "Move"
        stickerOption2.zhHans = "移动"
        stickerOption2.zhHant = "移動"
        
        let stickerOption3 =  StickerOption()
        stickerOption3.title = "Action"
        stickerOption3.zhHans = "行动"
        stickerOption3.zhHant = "行動"
        
        let stickerOption4 =  StickerOption()
        stickerOption4.title = "Talk"
        stickerOption4.zhHans = "谈论"
        stickerOption4.zhHant = "談論"
        
        let stickerOption5 =  StickerOption()
        stickerOption5.title = "Tools"
        stickerOption5.zhHans = "工具"
        stickerOption5.zhHant = "工具"
        
        let stickerOption6 =  StickerOption()
        stickerOption6.title = "Strange"
        stickerOption6.zhHans = "奇怪"
        stickerOption6.zhHant = "奇怪"
        
        arrayStickerOption.append(stickerOption1)
        arrayStickerOption.append(stickerOption2)
        arrayStickerOption.append(stickerOption3)
        arrayStickerOption.append(stickerOption4)
        arrayStickerOption.append(stickerOption5)
        arrayStickerOption.append(stickerOption6)
        return arrayStickerOption
    }
}

class Utility {
    public class func validPhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    public class func getDateByRequiredFormatter(format : String, date : String) -> String {
        let timezone = NSTimeZone.default;
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let objDate = dateFormatter.date(from: date);
        let seconds = timezone.secondsFromGMT(for: objDate!);
        let newObjDate = NSDate(timeInterval: TimeInterval(seconds), since: objDate!);
        dateFormatter.locale =  NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: newObjDate as Date);
    }
}

public func timeAgoSince(_ date: Date) -> String {
    
    let calendar = Calendar.current
    let now = Date()
    let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
    let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
    
    if let year = components.year, year >= 2 {
        return "\(year) years ago"
    }
    
    if let year = components.year, year >= 1 {
        return "Last year"
    }
    
    if let month = components.month, month >= 2 {
        return "\(month) months ago"
    }
    
    if let month = components.month, month >= 1 {
        return "Last month"
    }
    
    if let week = components.weekOfYear, week >= 2 {
        return "\(week) weeks ago"
    }
    
    if let week = components.weekOfYear, week >= 1 {
        return "Last week"
    }
    
    if let day = components.day, day >= 2 {
        return "\(day) days ago"
    }
    
    if let day = components.day, day >= 1 {
        return "Yesterday"
    }
    
    if let hour = components.hour, hour >= 2 {
        return "\(hour) hours ago"
    }
    
    if let hour = components.hour, hour >= 1 {
        return "An hour ago"
    }
    
    if let minute = components.minute, minute >= 2 {
        return "\(minute) minutes ago"
    }
    
    if let minute = components.minute, minute >= 1 {
        return "A minute ago"
    }
    
    if let second = components.second, second >= 3 {
        return "\(second) seconds ago"
    }
    return "Just now"
}
