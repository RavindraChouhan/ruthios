//
//  CPAlertAction.swift
//  CPAlertViewController
//
//  Created by mac on 25/10/17.
//  Copyright © 2017 Relibit Labs, LLC. All rights reserved.
//

import Foundation

enum CPAlertActionType{
    case normal
    case cancel
    case header
}

typealias CPAlertActionHandler = () -> Void

class CPAlertAction{
    
    let title: String
    let type: CPAlertActionType
    let handler: CPAlertActionHandler?
    
    init(title: String? = nil, type: CPAlertActionType, handler: CPAlertActionHandler?){
        if  title != nil {
            self.title = title!
        }else{
          self.title = ""
        }
        self.type = type
        self.handler = handler
    }
    
}
