//
//  CPAlertVC.swift
//  CPAlertView
//
//  Created by mac on 25/10/17.
//  Copyright © 2017 Relibit Labs, LLC. All rights reserved.
//

import UIKit

enum CPAlertAnimationType{
    case scale
    case rotate
    case bounceUp
    case bounceDown
    case center
}

class CPAlertVC: UIViewController, UITextFieldDelegate {

    //MARK: - DECLARE
    
    @IBOutlet weak var alertView: UIView!
//    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var messageLabel: UILabel!
//    
//    @IBOutlet weak var posButton: UIButton!
//    @IBOutlet weak var negButton: UIButton!
    
    
    // MARK: Alert View Outlet properties
    
    @IBOutlet weak var lblAlertTitle:UILabel!
    @IBOutlet weak var txtfield:UITextField!
    @IBOutlet weak var btnCancel:UIButton!
    @IBOutlet weak var btnCreate:UIButton!
    
    var isTextAlert = false
    var backgroundColor: UIColor = .black
    var backgroundOpacity: CGFloat = 0.5
    var animateDuration: TimeInterval = 1.0
    
    var scaleX: CGFloat = 0.3
    var scaleY: CGFloat = 1.5
    var rotateRadian:CGFloat = 1.5 // 1 rad = 57 degrees
    
    var springWithDamping: CGFloat = 0.7
    var delay: TimeInterval = 0
    
    private var titleMessage: String = ""
    private var message: String = ""
    private var animationType: CPAlertAnimationType = .scale
    
    private var negativeAction: CPAlertAction?
    private var positiveAction: CPAlertAction?
    
    //MARK: - LIFECYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.alpha = 0
        alertView.layer.cornerRadius = 4
        view.backgroundColor = backgroundColor.withAlphaComponent(backgroundOpacity)
//        if negButton != nil{
//            negButton.isHidden = true
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startAnimating(type: self.animationType)
    }
    
    //MARK: - CONFIG
    
    class func create() -> CPAlertVC{
        
        let alertStoryboard = UIStoryboard(name: "CPAlert", bundle: nil)
        let alertVC = alertStoryboard.instantiateViewController(withIdentifier: "CPAlertVC") as! CPAlertVC
        alertVC.isTextAlert = false
        return alertVC
        
    }
    
    class func createWithText() -> CPAlertVC{
        let alertStoryboard = UIStoryboard(name: "CPAlert", bundle: nil)
        let alertVC = alertStoryboard.instantiateViewController(withIdentifier: "CPAlertVCWithText") as! CPAlertVC
        alertVC.isTextAlert = true
        return alertVC
    }
    func config(title: String, message: String, animationType: CPAlertAnimationType = .scale) -> CPAlertVC{
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
        if !isTextAlert {
            self.titleMessage = title
            self.message = message
        }else{
            
        }
        self.animationType = animationType
        return self
    }
    
    func config(title: String, message: String, animationType: CPAlertAnimationType = .scale, withModel model : Any?) -> CPAlertVC{
        
      
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
        if !isTextAlert {
            self.titleMessage = title
            self.message = message
        }else{
            
        }
        self.animationType = animationType
        return self
    }
    
    func config(animationType: CPAlertAnimationType = .scale, withModel model : Any?) -> CPAlertVC{
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
        self.animationType = animationType
        return self
        
    }
    
    
    func show(into viewController: UIViewController){
        
        viewController.present(self, animated: false, completion: {
            self.setupButton()
            self.configUI()
        })
        
    }
    
    func configUI(){
    
        if !self.isTextAlert {
//            self.titleLabel.text = titleMessage
//            self.messageLabel.text = message
        }
        self.txtfield.text = ""
        if let posAction = self.positiveAction{
            self.btnCreate.setTitle(posAction.title, for: .normal)
        }
       
    }
    
    private func setupButton(){
        if !isTextAlert
        {
            if let posAction = self.positiveAction{
                self.btnCreate.setTitle(posAction.title, for: .normal)
            }
            
            if let negAction = self.negativeAction{
                self.btnCancel.isHidden = false
                self.btnCancel.setTitle(negAction.title, for: .normal)
            }
        }
    }

//    func setTextFieldeDelegate(){
//        
//        self.txtfield.delegate = self
//    }
    
    private func startAnimating(type: CPAlertAnimationType){
        alertView.alpha = 1
        switch type {
        case .rotate:
            alertView.transform = CGAffineTransform(rotationAngle: rotateRadian)
        case .bounceUp:
            let screenHeight = UIScreen.main.bounds.height/2 + alertView.frame.height/2
            alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
        case .bounceDown:
            let screenHeight = -(UIScreen.main.bounds.height/2 + alertView.frame.height/2)
            alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
        case .center:
            alertView.transform = CGAffineTransform(scaleX: 0, y: 0)
        default:
            alertView.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
        }
        UIView.animate(withDuration: animateDuration, delay: delay, usingSpringWithDamping: springWithDamping, initialSpringVelocity: 0, options: .allowUserInteraction, animations: {
            self.alertView.transform = .identity
        },  completion: { (isDone) in
            if type == .center
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: { 
                    self.dismiss(animated: true, completion: { })
                })
            }
        })
    }
    
    func addAction(_ action: CPAlertAction){
        switch action.type{
        case .normal:
            positiveAction = action
        case .cancel:
            negativeAction = action
        case .header:
            print("")
//            negativeAction = action
        }
    }
    
    //MARK: - ACTION
    
    @IBAction func tapPositiveButton(_ sender: Any) {
        if self.txtfield != nil  {
            if (self.txtfield.text?.trim().count)! > 0 {
                dismiss(animated: true, completion: {
                    if let posHandler = self.positiveAction?.handler{
                        posHandler()
                    }
                })
            }else{
                let alert = UIAlertController(title: "Alert", message: "Please enter location name.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }else{
            dismiss(animated: true, completion: {
                if let posHandler = self.positiveAction?.handler{
                    posHandler()
                }
            })
        }
       
    }
    @IBAction func tapNegativeButton(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let negHandler = self.negativeAction?.handler{
                negHandler()
            }
        })
    }
    @IBAction func actionHideAlert(_ btn: UIButton) {
        self.txtfield.resignFirstResponder()
        if let negHandler = self.negativeAction?.handler{
            let screenHeight = UIScreen.main.bounds.height + self.alertView.frame.height
            UIView.animate(withDuration: self.animateDuration + 0.5, delay: self.delay, usingSpringWithDamping: self.springWithDamping, initialSpringVelocity: 0, options: .allowUserInteraction, animations: {
                self.alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
            }, completion: { (isDone) in
                self.dismiss(animated: true, completion: {
                    negHandler()
                })
            })
        }
    }
    
    @IBAction func actionCreatePlaylist(_ btn: UIButton) {
        if self.txtfield != nil  {
            if (self.txtfield.text?.trim().count)! > 0 {
                self.txtfield.resignFirstResponder()
                if let posHandler = self.positiveAction?.handler{
                    let screenHeight = UIScreen.main.bounds.height + self.alertView.frame.height
                    UIView.animate(withDuration: self.animateDuration + 0.5, delay: self.delay, usingSpringWithDamping: self.springWithDamping, initialSpringVelocity: 0, options: .allowUserInteraction, animations: {
                        self.alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
                    }, completion: { (isDone) in
                        self.dismiss(animated: true, completion: {
                            posHandler()
                        })
                    })
                }
            }else{
                let alert = UIAlertController(title: "Alert", message: "Please enter location name.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            self.txtfield.resignFirstResponder()
            if let posHandler = self.positiveAction?.handler{
                let screenHeight = UIScreen.main.bounds.height + self.alertView.frame.height
                UIView.animate(withDuration: self.animateDuration + 0.5, delay: self.delay, usingSpringWithDamping: self.springWithDamping, initialSpringVelocity: 0, options: .allowUserInteraction, animations: {
                    self.alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
                }, completion: { (isDone) in
                    self.dismiss(animated: true, completion: {
                        posHandler()
                    })
                })
            }
        }
    }
   
    
}
