//
//  CPAlertVC.swift
//  CPAlertView
//
//  Created by mac on 25/10/17.
//  Copyright © 2017 Relibit Labs, LLC. All rights reserved.
//

import UIKit

enum CPAlertType{
    case alert_Type_ok
    case alert_Type_Decision
    
}

class CPNormalAlertVC: UIViewController {

    //MARK: - DECLARE
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var posButton: UIButton!
    @IBOutlet weak var negButton: UIButton!
    @IBOutlet weak var negBigButton: UIButton!
    
    var backgroundColor: UIColor = .black
    var backgroundOpacity: CGFloat = 0.5
    var animateDuration: TimeInterval = 1.0
    var scaleX: CGFloat = 0.3
    var scaleY: CGFloat = 1.5
    var rotateRadian:CGFloat = 1.5 // 1 rad = 57 degrees
    
    var springWithDamping: CGFloat = 0.7
    var delay: TimeInterval = 0
    
    private var alert_title: String = ""
    private var message: String = ""
    private var animationType: CPAlertAnimationType = .scale
     private var alertType: CPAlertType = .alert_Type_ok
    
    private var negativeAction: CPAlertAction?
    private var positiveAction: CPAlertAction?
    
    var mArrayList = [[String : Any]]()
    
    //MARK: - LIFECYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.alpha = 0
        alertView.layer.cornerRadius = 4
        view.backgroundColor = backgroundColor.withAlphaComponent(backgroundOpacity)
//        if negButton != nil{
//            negButton.isHidden = true
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startAnimating(type: self.animationType)
    }
    
    //MARK: - CONFIG
    
    class func create() -> CPNormalAlertVC{
        
        let alertStoryboard = UIStoryboard(name: "CPAlert", bundle: nil)
        let alertVC = alertStoryboard.instantiateViewController(withIdentifier: "CPNormalAlertVC") as! CPNormalAlertVC
        return alertVC
        
    }
    
    func config(title: String?, message: String?, icon: String?, alertType: CPAlertType = .alert_Type_ok, animationType: CPAlertAnimationType = .scale) -> CPNormalAlertVC{
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
        self.title = title!
        self.message = message!
        self.alertType = alertType
        self.animationType = animationType
        return self
    }
    
    
    func show(into viewController: UIViewController){
        
        viewController.present(self, animated: false, completion: {
            self.setupButton()
            self.configUI()
        })
    }
    
    func configUI(){
        self.lblTitle.text = self.alert_title
        self.lblMessage.text = self.message
//        self.lblMessage.sizeToFit()
        if self.alertType == CPAlertType.alert_Type_ok {
            self.btnView.isHidden = true
        }else
        {
            self.btnView.isHidden = false
        }
      // self.btnView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
       // self.imgIcon.image
    }
    
    private func setupButton(){
        if alertType == .alert_Type_Decision {
            if let posAction = self.positiveAction{
                self.posButton.setTitle(posAction.title, for: .normal)
              //  self.posButton.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
            }
            if let negAction = self.negativeAction{
                // self.negButton.isHidden = false
                self.negButton.setTitle(negAction.title, for: .normal)
              //  self.negButton.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
            }
        }else{
           
            if let negAction = self.negativeAction{
                // self.negButton.isHidden = false
                self.negBigButton.setTitle(negAction.title, for: .normal)
               // self.negBigButton.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
            }
        }
    }

    private func startAnimating(type: CPAlertAnimationType){
        
        alertView.alpha = 1
        switch type {
        case .rotate:
            alertView.transform = CGAffineTransform(rotationAngle: rotateRadian)
        case .bounceUp:
            let screenHeight = UIScreen.main.bounds.height/2 + alertView.frame.height/2
            alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
        case .bounceDown:
            let screenHeight = (UIScreen.main.bounds.height/2 + alertView.frame.height/2)
            alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
        case .center:
            alertView.transform = CGAffineTransform(scaleX: 0, y: 0)
        default:
            alertView.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
        }
        UIView.animate(withDuration: animateDuration, delay: delay, usingSpringWithDamping: springWithDamping, initialSpringVelocity: 0, options: .allowUserInteraction, animations: {
            self.alertView.transform = .identity
        },  completion: { (isDone) in
        })
        
    }
   
    func addAction(_ action: CPAlertAction){
        switch action.type{
        case .normal:
            positiveAction = action
        case .cancel:
            negativeAction = action
        case .header:
            print("")
        }
    }
    
    //MARK: - ACTION
    
    @IBAction func tapPositiveButton(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let posHandler = self.positiveAction?.handler{
                posHandler()
            }
        })
    }
    @IBAction func tapNegativeButton(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let negHandler = self.negativeAction?.handler{
                negHandler()
            }
        })
    }
}
