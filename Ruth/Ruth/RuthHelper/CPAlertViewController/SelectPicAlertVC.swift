//
//  CPAlertVC.swift
//  CPAlertView
//
//  Created by mac on 25/10/17.
//  Copyright © 2017 Relibit Labs, LLC. All rights reserved.
//

import UIKit


class SelectPicAlertVC: UIViewController {

    //MARK: - DECLARE
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblCamera: UILabel!
    @IBOutlet weak var btnMobileButton: UIButton!
    @IBOutlet weak var btnCameraButton: UIButton!
    @IBOutlet weak var negBigButton: UIButton!
    
    var backgroundColor: UIColor = .black
    var backgroundOpacity: CGFloat = 0.5
    var animateDuration: TimeInterval = 1.0
    var scaleX: CGFloat = 0.3
    var scaleY: CGFloat = 1.5
    var rotateRadian:CGFloat = 1.5 // 1 rad = 57 degrees
    
    var springWithDamping: CGFloat = 0.7
    var delay: TimeInterval = 0
    
    private var alert_title: String = ""
    private var message: String = ""
    private var animationType: CPAlertAnimationType = .scale
    private var alertType: CPAlertType = .alert_Type_ok
    
    private var negativeAction: CPAlertAction?
    private var cameraAction: CPAlertAction?
    private var mobileAction: CPAlertAction?
    var mArrayList = [[String : Any]]()
    
    //MARK: - LIFECYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.alpha = 0
        alertView.layer.cornerRadius = 4
        view.backgroundColor = backgroundColor.withAlphaComponent(backgroundOpacity)
//        if negButton != nil{
//            negButton.isHidden = true
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startAnimating(type: self.animationType)
    }
    
    //MARK: - CONFIG
    
    class func create() -> SelectPicAlertVC{
        
        let alertStoryboard = UIStoryboard(name: "CPAlert", bundle: nil)
        let alertVC = alertStoryboard.instantiateViewController(withIdentifier: "SelectPicAlertVC") as! SelectPicAlertVC
        return alertVC
        
    }
    
    func config(_ animationType: CPAlertAnimationType = .scale) -> SelectPicAlertVC{
        
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
        //self.title = title!
        self.animationType = animationType
        return self
    }
    
    
    func show(into viewController: UIViewController){
        
        viewController.present(self, animated: false, completion: {
            self.configUI()
        })
    }
    
    func configUI(){
        self.lblCamera.text = "拍照"
        self.lblMobile.text = "本機"
    }
   

    private func startAnimating(type: CPAlertAnimationType){
        
        alertView.alpha = 1
        switch type {
        case .rotate:
            alertView.transform = CGAffineTransform(rotationAngle: rotateRadian)
        case .bounceUp:
            let screenHeight = UIScreen.main.bounds.height/2 + alertView.frame.height/2
            alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
        case .bounceDown:
            let screenHeight = (UIScreen.main.bounds.height/2 + alertView.frame.height/2)
            alertView.transform = CGAffineTransform(translationX: 0, y: screenHeight)
        case .center:
            alertView.transform = .identity//CGAffineTransform(scaleX: 1, y: 1)
        default:
            alertView.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
        }
        UIView.animate(withDuration: animateDuration, delay: delay, usingSpringWithDamping: springWithDamping, initialSpringVelocity: 0, options: .allowUserInteraction, animations: {
            self.alertView.transform = .identity
        },  completion: { (isDone) in
        })
        
    }
   
    func addAction(_ action: CPAlertAction){
        switch action.type{
        case .normal:
            mobileAction = action
        case .cancel:
            negativeAction = action
        case .header:
            cameraAction = action
        }
    }
    
    //MARK: - ACTION
    
    @IBAction func tapMobileButton(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let posHandler = self.mobileAction?.handler{
                posHandler()
            }
        })
    }
    @IBAction func tapCameraButton(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let posHandler = self.cameraAction?.handler{
                posHandler()
            }
        })
    }
    
    @IBAction func tapNegativeButton(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let negHandler = self.negativeAction?.handler{
                negHandler()
            }
        })
    }
}
