//
//  UIViewController+Extensions.swift
//  Ruth
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
var isAlertShow = false
extension UIViewController {
    
    public func showAlertMessage(message : String?) {
        self.showMessageWithTitle(title: "", message: message)
    }
    
    private func showMessageWithTitle(title : String?, message : String?) {
        let attributedTitle   = NSMutableAttributedString(string: title ?? "")
        attributedTitle.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 18), range: NSRange(location: 0, length: attributedTitle.length))
        let attributedMessage = NSMutableAttributedString(string: message ?? "")
        attributedMessage.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 14), range: NSRange(location: 0, length: attributedMessage.length))
        
        let alertController = UIAlertController(title: "", message: "", preferredStyle:.alert)
        alertController.setValue(attributedTitle, forKey: "attributedTitle")
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        
        alertController.addAction(UIKit.UIAlertAction(title: "OK".localized(), style: .default))
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showMessageTitle(_ title : String?,_ message : String?,_ buttonOption : [String]? = nil , optionHandler : ((_ selectIdx : Int,_ selectBtnTitle : String) ->Swift.Void)? = nil ) {
        let attributedTitle   = NSMutableAttributedString(string: title ?? "")
        attributedTitle.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 18), range: NSRange(location: 0, length: attributedTitle.length))
        let attributedMessage = NSMutableAttributedString(string: message ?? "")
        attributedMessage.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 14), range: NSRange(location: 0, length: attributedMessage.length))
        
        let alertController = UIAlertController(title: "", message: "", preferredStyle:.alert)
        alertController.setValue(attributedTitle, forKey: "attributedTitle")
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        if buttonOption != nil {
            if (buttonOption?.count)! > 0{
                for btnTitle in buttonOption!{
                    alertController.addAction(UIAlertAction(title: btnTitle, style: .default, handler: { (action) in
                        if optionHandler != nil {
                            optionHandler!(0, btnTitle)
                        }
                    }))
                }
            }else{
                alertController.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { (action) in
                    if optionHandler != nil {
                        optionHandler!(0, "OK")
                    }
                }))
            }
        }else{
            alertController.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { (action) in
                if optionHandler != nil {
                    optionHandler!(0, "OK")
                }
            }))
        }
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
}
// extension String {
//    
//    func attributedComment() -> NSAttributedString {
//        let attrs = dsfvdsvfgadsf
//            .font(UIFont.defaultFont(size: 13))
//            .foregroundColor(UIColor.white)
//            .alignment(.left)
//            .lineSpacing(1)
//            .dictionary
//        return NSAttributedString(string: self, attributes: attrs)
//    }
//    
//    static func random(_ length: Int = 4) -> String {
//        let base = "abcdefghijklmnopqrstuvwxyz"
//        var randomString: String = ""
//        for _ in 0..<length {
//            let randomValue = arc4random_uniform(UInt32(base.characters.count))
//            randomString += "\(base[base.characters.index(base.startIndex, offsetBy: Int(randomValue))])"
//        }
//        return randomString
//    }
//    
//}
extension Date {
    func offsetFromnew(date: Date) -> String {
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        //        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: );
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: self, to: date)
        let strSecond = NSString(format: "%02d",difference.second!) as String
        let strMinutes = NSString(format: "%02d:",difference.minute!) as String
        let strHours = NSString(format: "%02d:",difference.hour!) as String //"\(difference.hour ?? 0):" //+  minutes
        if strHours != "00:"
        {
            return strHours+strMinutes+strSecond
        }else{
            return strSecond
        }
    }
    
    // MARK:- add Component in date
    static func addComponentNew(component : Calendar.Component,  n: Int, date :Date) -> Date {
        let cal = NSCalendar.current
        return cal.date(byAdding: component, value: n, to: date)!
    }
}
