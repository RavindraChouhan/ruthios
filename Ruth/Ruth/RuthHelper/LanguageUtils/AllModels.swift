//
//  AllModels.swift
//  PostCard
//
//  Created by Mac on 21/04/17.
//  Copyright © 2017 Linkites. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SVProgressHUD
import CoreLocation
import RealmSwift

class User: NSObject , CLLocationManagerDelegate  {
    var locationManager = CLLocationManager()
    var delegate = AppDelegate()
    var _id:String = ""
    
    var firstnameEmail = ""
    var lastNameEmail = ""
    var emailEmail = ""
    var postCodeEmail = ""
    var telephoneEmail = ""
    var messageEmail = ""
    var languageEmail = ""
    var inMarketEmail = ""
    var dealerdateEmail = ""
    var vehicleModelEmail = ""
    var vehicleTitleEmail = ""
    
    var active = false
    var createdAt:String = ""
    var dealerCode:String = ""
    var dealerName:String = ""
    var emails = ""
    var firstName:String = ""
    var formEmail = ""
    var language:String = ""
    var lastLoginAt = ""
    var lastName:String = ""
    var fullname:String = ""
    var phoneNumber = ""
    var position:String = ""
    var preloadedUserId:String = ""
    var services:String = ""
    var hashedToken = ""
    var when = ""
    var resume = ""
    var address = ""
    var verified = 0
    var password:String = ""
    var studentId:String = ""
    var username:String = ""
    var mobile = 0
    var dealerzone:String = ""
    var zoneid = ""
    var mobileforDisplay:String = ""
    var authToken:String = ""
    var notifications  =  [String:Any]()
    var info = [String : Any]()
    var isLogin:Bool = false
    var isBlock:Bool = false
    static let currentUser:User = User()
    var appBundleID:String = ""
    var currentLatitude: Double = Config.theParkLat
    var currentLongitude: Double = Config.theParkLng
    var isEnglishLanguageSelected = true
    var isLoadedLoaction:Bool = false
    var accountSetting: AccountSetting!
    var settingArr = [AccountSetting]()
   // var dealer:DealerData!
    var dealer:DealerDataRealm!
    var title = ""
    var isSelected = false
    var keySelectedString = ""
    func initWithInfo(isAPICall:Bool) {
        let preferences = UserDefaults.standard
        isLogin = preferences.bool(forKey: "isLogin")
        if isLogin {
            self.authToken = preferences.value(forKey: "authToken") as! String
            if (preferences.value(forKey: "info") != nil) {
                info = preferences.value(forKey: "info") as! [String : Any]
              //  print("info ==  \(info)")
                self.studentId = info["studentId"] as! String
                self._id = self.info["_id"] as! String
                self.active = self.info["active"] as! Bool
                self.createdAt = self.info["createdAt"] as! String
                self.dealerCode = self.info["dealerCode"] as! String
                self.dealerName = self.info["dealerName"] as! String
                if let emailss = self.info["emails"] as? [[String:Any]]
                {
                    for dict in emailss{
                        if dict["verified"] as! Bool {
                            self.emails = dict["address"] as! String
                            break
                        }
                    }
                }
                self.firstName = self.info["firstName"] as! String
                self.formEmail = self.info["formEmail"] as! String
                self.language = self.info["language"] as! String
                self.lastName = self.info["lastName"] as! String
                self.fullname = self.info["name"] as! String
                if let notificati = self.info["notifications"] as? [String:Any]
                {
                    self.notifications = notificati
                }
                self.position = self.info["position"] as! String
                
                let num = self.info["phoneNumber"]
                if num is String
                {
                    self.phoneNumber = num as! String
                    //print("num is Int")
                }
                self.preloadedUserId = self.info["preloadedUserId"] as! String
                self.username = self.info["username"] as! String
                self.zoneid = self.info["zone"] as! String
                if let dealer = self.info["dealerData"] as? [String:Any] {
//                    self.dealer = DealerDataRealm(info: dealer)
                    self.dealer = DealerDataRealm(value: dealer)
                }
                if let dealerzon = self.info["dealerzone"] as? String {
                    self.dealerzone = dealerzon
                }
            }
            if isAPICall {
                getInfo()
            }
        }
    }
    
    
    func sendMail() {
        let preferences = UserDefaults.standard
//        if preferences.object(forKey: "sendEmailWhenOnline") != nil {
        
            preferences.set(self.firstnameEmail, forKey: "firstnameEmail")
            preferences.set(self.lastNameEmail, forKey: "lastNameEmail")
            preferences.set(self.emailEmail, forKey: "emailEmail")
            preferences.set(self.postCodeEmail, forKey: "postCodeEmail")
            preferences.set(self.telephoneEmail, forKey: "telephoneEmail")
            preferences.set(self.messageEmail, forKey: "messageEmail")
            preferences.set(self.languageEmail, forKey: "languageEmail")
            preferences.set(self.inMarketEmail, forKey: "inMarketEmail")
            preferences.set(self.dealerdateEmail, forKey: "dealerdateEmail")
            preferences.set(self.vehicleModelEmail, forKey: "vehicleModelEmail")
            preferences.set(self.vehicleTitleEmail, forKey: "vehicleTitleEmail")
            
            self.firstnameEmail = preferences.string(forKey: "firstnameEmail")!
            self.lastNameEmail = preferences.string(forKey: "lastNameEmail")!
            self.emailEmail = preferences.string(forKey: "emailEmail")!
            self.postCodeEmail = preferences.string(forKey: "postCodeEmail")!
            self.telephoneEmail = preferences.string(forKey: "telephoneEmail")!
            self.messageEmail = preferences.string(forKey: "messageEmail")!
            self.languageEmail = preferences.string(forKey: "languageEmail")!
            self.inMarketEmail = preferences.string(forKey: "inMarketEmail")!
            self.dealerdateEmail = preferences.string(forKey: "dealerdateEmail")!
            self.vehicleModelEmail = preferences.string(forKey: "vehicleModelEmail")!
            self.vehicleTitleEmail = preferences.string(forKey: "vehicleTitleEmail")!
//        }
    }
    func checkInternetConectivity() {
        if Connectivity.isConnectedToInternet() {
            //            self.overLayController.hideInternetConectionPopup()
            NotificationCenter.default.post(name: AppNotifications.notificationReloadDataAfterReconectToInternet, object: nil)
        }else
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                self.checkInternetConectivity()
            })
        }
    }
    
    func login(parameters:[String:Any], completion:@escaping (_ success: Any) -> Void) {
        
        let url = Config.serverUrl + UserloginApi
       print("LOGIN Api === \(url)")
        SVProgressHUD.show()
        Service().post(url: url, parameters: parameters) { (result) in
            SVProgressHUD.dismiss()
            if result is [String : Any] {
                let dataInfo = result as! [String : Any]
              print("dataInfo**\(dataInfo)")
                if dataInfo["success"] as! Bool {
                    let data = dataInfo["user"] as! [String : Any]
                    self.isLogin = true
                    self.info["_id"]         = data["_id"]
                    self.info["active"]      = data["active"]
                    self.info["createdAt"]   = data["createdAt"]
                    self.info["dealerCode"]  = data["dealerCode"]
                    self.info["dealerName"]  = data["dealerName"]
                    self.info["phoneNumber"] = data["phoneNumber"]
                    self.info["emails"]      = data["emails"]
                    self.info["firstName"]   = data["firstName"]
                    self.info["language"]    = data["language"]
                    self.info["lastName"]    = data["lastName"]
                    self.info["name"]        = data["name"]
                    self.info["notifications"]  = data["notifications"]
                    self.info["position"]    = data["position"]
                    self.info["preloadedUserId"] = data["preloadedUserId"]
                    self.info["services"]    = data["services"]
                    self.info["studentId"]   = data["studentId"]
                    self.info["username"]    = data["username"]
                    self.info["formEmail"]   = data["formEmail"]
                    self.info["zone"]        = data["zone"]
                    if let dealer = dataInfo["dealerData"] as? [String:Any] {
                        self.info["dealerData"] =  dealer
                    }
                    if let dealerz = dataInfo["dealerzone"] as? String {
                        self.info["dealerzone"] =  dealerz
                    }
                    
                    if let dealer = self.info["dealerData"] as? [String:Any] {
//                        self.dealer = DealerData(info: dealer)
                        self.dealer = DealerDataRealm(value: dealer)
                    }
                    if let dealerzon = self.info["dealerzone"] as? String {
                        self.dealerzone = dealerzon
                    }
                    if let emailss = self.info["emails"] as? [[String:Any]]
                    {
                        for dict in emailss{
                            if dict["verified"] as! Bool {
                                self.emails = dict["address"] as! String
                                break
                            }
                        }
                        
                    }
                    if let notificati = self.info["notifications"] as? [String:Any]
                    {
                        self.notifications = notificati
                    }
                    self._id        = self.info["_id"] as! String
                    self.active     = self.info["active"] as! Bool
                    self.createdAt  = self.info["createdAt"] as! String
                    self.dealerCode = self.info["dealerCode"] as! String
                    self.dealerName = self.info["dealerName"] as! String
                    self.firstName  = self.info["firstName"] as! String
                    self.formEmail  = self.info["formEmail"] as! String
                    self.language   = self.info["language"] as! String
                    self.lastName   = self.info["lastName"] as! String
                    self.fullname   = self.info["name"] as! String
                    let num = self.info["phoneNumber"]
                    if num is String
                    {
                        self.phoneNumber = num as! String
                        //print("num is Int")
                    }
                    self.position   = self.info["position"] as! String
                    self.preloadedUserId = self.info["preloadedUserId"] as! String
                    self.username   = self.info["username"] as! String
                    self.studentId  = self.info["studentId"] as! String
                    self.zoneid     = self.info["zone"] as! String
                    User.currentUser.saveToLocal()
                    completion(result)
                }
                else{
                    completion(result)
                }
            }
            else {
                completion(result)
                print("error \(result)");
            }
            
        }
    }
    func userSignUp(parameters:[String:Any], completion:@escaping (_ success: Bool) -> Void) {
        
        let url = Config.serverUrl+SocialSignIn
       // print("LOGIN Api === \(url)")
        SVProgressHUD.show()
        Service().post(url: url, parameters: parameters) { (result) in
            SVProgressHUD.dismiss()
            if result is [String : Any] {
                let dataInfo = result as! [String : Any]
               // print("dataInfo**\(dataInfo)")
                if dataInfo["success"] as! Bool {
                    let data = dataInfo["user"] as! [String : Any]
                    self.isLogin = true
                    self.info["studentId"]   = data["studentId"]
                    self.info["email"]       = data["email"]
                    self.info["dealerCode"]  = data["dealerCode"]
                    self.info["firstname"]   = data["firstname"]
                    self.info["lastname"]    = data["lastname"]
                    self.info["language"]    = data["language"]
                    self.info["dealerCode"]  = data["dealerCode"]
                    self.info["formEmail"]  = data["formEmail"]
                    if let dealer = dataInfo["dealerData"] as? [String:Any] {
                        self.info["dealerData"] =  dealer
                    }
                    if let dealer = self.info["dealerData"] as? [String:Any] {
//                        self.dealer = DealerData(info: dealer)
                        self.dealer = DealerDataRealm(value: dealer)
                    }
                    if let num = self.info["phoneNumber"] {
                        if num is String
                        {
                            self.phoneNumber = num as! String
                            //print("num is Int")
                        }
                    }
                    //  self.info["categoryIds"] = data["categoryIds"]
                    self.language = self.info["language"] as! String
                    self.studentId = self.info["studentId"] as! String
                    self.firstName = self.info["firstname"] as! String
                    self.formEmail = self.info["formEmail"] as! String
                    self.lastName = self.info["lastname"] as! String
                    self.dealerCode = self.info["dealerCode"] as! String
                    User.currentUser.saveToLocal()
                    completion(true)
                }else{
                    
                    //                   showAlert(title: "Alert".localized(), message:"No record found.".localized())
                }
            }else if result is Error{
                let error = result as! Error
                 //showAlert(title: "Alert".localized(), message: error.localizedDescription)
            }
            
        }
    }
    //    func sendUserInformation(parameter: [String:Any], completion:@escaping (_ success: Bool) -> Void) {
    //
    //        print("Parameterds***\(parameter)")
    //        let url = Config.serverUrl+"states"
    //
    //        Service().post(url: url, parameters: parameter) { (result) in
    //            if result.isKind(of:NSDictionary.self) {
    //                completion(true)
    //            }else {
    //                print("error \(result)");
    //                completion(false)
    //            }
    //        }
    //    }
    
    //get userinfo from server
    func getInfo() {
        //        let url = Config.serverUrl+"me"
        //        Service().get(url: url) { (result) in
        //            if result is [String : Any] {
        //                let dinfo =  result as! [String : Any]
        //                //                print(dinfo)
        //                if dinfo["success"] as! Bool {
        //                    self.isLogin = true
        //                    let data = dinfo["data"] as! [String : Any]
        //
        //                    self.info["_id"] =  data["_id"]
        //                    self.info["firstName"] =  data["firstName"]
        //                    self.info["lastName"] =  data["lastName"]
        //                    self.info["mobile"] =  data["mobile"]
        //                    self.info["email"] =  data["email"]
        //                    self.info["authSource"] =  data["authSource"]
        //                    self.info["currentLocation"] =  data["currentLocation"]
        //                    self.info["devices"] =  data["devices"]
        //                    self.info["env"] =  data["env"]
        //                    self.info["invitedUsers"] =  data["invitedUsers"]
        //                    self.info["profilePicture"] =  data["profilePicture"]
        //                    self.info["roles"] =  data["roles"]
        //                    self.info["savedLocations"] =  data["savedLocations"]
        //                    self.info["userPhotos"] =  data["userPhotos"]
        //                    self.info["publicationIds"] = data["publicationIds"]
        //                    self.info["categoryIds"] = data["categoryIds"]
        //                    self.saveToLocal()
        self.initWithInfo(isAPICall: false)
        //                }
        //            }
        //        }
    }
    //save userinfo to server
    //func saveToServer() {
    // let name = self.name
    // + " " + self.lastname
    
    //        self.info["firstName"]  =  firstName
    //        self.info["lastName"]   = lastName
    //        self.info["dealerName"] = dealerName
    //        self.info["dealerCode"] = dealerCode
    //        self.info["emails"]      = emails
    
    //        let parmeters:Parameters = ["user":info];
    //        let url = Config.serverUrl+"users/create"
    //        Service().post(url: url, parameters: parmeters) { (result) in
    //
    //            if result is [String : Any] {
    //                //print(result);
    //                let dataInfo = result as! [String : Any]
    //                self.info = dataInfo["user"] as! [String : Any]
    //                self.id = self.info["_id"] as! String
    //                self.isLogin = true
    //                self.saveToLocal()
    //            }else {
    //                print("error \(result)");
    //            }
    //        }
    //}
    
    //    func updateToServer(parametes: [String:Any], completion:@escaping (_ success: Bool) -> Void) {
    //
    //        let url = Config.serverUrl+"users/"+self.id
    //        print("updateToServer parametes = \(parametes)")
    //        SVProgressHUD.show()
    //        Service().put(url: url, parameters: parametes) { (result) in
    //            SVProgressHUD.dismiss()
    //            if result.isKind(of:NSDictionary.self) {
    //                print(result);
    //                DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
    //                    self.getInfo()
    //                    completion(true)
    //                })
    //            }else {
    //                print("error \(result)");
    //                completion(false)
    //            }
    //        }
    //    }
    
    // save user info
    func saveToLocal() {
        let preferences = UserDefaults.standard
        preferences.set(isLogin, forKey:"isLogin")
        preferences.set(authToken, forKey:"authToken")
        preferences.set(self.info, forKey: "info")
        preferences.set(self.language, forKey: "language")
        preferences.set(self.studentId, forKey: "studentId")
        preferences.synchronize()
    }
    
    //delete current logged in user info
    func logout() {
        self.studentId = ""  // For Photo view count API -- 19-06-2017
        let preferences = UserDefaults.standard
        preferences.set(isLogin, forKey:"isLogin")
        preferences.removeObject(forKey: "info")
        self.authToken = ""
        preferences.removeObject(forKey: "isAlreadyLogin")
        preferences.synchronize()
    }
    //ask for user's location
    func askUserCurrentLocation(completion:@escaping (_ success: Bool) -> Void) {
        if ((CLLocationManager.authorizationStatus() == .denied) || (CLLocationManager.authorizationStatus() == .restricted)) {
            completion(false)
        }else{
            self.locationManager = CLLocationManager()
            self.locationManager.delegate = self
            self.locationManager.distanceFilter = kCLDistanceFilterNone
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.startUpdatingLocation()
            if self.locationManager.responds(to: #selector(self.locationManager.requestWhenInUseAuthorization)) {
                self.locationManager.requestWhenInUseAuthorization()
            }
            completion(true)
        }
    }
    //start updating user's location when required
    func startUpdatingLocation(completion:@escaping (_ success: Bool) -> Void) {
        
    }
    //stop updating user's location
    func stopUpdatingLocation(completion:@escaping (_ success: Bool) -> Void) {
        
    }
    // MARK:- CLLocation Manager Delegate----
    func requestUserLocation() {
        if ((CLLocationManager.authorizationStatus() == .denied) || (CLLocationManager.authorizationStatus() == .restricted)) {
            return
        }
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
        if self.locationManager.responds(to: #selector(self.locationManager.requestWhenInUseAuthorization)) {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //self.locationManager.stopUpdatingLocation()
    }
    
    func getUserCurrentLocation() {
        self.locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newLocation: CLLocation? = locations.last
        let latitude: Double? = newLocation?.coordinate.latitude
        let longitude: Double? = newLocation?.coordinate.longitude
        //        print("didUpdateLocations***Latitude**\(String(describing: latitude))****Longitude**\(String(describing: longitude))")
        User.currentUser.currentLatitude = latitude!
        User.currentUser.currentLongitude = longitude!
        User.currentUser.isLoadedLoaction = true
        self.locationManager.stopUpdatingLocation()
    }
}
class AccountSetting {
    var venueDefaultImage = ""
    var googleKey = ""
    var termAndConditionUrl = ""
    var privacyUrl = ""
    var contactEmail = ""
    var MyPurchaseReportEmail = "help@gopostcard.com"
    var appLogo = ""
    var awsAccessKeyId = "AKIAIL4KCVKK5KKMNPDQ"
    var awsSecretAccessKey = "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"
    var awsRegion = "us-west-2"
    var awsBucket = "postcardapp"
    var venueListTitle = ""
    var venueListSubTitle = ""
    var venuerRegularListTitle = ""
    var venuerRegularListSubTitle = ""
    var clientId = ""
    var redirect_url = ""
    var auth_url = ""
    var tipPercentage =  0.0
    var taxPercentage = 0.0
    public init (){}
    public init (info:NSDictionary) {
        self.venueDefaultImage = info["venueDefaultImage"] as! String
        self.googleKey = info["googleKey"] as! String
        self.termAndConditionUrl = info["termAndConditionUrl"] as! String
        self.privacyUrl = info["privacyUrl"] as! String
        self.contactEmail = info["contactEmail"] as! String
        self.appLogo = info["appLogo"] as! String
        self.tipPercentage =  info["tipPercentage"] as! Double
        self.taxPercentage = info["taxPercentage"] as! Double
        let aws = info["aws"] as! [String:String]
        self.awsAccessKeyId = aws["accessKeyId"]!
        self.awsSecretAccessKey = aws["secretAccessKey"]!
        self.awsRegion = aws["region"]!
        self.awsBucket = aws["bucket"]!
        self.venueListTitle = info["venueListTitle"] as! String
        self.venueListSubTitle = info["venueListSubTitle"] as! String
        self.venuerRegularListTitle = info["venuerRegularListTitle"] as! String
        self.venuerRegularListSubTitle = info["venuerRegularListSubTitle"] as! String
        let instagram = info["instagram"] as! [String:String]
        self.clientId = instagram["clientId"]!
        self.auth_url = instagram["auth_url"]!
        self.redirect_url = instagram["redirect_url"]!
    }
}
//TODO :Realm //offline database
class vehData: Object {
    dynamic var headerDescription = ""
    dynamic var modelYear = ""
    dynamic var photoPath = ""
    dynamic var trimName = ""
    dynamic var vehicleID = ""
}
class SelectTrimRealm: Object {
    dynamic var title = ""
     var vehicleData = List<vehData>()
    var vehicleModel = List<CompetitorsRealm>()
}
class DealerDataRealm: Object {
    // dynamic var categories = [String:Any]()
    dynamic var id = ""
    dynamic var address = ""
    dynamic var dealerName = ""
    dynamic var dealerCode = ""
    dynamic var city = ""
    dynamic var email = ""
    dynamic var postalCode = ""
    dynamic var pro = ""
    dynamic var telephone = ""
    dynamic var website = ""
    dynamic var zoneid:Int64 = 0
    dynamic var isSelected = false
}

class HomeRealmCategory : Object {
    // dynamic var categories = [String:Any]()
    dynamic var id = ""
    dynamic var title = ""
    dynamic var title_fr = ""
    dynamic var thumbImage = ""
    dynamic var photoUrl = ""
    dynamic var sortIndex = 0
    dynamic var dateCreated = ""
    dynamic var isSelected = false
}
class SubCategoriesRealm : Object {
    dynamic var id = ""
    dynamic var title = ""
    dynamic var title_fr = ""
    dynamic var thumbImage = ""
    dynamic var parent_id = ""
    dynamic var sortIndex = 0
    dynamic var dateCreated = ""
    dynamic var isSelected = false
}
class brochureUrl: Object {
    dynamic var string = ""
}
class improvements: Object {
    dynamic var en = ""
    dynamic var fr = ""
}
class reasonsToBuyRealm: Object {
    dynamic var en = ""
    dynamic var fr = ""
}
class slides: Object {
    dynamic var strImg = ""
}
class vehiclesDataRealm : Object {
    dynamic var id = ""
    dynamic var title = ""
    dynamic var base_price = ""
    dynamic var title_fr = ""
    dynamic var dateCreated = ""
    dynamic var categoryId = ""
    dynamic var model = ""
    dynamic var vehicle_id = ""
    dynamic var photo_path = ""
    dynamic var competitors = ""
    dynamic var thumbImage = ""
    dynamic var slides = ""
    dynamic var competitorId = ""
    dynamic var reasonsToBuy = ""
    dynamic var subCategoryId = ""
    dynamic var photoUrl = ""
    dynamic var isSelected = false
}
class vehicleDataRealm: Object {
    var reasonsTBRealm = List<reasonsToBuyRealm>()
    var improvements = List<improvements>()
    var specifications = List<SpecificationsRealm>()
    var brochureUrl = List<brochureUrl>()
    var competitorsRealm = List<CompetitorsRealm>()
    var allVideosRealm =  List<VideosRealm>()
    var suitabilityQuestionaireRealm = List<QuestionRealm>()
    var slides = List<slides>()
    var tipsRealm = List<TipsRealm>()
    var strTips = ""
    dynamic var id = ""
    dynamic var title = ""
    dynamic var title_fr = ""
    dynamic var categoryId = ""
    dynamic var model = ""
    dynamic var vehicle_id = ""
    dynamic var photo_path = ""
    dynamic var thumbImage = ""
    dynamic var pullQuote = ""
    dynamic var pullQuote_fr = ""
    dynamic var competitorId = ""
    dynamic var subCategoryId = ""
    dynamic var photoUrl = "https://s3.amazonaws.com/mitsubishiadv/images"
    dynamic var isSelected = false
    override static func primaryKey() -> String? {
        return "id"
    }
}
class SpecificationsRealm: Object {
    dynamic var attrId = ""
    dynamic var val = ""
    dynamic var val_fr = ""
    dynamic var advantage = ""
    dynamic var advantage_fr = ""
    dynamic var isSelected = false
    dynamic var title = ""
    dynamic var title_fr = ""
}
class QuestionRealm: Object {
    dynamic var question = ""
    dynamic var question_fr = ""
    dynamic var answer = ""
    dynamic var answer_fr = ""
    dynamic var isSelected = false
}
class CompetitorsRealm: Object {
    dynamic var headerDescription = ""
    dynamic var modelYear = ""
    dynamic var photoPath = ""
    dynamic var vehicleID = ""
    dynamic var isSelected = false
}
class VideosRealm: Object {
    dynamic var title = ""
    dynamic var title_fr = ""
    dynamic  var video_link = ""
    dynamic var video_link_fr = ""
    dynamic var type = ""
    dynamic var type_fr = ""
    dynamic var dscrion = ""
    dynamic var description_fr = ""
    dynamic var preview_image = ""
    dynamic var preview_image_fr = ""
    dynamic var isSelected = false
}
class TipsRealm: Object {
    dynamic var tip_location = ""
    dynamic var tip_category = ""
    dynamic var tip_en = ""
    dynamic var tip_fr = ""
    dynamic var isSelected = false
}
class ContactDataRealm: Object {
    dynamic var contacts = ""
    dynamic var Id = ""
    dynamic var firstName = ""
    dynamic var lastName = ""
    dynamic var email = ""
    dynamic var notes = ""
    dynamic var telephone = 0
    dynamic var dealerId = ""
    dynamic var createdAt = ""
    dynamic var vehicleModel = ""
    dynamic var vehicleTitle = ""
    dynamic var isSelected = false
}
class ObjectParser:NSObject {
    static let parser:ObjectParser = ObjectParser()
    
    func parseVehDataRealm(dictSong:[String : Any]) -> vehData {
        let  VehModel = vehData()
        if let headerD = dictSong["headerDescription"] as? String {
            VehModel.headerDescription = headerD
        }
        if let modelY = dictSong["modelYear"] as? String {
            VehModel.modelYear = modelY
        }
        if let photoP = dictSong["photoPath"] as? String {
            VehModel.photoPath = photoP
        }
        if let trimN = dictSong["trimName"] as? String {
            VehModel.trimName = trimN
        }
//        if let vehicleI = dictSong["vehicleID"] as? String {
            let idd = dictSong["vehicleID"]
            var strID = ""
            if idd is Int
            {
                strID = String(describing: idd!)
            }else if idd is String
            {
                strID = idd as! String
            }else{
                strID = String(describing: idd!)
            }
            VehModel.vehicleID = strID
//        }
//        VehModel.isSelected = false
        return VehModel
    }
    
    
    
    func parseSelectTrimRealm(trim:String , vehicleArray:[[String : Any]]) -> SelectTrimRealm {
        let  TrimModel = SelectTrimRealm()
        TrimModel.title = trim
        for dict in vehicleArray {
            if dict["trimName"] as! String == trim
            {
                let compe = parseCompetitorsDate(dictSong:dict)
                TrimModel.vehicleModel.append(compe)
            }
        }
        return TrimModel
    }
    
    func parseDealerDataRealm(dictSong:[String : Any]) -> DealerDataRealm {
        let  DealerDModel = DealerDataRealm()
        if let _id = dictSong["_id"] as? String {
            DealerDModel.id = _id
        }
        if let dealerName = dictSong["dealerName"] as? String {
            DealerDModel.dealerName = dealerName
        }
        if let address = dictSong["address"] as? String {
            DealerDModel.address = address
        }
        if let dealerc = dictSong["dealerCode"] as? String {
            DealerDModel.dealerCode = dealerc
        }
        if let email = dictSong["email"] as? String {
            DealerDModel.email = email
        }
        if let cit = dictSong["city"] as? String {
            DealerDModel.city = cit
        }
        
        if let postalCode = dictSong["postalCode"] as? String {
            DealerDModel.postalCode = postalCode
        }
        if let pro = dictSong["pro"] as? String {
            DealerDModel.pro = pro
        }
        if let telephon = dictSong["telephone"] as? String {
            DealerDModel.telephone = telephon
        }
        if let websit = dictSong["website"] as? String {
            DealerDModel.website = websit
        }
        if let zonei = dictSong["zoneid"] as? Int {
            DealerDModel.zoneid = Int64(zonei)
        }
        DealerDModel.isSelected = false
        return DealerDModel
    }
    
    func parseHome(dictSong:[String : Any], _ photoURL:String? ) -> HomeRealmCategory {
        let homeModel = HomeRealmCategory()
        if let _id = dictSong["_id"] as? String {
            homeModel.id = _id
        }
        if let dictTitle = dictSong["title"] as? [String:Any] {
            homeModel.title = (dictTitle["en"] as? String)!
            homeModel.title_fr = (dictTitle["fr"] as? String)!
        }
        if let thumbI = dictSong["thumb"] as? String {
            homeModel.thumbImage  = photoURL! + "/" + thumbI
            //print("++photoUrl++thumb++  \(homeModel.thumbImage)")
        }
        if let sortIn = dictSong["sortIndex"] as? Int {
            homeModel.sortIndex = sortIn
        }
        if let dateCreat = dictSong["dateCreated"] as? String {
            homeModel.dateCreated = dateCreat
        }
        homeModel.isSelected = false
        return homeModel
    }
    func parseSubCat(dictSong:[String : Any]) -> SubCategoriesRealm {
        let  SubCatModel = SubCategoriesRealm()
        if let _id = dictSong["_id"] as? String {
            SubCatModel.id = _id
        }
        if let pareid = dictSong["parent_id"] as? String {
            SubCatModel.parent_id = pareid
        }
        if let dictTitle = dictSong["title"] as? [String:Any] {
            SubCatModel.title = (dictTitle["en"] as? String)!
            SubCatModel.title_fr = (dictTitle["fr"] as? String)!
        }
        if let thumbI = dictSong["thumb"] as? String {
            SubCatModel.thumbImage  = thumbI
        }
        if let sortIn = dictSong["sortIndex"] as? Int {
            SubCatModel.sortIndex = sortIn
        }
        if let dateCreat = dictSong["dateCreated"] as? String {
            SubCatModel.dateCreated = dateCreat
        }
        SubCatModel.isSelected = false
        
        return SubCatModel
    }
    func parseVehiclesDate(dictSong:[String : Any], _ photoURL:String?) -> vehiclesDataRealm {
        let  VehiclesModel = vehiclesDataRealm()
        if let _id = dictSong["_id"] as? String {
            VehiclesModel.id = _id
        }
        if let base_pric = dictSong["base_price"] as? String {
            VehiclesModel.base_price = base_pric
        }
        if let dictTitle = dictSong["title"] as? [String:Any] {
            VehiclesModel.title = (dictTitle["en"] as? String)!
            VehiclesModel.title_fr = (dictTitle["fr"] as? String)!
        }
        if let thumbI = dictSong["thumb"] as? String {
            VehiclesModel.thumbImage  = photoURL! + "/" + thumbI
            //print("++photoUrl++thumb++  \(thumbImage)")
        }
        if let categoryI = dictSong["categoryId"] as? String {
            VehiclesModel.categoryId = categoryI
        }
        if let dateCreat = dictSong["dateCreated"] as? String {
            VehiclesModel.dateCreated = dateCreat
        }
        if let mode = dictSong["model"] as? String {
            VehiclesModel.model = mode
        }
        if let vehicle = dictSong["vehicle_id"] as? String {
            VehiclesModel.vehicle_id = vehicle
        }
        if let photo_p = dictSong["photo_path"] as? String {
            VehiclesModel.photo_path = photo_p
        }
        if let competitor = dictSong["competitors"] as? String {
            VehiclesModel.competitors  = competitor
        }
        if let slide = dictSong["slides"] as? String {
            VehiclesModel.slides = slide
        }
        if let competId = dictSong["competitorId"] as? String {
            VehiclesModel.competitorId  = competId
        }
        if let reasonsToB = dictSong["reasonsToBuy"] as? String {
            VehiclesModel.reasonsToBuy = reasonsToB
        }
        if let subCategoryI = dictSong["subCategoryId"] as? String {
            VehiclesModel.subCategoryId = subCategoryI
        }
        VehiclesModel.isSelected = false
        return VehiclesModel
    }
    func parseVehicleDate(dictSong:[String : Any], _ photoURL:String?) -> vehicleDataRealm {
        let  VehicleModel = vehicleDataRealm()
        if let slide = dictSong["slides"] as? [String]{
            for img in slide
            {
                let slider = slides()
                slider.strImg = photoURL! + "/" + img
                VehicleModel.slides.append(slider)
            }
        }
        if let thumbI = dictSong["thumb"] as? String {
            VehicleModel.thumbImage  = photoURL! + "/" + thumbI
        }
        if let _id = dictSong["_id"] as? String {
            VehicleModel.id = _id
        }
        if let dictTitle = dictSong["title"] as? [String:Any] {
            VehicleModel.title = (dictTitle["en"] as? String)!
            VehicleModel.title_fr = (dictTitle["fr"] as? String)!
        }
        if let categoryI = dictSong["categoryId"] as? String {
            VehicleModel.categoryId = categoryI
        }
        if let mode = dictSong["model"] as? String {
            VehicleModel.model = mode
        }
        if let vehicle = dictSong["vehicle_id"] as? String {
            VehicleModel.vehicle_id = vehicle
        }
        if let photo_p = dictSong["photo_path"] as? String {
            VehicleModel.photo_path = photo_p
        }
        if let compet = dictSong["competitors"] as? [[String:Any]] {
            
            for compe in compet
            {
                let competitorsRealm = parseCompetitorsDate(dictSong: compe)
                VehicleModel.competitorsRealm.append(competitorsRealm)
            }
        }
        if let pullQuot = dictSong["pullQuote"] as? [String:Any] {
            VehicleModel.pullQuote = (pullQuot["en"] as? String)!
            VehicleModel.pullQuote_fr = (pullQuot["fr"] as? String)!
        }
        if let competId = dictSong["competitorId"] as? String {
            VehicleModel.competitorId  = competId
        }
        if let reasonsTB = dictSong["reasonsToBuy"] as? [[String : Any]]{
            for rea in reasonsTB
            {
                let reasRealm = parseReasonsTBuyRealm(dictSong: rea)
                VehicleModel.reasonsTBRealm.append( reasRealm)
            }
        }
        if let videos = dictSong["videos"] as? [[String:Any]] {
            
            for vid in videos
            {
                let videRealm = parseVideos(dictSong: vid)
                VehicleModel.allVideosRealm.append(videRealm)
            }
        }
        if let suitabilityQ = dictSong["suitabilityQuestionaire"] as? [[String:Any]] {
            
            for qus in suitabilityQ
            {
                let questionRealm = parseQuestionDate(dictSong: qus)
                VehicleModel.suitabilityQuestionaireRealm.append(questionRealm)
            }
        }
        
        if let improvement = dictSong["improvements"] as? [[String:Any]] {
            for improve in improvement
            {
                let improvement = parseImprovementsModel(dictSong: improve)
                VehicleModel.improvements.append(improvement)
            }
        }
        if let dicttips = dictSong["tips"] as? [[String:Any]] {
            var arr = [String]()
            for i in 0..<dicttips.count
            {
                let dict = dicttips[i]
                let strTitle = dict["tip_location"] as! String
                if !arr.contains(strTitle)
                {
                    arr.append(strTitle)
                }
            }
            var tempArr = [[String:Any]]()
            
            for tip_location in arr
            {
                let locationArr = dicttips.filter { ($0["tip_location"] as! String) == tip_location }
//                var tipsArr = [TipsRealm]()
                var tipsArr = [[String:Any]]()
                for dict in locationArr {
//                    let tips = parseTipsDate(dictSong: dict)
                    tipsArr.append(dict)
                    //print("jikuiduirsdu \(dict)")
                }
                if tip_location.hasPrefix("Front")
                {
                    let dict:[String : Any] = ["title":"Front of Vehicle", "data":tipsArr]
                    tempArr.append(dict)
                }else if tip_location.hasPrefix("Rear")
                {
                    let dict:[String : Any] = ["title":"Rear of Vehicle", "data":tipsArr]
                    tempArr.append(dict)
                }else if tip_location.hasPrefix("Driver Side")
                {
                    let dict:[String : Any] = ["title":"Driver Side", "data":tipsArr]
                    tempArr.append(dict)
                }else if tip_location.hasPrefix("Passenger")
                {
                    let dict:[String : Any] = ["title":"Passenger Side", "data":tipsArr]
                   tempArr.append(dict)
                }else if tip_location.hasPrefix("Interior")
                {
                    let dict:[String : Any] = ["title":"Interior", "data":tipsArr]
                    tempArr.append(dict)
                    
                }else{
                    let dict:[String : Any] = ["title":tip_location, "data":tipsArr]
                    tempArr.append(dict)
                }
           }
            
        if let data = try? JSONSerialization.data(withJSONObject: tempArr, options: .prettyPrinted),
            let str = String(data: data, encoding: .utf8) {
            VehicleModel.strTips = str
            UserDefaults.standard.set(str, forKey: VehicleModel.id)
            UserDefaults.standard.synchronize()
        }

        }
        
        if let specification = dictSong["specifications"] as? [[String:Any]] {
            for specific in specification
            {
                let specification = parseSpecification(dictSong: specific)
                VehicleModel.specifications.append(specification)
            }
        }
        if let subCategoryI = dictSong["subCategoryId"] as? String {
            VehicleModel.subCategoryId = subCategoryI
        }
        VehicleModel.isSelected = false
        return VehicleModel
    }
    
    func parseSpecification(dictSong:[String : Any]) -> SpecificationsRealm {
        let  SpecifiModel = SpecificationsRealm()
        if let attrId = dictSong["attrId"] as? String {
            SpecifiModel.attrId = attrId
        }
        if let title = dictSong["title"] as? [String: String] {
            SpecifiModel.title = title["en"]!
            SpecifiModel.title_fr = title["fr"]!
        }
        if let val = dictSong["value"] as? [String: String] {
            let str_en = val["en"]!
            let str_fr = val["fr"]!
            SpecifiModel.val = str_en.replacingOccurrences(of: "<br />", with: "\n")
            SpecifiModel.val_fr = str_fr.replacingOccurrences(of: "<br />", with: "\n")
        }
        if let advantage = dictSong["advantage"] as? [String: String] {
            SpecifiModel.advantage = advantage["en"]!
            SpecifiModel.advantage_fr = advantage["fr"]!
        }
        return SpecifiModel
    }
    func parseVideos(dictSong:[String : Any]) -> VideosRealm {
        let VideosModel = VideosRealm()
        if let en = dictSong["en"] as? [String: Any] {
            VideosModel.title = (en["title"] as? String)!
            VideosModel.video_link = (en["video_link"] as? String)!
            VideosModel.type = (en["type"] as? String)!
            VideosModel.dscrion = (en["description"] as? String)!
        }
        if let fr = dictSong["fr"] as? [String: Any] {
            VideosModel.title_fr = (fr["title"] as? String)!
            VideosModel.video_link_fr = (fr["video_link"] as? String)!
            VideosModel.type_fr = (fr["type"] as? String)!
            VideosModel.description_fr = (fr["description"] as? String)!
        }
        return VideosModel
    }
    func parseCompetitorsDate(dictSong:[String : Any]) -> CompetitorsRealm {
        let CompetiModel = CompetitorsRealm()
        if let headerdd = dictSong["headerDescription"] as? String {
            CompetiModel.headerDescription = headerdd
        }
        if let modelYea = dictSong["modelYear"] as? String {
            CompetiModel.modelYear = modelYea
        }
        if let photoPat = dictSong["photoPath"] as? String {
            
            CompetiModel.photoPath = photoPat.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
        if let id = dictSong["vehicleId"]
        {
            if id is Int
            {
                CompetiModel.vehicleID = String(describing: id)
            }else if id is String
            {
                CompetiModel.vehicleID = id as! String
            }else{
                CompetiModel.vehicleID = String(describing: id)
            }
        }
        if CompetiModel.vehicleID.trim().isEmpty
        {
            if let id = dictSong["vehicleID"]
            {
                if id is Int
                {
                    CompetiModel.vehicleID = String(describing: id)
                }else if id is String
                {
                    CompetiModel.vehicleID = id as! String
                }else{
                    CompetiModel.vehicleID = String(describing: id)
                }
            }
        }
        return CompetiModel
    }
    func parseQuestionDate(dictSong:[String : Any]) -> QuestionRealm {
        let QuestionModel = QuestionRealm()
        if let en = dictSong["en"] as? [String: String] {
            QuestionModel.question = en["question"]!
            let str_answer = en["answer"]!
            QuestionModel.answer = str_answer.replacingOccurrences(of: "<br />", with: "\n")
        }
        if let fr = dictSong["fr"] as? [String: String] {
            QuestionModel.question_fr = fr["question"]!
            let str_answer = fr["answer"]!
            QuestionModel.answer_fr = str_answer.replacingOccurrences(of: "<br />", with: "\n")
        }
        return QuestionModel
    }
    func parseTipsDate(dictSong:[String : Any]) -> TipsRealm {
        let  TipsModel = TipsRealm()
        TipsModel.tip_location  = dictSong["tip_location"] as! String
        TipsModel.tip_category = dictSong["tip_category"] as! String
        TipsModel.tip_en  = dictSong["tip_en"] as! String
        TipsModel.tip_fr = dictSong["tip_fr"] as! String
        if let tip_en = dictSong["tip_en"] as? String {
            let str = tip_en.replacingOccurrences(of: "<sup>", with: "")
            TipsModel.tip_en = str.replacingOccurrences(of: "</sup>", with: "")
        }
        if let tip_fr = dictSong["tip_fr"] as? String {
            let str = tip_fr.replacingOccurrences(of: "<sup>", with: "")
            TipsModel.tip_fr = str.replacingOccurrences(of: "</sup>", with: "")
        }
        return TipsModel
    }
    func parseReasonsTBuyRealm(dictSong:[String : Any]) -> reasonsToBuyRealm {
        let reasss = reasonsToBuyRealm()
        reasss.en = dictSong["en"] as! String
        reasss.fr = dictSong["fr"] as! String
        return reasss
    }
    func parseImprovementsModel(dictSong:[String : Any]) -> improvements {
        let improve = improvements()
        improve.en = dictSong["en"] as! String
        improve.fr = dictSong["fr"] as! String
        return improve
    }
    func parseContactDate(dictSong:[String : Any]) -> ContactDataRealm {
        let  ContactModel = ContactDataRealm()
        if let contact = dictSong["contacts"] as? String {
            ContactModel.contacts = contact
        }
        if let notes = dictSong["notes"] as? String {
            ContactModel.notes = notes
        }
        if let id = dictSong["_id"] as? String {
            ContactModel.Id = id
        }
        if let tname = dictSong["firstName"] as? String {
            ContactModel.firstName = tname
        }
        if let lname = dictSong["lastName"] as? String {
            ContactModel.lastName = lname
        }
        if let phonecode = dictSong["telephone"] as? Int {
            ContactModel.telephone = phonecode
        }
        if let createdA = dictSong["createdAt"] as? String {
            ContactModel.createdAt = createdA
        }
        if let mail = dictSong["email"] as? String {
            ContactModel.email = mail
        }
        if let dealerI = dictSong["dealerId"] as? String {
            ContactModel.dealerId = dealerI
        }
        if let vehicleM = dictSong["vehicleModel"] as? String {
            ContactModel.vehicleModel = vehicleM
        }
        if let vehicleT = dictSong["vehicleTitle"] as? String {
            ContactModel.vehicleTitle = vehicleT
        }
        ContactModel.isSelected = false
        return ContactModel
    }
}
//TODO: online database
class DealerData {
    var dealerData = [String:Any]()
    var _id = ""
    var dealerName = ""
    var address = ""
    var dealerCode = ""
    var city = ""
    var email = ""
    var postalCode = ""
    var pro = ""
    var telephone = ""
    var website = ""
    var zoneid:Int64 = 0
    public init (info:[String:Any]) {
        self.dealerData = info
        if let _id = info["_id"] as? String {
            self._id = _id
        }
        if let dealerName = info["dealerName"] as? String {
            self.dealerName = dealerName
        }
        if let address = info["address"] as? String {
            self.address = address
        }
        if let dealerc = info["dealerCode"] as? String {
            self.dealerCode = dealerc
        }
        if let email = info["email"] as? String {
            self.email = email
        }
        if let cit = info["city"] as? String {
            self.city = cit
        }
        
        if let postalCode = info["postalCode"] as? String {
            self.postalCode = postalCode
        }
        if let pro = info["pro"] as? String {
            self.pro = pro
        }
        if let telephon = info["telephone"] as? String {
            self.telephone = telephon
        }
        if let websit = info["website"] as? String {
            self.website = websit
        }
    }
}
class SettingData {
    var id = ""
    var title = ""
    var selectedImage = ""
    var isSelected = false
    public init (){}
    
    public init (info:NSDictionary) {
        self.selectedImage = "checked-icon.png"
        self.isSelected = false
    }
}
class Makes {
    var makes = [String:Any]()
    var isSelected = false
    
    public init (){}
    
    public init (info:[String : Any]) {
        if let make = info["makes"] as? [String:Any] {
            self.makes = make
        }
    }
}
class VehicleModelData {
    var name = ""
    var years = [String]()
    var isSelected = false
    
    public init (){}
    public init (info:[String : Any]) {
        if let name = info["name"] as? String {
            self.name = name
        }
        if let year = info["years"] as? [String] {
            self.years = year
        }
    }
}
class SelectTrim {
    var title = ""
    var vehicleModel = [Competitors]()
    var vehicleData = [[String : Any]]()
    var isSelected = false
    
    public init (){}
    public init (trim:String, vehicleArray: [[String : Any]]) {
        title = trim
        var arr = [Competitors]()
        for dict in vehicleArray {
            if dict["trimName"] as! String == title
            {
                let compe = Competitors(info: dict)
                arr.append(compe)
                vehicleData.append(dict)
            }
        }
        self.vehicleModel = arr
    }
}
class HomeCategories {
    var categories = [String:Any]()
    var id = ""
    var title = ""
    var title_fr = ""
    var thumbImage = ""
    var photoUrl = ""
    var sortIndex = 0
    var dateCreated = ""
    var isSelected = false
    public init (){}
    public init (info:[String : Any] , _ photoURL:String? ) {
        if let categor = info["categories"] as? [String:Any] {
            self.categories = categor
        }
        if let _id = info["_id"] as? String {
            self.id = _id
        }
        if let dictTitle = info["title"] as? [String:Any] {
            self.title = (dictTitle["en"] as? String)!
            self.title_fr = (dictTitle["fr"] as? String)!
        }
        //Config.PhotoUrl + (info["thumb"])
        if let thumbI = info["thumb"] as? String {
            self.thumbImage  = photoURL! + "/" + thumbI
           // print("++photoUrl++thumb++  \(thumbImage)")
        }
        if let sortIn = info["sortIndex"] as? Int {
            self.sortIndex = sortIn
        }
        if let dateCreat = info["dateCreated"] as? String {
            self.dateCreated = dateCreat
        }
        self.isSelected = false
    }
}
class SubCategoriesData {
    var subCategories = [String:Any]()
    var id = ""
    var title = ""
    var title_fr = ""
    var thumbImage = ""
    var sortIndex = 0
    var dateCreated = ""
    var parent_id = ""
    var isSelected = false
        public init (){}
        public init (info:[String : Any]) {
        if let subCat = info["subCategories"] as? [String:Any] {
            self.subCategories = subCat
        }
        if let _id = info["_id"] as? String {
            self.id = _id
        }
        if let dictTitle = info["title"] as? [String:Any] {
            self.title = (dictTitle["en"] as? String)!
            self.title_fr = (dictTitle["fr"] as? String)!
        }
        if let thumbI = info["thumb"] as? String {
            self.thumbImage  = thumbI
        }
        if let sortIn = info["sortIndex"] as? Int {
            self.sortIndex = sortIn
        }
        if let dateCreat = info["dateCreated"] as? String {
            self.dateCreated = dateCreat
        }
        if let pareid = info["parent_id"] as? String {
            self.parent_id = pareid
        }
        self.isSelected = false
    }
}
class vehiclesData {
    var vehicles = [String:Any]()
    var exploreArr = [String:Any]()
    var id = ""
    var title = ""
    var base_price = ""
    var title_fr = ""
    var dateCreated = ""
    var categoryId = ""
    var model = ""
    var vehicle_id = ""
    var photo_path = ""
    var competitors = ""
    var thumbImage = ""
    var slides = ""
    var pullQuote = [String:Any]()
    var brochureUrl = [String:Any]()
    var competitorId = ""
    var reasonsToBuy = ""
    var videos = [Any]()
    var suitabilityQuestionaire = [Any]()
    var tips = [String:Any]()
    var improvements = [String:Any]()
    var specifications = [String:Any]()
    var subCategoryId = ""
    var photoUrl = ""
    var isSelected = false
    public init (){}
    public init (info:[String : Any] , _ photoURL:String?) {
        
        if let vehic = info["vehicles"] as? [String:Any] {
            self.vehicles = vehic
        }
        if let thumbI = info["thumb"] as? String {
            self.thumbImage  = photoURL! + "/" + thumbI
            //print("++photoUrl++thumb++  \(thumbImage)")
        }
        if let _id = info["_id"] as? String {
            self.id = _id
        }
        if let pic = info["base_price"] as? String {
            self.base_price = pic
        }
        if let dateCreat = info["dateCreated"] as? String {
            self.dateCreated = dateCreat
        }
        if let dictTitle = info["title"] as? [String:Any] {
            self.title = (dictTitle["en"] as? String)!
            self.title_fr = (dictTitle["fr"] as? String)!
        }
        if let categoryI = info["categoryId"] as? String {
            self.categoryId = categoryI
        }
        if let mode = info["model"] as? String {
            self.model = mode
        }
        if let vehicle = info["vehicle_id"] as? String {
            self.vehicle_id = vehicle
        }
        if let photo_p = info["photo_path"] as? String {
            self.photo_path = photo_p
        }
        if let competitor = info["competitors"] as? String {
            self.competitors  = competitor
        }
        
        if let slide = info["slides"] as? String {
            self.slides = slide
        }
        if let pullQuot = info["pullQuote"] as? [String:Any] {
            self.pullQuote = pullQuot
        }
        if let brochureU = info["brochureUrl"] as? [String:Any] {
            self.brochureUrl = brochureU
        }
        if let competId = info["competitorId"] as? String {
            self.competitorId  = competId
        }
        if let reasonsToB = info["reasonsToBuy"] as? String {
            self.reasonsToBuy = reasonsToB
        }
        
        if let video = info["videos"] as? [Any] {
            self.videos = video
        }
        if let suitabilityQ = info["suitabilityQuestionaire"] as? [Any] {
            self.suitabilityQuestionaire = suitabilityQ
        }
        if let tip = info["tips"] as? [String:Any] {
            self.tips = tip
        }
        if let improvement = info["improvements"] as? [String:Any] {
            self.improvements = improvement
        }
        if let specification = info["specifications"] as? [String:Any] {
            self.specifications = specification
        }
        if let subCategoryI = info["subCategoryId"] as? String {
            self.subCategoryId = subCategoryI
        }
        self.isSelected = false
    }
}
class VehicleData {
    var vehicle = [String:Any]()
    var exploreArr = [String:Any]()
    var id = ""
    var title = ""
    var title_fr = ""
    var categoryId = ""
    var model = ""
    var vehicle_id = ""
    var photo_path = ""
    var competitors = [Competitors]()
    var thumbImage = ""
    var slides = [String]()
    var pullQuote = ""
    var pullQuote_fr = ""
    var brochureUrl = [String:Any]()
    var competitorId = ""
    var reasonsToBuy = [[String:Any]]()
    var allVideos = [Videos]()
    var suitabilityQuestionaire = [Question]()
    var tips = [[String:Any]]()
    var improvements = [[String:Any]]()
    var specifications = [[String:Any]]()
    var subCategoryId = ""
    
    var photoUrl = "https://s3.amazonaws.com/mitsubishiadv/images"
    var isSelected = false
    public init (){}
    
    public init (info:[String : Any] , _ photoURL:String?) {
        if let slide = info["slides"] as? [String] {
            for img in slide
            {
                slides.append(photoURL! + "/" + img)
            }
        }
        if let thumbI = info["thumb"] as? String {
            self.thumbImage  = photoURL! + "/" + thumbI
            //print("++photoUrl++thumb++  \(thumbImage)")
        }
        
        if let vehic = info["vehicles"] as? [String:Any] {
            self.vehicle = vehic
        }
        if let _id = info["_id"] as? String {
            self.id = _id
        }
        if let dictTitle = info["title"] as? [String:Any] {
            self.title = (dictTitle["en"] as? String)!
            self.title_fr = (dictTitle["fr"] as? String)!
        }
        if let categoryI = info["categoryId"] as? String {
            self.categoryId = categoryI
        }
        if let mode = info["model"] as? String {
            self.model = mode
        }
        if let vehicle = info["vehicle_id"] as? String {
            self.vehicle_id = vehicle
        }
        if let photo_p = info["photo_path"] as? String {
            self.photo_path = photo_p
        }
        if let compet = info["competitors"] as? [[String:Any]] {
            
            for compe in compet
            {
                let competit = Competitors(info: compe)
                self.competitors.append(competit)
            }
        }
        if let pullQuot = info["pullQuote"] as? [String:Any] {
            self.pullQuote = (pullQuot["en"] as? String)!
            self.pullQuote_fr = (pullQuot["fr"] as? String)!
        }
        if let brochureU = info["brochureUrl"] as? [String:Any] {
            self.brochureUrl = brochureU
        }
        if let competId = info["competitorId"] as? String {
            self.competitorId  = competId
        }
        if let reasonsToBuy = info["reasonsToBuy"] as? [[String:Any]] {
            self.reasonsToBuy = reasonsToBuy
        }
        if let videos = info["videos"] as? [[String:Any]] {
            
            for vid in videos
            {
                let Vide = Videos(info: vid)
                self.allVideos.append(Vide)
            }
        }
        if let suitabilityQ = info["suitabilityQuestionaire"] as? [[String:Any]] {
            
            for qus in suitabilityQ
            {
                let question = Question(info: qus)
                self.suitabilityQuestionaire.append(question)
            }
        }
        
        if let dicttips = info["tips"] as? [[String:Any]] {
            var arr = [String]()
            for i in 0..<dicttips.count
            {
                let dict = dicttips[i]
                let strTitle = dict["tip_location"] as! String
                if !arr.contains(strTitle)
                {
                    arr.append(strTitle)
                }
            }
            for tip_location in arr
            {
                let locationArr = dicttips.filter { ($0["tip_location"] as! String) == tip_location }
                var tipsArr = [Tips]()
                for dict in locationArr {
                    let tips = Tips(info: dict)
                    tipsArr.append(tips)
                }
                if tip_location.hasPrefix("Front")
                {
                    let dict:[String : Any] = ["title":"Front of Vehicle", "data":tipsArr]
                    self.tips.append(dict)
                }else if tip_location.hasPrefix("Rear")
                {
                    let dict:[String : Any] = ["title":"Rear of Vehicle", "data":tipsArr]
                    self.tips.append(dict)
                }else if tip_location.hasPrefix("Driver Side")
                {
                    let dict:[String : Any] = ["title":"Driver Side", "data":tipsArr]
                    self.tips.append(dict)
                }else if tip_location.hasPrefix("Passenger")
                {
                    let dict:[String : Any] = ["title":"Passenger Side", "data":tipsArr]
                    self.tips.append(dict)
                }else if tip_location.hasPrefix("Interior")
                {
                    let dict:[String : Any] = ["title":"Interior", "data":tipsArr]
                    self.tips.append(dict)
                }else{
                    let dict:[String : Any] = ["title":tip_location, "data":tipsArr]
                    self.tips.append(dict)
                }
            }
        }
        if let improvement = info["improvements"] as? [[String:Any]] {
            self.improvements = improvement
        }
        
        if let specification = info["specifications"] as? [[String:Any]] {
            self.specifications = specification
        }
        if let subCategoryI = info["subCategoryId"] as? String {
            self.subCategoryId = subCategoryI
        }
        self.isSelected = false
    }
}
class Specifications {
    var attrId = ""
    var val = ""
    var val_fr = ""
    var advantage = ""
    var advantage_fr = ""
    var isSelected = false
    var title = ""
    var title_fr = ""
    public init (){}
    public init (info:[String : Any]) {
        if let attrId = info["attrId"] as? String {
            self.attrId = attrId
        }
        if let title = info["title"] as? [String: String] {
            self.title = title["en"]!
            self.title_fr = title["fr"]!
        }
        if let val = info["value"] as? [String: String] {
            let str_en = val["en"]!
            let str_fr = val["fr"]!
            self.val = str_en.replacingOccurrences(of: "<br />", with: "\n")
            self.val_fr = str_fr.replacingOccurrences(of: "<br />", with: "\n")
        }
        if let advantage = info["advantage"] as? [String: String] {
            self.advantage = advantage["en"]!
            self.advantage_fr = advantage["fr"]!
        }
    }
}
class Question {
    var question = ""
    var question_fr = ""
    var answer = ""
    var answer_fr = ""
    var isSelected = false
    public init (){}
    public init (info:[String : Any]) {
        
        if let en = info["en"] as? [String: String] {
            question = en["question"]!
            let str_answer = en["answer"]!
            self.answer = str_answer.replacingOccurrences(of: "<br />", with: "\n")
        }
        if let fr = info["fr"] as? [String: String] {
            question_fr = fr["question"]!
            let str_answer = fr["answer"]!
            self.answer_fr = str_answer.replacingOccurrences(of: "<br />", with: "\n")
        }
    }
}
class Competitors {
    var headerDescription = ""
    var modelYear = ""
    var photoPath = ""
    var vehicleID = ""
    var isSelected = false
    public init (){}
    public init (info:[String : Any]) {
        
        if let headerdd = info["headerDescription"] as? String {
            self.headerDescription = headerdd
        }
        
        if let modelYea = info["modelYear"] as? String {
            self.modelYear = modelYea
        }
        if let photoPat = info["photoPath"] as? String {
            
            self.photoPath = photoPat.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
        if let id = info["vehicleId"]
        {
            if id is Int
            {
                self.vehicleID = String(describing: id)
            }else if id is String
            {
                self.vehicleID = id as! String
            }else{
                self.vehicleID = String(describing: id)
            }
        }
        if self.vehicleID.trim().isEmpty
        {
            if let id = info["vehicleID"]
            {
                if id is Int
                {
                    self.vehicleID = String(describing: id)
                }else if id is String
                {
                    self.vehicleID = id as! String
                }else{
                    self.vehicleID = String(describing: id)
                }
            }
        }
    }
}

class Videos {
    var title = ""
    var title_fr = ""
    var video_link = ""
    var video_link_fr = ""
    var type = ""
    var type_fr = ""
    var description = ""
    var description_fr = ""
    var preview_image = ""
    var preview_image_fr = ""
    var isSelected = false
    public init (){}
    public init (info:[String : Any]) {
        if let en = info["en"] as? [String: Any] {
            title = (en["title"] as? String)!
            video_link = (en["video_link"] as? String)!
            type = (en["type"] as? String)!
            description = (en["description"] as? String)!
        }
        if let fr = info["fr"] as? [String: Any] {
            title_fr = (fr["title"] as? String)!
            video_link_fr = (fr["video_link"] as? String)!
            type_fr = (fr["type"] as? String)!
            description_fr = (fr["description"] as? String)!
        }
    }
}
class Tips {
    var tip_location = ""
    var tip_category = ""
    var tip_en = ""
    var tip_fr = ""
    var isSelected = false
    public init (){}
    public init (info:[String : Any]) {
        if let tip_location = info["tip_location"] as? String {
            self.tip_location = tip_location
        }
        if let tip_category = info["tip_category"] as? String {
            self.tip_category = tip_category
        }
        if let tip_en = info["tip_en"] as? String {
            //self.tip_en = tip_en
            let str = tip_en.replacingOccurrences(of: "<sup>", with: "")
            self.tip_en = str.replacingOccurrences(of: "</sup>", with: "")
        }
        if let tip_fr = info["tip_fr"] as? String {
            let str = tip_fr.replacingOccurrences(of: "<sup>", with: "")
            self.tip_fr = str.replacingOccurrences(of: "</sup>", with: "")
            // self.tip_fr = tip_fr
            
        }
    }
}
class ContactData {
    var contacts = ""
    var Id = ""
    var firstName = ""
    var lastName = ""
    var email = ""
    var notes = ""
    var telephone = 0
    var dealerId = ""
    var createdAt = ""
    var vehicleModel = ""
    var vehicleTitle = ""
    var isSelected = false
    
    public init () {}
    public init (info:[String : Any]) {
        
        if let contact = info["contacts"] as? String {
            self.contacts = contact
        }
        if let notes = info["notes"] as? String {
            self.notes = notes
        }
        if let id = info["_id"] as? String {
            self.Id = id
        }
        if let tname = info["firstName"] as? String {
            self.firstName = tname
        }
        if let lname = info["lastName"] as? String {
            self.lastName = lname
        }
        if let phonecode = info["telephone"] as? Int {
            self.telephone = phonecode
        }
        if let createdA = info["createdAt"] as? String {
            self.createdAt = createdA
        }
        if let mail = info["email"] as? String {
            self.email = mail
        }
        if let dealerI = info["dealerId"] as? String {
            self.dealerId = dealerI
        }
        if let vehicleM = info["vehicleModel"] as? String {
            self.vehicleModel = vehicleM
        }
        if let vehicleT = info["vehicleTitle"] as? String {
            self.vehicleTitle = vehicleT
        }
        //self.selectedImage = info["selectedImage"] as! String
        self.isSelected = false
    }
}


