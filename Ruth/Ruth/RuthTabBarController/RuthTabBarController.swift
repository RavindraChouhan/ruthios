//
//  RuthTabBarController.swift
//  Ruth
//
//  Created by mac on 10/05/18.
//  Copyright © 2018 Linkites. All rights reserved.
//

import UIKit

class RuthTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    private var btnMiddle: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpLayout()
    }
    
    func setUpLayout() {
        self.delegate = self;
        UITabBar.appearance().barStyle = .default;
        UITabBar.appearance().backgroundColor = UIColor.white;
//        UITabBar.appearance().tintColor = UIColor.white;
        self.selectedIndex = 0;
        
        let objHomeTab = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabbarFeed")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage.init(named: "TabbarFeed-S")?.withRenderingMode(.alwaysOriginal))
        objHomeTab.imageInsets = UIEdgeInsetsMake(8, 0, -8, 0);
        objHomeTab.tag = 0;
        
        let objFollowTab = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabbarProfile")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage.init(named: "TabbarProfile-S")?.withRenderingMode(.alwaysOriginal));
        objFollowTab.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        objFollowTab.tag = 1;
        
        let objEmptyTab = UITabBarItem.init();
        objEmptyTab.isEnabled = false;
        objEmptyTab.tag = 2;
        
        let objEventsTab = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabbarAnalytic")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage.init(named: "TabbarAnalytic-S")?.withRenderingMode(.alwaysOriginal));
        objEventsTab.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        objEventsTab.tag = 3;
        
        let objExploreTab = UITabBarItem.init(title: nil, image: UIImage.init(named: "TabbarMore")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage.init(named: "TabbarMore-S")?.withRenderingMode(.alwaysOriginal));
        objExploreTab.imageInsets = UIEdgeInsetsMake(8, 0, -8, 0);
        objExploreTab.tag = 4;
        
        let objHomeController = HomeViewController(nibName: "HomeViewController", bundle: nil);
        objHomeController.tabBarItem = objHomeTab;
        let objNavHomeController = UINavigationController(rootViewController: objHomeController)
        objNavHomeController.isNavigationBarHidden = true;
        
        let objFollowController = HomeViewController(nibName: "HomeViewController", bundle: nil);
        objFollowController.tabBarItem = objFollowTab;
        let objNavFollowController = UINavigationController(rootViewController: objFollowController)
        objNavFollowController.isNavigationBarHidden = true;
        
        let objCaptureViewController = HomeViewController(nibName: "HomeViewController", bundle: nil);
        objCaptureViewController.tabBarItem = objEmptyTab;
        let objNavContentController = UINavigationController(rootViewController: objCaptureViewController);
        objNavContentController.isNavigationBarHidden = true;
        
        let objEventsController = HomeViewController(nibName: "HomeViewController", bundle: nil);
        objEventsController.tabBarItem = objEventsTab;
        let objNavEventsController = UINavigationController(rootViewController: objEventsController)
        objNavEventsController.isNavigationBarHidden = true;
        
        let objExploreController = HomeViewController(nibName: "HomeViewController", bundle: nil);
        objExploreController.tabBarItem = objExploreTab;
        let objNavExploreController = UINavigationController(rootViewController: objExploreController)
        objNavExploreController.isNavigationBarHidden = true;
        
        self.viewControllers = [objNavHomeController, objNavFollowController, objNavContentController, objNavEventsController, objNavExploreController];
    
        self.setupMiddleButton();
    }
    
    func setupMiddleButton()
    {
        let image  = UIImage(named: "TabbarAddd")
        self.btnMiddle = UIButton.init(type: .custom)
        self.btnMiddle.frame = CGRect(x: 0, y: 0, width: (image?.size.width)!, height: (image?.size.height)!);
        
        var centerFrame = self.btnMiddle.frame
        let tmpY = DeviceType.iPhoneX ? (centerFrame.height + 8) : (centerFrame.height - 8)
        centerFrame.origin.y = view.bounds.height - tmpY
        centerFrame.origin.x = view.bounds.width/2 - centerFrame.size.width/2
        self.btnMiddle.frame = centerFrame
        self.view.addSubview(self.btnMiddle)
        
        self.btnMiddle.setImage(image, for: .normal)
        self.btnMiddle.setImage(image, for: .highlighted)
        self.btnMiddle.addTarget(self, action: #selector(self.btnMiddlePressed(_:)), for: .touchUpInside)
        self.view.layoutIfNeeded()
    }
    
    @objc private func btnMiddlePressed(_ sender : UIButton)
    {
        print("TAB Button : 2")
    }
    
    func hideMoxeeTabBar()
    {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBar.alpha = 0.0;
            self.btnMiddle.alpha = 0.0;
        })
    }
    
    func showMoxeeTabBar()
    {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBar.alpha = 1.0;
            self.btnMiddle.alpha = 1.0;
        })
    }
    
    //MARK: UITabBarControllerDelegate methods
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("didSelect == \(tabBarController.selectedIndex)")
    }
}
