//
//  SearchViewC.swift
//  Ruth
//
//  Created by mac on 11/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class SearchViewC: UIViewController,FSPagerViewDelegate,FSPagerViewDataSource ,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var btnMenu : UIButton!
    @IBOutlet var btnFilter:UIButton!
    @IBOutlet var tblView : UITableView!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var pagerControl: FSPageControl!
    var refreshControl: UIRefreshControl!
    var arrBanner = [RuthStoryBanner]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let ab = RuthStoryBanner(imgTitlee: "searchmask.png", isSelectedd: true)
        arrBanner.append(ab)
        self.SearchRegisterCell()
        self.SearchFSPagerView()
        self.setupSearchPager()
        self.makeTransprentNavigationBar()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: true, animated: true)
        self.navigationController?.isNavigationBarHidden = false
    }
    func SearchRegisterCell()  {
        self.tblView.register(UINib(nibName: "SearchTCell",  bundle: nil)
            , forCellReuseIdentifier: "SearchTCell")
//        self.tblView.register(UINib(nibName: "HomeTblCell",  bundle: nil)
//            , forCellReuseIdentifier: "HomeTblCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    func SearchFSPagerView() {
        self.pagerView.register(UINib(nibName: "SearchFSPagerCell", bundle: nil), forCellWithReuseIdentifier: "SearchFSPagerCell")
        self.pagerView.isInfinite = true
        //        self.pagerView.automaticSlidingInterval = 3
        self.pagerControl.currentPage = self.arrBanner.count
       
        self.pagerControl.setFillColor(UIColor .hexStringToColor(hex: "00FF21"), for: UIControlState.selected)
    }
    func setupSearchPager() {
        self.pagerControl.numberOfPages = self.arrBanner.count
        pagerView.transformer = FSPagerViewTransformer(type: .linear)
        self.pagerView.interitemSpacing = 2.0 * 20
        let width = UIScreen.main.bounds.width-10
        self.pagerView.itemSize = CGSize(width: width, height: width)
        let transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.reloadData()
    }
    
    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {
        //        self.pagerControl.currentPage =
    }
    // MARK:- FSPagerViewDelegate, FSPagerViewDataSource METHOD ***** *** ***** **
    func numberOfItems(in pagerView:  FSPagerView) -> Int {
        return self.arrBanner.count
    }
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "SearchFSPagerCell", at: index) as! SearchFSPagerCell
        cell.imgPoster.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 5.0, scale: true)
      //  let banner = self.arrBanner[index]
        //cell.imgPoster.sd_setImage(with: URL(string: banner.imgTitle), placeholderImage: nil)
        self.pagerControl.currentPage = index
        return cell
        
    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
    //HottestCreepyPictures
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let Headerview:CustomTableHeader = Bundle.main.loadNibNamed("CustomTableHeader", owner: self, options: nil)?[0] as! CustomTableHeader
            Headerview.lblHeader.text = "Latest Stories".localized()
            return Headerview
        } else {
            let def = UIView()
            return def
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "SearchTCell", for: indexPath) as! SearchTCell
        cell.selectionStyle = .none
        cell.viewImgBG.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 7.0, scale: true)
        cell.lblFans.text = "6,860" + " 名粉絲"
//        cell.btnMore.addTarget(self, action: #selector(actionMore(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    @IBAction func actionRank(_ btn : UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
