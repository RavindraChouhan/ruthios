//
//  HomeCollCell.swift
//  Ruth
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class StickerCell: UICollectionViewCell {
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet var imgGold: UIImageView!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }
}



