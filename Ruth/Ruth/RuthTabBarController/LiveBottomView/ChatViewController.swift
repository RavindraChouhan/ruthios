//
//  ChatViewController.swift
//  LGFit
//
//  Created by Mac on 21/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import SocketIO

class ChatViewController: UIViewController {
    var navigationTitle : String?
    private let dateFormattor = DateFormatter()
    let manager = SocketManager(socketURL: URL(string: SocketUrl)!,config: [.log(true)])
    var socket:SocketIOClient!
    var messages = [String : Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        addCancelButtonLeft()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        title = navigationTitle ?? "Chat"
        self.setTabBarVisible(visible: false, animated: true)
        self.socket = manager.defaultSocket
        self.setSocketEvents()
        self.socket.connect()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func addCancelButtonLeft() {
        let button = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_Back"), style: .plain, target: self, action: #selector(popController))
        button.tintColor = UIColor.black
        navigationItem.leftBarButtonItem = button
    }
    
    @objc func popController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func emitComment() {
        let jsonObject = ["type" : "comment", "broadcastId" : "", "userId" :UserNew.currentUser.id, "message" : ""] as [String : Any]
        var arr = [[String:Any]]()
        arr.removeAll()
        arr.append(jsonObject)
        self.socket.emit("RECEIVE_MESSAGE", with: arr)
    }

    private func setSocketEvents()
    {
        
        self.socket.on(clientEvent: .connect) {data, ack in
            print("socket connected");
        }
        
        self.socket.on("COMMENTS") { (data, ack) in
            let dataArray = data as NSArray
            let recievedData = dataArray[0] as! NSArray
            let recievedObject = recievedData[0] as! NSDictionary
//            let messageId = recievedObject["_id"] as! String
//            let senderInfo = recievedObject["author"] as! [String:Any]
//            let message = recievedObject["body"] as! String
//            let senderId = senderInfo["_id"] as! Str1ing
            
//                        let model = LocalConversationsModel()
//                        model.isDelivered = messages.isDelivered
//                        model.isRead = messages.markRead
//                        model.messageDate = messages.messageDate
//                        self.RecivedMessagesArr.append(model)

        }
        
        self.socket.on("LIKES") { (data, ack) in
        let paramsters: [String:Any] = ["type":"comment" ,"broadcastId":"" ,"userId":"" ,"message":""]

        }
        
        self.socket.on("STICKER") { (data, ack) in

        }
    }
    
    @IBAction func actionArrowBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
