//
//  CommentTCell.swift
//  Ruth
//
//  Created by mac on 29/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit


class CommentTCell: UITableViewCell {
    
    @IBOutlet var imgProfile:UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    var comment: LocalConversationsModel! {
        didSet {
            updateUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgProfile.setCornerRadius(10)
//        self.lblComment.layer.cornerRadius = self.lblComment.frame.height/2
//        self.lblComment.layer.masksToBounds = true
//        self.lblComment.backgroundColor = UIColor.lightGray
//        self.lblName.layer.cornerRadius = self.lblName.frame.height/2
//        self.lblName.layer.masksToBounds = true
//        self.lblName.backgroundColor = UIColor.lightGray
        // Initialization code
    }
    func updateUI() {
        self.lblComment.text = comment.message
        self.lblName.text = comment.username
        imgProfile.sd_setImage(with: URL(string:comment.photoUrl), placeholderImage:#imageLiteral(resourceName: "emoji1"))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
class CustomOrderTCell: UITableViewCell {
    @IBOutlet var imgProfile:UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
}
