//
//  CPConfettiView.swift
//  CPConfettiViewExample
//
//  Created by framgia on 3/3/17.
//  Copyright © 2017 Vo Nguyen Chi Phuong. All rights reserved.
//

import Foundation

import UIKit
import QuartzCore

public enum CPConfettiDirection {
    case Top
    case Bottom
}

public class CPConfettiView: UIView {

    var emitter: CAEmitterLayer = CAEmitterLayer()
    public var intensity: Float!
    private var active :Bool!
    private var image: UIImage?
    var direction: CPConfettiDirection = .Top
    
    open var amplitudeRange = 3
    open var amplitude = 12
    open var duration: CFTimeInterval = 4
    open var durationRange: CFTimeInterval = 1
    open var maximumCount = 100
    var currentCount = 0
    var unusedLayers: [CALayer] = []
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    func setup() {
//        intensity = 0.5
//        direction = .Top
//        active = false
    }

    // Duration is time active anitmation.Default is 0 -> It won't stop
    public func startConfetti(duration: TimeInterval = 0) {
        guard let _ = image else {
            return
        }
        let x = frame.size.width / 2
        let y = direction == .Top ? 0 : frame.size.height
        emitter.emitterPosition = CGPoint(x: x, y: y)
        emitter.emitterShape = kCAEmitterLayerLine
        emitter.emitterSize = CGSize(width: frame.size.width, height: 1)
        emitter.birthRate = 1
        emitter.emitterCells = [confettiWithColor()]

        layer.addSublayer(emitter)
        active = true
        if duration != 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: {
                self.stopConfetti()
            })
        }
    }

    public func stopConfetti() {
        emitter.birthRate = 0
        active = false
    }

    public func setImageForConfetti(image: UIImage) {
        self.image = image
    }

    func confettiWithColor() -> CAEmitterCell {
        let confetti = CAEmitterCell()
        confetti.birthRate = 1 * intensity
        confetti.lifetime = 1
        confetti.velocity = CGFloat(350 * intensity)
        confetti.velocityRange = CGFloat(80.0 * intensity)
        confetti.emissionLongitude = direction == .Top ? CGFloat(M_PI) : CGFloat(0)
        confetti.emissionRange = CGFloat(M_PI / 12)
        confetti.scale = 1
        confetti.scaleRange = 0.5
        confetti.contents = image!.cgImage
        return confetti
    }
    
    public func isActive() -> Bool {
        return self.active
    }
    func getPathInRect(_ rect: CGRect) -> UIBezierPath {
        let centerX = rect.midX;
        let height = rect.height;
        let path = UIBezierPath();
        let offset = Float(arc4random() % 1000);
        let finalAmplitude = amplitude + Int(arc4random()) % amplitudeRange * 2 - amplitudeRange;
        var delta = CGFloat(0);
        var y = height
        while y >= 0  {
            let x = Float(finalAmplitude) * sinf((Float(y) + offset) * Float(M_PI) / 180);
            if y == height {
                delta = CGFloat(x)
                path.move(to: CGPoint(x:centerX, y: y))
            } else {
                path.addLine(to: CGPoint(x:CGFloat(x) + centerX - delta, y: y))
            }
            y = y - 1
        }
        return path
    }
    open func emitImage(_ image: UIImage) {
        guard currentCount < maximumCount else {
            return
        }
        currentCount = currentCount + 1
        
        let height = bounds.height
        let percent = Double(arc4random() % 100) / 100.0
        let duration = self.duration + percent * durationRange * 2 - durationRange
        var layer: CALayer
        if unusedLayers.count > 0 {
            layer = unusedLayers.last!
            unusedLayers.removeLast()
        } else {
            layer = CALayer();
        }
        layer.contents = image.cgImage
        layer.opacity = 1;
        layer.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        layer.position = CGPoint(x: self.bounds.midX, y: height)
        self.layer.addSublayer(layer)
        
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            layer.removeFromSuperlayer()
            self.unusedLayers.append(layer)
            self.currentCount = self.currentCount - 1
        }
        
        let position = CAKeyframeAnimation(keyPath: "position")
        position.path = getPathInRect(bounds).cgPath
        position.duration = duration
        layer.add(position, forKey: "position")
        
        let delay = duration / 2;
        let opacity = CABasicAnimation(keyPath: "opacity")
        opacity.fromValue = 1
        opacity.toValue = 0
        opacity.beginTime = CACurrentMediaTime() + delay
        opacity.isRemovedOnCompletion = false
        opacity.fillMode = kCAFillModeForwards
        opacity.duration = duration - delay - 0.1
        layer.add(opacity, forKey: "opacity")
        
        CATransaction.commit()
    }
}
