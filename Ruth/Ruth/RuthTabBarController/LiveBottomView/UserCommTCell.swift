//
//  UserCommTCell.swift
//  Ruth
//
//  Created by mac on 11/10/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class UserCommTCell: UITableViewCell {
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet var ImgComm: UIImageView!
    @IBOutlet var btnComm: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblComment.layer.borderColor = UIColor.hexStringToColor(hex: "#FF6E00").cgColor
        self.lblComment.layer.borderWidth = 3
        self.lblComment.layer.cornerRadius = self.lblComment.frame.height/2
        self.lblComment.layer.masksToBounds = true
        self.ImgComm.layer.cornerRadius = self.ImgComm.frame.height/2
        self.ImgComm.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
