//
//  ShareBrotcastTC.swift
//  Ruth
//
//  Created by mac on 21/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ShareBrotcastTC: UITableViewCell {

    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var btnDonu: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDetil: UILabel!
    @IBOutlet weak var imgName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var btnWechat: UIButton!
    @IBOutlet weak var btnFb: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
     @IBOutlet weak var viewtxt: UIView!
    
//    @IBOutlet weak var imgBg: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewtxt.setCornerRadius(5)
        self.txtView.setCornerRadius(5)
        self.img.setCornerRadius(5)
        self.viewtxt.layer.masksToBounds = true
        self.viewtxt.layer.borderColor = UIColor.gray.cgColor
        self.viewtxt.layer.borderWidth = 2
        self.img.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 5.0, scale: true)
        //帖子類別 ,影片,標題,新增 #標籤 在您的帖子,請輸入您的帖子描述......,上傳影片,分享
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
