//
//  ShareBrotcastVC.swift
//  Ruth
//
//  Created by mac on 21/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ShareBrotcastVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet var btnShare: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var tblView: UITableView!
     @IBOutlet weak var pickerView: UIPickerView!
    
    let fruits = ["- 影片 -", "- 圖片 -","- 文章 -","- 錄音 -","- 其他 -"]
    override func viewDidLoad() {
        super.viewDidLoad()
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(picker)
        self.makeTransprentNavigationBar()
        self.setupNavigationBar()
        self.navigationItem.title = "分享"
        self.RegisterCell()
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        self.addRightbuttomWithController(withCustomView: self.btnShare)
//ShareBrotcastTC
        // Do any additional setup after loading the view.
        
        picker.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        picker.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        picker.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    func RegisterCell(){
        self.setTabBarVisible(visible: false, animated: true)
        self.tblView.register(UINib(nibName: "ShareBrotcastTC",  bundle: nil)
            , forCellReuseIdentifier: "ShareBrotcastTC")
        self.tblView.delegate = self
        self.tblView.dataSource = self
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return fruits.count
        }
        
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if component == 0 {
                return 10
            } else {
                return 100
            }
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            if component == 0 {
                return "First \(row)"
            } else {
                return "Second \(row)"
            }
        }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSave(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 671
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "ShareBrotcastTC", for: indexPath) as! ShareBrotcastTC
        cell.selectionStyle = .none
      //  cell.comment = self.RecivedMessagesArr[indexPath.row]
        //        cell.lblName.text =  data.username
        //        cell.comment = comments[(indexPath as NSIndexPath).row]
        //        cell.lblComment.text = data.message
        return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
