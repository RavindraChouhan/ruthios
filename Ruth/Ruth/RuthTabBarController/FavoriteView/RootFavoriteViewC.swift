//
//  RootFavoriteViewC.swift
//  Ruth
//
//  Created by mac on 07/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import CarbonKit

class RootFavoriteViewC: UIViewController,CarbonTabSwipeNavigationDelegate {
    @IBOutlet var btnFilter: UIButton!
    var categoryNames = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
//        self.navigationItem.title = "Favorite".localized()
        self.categoryNames = ["用戶","直播","影片","錄音","圖片","文章"]
        self.setupTopBarAndNavigation()
        self.makeTransprentNavigationBar()
        self.addRightbuttomWithController(withCustomView: btnFilter)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
     func setupTopBarAndNavigation() {
    //         tabSwipe.setupCarbonPages(carbonTabSwipeNavigation, tabTitles: menuTabTitles, totalWidth: self.view.frame.width).insertIntoRootViewController(self)
    let tabSwipe = CarbonTabSwipeNavigation(items: self.categoryNames,delegate: self)
    tabSwipe.setNormalColor(UIColor.black, font: UIFont(name: ".SFUIText-Medium", size: 13)!)
    tabSwipe.setSelectedColor(UIColor.white, font: UIFont(name: ".SFUIText-Medium", size: 13)!)
    tabSwipe.carbonTabSwipeScrollView.backgroundColor = .green//UIColor.hexStr(hexStr: "f0f0f0")
    
    tabSwipe.setTabExtraWidth(20)
    tabSwipe.setTabBarHeight(26)
    tabSwipe.setIndicatorColor(UIColor.white)
    tabSwipe.pagesScrollView?.isScrollEnabled = true
    tabSwipe.carbonSegmentedControl?.backgroundColor = .green
    tabSwipe.insert(intoRootViewController: self)
}
func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
    let stroryBord = UIStoryboard(name: "Main", bundle: nil)
    let VC = stroryBord.instantiateViewController(withIdentifier: "FavoriteViewC") as! FavoriteViewC
    return VC
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
