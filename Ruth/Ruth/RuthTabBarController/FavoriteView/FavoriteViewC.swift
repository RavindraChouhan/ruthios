//
//  FavoriteViewC.swift
//  Ruth
//
//  Created by mac on 11/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class FavoriteViewC: UIViewController,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView : UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(UINib.init(nibName: "FavoriteCCell", bundle: nil), forCellWithReuseIdentifier: "FavoriteCCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    @objc func actionFollow(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
        if button.isSelected {
            button.setImage(#imageLiteral(resourceName: "btncheck"), for: UIControlState.normal)
            button.setTitle("", for: UIControlState.normal)
            button.backgroundColor = .white
            button.alpha = 1.0
        }else{
            //button.setImage(UIImage(named : "Unfollow"), for: UIControlState.normal)
            button.setImage(UIImage(), for: UIControlState.normal)
            button.setTitle("關注", for: UIControlState.normal)
            button.backgroundColor = .black
            button.alpha = 0.7
            //
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 14
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteCCell", for: indexPath) as! FavoriteCCell
        cell.btnFollow.addTarget(self, action: #selector(actionFollow(sender:)), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(UIScreen.main.bounds.width - 80)/3, height: 120)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
