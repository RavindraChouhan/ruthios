//
//  FavoriteCCell.swift
//  Ruth
//
//  Created by mac on 07/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class FavoriteCCell: UICollectionViewCell {

    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
