//
//  HorrorViewController.swift
//  Ruth
//
//  Created by mac on 13/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class HorrorViewController: UIViewController {
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var viewHorr : UIView!
    @IBOutlet var viewVip : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

          self.viewHorr.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 0.45, offSet: CGSize(width: 1, height: 1), radius: 15, scale: true)
        // Do any additional setup after loading the view.
    }
    @IBAction func actionCloseView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
