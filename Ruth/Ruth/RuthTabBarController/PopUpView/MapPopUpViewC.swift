//
//  MapPopUpViewC.swift
//  Ruth
//
//  Created by mac on 11/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class MapPopUpViewC: UIViewController {

    @IBOutlet var btnClose: UIButton!
    @IBOutlet var btnOk : UIButton!
    @IBOutlet var btnNo : UIButton!
    @IBOutlet var viewSmile : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnOk.setCornerRadius(20)
        self.btnNo.setCornerRadius(20)

        // Do any additional setup after loading the view.
    }
    @IBAction func actionCloseView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionCenal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionOK(_ sender: Any) {
        let aVC = MapPopUpViewC.init(nibName: "MapPopUpViewC", bundle: nil)
        aVC.modalPresentationStyle = .overCurrentContext
        aVC.modalTransitionStyle = .crossDissolve
        self.present(aVC, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
