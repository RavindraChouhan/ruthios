//
//  VipViewController.swift
//  Ruth
//
//  Created by mac on 13/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class VipViewController: UIViewController {
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var viewHorr : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

          self.viewHorr.dropShadow(color: .hexStringToColor(hex: "00FF21"), opacity: 0.3, offSet: CGSize(width: 0, height: 0), radius: 15, scale: true)
        // Do any additional setup after loading the view.
    }
    @IBAction func actionCloseView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
