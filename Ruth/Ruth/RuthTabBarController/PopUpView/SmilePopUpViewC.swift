//
//  SmilePopUpViewC.swift
//  Ruth
//
//  Created by mac on 11/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class SmilePopUpViewC: UIViewController {

    @IBOutlet var btnClose: UIButton!
    @IBOutlet var btnOk : UIButton!
    @IBOutlet var btnNo : UIButton!
    @IBOutlet var viewSmile : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        viewSmile.layer.shadowOpacity = 0.7
        viewSmile.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewSmile.layer.shadowRadius = 15.0
        viewSmile.layer.shadowColor = UIColor.hexStringToColor(hex: "7FFF00").cgColor
        view.backgroundColor = UIColor.black
        
//        viewSmile.layer.borderColor = UIColor.hexStringToColor(hex:"00FF21").cgColor
//        viewSmile.layer.borderWidth = 1
//          self.viewSmile.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 0.45, offSet: CGSize(width: 1, height: 1), radius: 15, scale: true)
//        self.viewSmile.setCornerRadius( 15)
//        self.btnOk.setCornerRadius( 20)
//        self.btnNo.setCornerRadius( 20)
//        self.btnNo.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1.0, offSet: CGSize(width: 0.0, height: 0.0), radius: 7.0, scale: true)
        // Do any additional setup after loading the view.
    }
    @IBAction func actionCloseView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionCenal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionOK(_ sender: Any) {
        let aVC = SadPopUpViewC.init(nibName: "SadPopUpViewC", bundle: nil)
        aVC.modalPresentationStyle = .overCurrentContext
        aVC.modalTransitionStyle = .crossDissolve
        self.present(aVC, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
