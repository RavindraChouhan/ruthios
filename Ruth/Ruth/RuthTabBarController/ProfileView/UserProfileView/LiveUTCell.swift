//
//  LiveUTCell.swift
//  Ruth
//
//  Created by mac on 20/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class LiveUTCell: UITableViewCell {

    @IBOutlet weak var btnTee: UIButton!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var btnPlya: UIButton!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblD: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
