//
//  UserHeaderProfileVC.swift
//  Ruth
//
//  Created by mac on 21/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import SJSegmentedScrollView

class UserHeaderProfileVC: UIViewController {

    @IBOutlet var btnFollow: UIButton!
    @IBOutlet var btnUserManu: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnOnlie: UIButton!
    @IBOutlet var lblFollowing: UILabel!
    @IBOutlet var lblFollowers: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblUserEmail: UILabel!
    @IBOutlet var userImg: UIImageView!
    
    var dataAllFollowU:AllFollowU!
    var segmentController:SJSegmentedViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userImg.sd_setImage(with: URL(string:self.dataAllFollowU.photoUrl), placeholderImage: nil)
        self.lblUserName.text = self.dataAllFollowU.name
        self.lblTitle.text = self.dataAllFollowU.userName
        self.lblUserEmail.text = "@" + self.dataAllFollowU.userName
        self.lblFollowers.text = "\(self.dataAllFollowU.followers)"
        self.lblFollowing.text = "\(self.dataAllFollowU.followers)"
        self.addRightbuttomWithController(withCustomView: self.btnUserManu)
//        self.addLeftbuttomWithController(withCustomView: self.btnBack)
//        self.navigationController?.isNavigationBarHidden = 
        self.userImg.layer.cornerRadius = self.userImg.frame.height/2
        self.userImg.layer.masksToBounds = true
        self.userImg.layer.borderColor = UIColor.white.cgColor
        self.userImg.layer.borderWidth = 3
//        self.navigationController?.navigationBar.isTranslucent = true
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .clear
    }
    @IBAction func actionBack(_ btn : UIButton) {
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionUserManu(_ btn : UIButton) {
      
    }
    @IBAction func actionUserFollow(_ btn : UIButton) {
        if navigationController?.isNavigationBarHidden == false {
            let viewController = self.storyboard?
                .instantiateViewController(withIdentifier: "HeaderDetailViewController")
            self.parent?.navigationController?.pushViewController(viewController!,
                                                                  animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
