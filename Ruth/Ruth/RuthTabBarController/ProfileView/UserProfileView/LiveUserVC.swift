//
//  LiveUserVC.swift
//  Ruth
//
//  Created by mac on 20/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
class LiveUserVC: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var tblview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.RegisterCell()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    func RegisterCell(){
        self.tblview.register(UINib(nibName: "LiveUTCell",  bundle: nil)
            , forCellReuseIdentifier: "LiveUTCell")
        self.tblview.delegate = self
        self.tblview.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 //self.arrImg.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblview.dequeueReusableCell(withIdentifier: "LiveUTCell", for: indexPath) as! LiveUTCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
