
//
//  UserProfileViewC.swift
//  Ruth
//
//  Created by mac on 11/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
class UserProfileViewC: UIViewController,UITableViewDataSource ,UITableViewDelegate {
    @IBOutlet var btnFollow: UIButton!
    @IBOutlet var btnUserManu: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnOnlie: UIButton!
    @IBOutlet var lblFollowing: UILabel!
    @IBOutlet var lblFollowers: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblUserEmail: UILabel!
    @IBOutlet var userImg: UIImageView!
    @IBOutlet var tblview: UITableView!
   // @IBOutlet weak var viewRight:UIView!
    var refreshControl: UIRefreshControl!
    var dataAllFollowU:AllFollowU!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addRightbuttomWithController(withCustomView: self.btnUserManu)
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        
        self.userImg.layer.cornerRadius = self.userImg.frame.height/2
        self.userImg.layer.masksToBounds = true
        self.userImg.layer.borderColor = UIColor.white.cgColor
        self.userImg.layer.borderWidth = 3
        self.UserRegisterCell()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    @objc func pulltorefresh(){
        tblview.reloadData()
        refreshControl.endRefreshing()
    }
    func UserRegisterCell()  {
        self.tblview.register(UINib(nibName: "UserProfileTCell",  bundle: nil)
            , forCellReuseIdentifier: "UserProfileTCell")
        self.tblview.delegate = self
        self.tblview.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblview.addSubview(refreshControl)
    }
    @IBAction func actionBack(_ btn : UIButton) {
        self.dismiss(animated: true) {
            
        }
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionUserManu(_ btn : UIButton) {
 
    }
    @IBAction func actionUserFollow(_ btn : UIButton) {
      
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 //self.arrImg.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tblview.dequeueReusableCell(withIdentifier: "UserProfileTCell", for: indexPath) as! UserProfileTCell
            cell.selectionStyle = .none
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
