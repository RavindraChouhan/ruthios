//
//  UserProfileTCell.swift
//  Ruth
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class UserProfileTCell: UITableViewCell {

    @IBOutlet weak var btnTee: UIButton!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var btnPlya: UIButton!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblD: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnPlya.setCornerRadius(7)
        self.imgBG.setCornerRadius(7)
        self.imgBG.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 0.4, offSet: CGSize(width: 0.0, height: 0.0), radius: 15, scale: true)
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
