//
//  PositiveTCell.swift
//  Ruth
//
//  Created by mac on 14/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class PositiveTCell: UITableViewCell {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblSpent : UILabel!
    @IBOutlet var lblMins : UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var viewBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewBg.setCornerRadius(12)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
