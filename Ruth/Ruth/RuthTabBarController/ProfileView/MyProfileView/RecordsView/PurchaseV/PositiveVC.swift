//
//  PositiveVC.swift
//  Ruth
//
//  Created by mac on 13/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class PositiveVC: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var tblView: UITableView!
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PositiveRegisterCell()
    }
    func PositiveRegisterCell()  {
        self.tblView.register(UINib(nibName: "PositiveTCell",  bundle: nil)
            , forCellReuseIdentifier: "PositiveTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
        self.tblView.reloadData()
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    @objc func actionConnect(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "PositiveTCell", for: indexPath) as! PositiveTCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
