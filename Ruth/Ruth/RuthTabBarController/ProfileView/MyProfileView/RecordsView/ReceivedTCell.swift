//
//  ReceivedTCell.swift
//  Ruth
//
//  Created by mac on 05/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ReceivedTCell: UITableViewCell {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblMins : UILabel!
    @IBOutlet var lblVCount : UILabel!
    @IBOutlet var lblLCount : UILabel!
    @IBOutlet var lblECount : UILabel!
    @IBOutlet var lblMCount : UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var viewBg: UIView!
    @IBOutlet var btn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
       // self.viewBg.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 0.4, offSet: CGSize(width: 0.0, height: 0.0), radius: 15, scale: true)
        self.viewBg.setCornerRadius(7)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
