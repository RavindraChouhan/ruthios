//
//  ReceivedViewC.swift
//  Ruth
//
//  Created by mac on 05/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ReceivedViewC: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var tblView: UITableView!
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.RegisterTCell()
    }
    func RegisterTCell()  {
        self.tblView.register(UINib(nibName: "ReceivedTCell",  bundle: nil)
            , forCellReuseIdentifier: "ReceivedTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
        self.tblView.reloadData()
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    @objc func actionConnect(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "ReceivedTCell", for: indexPath) as! ReceivedTCell
         cell.viewBg.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius: 7.0, scale: true)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
