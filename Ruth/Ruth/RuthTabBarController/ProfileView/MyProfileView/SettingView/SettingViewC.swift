//
//  SettingViewC.swift
//  Ruth
//
//  Created by mac on 10/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class SettingViewC: UIViewController {

    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtPassWord: UITextField!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var lblUserAccount: UILabel!
    @IBOutlet var lblSound: UILabel!
    @IBOutlet var lblEdit: UILabel!
    @IBOutlet var lblOfflineMode: UILabel!
    @IBOutlet var lblSoundQuality: UILabel!
    @IBOutlet var lblAutoplay: UILabel!
    @IBOutlet var lblAudioNormalization: UILabel!
    @IBOutlet var lblLinktoSocialMediaAccounts: UILabel!
    @IBOutlet var lblFacebook: UILabel!
    @IBOutlet var lblTwitter: UILabel!
    @IBOutlet var lblInstagram: UILabel!
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var btnTwitter: UIButton!
    @IBOutlet var btnInstagram: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.isNavigationBarHidden = false
         self.navigationItem.title = "設定"
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    @IBAction func actionBack(_ btn : UIButton) {
      ///  self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionFacebook(_ btn : UIButton) {
    }
    @IBAction func actionTwitter(_ btn : UIButton) {
    }
    @IBAction func actionInstagram(_ btn : UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
