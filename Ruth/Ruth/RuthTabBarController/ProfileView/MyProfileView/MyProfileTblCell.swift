//
//  MyProfileTblCell.swift
//  Ruth
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//
import UIKit
import SDWebImage
class MyProfileTblCell: UITableViewCell {
    
   //var arrData = [[String:Any]]()
    //var arrHomeGridNew = [HomeGrid]()
    @IBOutlet weak var lblMy : UILabel!
    @IBOutlet weak var lblRecord : UILabel!
    @IBOutlet weak var lblAbout : UILabel!
    @IBOutlet var btnMoneyBag: UIButton!
    @IBOutlet var btnVip: UIButton!
    @IBOutlet var btnFunds: UIButton!
    @IBOutlet var btnTarget: UIButton!
    @IBOutlet var btnShape: UIButton!
    @IBOutlet var btnNetwork: UIButton!
    @IBOutlet var btnBackPack: UIButton!
    @IBOutlet var btnAccount: UIButton!
    
    @IBOutlet var btnRemove: UIButton!
    @IBOutlet var btnCard: UIButton!
    @IBOutlet var btnGiftbox: UIButton!
    @IBOutlet var btnGift: UIButton!
    @IBOutlet var btnPlay: UIButton!
    @IBOutlet var btnVideos: UIButton!
    @IBOutlet var btnComment: UIButton!
    @IBOutlet var btnMotivation: UIButton!
    
    @IBOutlet var btnList: UIButton!
    @IBOutlet var btnSlideShare: UIButton!
    @IBOutlet var btnChat: UIButton!
    @IBOutlet var btnPlaceholder: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblMy.text = "My".localized()
        self.lblRecord.text = "Record".localized()
        self.lblAbout.text = "About".localized()
      
    }
}
