//
//  MyProfileViewController.swift
//  Ruth
//
//  Created by mac on 27/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController ,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var btnLogout: UIButton!
    @IBOutlet var btnAddfriend: UIButton!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnSetting: UIButton!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblFollowing: UILabel!
    @IBOutlet var lblFan: UILabel!
    @IBOutlet var lblGoldPrice: UILabel!
    @IBOutlet var lblSilverPrice: UILabel!
    @IBOutlet var lblCoinPrice: UILabel!
    @IBOutlet var tblView: UITableView!
    var isFromLogout = false
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.addLeftbuttomWithController(withCustomView: self.btnAddfriend)
        self.addRightbuttomWithController(withCustomView: self.btnSetting)
        
        self.makeTransprentNavigationBar()
        self.imgProfile.setCornerRadius(35)
        self.lblName.text = UserNew.currentUser.name
        self.lblEmail.text = UserNew.currentUser.email
        self.imgProfile.sd_setImage(with: URL(string:UserNew.currentUser.photoUrl), placeholderImage:#imageLiteral(resourceName: "defaultProfilePicture"))
        self.setupNavigationBar()
        self.navigationItem.title = "Jack (在線上)"
       self.MyProfileRegisterCell ()
        
        if let arrayPoint = mainUser?.pointEarn
        {
            for point in arrayPoint{
                if point._id == "金" {//gold
                    self.lblGoldPrice.text = "\(point.total!)"
                }else if point._id == "银" { //Silver
                    self.lblSilverPrice.text = "\(point.total!)"
                }else{ //Coin 硬币
                    self.lblCoinPrice.text = "\(point.total!)"
                }
            }
        }
//        if let giftCount = mainUser?.giftCount
//        {
//            self.lblGiftCount.text = "\(giftCount)"
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: true, animated: true)
      //  self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.btnLogout.setTitle("Log out".localized(), for: .normal)
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    @IBAction func actionEdit(_ btn : UIButton) {
        let alertVC = SelectPicAlertVC.create().config(CPAlertAnimationType.center)
        alertVC.addAction(CPAlertAction(type: .normal, handler: { self.selectPic(false) }))
        alertVC.addAction(CPAlertAction(type: .header, handler: { self.selectPic(true) }))
        alertVC.addAction(CPAlertAction(type: .cancel, handler: {}))
        alertVC.show(into: self)
        
    }
    func selectPic(_ isFromCamera: Bool) {
        Util.util.openCamera(isFromCamera, self, callback: { (selectedImg) in
            self.imgProfile.image = selectedImg
            //setImage(selectedImg, for: .normal)
        })
    }
    @IBAction func actionAddFriend(_ btn : UIButton) {
        let myTopUpViewC = TopUpViewC(nibName: "TopUpViewC", bundle: nil)
        self.navigationController?.pushViewController(myTopUpViewC, animated: true)
    }
    @IBAction func actionSetting(_ btn : UIButton) {
        let mySettingViewC = SettingViewC(nibName: "SettingViewC", bundle: nil)
        self.navigationController?.pushViewController(mySettingViewC, animated: true)
    }
        func MyProfileRegisterCell ()  {
            self.tblView.register(UINib(nibName: "MyProfileTblCell",  bundle: nil)
                , forCellReuseIdentifier: "MyProfileTblCell")
            self.tblView.delegate = self
            self.tblView.dataSource = self
            refreshControl = UIRefreshControl()
            refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
            self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
            self.tblView.addSubview(refreshControl)
            self.tblView.reloadData()
        }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "MyProfileTblCell", for: indexPath) as! MyProfileTblCell
        cell.btnBackPack.tag = indexPath.row
        cell.selectionStyle = .none
        cell.btnMoneyBag.addTarget(self, action: #selector(actionMoneyBag(sender:)), for: .touchUpInside)
        cell.btnVip.addTarget(self, action: #selector(actionVip(sender:)), for: .touchUpInside)
        cell.btnFunds.addTarget(self, action: #selector(actionFunds(sender:)), for: .touchUpInside)
        cell.btnTarget.addTarget(self, action: #selector(actionTarget(sender:)), for: .touchUpInside)
        cell.btnShape.addTarget(self, action: #selector(actionShape(sender:)), for: .touchUpInside)
        cell.btnNetwork.addTarget(self, action: #selector(actionNetwork(sender:)), for: .touchUpInside)
        cell.btnBackPack.addTarget(self, action: #selector(actionBackpackV(sender:)), for: .touchUpInside)
        cell.btnAccount.addTarget(self, action: #selector(actionRootAccountV(sender:)), for: .touchUpInside)
        cell.btnRemove.addTarget(self, action: #selector(actionRemove(sender:)), for: .touchUpInside)
        cell.btnCard.addTarget(self, action: #selector(actionCard(sender:)), for: .touchUpInside)
        cell.btnGiftbox.addTarget(self, action: #selector(actionGiftbox(sender:)), for: .touchUpInside)
        cell.btnGift.addTarget(self, action: #selector(actionGift(sender:)), for: .touchUpInside)
        cell.btnPlay.addTarget(self, action: #selector(actionPlay(sender:)), for: .touchUpInside)
        cell.btnVideos.addTarget(self, action: #selector(actionVideos(sender:)), for: .touchUpInside)
        cell.btnComment.addTarget(self, action: #selector(actionComment(sender:)), for: .touchUpInside)
        cell.btnMotivation.addTarget(self, action: #selector(actionMotivation(sender:)), for: .touchUpInside)
        cell.btnList.addTarget(self, action: #selector(actionList(sender:)), for: .touchUpInside)
        cell.btnSlideShare.addTarget(self, action: #selector(actionSlideShare(sender:)), for: .touchUpInside)
        cell.btnChat.addTarget(self, action: #selector(actionChat(sender:)), for: .touchUpInside)
        cell.btnPlaceholder.addTarget(self, action: #selector(actionPlaceholder(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 615
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    @objc func actionMoneyBag(sender: UIButton) {
        let myTopUpViewC = TopUpViewC(nibName: "TopUpViewC", bundle: nil)
        self.navigationController?.pushViewController(myTopUpViewC, animated: true)
    }
    @objc func actionVip(sender: UIButton) {
        let myMyVipViewC = MyVipViewC(nibName: "MyVipViewC", bundle: nil)
        self.navigationController?.pushViewController(myMyVipViewC, animated: true)
    }
    @objc func actionFunds(sender: UIButton) {
        let myRootIncomeVC = RootIncomeVC(nibName: "RootIncomeVC", bundle: nil)
        self.navigationController?.pushViewController(myRootIncomeVC, animated: true)
    }
    @objc func actionTarget(sender: UIButton) {
        let myMissionsViewC = MissionsViewC(nibName: "MissionsViewC", bundle: nil)
        self.navigationController?.pushViewController(myMissionsViewC, animated: true)
    }
    @objc func actionShape(sender: UIButton) {
        let myRootRantVC = RootRantVC(nibName: "RootRantVC", bundle: nil)
        self.navigationController?.pushViewController(myRootRantVC, animated: true)
    }
    @objc func actionNetwork(sender: UIButton) {
        let myRootLinkingVC = RootLinkingVC(nibName: "RootLinkingVC", bundle: nil)
        self.navigationController?.pushViewController(myRootLinkingVC, animated: true)
    }
    @objc func actionBackpackV(sender: UIButton) {
        let myBackpackViewC = BackpackViewC(nibName: "BackpackViewC", bundle: nil)
        self.navigationController?.pushViewController(myBackpackViewC, animated: true)
    }
    @objc func actionRootAccountV(sender: UIButton) {
        let myRootAccountVC = RootAccountVC(nibName: "RootAccountVC", bundle: nil)
        self.navigationController?.pushViewController(myRootAccountVC, animated: true)
    }
    @objc func actionRemove(sender: UIButton) {
//        let myRootPurchaseVC = RootPurchaseVC(nibName: "RootPurchaseVC", bundle: nil)
//        self.navigationController?.pushViewController(myRootPurchaseVC, animated: true)
    }
    @objc func actionCard(sender: UIButton) {
        let myRootPurchaseVC = RootPurchaseVC(nibName: "RootPurchaseVC", bundle: nil)
        self.navigationController?.pushViewController(myRootPurchaseVC, animated: true)
    }
    @objc func actionGiftbox(sender: UIButton) {
        let myRootReceivedVC = RootReceivedVC(nibName: "RootReceivedVC", bundle: nil)
        self.navigationController?.pushViewController(myRootReceivedVC, animated: true)
    }
    @objc func actionGift(sender: UIButton) {
//        let myBackpackViewC = BackpackViewC(nibName: "BackpackViewC", bundle: nil)
//        self.navigationController?.pushViewController(myBackpackViewC, animated: true)
    }
    @objc func actionPlay(sender: UIButton) {
//        let myBackpackViewC = BackpackViewC(nibName: "BackpackViewC", bundle: nil)
//        self.navigationController?.pushViewController(myBackpackViewC, animated: true)
    }
    @objc func actionVideos(sender: UIButton) {
//        let myBackpackViewC = BackpackViewC(nibName: "BackpackViewC", bundle: nil)
//        self.navigationController?.pushViewController(myBackpackViewC, animated: true)
    }
    @objc func actionComment(sender: UIButton) {
//        let myBackpackViewC = BackpackViewC(nibName: "BackpackViewC", bundle: nil)
//        self.navigationController?.pushViewController(myBackpackViewC, animated: true)
    }
    @objc func actionMotivation(sender: UIButton) {
        let myRootSummonVC = RootSummonVC(nibName: "RootSummonVC", bundle: nil)
        self.navigationController?.pushViewController(myRootSummonVC, animated: true)
    }
    @objc func actionList(sender: UIButton) {
        let myTermsConditionsVC = TermsConditionsVC(nibName: "TermsConditionsVC", bundle: nil)
        self.navigationController?.pushViewController(myTermsConditionsVC, animated: true)
    }
    @objc func actionSlideShare(sender: UIButton) {
        let myReviewsViewC = ReviewsViewC(nibName: "ReviewsViewC", bundle: nil)
        self.navigationController?.pushViewController(myReviewsViewC, animated: true)
    }
    @objc func actionChat(sender: UIButton) {
        let myRootFAQVC = RootFAQVC(nibName: "RootFAQVC", bundle: nil)
        self.navigationController?.pushViewController(myRootFAQVC, animated: true)
    }
    @objc func actionPlaceholder(sender: UIButton) {
        let myContactUsViewC = ContactUsViewC(nibName: "ContactUsViewC", bundle: nil)
        self.navigationController?.pushViewController(myContactUsViewC, animated: true)
    }
    @IBAction func actionLogout(_ btn : UIButton) {
        UserDefaults.standard.set(isFromLogout, forKey: "isFromLogout")
        UserDefaults.standard.synchronize()
        let alertMessage = UIAlertController(title: "", message: "Are you sure you want to Log Out?".localized(), preferredStyle: UIAlertControllerStyle.alert)
        let alertOk = UIAlertAction(title: "Log out".localized(), style: .default, handler: { (action) -> Void in
            UserNew.currentUser.logout()
            let controllerArray = self.navigationController?.childViewControllers
            print("controllerArray == \(String(describing: controllerArray))")
            UserNew.currentUser.isLogin = false
            UserNew.currentUser.logout()
            if  let controller = controllerArray!.last
            {
                if !(controller is LoginController) {
                    let stroryBord = UIStoryboard(name: "Main", bundle: nil)
                    let landingViewController = stroryBord.instantiateViewController(withIdentifier: "LandingViewController") as! LandingViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    var addWindow: UIWindow!
                    if let window = appDelegate.window
                    {
                        addWindow = window
                    }else{
                        addWindow = UIWindow(frame: UIScreen.main.bounds)
                    }
                    appDelegate.window = addWindow
                    let nav1 = UINavigationController(rootViewController: landingViewController)
                    appDelegate.window?.rootViewController = nav1
                    appDelegate.window?.makeKeyAndVisible()
                }
            }
        })
        let cancel = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) -> Void in
            print("Cancel button click...")
        }
        alertMessage.addAction(alertOk)
        alertMessage.addAction(cancel)
        self.present(alertMessage, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
