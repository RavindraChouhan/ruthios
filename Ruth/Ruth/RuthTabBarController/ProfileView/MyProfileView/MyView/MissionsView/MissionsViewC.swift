//
//  MissionsViewC.swift
//  Ruth
//
//  Created by mac on 11/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class MissionsViewC: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var btnBack: UIButton!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var btnFree: UIButton!
    @IBOutlet var imgBg: UIImageView!
    @IBOutlet var lblFreeGold: UILabel!
    @IBOutlet var lblYouhave: UILabel!
    @IBOutlet var lblWatchvideo: UILabel!
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.MissionsRegisterCell()
         self.addLeftbuttomWithController(withCustomView: btnBack)
        self.makeTransprentNavigationBar()
        self.setupNavigationBar()
        self.navigationItem.title = "Missions".localized()
        //Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    func MissionsRegisterCell()  {
        self.tblView.register(UINib(nibName: "MissionsTCell",  bundle: nil)
            , forCellReuseIdentifier: "MissionsTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
        self.tblView.reloadData()
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8//self.arrImg.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "MissionsTCell", for: indexPath) as! MissionsTCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
