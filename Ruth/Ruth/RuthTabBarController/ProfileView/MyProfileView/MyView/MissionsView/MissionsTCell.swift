//
//  MissionsTCell.swift
//  Ruth
//
//  Created by mac on 11/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class MissionsTCell: UITableViewCell {

    @IBOutlet var lblgold: UILabel!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblDetial: UILabel!
    @IBOutlet var btnReceive: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
