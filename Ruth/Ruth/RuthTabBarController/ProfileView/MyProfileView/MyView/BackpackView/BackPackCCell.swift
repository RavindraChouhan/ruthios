//
//  BackPackCCell.swift
//  Ruth
//
//  Created by mac on 13/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class BackPackCCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblgold: UILabel!
    @IBOutlet weak var lblgeern: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewBg.setCornerRadius(5)
        // Initialization code
    }

}
