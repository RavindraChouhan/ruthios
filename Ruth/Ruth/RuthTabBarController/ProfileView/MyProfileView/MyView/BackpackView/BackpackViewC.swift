//
//  BackpackViewC.swift
//  Ruth
//
//  Created by mac on 13/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class BackpackViewC: UIViewController,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var btnBack : UIButton!
    @IBOutlet var collectionView : UICollectionView!
    var arrTitle = ["冥通銀行紀念鈔","紙紮公仔","祖先金","紅蠟燭"]
    var arrImg = [#imageLiteral(resourceName: "meditation"),#imageLiteral(resourceName: "paper_doll"),#imageLiteral(resourceName: "gold_ancestor"),#imageLiteral(resourceName: "redcandle")]
    override func viewDidLoad() {
        super.viewDidLoad()
         self.addLeftbuttomWithController(withCustomView: btnBack)
        
        self.collectionView.register(UINib.init(nibName: "BackPackCCell", bundle: nil), forCellWithReuseIdentifier: "BackPackCCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func actionBack(_ sender: Any) {
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BackPackCCell", for: indexPath) as! BackPackCCell
        let data = self.arrTitle[indexPath.item]
        let Imgdata = self.arrImg[indexPath.item]
        cell.lblName.text = data
        cell.img.image = Imgdata
//        cell.btnFollow.addTarget(self, action: #selector(actionFollow(sender:)), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(UIScreen.main.bounds.width - 80)/4, height: 104)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
