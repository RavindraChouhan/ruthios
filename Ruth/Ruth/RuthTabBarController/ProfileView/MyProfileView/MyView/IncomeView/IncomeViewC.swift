//
//  IncomeViewC.swift
//  Ruth
//
//  Created by mac on 11/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class IncomeViewC: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var tblView: UITableView!
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.IncomeRegisterCell()
        self.setupNavigationBar()
        self.navigationItem.title = "Income"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    func IncomeRegisterCell()  {
        self.tblView.register(UINib(nibName: "IncomeTCell",  bundle: nil)
            , forCellReuseIdentifier: "IncomeTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
        self.tblView.reloadData()
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8//self.arrImg.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "IncomeTCell", for: indexPath) as! IncomeTCell
        cell.lblAdd.text = "兌" + " HKD10.00"
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
