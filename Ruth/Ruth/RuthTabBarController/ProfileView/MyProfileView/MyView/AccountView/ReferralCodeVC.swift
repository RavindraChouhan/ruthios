//
//  ReferralCodeVC.swift
//  Ruth
//
//  Created by mac on 13/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ReferralCodeVC: UIViewController {

     @IBOutlet weak var btnReferralCode: UIButton!
     @IBOutlet weak var btnFB: UIButton!
     @IBOutlet weak var btnWat: UIButton!
     @IBOutlet weak var btnGoogle: UIButton!
     @IBOutlet var lblshr : UILabel!
     @IBOutlet var lblCode : UILabel!
     @IBOutlet var lblTCode : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.lblshr.text = "Share".localized()
       self.lblTCode.text = "Share Your Referral Code".localized()
       self.btnReferralCode.setTitle("ReferralCode".localized(), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
