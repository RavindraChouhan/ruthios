//
//  RootAccountVC.swift
//  Ruth
//
//  Created by mac on 31/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import CarbonKit

class RootAccountVC: UIViewController,CarbonTabSwipeNavigationDelegate {

    @IBOutlet var btnBack : UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    var categoryNames = [String]()
    var categoryID = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftbuttomWithController(withCustomView: btnBack)
         self.addRightbuttomWithController(withCustomView: btnSearch)
        self.categoryNames = ["個人資料","身份驗證","等級","邀請碼"]
        self.setupTopBarAndNavigation()
        self.setupNavigationBar()
        self.navigationItem.title = "帳戶"
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func actionBack(_ sender: Any) {
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    func setupTopBarAndNavigation() {
        
//         tabSwipe.setupCarbonPages(carbonTabSwipeNavigation, tabTitles: menuTabTitles, totalWidth: self.view.frame.width).insertIntoRootViewController(self)
        let tabSwipe = CarbonTabSwipeNavigation(items: self.categoryNames,delegate: self)
        tabSwipe.setNormalColor(UIColor.black, font: UIFont(name: ".SFUIText-Medium", size: 13)!)
        tabSwipe.setSelectedColor(UIColor.white, font: UIFont(name: ".SFUIText-Medium", size: 13)!)
        tabSwipe.carbonTabSwipeScrollView.backgroundColor = .green//UIColor.hexStr(hexStr: "f0f0f0")
        
        tabSwipe.setIndicatorColor(UIColor.white)
        tabSwipe.pagesScrollView?.isScrollEnabled = true
        tabSwipe.setTabExtraWidth(30)
         tabSwipe.setTabBarHeight(26)
        tabSwipe.carbonSegmentedControl?.backgroundColor = .green
        tabSwipe.insert(intoRootViewController: self)
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            let VC = PersonalInformationVC(nibName: "PersonalInformationVC", bundle: nil)
            return VC
    
        }
        if index == 1 {
            let VC = IdentityVerificationVC(nibName: "IdentityVerificationVC", bundle: nil)
            return VC
        }
        if index == 2 {
            let VC = LevelsViewController(nibName: "LevelsViewController", bundle: nil)
            return VC
        }
        if index == 3 {
            let VC = ReferralCodeVC(nibName: "ReferralCodeVC", bundle: nil)
            return VC
        }
        return UIViewController()
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
