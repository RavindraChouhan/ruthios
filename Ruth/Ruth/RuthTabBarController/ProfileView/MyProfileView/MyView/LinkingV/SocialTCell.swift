//
//  SocialTCell.swift
//  Ruth
//
//  Created by mac on 14/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class SocialTCell: UITableViewCell {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var img : UIImageView!
    @IBOutlet var btnConnect: UIButton!
    var arrayList = [[String:Any]]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
//    func setData(_ arrData: [[String:Any]]) {
//        self.lblTitle.text = arrData["title"] as? String
//        self.img.image = arrData["icon"] as? UIImage
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
