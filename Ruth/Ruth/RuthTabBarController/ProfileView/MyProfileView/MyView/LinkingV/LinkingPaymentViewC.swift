//
//  LinkingViewC.swift
//  Ruth
//
//  Created by mac on 04/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class LinkingPaymentViewC: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var tblView: UITableView!
    @IBOutlet var btnAdd: UIButton!
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PaymentRegisterCell()
//Payment Method | Social Media Accounts
        // Do any additional setup after loading the view.
    }
    func PaymentRegisterCell()  {
        self.tblView.register(UINib(nibName: "PaymentTCell",  bundle: nil)
            , forCellReuseIdentifier: "PaymentTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
//        self.tblView.estimatedRowHeight = 174
//        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.reloadData()
    }
    @IBAction func actionAdd(_ sender:UIButton){
        rLog("actionAdd")
     }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "PaymentTCell", for: indexPath) as! PaymentTCell
        cell.selectionStyle = .none
        return cell
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 174
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
