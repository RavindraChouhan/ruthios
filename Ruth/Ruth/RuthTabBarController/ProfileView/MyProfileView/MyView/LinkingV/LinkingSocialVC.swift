//
//  LinkingSocialVC.swift
//  Ruth
//
//  Created by mac on 13/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class LinkingSocialVC: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var tblView: UITableView!
    var arrName = [["icon":#imageLiteral(resourceName: "ic_fb"),"title":"Facebook"],["icon": #imageLiteral(resourceName: "ic_inst"),"title":"Instagram"],["icon":#imageLiteral(resourceName: "ic_wechat"),"title":"WeChat"],["icon":#imageLiteral(resourceName: "chines"), "title":"新浪微博"]]
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SocialRegisterCell()
       // var dict = [String:Any]()
     //   dict["data"] = [["icon":#imageLiteral(resourceName: "money-bag"),"title":"Facebook"],["icon": #imageLiteral(resourceName: "vip"),"title":"Instagram"],["icon":#imageLiteral(resourceName: "funds"),"title":"WeChat"],["icon":#imageLiteral(resourceName: "target"), "title":"新浪微博"]]
           // self.arrName.append(dict)
        // Do any additional setup after loading the view.
    }
    func SocialRegisterCell()  {
        self.tblView.register(UINib(nibName: "SocialTCell",  bundle: nil)
            , forCellReuseIdentifier: "SocialTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
        self.tblView.estimatedRowHeight = 54
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.reloadData()
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    @objc func actionConnect(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrName.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "SocialTCell", for: indexPath) as! SocialTCell
        let dict = self.arrName[indexPath.row]
//        cell.setData(dict["data"] as! [[String : Any]])
        cell.lblTitle.text = dict["title"] as? String
        cell.img.image = dict["icon"] as? UIImage
        cell.btnConnect.addTarget(self, action: #selector(actionConnect(sender:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
