//
//  TopUpViewC.swift
//  Ruth
//
//  Created by mac on 11/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class TopUpViewC: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var btnBack: UIButton!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var btnFree: UIButton!
    @IBOutlet var imgBg: UIImageView!
    @IBOutlet var lblFreeGold: UILabel!
    @IBOutlet var lblYouhave: UILabel! 
    @IBOutlet var lblWatchvideo: UILabel!
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.isNavigationBarHidden = true
        self.navigationItem.title = "TopUp".localized()
        self.lblYouhave.text = "Youhave".localized()
        self.lblWatchvideo.text = "Watchvideo".localized()
        self.lblFreeGold.text = "FreeGold".localized()
        self.btnFree.setTitle("Free", for: .normal)
        self.TopUpRegisterCell()
        self.setupNavigationBar()
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    func TopUpRegisterCell()  {
        self.tblView.register(UINib(nibName: "TopUpTCell",  bundle: nil)
            , forCellReuseIdentifier: "TopUpTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
        self.tblView.reloadData()
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    @IBAction func actionBack(_ btn : UIButton) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8//self.arrImg.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "TopUpTCell", for: indexPath) as! TopUpTCell
            cell.lblAdd.text = " +" + " 10"
            cell.selectionStyle = .none
            return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
