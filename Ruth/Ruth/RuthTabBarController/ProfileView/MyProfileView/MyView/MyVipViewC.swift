//
//  MyVipViewC.swift
//  Ruth
//
//  Created by mac on 13/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class MyVipViewC: UIViewController {

    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnHMonthly: UIButton!
    @IBOutlet var lblTerms: UILabel!
    @IBOutlet var lblDetial: UILabel!
    @IBOutlet var btnTerms: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        //Login to agree to the Terms & Conditions
        let strApp = "Login to agree to the ".localized()
        let strTerms = "Terms and conditions.".localized()
        //SFUIText-Medium
        let attributedTermsString = NSMutableAttributedString(attributedString: NSAttributedString(string: strApp, attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size: 14)!]))
        attributedTermsString.append(NSAttributedString(string: strTerms, attributes: [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.underlineColor: UIColor.lightGray, NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size: 14)!]))
        //cell.btnTerms.setAttributedTitle(attributedTermsString, for: .normal)
        self.lblTerms.attributedText = attributedTermsString
        self.setupNavigationBar()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isTranslucent = false
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    @IBAction func actionTermsCondition(_ sender: Any) {
        LandingStoryboard.showTermsModalView(controller: self)
    }
    @IBAction func actionBack(_ btn : UIButton) {
         objAppDelegate.checkLoginInAppLaunch()
        self.dismiss(animated: true) {
        }
////        self.navigationController?.popToRootViewController(animated: true)
//        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionHkdMonthly(_ btn : UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
