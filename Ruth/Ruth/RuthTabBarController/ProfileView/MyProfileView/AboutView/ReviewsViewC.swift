//
//  ReviewsViewC.swift
//  Ruth
//
//  Created by mac on 17/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ReviewsViewC: UIViewController {
    @IBOutlet var txtEmail : UITextField!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var txtView: UITextView!
    @IBOutlet var lblDe: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var lblReview: UILabel!
    @IBOutlet var lblName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblDe.text = "Please leave your email address so that we can contact you".localized()
        self.lblEmail.text = "Email".localized()
        self.lblName.text = "Name".localized()
        self.lblRating.text = "Your Rating".localized()
        self.lblReview.text = "Your Review".localized()
        self.btnSubmit.setTitle("Submit".localized(), for: .normal)
        self.txtEmail.placeholder = "Enter Email".localized()
        self.txtName.placeholder = "Name".localized()
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        self.setupNavigationBar()
        self.navigationItem.title = "意見反映"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    @IBAction func actionBack(_ btn : UIButton) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSubmit(_ btn : UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
