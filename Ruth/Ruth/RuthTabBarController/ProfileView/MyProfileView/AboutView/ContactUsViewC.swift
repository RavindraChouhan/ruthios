//
//  ContactUsViewC.swift
//  Ruth
//
//  Created by mac on 17/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ContactUsViewC: UIViewController {
//    @IBOutlet var lblD: UILabel!
    @IBOutlet var lblAdd: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblLike: UILabel!
    @IBOutlet var lblMobileN: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lbladd: UILabel!
    @IBOutlet var lblcon: UILabel!
    @IBOutlet var lblemail: UILabel!
    @IBOutlet var lbllike: UILabel!
    @IBOutlet var lblmobileN: UILabel!
    @IBOutlet var lblOpen: UILabel!
    @IBOutlet var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblcon.text = "Contact us".localized()
        self.lbladd.text = "Address".localized()
        self.lblmobileN.text = "Customer Service Hotline".localized()
        self.lblemail.text = "Email".localized()
        self.lblOpen.text = "Opening times".localized()
        self.lbllike.text = "Website".localized()
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        self.makeTransprentNavigationBar()
        self.setupNavigationBar()
        self.navigationItem.title = "聯絡我們"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    @IBAction func actionBack(_ btn : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
