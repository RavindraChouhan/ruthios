//
//  Terms&ConditionsVC.swift
//  Ruth
//
//  Created by mac on 04/09/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class TermsConditionsVC: UIViewController {

    @IBOutlet var lbltitle: UILabel!
   @IBOutlet var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.navigationItem.title = "條款及細則"
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        //UserProfileTCell
    }
    @IBAction func actionBack(_ btn : UIButton) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
