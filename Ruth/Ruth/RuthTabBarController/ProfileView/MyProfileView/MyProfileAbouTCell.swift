//
//  MyProfileAbouTCell.swift
//  Ruth
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import SDWebImage
class MyProfileAbouTCell: UITableViewCell ,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet var collectionView : UICollectionView!
    var arrHomeGridNew = [HomeGrid]()
    var arrayList = [[String : Any]]()
    //     var controller :MapViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib.init(nibName: "MyProfileAboutCCell", bundle: nil), forCellWithReuseIdentifier: "MyProfileAboutCCell")
    }
    
    func setData(_ arrData : [[String : Any]] ){
        self.arrayList = arrData
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyProfileAboutCCell", for: indexPath) as! MyProfileAboutCCell
        switch indexPath.row  {
        case 3:
            cell.imgV.isHidden = true
            break
        default:
            break
        }
        let bottomData1 = arrayList[indexPath.item]
        cell.lblTitle.text = bottomData1["title"] as? String
        cell.imgTitle.image = bottomData1["icon"] as? UIImage
        return cell
    }
    
    var indeexx = -1
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0..<arrHomeGridNew.count {
            let data = arrHomeGridNew[i]
            if i == indexPath.item {
                data.isSelected = !data.isSelected
                if data.isSelected {
                   // self.collectionView.reloadData()
                    switch indexPath.item {
                    case 0:
                        NotificationCenter.default.post(name:AppNotifications.notificationMap, object: nil)
                        break
                    case 1:
                        NotificationCenter.default.post(name:AppNotifications.notificationMap, object: nil)
                        break
                    case 2:
                        NotificationCenter.default.post(name:AppNotifications.notificationRuthVid, object: nil)
                        break
                    case 3:
                        NotificationCenter.default.post(name:AppNotifications.notificationRuthStory, object: nil)
                        break
                    default:
                        break
                    }
                }
            }else{
                data.isSelected = false
            }
        }
        //self.collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = (UIScreen.main.bounds.width - 20) / 4
        return CGSize(width: cellSize, height: cellSize)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //    func pushView(controller:MapViewController) {
    //        self.controller = controller
    //    }
}
