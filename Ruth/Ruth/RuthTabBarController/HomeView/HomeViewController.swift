//
//  HomeViewController.swift
//  Ruth NpSir
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import FBSDKCoreKit

class HomeViewController: UIViewController,FSPagerViewDelegate,FSPagerViewDataSource ,UITableViewDataSource ,UITableViewDelegate {
    var tabController : UITabBarController!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btn1:UIButton!
    @IBOutlet var btn2:UIButton!
    @IBOutlet var btn3:UIButton!
    @IBOutlet var headerView : UIView!
    @IBOutlet var tblView : UITableView!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var pagerControl: FSPageControl!
    @IBOutlet weak var view1:UIView!
    @IBOutlet weak var view2:UIView!
    @IBOutlet weak var view3:UIView!
    @IBOutlet weak var viewRight:UIView!
    @IBOutlet weak var viewIndicator:UIView!
    let btnheight = UIScreen.main.bounds.width/10
    var arrHomeChannel = [[String : Any]]()
    var homeBanner = [String]()
    var refreshControl: UIRefreshControl!
    var UBroadCustomOrder = [UserBroadCustomOrder]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btn1.isSelected = true
        self.tabController = self.tabBarController!
        self.setupNavigationBar()
        self.HomeRegisterCell()
        self.HomeFSPagerView()
        self.setupFeaturedvideos()
        self.getAdvertisements()
        self.getAllChannels()
        self.makeTransprentNavigationBar()
        self.addInvaildDeviceNotificationObserver()
        self.addRightbuttomWithController(withCustomView: self.viewRight)
        self.tabController.addLeftbuttomWithController(withCustomView: btnBack)
        
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
       // self.navigationItem.setHidesBackButton(true, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionAddNot), name:AppNotifications.notificationMap, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionRuthVid), name:AppNotifications.notificationRuthVid, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionRuthKnows), name:AppNotifications.notificationRuthKnows, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionRuthStory), name: AppNotifications.notificationRuthStory, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionUserProfile(notification:)), name:AppNotifications.notificationUserProfile, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionBible), name:AppNotifications.notificationBible, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionVIPShare), name: AppNotifications.notificationVIPShare, object: nil)
        //         UserDefaults.standard.set(true, forKey: "isTutorialViewed")
        if UserNew.currentUser.isNewUser
        {
            let tutorialView = self.storyboard?.instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
            self.tabController.present(tutorialView, animated: true, completion: nil)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
         
        }
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setTabBarVisible(visible: true, animated: true)
    }
    //channels/getAll
    func getAllChannels() {
        let geturl = BaseURL + "/channels/getAll"
        //        Util.util.showHUD()
        Service.service.get(url: geturl) { (result) in
            print("getAllChannels ==\(result)" , "geturl = \(geturl) ")
            //            Util.util.hideHUD()
            self.arrHomeChannel.removeAll()
            if result is [String:Any] {
                let response = result as! [String:Any]
                if response["status"] as! Bool {
                    let arrdata = response["data"] as! [[String:Any]]
                    var arrayUpcoming = [HomeChannels]()
                    var arrayLive = [HomeChannels]()
                    for dict in arrdata {
                        let channels = HomeChannels(info: dict)
                        if channels.onAir == "live"{
                            arrayLive.append(channels)
                        }else{
                            arrayUpcoming.append(channels)
                        }
                        // self.arrHomeChannel.append(channels)
                    }
                    let dictEffect: [String : Any] = ["type":"Effect", "data":[Any]()]
                    self.arrHomeChannel.append(dictEffect)
                    
                    let dictLive: [String : Any] = ["type":"live", "data":arrayLive]
                    let dictRecomn: [String : Any]  = ["type":"Recommended", "data":[Any]()]
                    let dictUpcoming: [String : Any]  = ["type":"upcoming", "data":arrayUpcoming]
                    //                let dictRecomn: [String : Any]  = ["type":"Recommended", "data":arrayUpcoming]
                    if arrayLive.count > 0{
                        self.arrHomeChannel.append(dictLive)
                    }
                    self.arrHomeChannel.append(dictRecomn)
                    if arrayUpcoming.count > 0{
                        self.arrHomeChannel.append(dictUpcoming)
                    }
                    self.tblView.reloadData()
                } else {
                    let strMsg = Util.util.getMsgWithDict(response)
                    self.showAlertMessage(message: strMsg)
                }
                
            } else if result is Error
            {
                let error = result as! Error
                var strMsg = "Something went wrong".localized()
                strMsg = error.localizedDescription
                if strMsg == "The Internet connection appears to be offline."
                {
                    strMsg = "No internet connection".localized()
                }
                self.showAlertMessage(message: strMsg)
            }
        }
    }
    func getAdvertisements() {
        let Url = BaseURL + "/advertisements/getAll"
        Util.util.showHUD()
        self.homeBanner.removeAll()
        Service.service.get(url: Url) { (result) in
            //            print("advertisements  ==\(result)" , "Url $$$$  = \(Url) ")
            Util.util.hideHUD()
            if result is [String : Any] {
                let response = result as! [String:Any]
                if response["status"] as! Bool {
                    let arrdata = response["data"] as! [String]
                    for i in 0..<arrdata.count {
                        self.homeBanner.append(arrdata[i])
                    }
                    self.pagerControl.currentPage = 0
                    self.pagerControl.numberOfPages = self.homeBanner.count
                    self.pagerView.reloadData()
                }
                else {
                    let strMsg = Util.util.getMsgWithDict(response)
                    self.showAlertMessage(message: strMsg)
                }
            } else if result is Error
            {
                let error = result as! Error
                var strMsg = "Something went wrong".localized()
                strMsg = error.localizedDescription
                if strMsg == "The Internet connection appears to be offline."
                {
                    strMsg = "No internet connection".localized()
                }
                self.showAlertMessage(message: strMsg)
            }
        }
    }
    @objc func actionAddNot() {
        let stroryBord = UIStoryboard(name: "Main", bundle: nil)
        let myMapViewC = stroryBord.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(myMapViewC, animated: true)
    }
    @objc func actionRuthStory() {
        let myVC = RuthStoryViewC(nibName: "RuthStoryViewC", bundle: nil)
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    @objc func actionRuthVid() {
        let myVC = RuthVidViewC(nibName: "RuthVidViewC", bundle: nil)
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    @objc func actionRuthKnows() {
        let myRootVC = RootRKnowsViewC(nibName: "RootRKnowsViewC", bundle: nil)
        self.navigationController?.pushViewController(myRootVC, animated: true)
    }
    @objc func actionBible() {
        let myBibleVC = RootBibleViewC(nibName: "RootBibleViewC", bundle: nil)
        self.navigationController?.pushViewController(myBibleVC, animated: true)
    }
    @objc func actionVIPShare() {
        let myRootVC = RootVipShareVC(nibName: "RootVipShareVC", bundle: nil)
        self.navigationController?.pushViewController(myRootVC, animated: true)
    }
    @objc func actionUserProfile(notification:Notification) {
        let object = notification.object
        let stroryBord = UIStoryboard(name: "Main", bundle: nil)
        let myUserProfileVC = stroryBord.instantiateViewController(withIdentifier: "UserProfileViewC") as! UserProfileViewC
        myUserProfileVC.dataAllFollowU = object as! AllFollowU
        navigationController?.pushViewController(myUserProfileVC, animated: true)
    }
    func HomeRegisterCell()  {
        self.tblView.register(UINib(nibName: "HomeTblGridCell",  bundle: nil)
            , forCellReuseIdentifier: "HomeTblGridCell")
        self.tblView.register(UINib(nibName: "HomeTblCell",  bundle: nil)
            , forCellReuseIdentifier: "HomeTblCell")
        self.tblView.register(UINib(nibName: "HomeTblRecoCell",  bundle: nil)
            , forCellReuseIdentifier: "HomeTblRecoCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }

    @objc func actionMore(_ sender: UIButton) {
        let myVC = AllFollowUController(nibName: "AllFollowUController", bundle: nil)
        self.navigationController?.pushViewController(myVC, animated: true)
    }

    func HomeFSPagerView()  {
        self.pagerView.register(UINib(nibName: "HomeFSPagerCell", bundle: nil), forCellWithReuseIdentifier: "HomeFSPagerCell")
        self.pagerView.isInfinite = true
        self.pagerView.automaticSlidingInterval = 3//CGFloat(self.homeBanner.count)
        self.pagerControl.currentPage = 10
        self.pagerControl.itemSpacing = 10
        pagerControl.setPath(UIBezierPath(ovalIn: CGRect(x: 10, y: 10, width: 12, height: 12)), for: .normal)
        pagerControl.setPath(UIBezierPath(ovalIn: CGRect(x: 10, y: 10, width: 12, height: 12)), for: .selected)
        self.pagerControl.setFillColor(UIColor .hexStringToColor(hex: "00FF21"), for: UIControlState.selected)
    }
    func setupFeaturedvideos() {
        self.pagerControl.numberOfPages = self.homeBanner.count
        pagerView.delegate = self
        pagerView.dataSource = self
    }
    
    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {
        //        self.pagerControl.currentPage =
    }
    
    // MARK:- FSPagerViewDelegate, FSPagerViewDataSource METHOD ***** *** ***** **
    func numberOfItems(in pagerView:  FSPagerView) -> Int {
        return self.homeBanner.count
    }
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "HomeFSPagerCell", at: index) as! HomeFSPagerCell
        let banner = self.homeBanner[index]
        cell.imgPoster.sd_setImage(with: URL(string: banner), placeholderImage: nil)
        self.pagerControl.currentPage = index
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrHomeChannel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = self.arrHomeChannel[section]
        let dataArray = data["data"] as! [Any]
        let strTitle = data["type"] as! String
        if strTitle == "Effect" || strTitle ==  "Recommended"{
            return 1
        }else if strTitle == "live" || strTitle == "upcoming"{
            return dataArray.count
        }else
        {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.arrHomeChannel[indexPath.section]
        let dataArray = data["data"] as! [Any]
        let strTitle = data["type"] as! String
        if strTitle == "Effect"{
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "HomeTblGridCell", for: indexPath) as! HomeTblGridCell
            cell.selectionStyle = .none
            return cell
        }else if strTitle == "live" || strTitle ==  "upcoming"{
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "HomeTblCell", for: indexPath) as! HomeTblCell
            let data = dataArray[indexPath.row] as! HomeChannels
            cell.selectionStyle = .none
            cell.lblViewCout.text = String("\(data.views)")
            cell.lblLike.text = String("\(data.likes)")
            cell.setEmojis(data.emoji)
            //            cell.lblTime.text = String("\(data.likes)")
            cell.img.sd_setImage(with: URL(string: data.cover), placeholderImage: nil)
            cell.lblName.text = data.title
            cell.lblDetail.text = data.description
            var start = data.strStartTime
            let arr = start.components(separatedBy: ":")
            start = arr[0]
            var end = data.strEndTime
            end = end.replacingOccurrences(of:":0", with: "")
            cell.lblTime.text = start + "-" + end
            if data.onAir == "live"{
                cell.viewLine.dropShadow(color: .hexStringToColor(hex: "FF0000"), opacity: 1.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 7.0, scale: true)
            }else
            {
                cell.viewLine.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 7.0, scale: true)
            }
            cell.img.setCornerRadius(0.9)
            cell.selectionStyle = .none
            return cell
        }else if strTitle ==  "Recommended"
        {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "HomeTblRecoCell", for: indexPath) as! HomeTblRecoCell
            cell.selectionStyle = .none
            cell.btnMore.addTarget(self, action: #selector(actionMore(_:)), for: .touchUpInside)
            return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.arrHomeChannel[indexPath.section]
        let dataArray = data["data"] as! [HomeChannels]
        let strTitle = data["type"] as! String
        if strTitle == "Effect"{
        }else if strTitle == "live" {
            //|| strTitle ==  "upcoming"
        
            let channel = dataArray[indexPath.row]
            let myBroadcastDetailV = BroadcastDetailViewController(nibName: "BroadcastDetailViewController", bundle: nil)
            myBroadcastDetailV.homeChannel = channel
            self.navigationController?.pushViewController(myBroadcastDetailV, animated: true)
        }else if strTitle ==  "Recommended"
        {
            rLog("Recommended")
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //(UIScreen.main.bounds.width)/4
        let data = self.arrHomeChannel[indexPath.section]
        // let dataArray = data["data"] as! [Any]
        let strTitle = data["type"] as! String
        if strTitle == "Effect"{
             return 105
//           let height = (UIScreen.main.bounds.width)/2
//            return height*1
        }else if strTitle == "live" || strTitle ==  "upcoming"{
            return 123
        }else if strTitle ==  "Recommended"
        {
            return 180
        }
        return 1
    }
    @IBAction func actionNavigationBar(_ sender: UIButton) {
        let NaviIndex:NSInteger = sender.tag
        var x: CGFloat = 0
        self.btn1.isSelected = false
        self.btn1.isSelected = false
        switch (NaviIndex) {
        case 1:
            x = btnheight * 1
            self.btn1.isSelected = true
            break
        case 2:
            x = btnheight * 2
            self.btn2.isSelected = true
            //             NotificationCenter.default.post(name: AppNotifications.notificationHomeNavi1, object: nil)
            break
        case 3:
            x = btnheight * 3
            self.btn3.isSelected = true
            break
        default: break
        }
        if NaviIndex == 0 {
            UIView.animate(withDuration: 0.2, animations: {
                self.viewIndicator.frame = CGRect(x: x, y: 0, width: 30, height: 2)
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.viewIndicator.frame = CGRect(x: x, y: self.btnheight, width: 30, height: 2)
            })
        }
    }
    @IBAction func actionRank(_ btn : UIButton) {
        let myRootRantVC = RootRantVC(nibName: "RootRantVC", bundle: nil)
        self.navigationController?.pushViewController(myRootRantVC, animated: true)
    }
    @IBAction func actionSearch(_ btn : UIButton) {
//        let createdAt = "2018-10-18T06:01:10.511Z"
//        let currentTime = "2018-10-18T11:31:09.578Z"
//        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
//        let date2 = dateFormatter.date(from: createdAt)!
//        let date1 = dateFormatter.date(from: currentTime)!
//        let str = date2.offsetFrom(date : date1)
//        rLog("\(str)")
        
    }
    
}
