//
//  AllBroadcastViewC.swift
//  Ruth
//
//  Created by mac on 09/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class AllBroadcastViewC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var collectionAllB : UICollectionView!
    var allBoradcastChannel : Channel!
    var isForPastData = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.AllBroadcastRegister()
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
         self.navigationItem.title = "AllBroadcast".localized()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    func AllBroadcastRegister(){
      self.collectionAllB.register(UINib.init(nibName: "AllBroadcastCCell", bundle: nil), forCellWithReuseIdentifier: "AllBroadcastCCell")
        self.collectionAllB.delegate = self
        self.collectionAllB.dataSource = self
        self.collectionAllB.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isForPastData ? self.allBoradcastChannel.pastBroadcastArr.count : self.allBoradcastChannel.broadcastData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllBroadcastCCell", for: indexPath) as! AllBroadcastCCell
        let data : BroadcastChannels = isForPastData ? self.allBoradcastChannel.pastBroadcastArr[indexPath.row] : self.allBoradcastChannel.broadcastData[indexPath.row]
        
        cell.lblLikeCount.text = "\(data.broadcastLikes)"
        cell.imgProfile.sd_setImage(with: URL(string: data.broad_photoUrl), placeholderImage: nil)
        cell.imgBg.sd_setImage(with: URL(string: data.coverUrl), placeholderImage: nil)
        cell.lblViewCount.text = "\(data.broadcastViews)"
        cell.lblEmojiCount.text = "\(data.emoji)"
        cell.lblfans.text = "0 " + "名粉絲"
        cell.lblProfileN.text = data.broad_userName
        cell.setCornerRadius(7)
        if data.isLive {
            cell.scrollBg.dropShadow(color: .hexStringToColor(hex: "FF0000"), opacity: 1, offSet: CGSize(width: 0.0, height: 0.0), radius: 4, scale: true)
        } else {
            cell.scrollBg.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1, offSet: CGSize(width: 0.0, height: 0.0), radius:4, scale: true)
        }
        cell.btnFollow.tag = indexPath.item
        cell.btnFollow.addTarget(self, action: #selector(addButton(sender:)), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // let channel = self.allBoradcastChannel.broadcastData[indexPath.item]
        let channel : BroadcastChannels = isForPastData ? self.allBoradcastChannel.pastBroadcastArr[indexPath.row] : self.allBoradcastChannel.broadcastData[indexPath.row]
        let stroryBord = UIStoryboard(name: "Main", bundle: nil)
        let playerLiveViewController = stroryBord.instantiateViewController(withIdentifier: "PlayerLiveViewController") as! PlayerLiveViewController
        playerLiveViewController.channel = channel
        self.navigationController?.pushViewController(playerLiveViewController, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(UIScreen.main.bounds.width - 40)/2, height: (UIScreen.main.bounds.width - 40)/2)
    }
    
    @objc func addButton(sender: UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
