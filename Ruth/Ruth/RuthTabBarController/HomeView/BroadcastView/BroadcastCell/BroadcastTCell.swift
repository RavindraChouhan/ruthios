
//  BroadcastTCell.swift
//  Ruth
//
//  Created by mac on 18/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class BroadcastTCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var btnSeeAll : UIButton!
    @IBOutlet var lblBCount: UILabel!
    var boradcastChannel = [BroadcastChannels]()
    var isLiveBoradcast = false
    var selectedChannel : Channel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setDataOnCell(_ array :[BroadcastChannels],_ selectedChannel :Channel? = nil )  {
        self.selectedChannel = selectedChannel!
        self.boradcastChannel = array
     // rLog(array)
        self.lblBCount.text =  "參加者 " + "(\(self.boradcastChannel.count))"
        self.collectionView.register(UINib.init(nibName: "BroadcastCollCell", bundle: nil), forCellWithReuseIdentifier: "BroadcastCollCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.boradcastChannel.count >= 4 {
            return 4
        }else{
            return self.boradcastChannel.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BroadcastCollCell", for: indexPath) as! BroadcastCollCell
        let broadInfo = self.boradcastChannel[indexPath.item]
        cell.lblLikeCount.text = "\(broadInfo.broadcastLikes)"
//         cell.imgProfile.image = broadInfo.broad_photoUrl as? UIImage
        cell.imgProfile.sd_setImage(with: URL(string: broadInfo.broad_photoUrl), placeholderImage:nil)
        cell.imgBg.sd_setImage(with: URL(string: broadInfo.coverUrl), placeholderImage: nil)
        cell.lblViewCount.text = "\(broadInfo.broadcastViews)"
        cell.lblEmojiCount.text = "\(broadInfo.emoji)" 
        cell.lblfans.text = "\(broadInfo.broad_followers)"  + " 名粉絲"
        cell.lblProfileN.text = broadInfo.broad_userName
        cell.setCornerRadius(7)
        //"upcoming"
        if broadInfo.isLive {
            cell.scrollBg.dropShadow(color: .hexStringToColor(hex: "FF0000"), opacity: 1, offSet: CGSize(width: 0.0, height: 0.0), radius: 4, scale: true)
        } else {
            cell.scrollBg.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1, offSet: CGSize(width: 0.0, height: 0.0), radius:4, scale: true)
            
        }
         cell.btnFollow.tag = indexPath.item
         cell.btnFollow.addTarget(self, action: #selector(addButton(sender:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let broadInfo = self.boradcastChannel[indexPath.item]
        if broadInfo.isLive {
           NotificationCenter.default.post(name:  AppNotifications.notificationLoadPlayerViewController, object: broadInfo)
        } else {
           
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(UIScreen.main.bounds.width - 30)/2, height: (UIScreen.main.bounds.width - 30)/2)
    }
    
    @objc func addButton(sender: UIButton) {
        let boradChannel = BroadcastChannels()
        boradChannel.followAndUnfollow { (success) in
            sender.isSelected =  boradChannel.broad_isFollow
        }
        //sender.isSelected = !sender.isSelected
       
    }
//   func getFollowMobileUpdate() {
//     let parmeters:[String : Any] = ["type":"follow", "userId":UserNew.currentUser.id ,"followingId":boradcastChannel.broad_id ,"isFollow" : boradcastChannel.broad_isFollow == true ? "false" : "true" ]
//    rLog(parmeters)
//    Util.util.userMobileUpdateFollow(parametes: parmeters) { (result) in
//        if result is [String: Any]{
//            let response = result as! [String : Any]
//            if response["status"] as! Bool {
////                broadInfo.broad_isFollow = !broadInfo.broad_isFollow
//            }
//        }
//      }
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
