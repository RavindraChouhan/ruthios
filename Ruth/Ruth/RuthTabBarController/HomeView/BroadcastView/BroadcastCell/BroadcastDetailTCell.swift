//
//  BroadcastDetailTCell.swift
//  Ruth
//
//  Created by mac on 18/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class BroadcastDetailTCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblfans: UILabel!
    @IBOutlet weak var lblViewCount: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblProfileN: UILabel!
    @IBOutlet weak var lblEmojiCount: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var btnUpAro: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnEmoji: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnProfile.layer.borderColor = UIColor.hexStringToColor(hex:"FFFFFF").cgColor
        btnProfile.layer.borderWidth = 1
        imgProfile.setCornerRadius(20)
        btnProfile.setCornerRadius(7)
        self.lblAdd.setCornerRadius(8)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
