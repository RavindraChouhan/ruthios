//
//  BroadcastCollCell.swift
//  Ruth
//
//  Created by mac on 18/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class BroadcastCollCell: UICollectionViewCell {

    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var scrollBg: UIView!
    @IBOutlet weak var lblfans: UILabel!
    @IBOutlet weak var lblViewCount: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var lblProfileN: UILabel!
    @IBOutlet weak var lblEmojiCount: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgProfile.layer.borderColor = UIColor.hexStringToColor(hex:"FFFFFF").cgColor
        imgProfile.layer.borderWidth = 2
        imgProfile.setCornerRadius(16)
        imgBg.setCornerRadius(7)
    }

}
