//
//  BroadcastDetailViewController.swift
//  Ruth
//
//  Created by mac on 17/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class BroadcastDetailViewController: UIViewController,UITableViewDelegate ,UITableViewDataSource {
    var homeChannel : HomeChannels!
    var selectedChannel : Channel!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnSharing: UIButton!
    @IBOutlet var tblView : UITableView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgPoster: UIImageView!
    var isselected = false
    var indExpandable : CGFloat = 128
    var refreshControl: UIRefreshControl!
    var broadcastChannel : BroadcastChannels!
    var btntag = 0
    var isSub = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.getChannelsDetail()
        self.BroadRegisterCell()
        self.setTabBarVisible(visible: false, animated: true)
        self.lblName.text = homeChannel.title + "!"
        self.imgPoster.sd_setImage(with: URL(string: homeChannel.cover), placeholderImage:#imageLiteral(resourceName: "defaultProfilePicture"))
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectBroadcastChannel(_:)), name: AppNotifications.notificationLoadPlayerViewController, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.getChannelsDetail()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .clear
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //       self.setupNavigationBar()
    }
    func BroadRegisterCell() {
        self.tblView.register(UINib(nibName: "BroadcastTCell",  bundle: nil)
            , forCellReuseIdentifier: "BroadcastTCell")
        self.tblView.register(UINib(nibName: "BroadcastDetailTCell",  bundle: nil)
            , forCellReuseIdentifier: "BroadcastDetailTCell")
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    
    @objc func pulltorefresh(){
        tblView.reloadData()
        //        self.getSubscribeChannels()
        self.getChannelsDetail()
        refreshControl.endRefreshing()
    }
    @objc func actionAllBroadcast(sender: UIButton){
        let tag = sender.tag
        let myAllBroadcastVC = AllBroadcastViewC(nibName: "AllBroadcastViewC", bundle: nil)
        myAllBroadcastVC.allBoradcastChannel = self.selectedChannel
        myAllBroadcastVC.isForPastData = tag == 1 ? false : true
        self.navigationController?.pushViewController(myAllBroadcastVC, animated: true)
    }
    
    func getChannelsDetail() {
        let geturl = BaseURL + "/channels/" + self.homeChannel.id
        //         rLog("geturl  ==\(geturl)")
        Service.service.get(url: geturl) { (result) in
            //          rLog("Channelsresult  ==\(result)")
            if result is [String : Any] {
                let response = result as! [String:Any]
                if response["status"] as! Bool {
                    let arrdata = response["data"] as! [String:Any]
                    self.selectedChannel = Channel(info: arrdata)
                }
                self.tblView.delegate = self
                self.tblView.dataSource = self
                self.tblView.reloadData()
            }
        }
    }
    @objc func actionDetail(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            indExpandable = 240
        }else{
            indExpandable = 128
        }
        tblView.reloadData()
    }
    @objc func actionLike(sender : UIButton) {
        sender.isSelected = !sender.isSelected
        
        let parmeters:[String : Any] = ["type":"like", "userId":UserNew.currentUser.id ,"channelId":self.selectedChannel.id ,"isLike": self.selectedChannel.isLike ? false : true]
        rLog("getLikeChannels parmeters ==== \(parmeters)")
        self.homeChannel.getLikeChannels(parametes: parmeters) { (result) in
            if result is [String: Any]{
                let response = result as! [String : Any]
                rLog("getLikeChannels response ==== \(response)")
                if response["status"] as! Bool {
                    
                } else {
                    let strMsg = Util.util.getMsgWithDict(response)
                    self.showAlertMessage(message: strMsg)
                }
                self.tblView.reloadData()
            } else if result is Error
            {
                let error = result as! Error
                var strMsg = "Something went wrong".localized()
                strMsg = error.localizedDescription
                if strMsg == "The Internet connection appears to be offline."
                {
                    strMsg = "No internet connection".localized()
                }
                self.showAlertMessage(message: strMsg)
            }
        }
    }
    @objc func actionSubscribe(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let parmeters:[String : Any] = ["type":"subscribe", "userId":UserNew.currentUser.id ,"channelId":self.selectedChannel.id ,"isSubscribe": selectedChannel.isSubscribe ? false : true]
        rLog("getSubscribeChannels parmeters ==== \(parmeters)")
        self.homeChannel.getLikeChannels(parametes: parmeters) { (result) in
            if result is [String: Any]{
                let response = result as! [String : Any]
                rLog("getSubscribeChannels response ==== \(response)")
                if response["status"] as! Bool {
                    self.tblView.reloadData()
                }
                
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.selectedChannel.pastBroadcastArr.count > 0 && self.selectedChannel.broadcastData.count > 0{
            return 3
        }else if self.selectedChannel.broadcastData.count > 0 && self.selectedChannel.pastBroadcastArr.count == 0 {
            return 2
        }else if self.selectedChannel.broadcastData.count == 0 && self.selectedChannel.pastBroadcastArr.count > 0 {
            return 2
        }else if self.selectedChannel.broadcastData.count == 0 && self.selectedChannel.pastBroadcastArr.count == 0 {
            return 1
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier:
                "BroadcastDetailTCell", for: indexPath) as! BroadcastDetailTCell
            cell.lblName.text = self.selectedChannel.title + " !"
            cell.lblProfileN.text = self.selectedChannel.title 
            cell.lblEmojiCount.text = "\(self.selectedChannel.emoji)"
            cell.lblLikeCount.text = "\(self.selectedChannel.likes)"
            cell.lblfans.text = "\(self.selectedChannel.subscribers)" + " 名粉絲"
            cell.lblViewCount.text = "\(self.selectedChannel.totalViews)" + " views"
            cell.lblDetail.text = self.selectedChannel.description
            cell.imgProfile.sd_setImage(with: URL(string: self.selectedChannel.profile), placeholderImage: nil)
            var start = self.selectedChannel.strStartTime
            let arr = start.components(separatedBy: ":")
            start = arr[0]
            var end = self.selectedChannel.strEndTime
            end = end.replacingOccurrences(of:":0", with: "")
            cell.lblTime.text = start + "-" + end
            cell.selectionStyle = .none
            cell.btnUpAro.addTarget(self, action: #selector(actionDetail(sender:)), for: .touchUpInside)
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(actionLike(sender:)), for: .touchUpInside)
            cell.btnLike.isSelected = selectedChannel.isLike
            cell.btnProfile.tag = indexPath.row
            cell.btnProfile.addTarget(self, action: #selector(actionSubscribe(sender:)), for: .touchUpInside)
            //            cell.btnProfile.isSelected = selectedChannel.isSubscribe
            return cell
        }
        else {
            var broadcastData = [BroadcastChannels]()
            if indexPath.row == 1 {
                if self.selectedChannel.broadcastData.count > 0 {
                    //                    broadcastData = self.selectedChannel.broadcastData
                    
                    var liveArr = [BroadcastChannels]()
                    var pastArr = [BroadcastChannels]()
                    for tmp in self.selectedChannel.broadcastData {
                        if tmp.isLive {
                            liveArr.append(tmp)
                        }
                        else {
                            pastArr.append(tmp)
                        }
                    }
                    broadcastData = liveArr + pastArr
                }
                else {
                    broadcastData = self.selectedChannel.pastBroadcastArr
                }
            }
            else {
                broadcastData = self.selectedChannel.pastBroadcastArr
            }
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "BroadcastTCell", for: indexPath) as! BroadcastTCell
            let broadcastInfo = broadcastData
            cell.btnSeeAll.tag = indexPath.row
            cell.btnSeeAll.addTarget(self, action: #selector(actionAllBroadcast(sender:)), for: .touchUpInside)
            cell.selectionStyle = .none
            cell.setDataOnCell(broadcastInfo, self.selectedChannel)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return indExpandable
        }
        if indexPath.row == 1{
            if self.selectedChannel.broadcastData.count > 0{
                if self.selectedChannel.broadcastData.count == 0{
                    return  0
                }else if self.selectedChannel.broadcastData.count >= 3 {
                    let boxSize = (UIScreen.main.bounds.width - 30)/2
                    return boxSize + boxSize + 60
                }else if self.selectedChannel.broadcastData.count > 0 && self.selectedChannel.broadcastData.count < 3{
                    let boxSize = (UIScreen.main.bounds.width - 30)/2
                    return boxSize + 40
                }else{
                    return  0
                }
            }else{
                if self.selectedChannel.pastBroadcastArr.count == 0{
                    return  0
                }else if self.selectedChannel.pastBroadcastArr.count >= 3 {
                    let boxSize = (UIScreen.main.bounds.width - 30)/2
                    return boxSize + boxSize + 60
                }else if self.selectedChannel.pastBroadcastArr.count > 0 && self.selectedChannel.pastBroadcastArr.count < 3{
                    let boxSize = (UIScreen.main.bounds.width - 30)/2
                    return boxSize + 40
                }else{
                    return  0
                }
            }
        }else{
            if self.selectedChannel.pastBroadcastArr.count == 0{
                return  0
            }else if self.selectedChannel.pastBroadcastArr.count >= 3 {
                let boxSize = (UIScreen.main.bounds.width - 30)/2
                return boxSize + boxSize + 60
            }else if self.selectedChannel.pastBroadcastArr.count > 0 && self.selectedChannel.pastBroadcastArr.count < 3{
                let boxSize = (UIScreen.main.bounds.width - 30)/2
                return boxSize + 40
            }else{
                return  0
            }
        }
    }
    @objc func didSelectBroadcastChannel(_ notification : Notification) {
        let broadcastChannel = notification.object as!  BroadcastChannels
        let stroryBord = UIStoryboard(name: "Main", bundle: nil)
        let playerLiveViewController = stroryBord.instantiateViewController(withIdentifier: "PlayerLiveViewController") as! PlayerLiveViewController
        playerLiveViewController.channel = broadcastChannel
        self.navigationController?.pushViewController(playerLiveViewController, animated: true)
    }
    
    @IBAction func actionSharing(_ btn : UIButton) {
        self.showToast(message: "actionSharing")
    }
}
