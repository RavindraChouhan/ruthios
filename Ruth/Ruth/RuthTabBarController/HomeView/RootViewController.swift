//
//  RootViewController.swift
//  Ruth
//
//  Created by mac on 17/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import SwipeViewController

class RootViewController: SwipeViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//直播,影⽚,音頻
        let VC1 = UIViewController()
        VC1.view.backgroundColor = UIColor(red: 0.19, green: 0.36, blue: 0.60, alpha: 1.0)
        VC1.title = "直播"
        let VC2 = UIViewController()
        VC2.view.backgroundColor = UIColor(red: 0.70, green: 0.23, blue: 0.92, alpha: 1.0)
        VC2.title = "影⽚"
        let VC3 = UIViewController()
        VC3.view.backgroundColor = UIColor(red: 0.17, green: 0.70, blue: 0.27, alpha: 1.0)
        VC3.title = "音頻"
        
        setViewControllerArray([VC1, VC2, VC3])
        setFirstViewController(0)
        setSelectionBar(80, height: 3, color: UIColor(red: 0.23, green: 0.55, blue: 0.92, alpha: 1.0))
        setButtonsWithSelectedColor(UIFont.systemFont(ofSize: 18), color: UIColor.black, selectedColor: UIColor(red: 0.23, green: 0.55, blue: 0.92, alpha: 1.0))
        equalSpaces = false
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(push))
        setNavigationWithItem(UIColor.green, leftItem: barButtonItem, rightItem: nil)
    }
    @objc func push(sender: UIBarButtonItem) {
        let VC4 = UIViewController()
        VC4.view.backgroundColor = UIColor.purple
        VC4.title = "Cool"
        self.pushViewController(VC4, animated: true)
    }
}
