//
//  MapViewController.swift
//  Ruth
//
//  Created by mac on 26/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class MapViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate {
    
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet var collectionMap : UICollectionView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewRight:UIView!
    @IBOutlet var mapView : GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addRightbuttomWithController(withCustomView: self.viewRight)
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        self.collectionMap.register(UINib.init(nibName: "MapCollCell", bundle: nil), forCellWithReuseIdentifier: "MapCollCell")
        self.collectionMap.delegate = self
        self.collectionMap.dataSource = self
        self.collectionMap.reloadData()
       self.loadMapView()
        // Do any additional setup after loading the view.
    }
    func loadMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: currentLatitude, longitude: currentLongitude, zoom: 11)
        self.mapView.camera = camera
        self.mapView.isMyLocationEnabled = true
        self.mapView.mapStyle = objAppDelegate.mapStyle
        self.mapView.settings.myLocationButton = true
        let marker = GMSMarker()
        marker.icon = UIImage(named: "teamwork.png")
        marker.map = self.mapView
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.setTabBarVisible(visible: false, animated: true)
    }
 
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionFilter(_ sender: Any) {
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionMap.dequeueReusableCell(withReuseIdentifier: "MapCollCell", for: indexPath) as! MapCollCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(UIScreen.main.bounds.width - 30)/3, height: (UIScreen.main.bounds.height - 185)/3)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
