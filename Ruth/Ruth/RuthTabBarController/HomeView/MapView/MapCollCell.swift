//
//  MapCollCell.swift
//  Ruth
//
//  Created by mac on 26/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class MapCollCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblAdd: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
         self.viewBg.setCornerRadius(7)
         self.img.setCornerRadius(7)
         self.viewBg.dropShadow(color: .hexStringToColor(hex: "979797"), opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius:4, scale: true)
         self.img.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius:4, scale: true)
        // Initialization code
    }

}
