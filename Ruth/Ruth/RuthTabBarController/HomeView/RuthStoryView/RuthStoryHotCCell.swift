//
//  RuthStoryHotCCell.swift
//  Ruth
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RuthStoryHotCCell: UICollectionViewCell {

    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var viewImgBg: UIView!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var btnPlya: UIButton!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewImgBg.setCornerRadius(7)
        self.imgBg.setCornerRadius(7)
        
        // Initialization code
    }

}
