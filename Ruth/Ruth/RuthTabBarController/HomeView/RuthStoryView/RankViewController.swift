//
//  RankViewController.swift
//  Ruth
//
//  Created by mac on 17/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit 

class RankViewController: UIViewController,UITableViewDelegate ,UITableViewDataSource {
  
    @IBOutlet var tblView : UITableView!
    @IBOutlet var btnBack : UIButton!
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
     self.navigationController?.isNavigationBarHidden = false
     self.RantRegisterCell()
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: true)
        
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    func RantRegisterCell () {
        self.tblView.register(UINib(nibName: "RankTCell",  bundle: nil)
            , forCellReuseIdentifier: "RankTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
  

    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier:
            "RankTCell", for: indexPath) as! RankTCell
        cell.lblViewCount.text = "0" + " 名粉絲"
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 60
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
