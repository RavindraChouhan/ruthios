//
//  RuthVidTCell.swift
//  Ruth
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RuthVidHotTCell: UITableViewCell,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var lblHot: UILabel!
    @IBOutlet var lblLatest: UILabel!
    @IBOutlet var btnHottest: UIButton!
    @IBOutlet var btnLatest: UIButton!
    @IBOutlet var lblHmore: UILabel!
    @IBOutlet var lblLmore: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblHot.text = "Hottest".localized()
        self.lblLatest.text = "Latest".localized()
        self.lblHmore.text = "more".localized()
        self.lblLmore.text = "more".localized()
       
        self.collectionView.register(UINib.init(nibName: "RuthVidHotCCell", bundle: nil), forCellWithReuseIdentifier: "RuthVidHotCCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
        // Initialization code
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RuthVidHotCCell", for: indexPath) as! RuthVidHotCCell
       cell.viewImgBg.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius: 5.0, scale: true)
//        switch indexPath.item {
//        case 0,1:
//            cell.scrollBg.dropShadow(color: .hexStringToColor(hex: "FF0000"), opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius: 4, scale: true)
//            cell.setCornerRadius(7)
//        case 2,3:
//            cell.scrollBg.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 0.5, offSet: CGSize(width: 0.0, height: 0.0), radius:4, scale: true)
//            cell.setCornerRadius(7)
//        default:
//            print("")
//        }
//        cell.btnFollow.addTarget(self, action: #selector(addButton(sender:)), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 130)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
