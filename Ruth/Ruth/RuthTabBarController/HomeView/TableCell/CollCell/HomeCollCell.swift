//
//  HomeCollCell.swift
//  Ruth
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class HomeCollCell: UICollectionViewCell {

    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblAddess: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lbl123: UILabel!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var lblBorn: UILabel!
    @IBOutlet weak var lblFan: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewLine.layer.borderColor = UIColor.hexStringToColor(hex:"D0021B").cgColor
        viewLine.layer.borderWidth = 1
        // Initialization code
    }
}
class HomeCardCollCell: UICollectionViewCell {
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewLine.layer.borderColor = UIColor.hexStringToColor(hex:"D0021B").cgColor
        viewLine.layer.borderWidth = 2
    }
}
class CollGridCell: UICollectionViewCell {
    @IBOutlet weak var imgPoster : UIImageView!
    @IBOutlet weak var imgTitle : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var viewSelected: UIView!
    @IBOutlet weak var viewFill: UIView!
    @IBOutlet weak var imgFillPoster : UIImageView!
    @IBOutlet weak var lblFillTitle : UILabel!
    @IBOutlet weak var viewHorizoFillR: UIView!
    @IBOutlet weak var viewHorizoUR: UIView!
    @IBOutlet weak var viewHorizoFillUp: UIView!
    @IBOutlet weak var viewHorizoUDo: UIView!
    @IBOutlet weak var viewVL: UIView!
    @IBOutlet weak var viewVUR: UIView!
    @IBOutlet weak var viewVUp: UIView!
    @IBOutlet weak var viewVUDo: UIView!

        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
  }
}
