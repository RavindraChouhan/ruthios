//
//  HomeTblRecoCell.swift
//  Ruth
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class HomeTblRecoCell: UITableViewCell ,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var  lblRecommended: UILabel!
    @IBOutlet var btnMore : UIButton!
    var controller :HomeViewController!
    var arrFollowUsers = [AllFollowU]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //MMMM d, yyyy
        //September 18, 2018
        self.getAllfollowUsersHome()
        self.lblRecommended.text = "Recommended anchor".localized()
        self.btnMore.setTitle("More".localized(), for: .normal)
        collectionView.register(UINib.init(nibName: "HomeCollCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
        // Initialization code
    }
    
//    func getFollowUsers() {
//        NetworkClient.shared.getFollowUser { (followUserArr, error) -> (Void) in
//            guard error == nil else {
//                //error msg alert
//                return
//            }
//            if let followUsers = followUserArr  as! [String:Any]{
//                self.dataFollowU = followUsers
//                //self.reload
//            }
//        }
//    }
//
    //MARK:- AllfollowUsersHome Api
    func getAllfollowUsersHome() {
        let Url = BaseURL + followUsersHome
        Util.util.showHUD()
        Service.service.get(url: Url) { (result) in
            //print("followUsers  ==\(result)" , "Url followUsers  = \(Url) ")
            Util.util.hideHUD()
            self.arrFollowUsers.removeAll()
            if result is [String:Any] {
                let response = result as! [String:Any]
                if response["status"] as! Bool {
                    let data = response["data"] as! [[String:Any]]
                    for dict in data {
                        let follow = AllFollowU(info: dict)
                        self.arrFollowUsers.append(follow)
                    }
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.reloadData()
                } else {
                    let strMsg = Util.util.getMsgWithDict(response)
                    self.controller.showAlertMessage(message: strMsg)
                }
                
            } else if result is Error
            {
                let error = result as! Error
                var strMsg = "Something went wrong".localized()
                strMsg = error.localizedDescription
                if strMsg == "The Internet connection appears to be offline."
                {
                    strMsg = "No internet connection".localized()
                }
                self.controller.showAlertMessage(message: strMsg)
                
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFollowUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollCell", for: indexPath) as! HomeCollCell
        let data = arrFollowUsers[indexPath.row]
        cell.lblName.text = data.name
        cell.img.sd_setImage(with: URL(string: data.photoUrl as String), placeholderImage:nil)
        cell.lblFollowers.text = "\(data.followers)"
        if data.gender == "male" {
            cell.lblMale.text = "男"
        }
        if data.gender == "female" {
            cell.lblMale.text = "女"
        }
//        cell.lblAddess.text = data.address
        cell.dropShadow(color: .hexStringToColor(hex: "555555"), opacity: 0.45, offSet: CGSize(width: 2, height: 2), radius: 0.0, scale: true)
        cell.btnFollow.tag = indexPath.row
        cell.btnFollow.isSelected = data.isFollow
        cell.btnFollow.addTarget(self, action: #selector(addButton(sender:)), for: .touchUpInside)
//        let strMonth = Util.util.getFormatedDateWithRequiredFormat(strFormat: "MMMM ", withDate: data.dob)
//        let strDay = Util.util.getFormatedDateWithRequiredFormat(strFormat: "d", withDate: data.dob)
//        cell.lblData.text = Util.util.changeChineseMonth(strMonth) + Util.util.changeChineseDay(strDay) + " 日"
//        let strYear = Util.util.getFormatedDateWithRequiredFormat(strFormat: "yyyy", withDate: data.dob)
//        cell.lblBorn.text = "生於" + Util.util.changeChineseYear(strYear) + "年"
        return cell
    }
    @objc func addButton(sender: UIButton) {
        let data = self.arrFollowUsers[sender.tag]
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.setImage(#imageLiteral(resourceName: "btncheck"), for: UIControlState.normal)
            sender.setTitle("", for: UIControlState.normal)
            sender.backgroundColor = .white
            sender.alpha = 1.0
        }else{
            sender.setImage(UIImage(), for: UIControlState.normal)
            sender.setTitle("關注", for: UIControlState.normal)
            sender.backgroundColor = .black
            sender.alpha = 0.7
            //
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: AppNotifications.notificationUserProfile, object: self.arrFollowUsers[indexPath.item])
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:106, height: 139)
    }
    func pushView(controller:HomeViewController) {
        self.controller = controller
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
