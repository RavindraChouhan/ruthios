//
//  HomeFSPagerFeaturedCell.swift
//  Ruth
//
//  Created by mac on 17/03/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class HomeFSPagerCell: FSPagerViewCell {

    @IBOutlet weak var imgPoster : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnPlay : UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
      
    }
 
}
