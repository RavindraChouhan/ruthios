//
//  HomeTblCell.swift
//  Ruth
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class HomeTblCell: UITableViewCell {

    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblViewCout: UILabel!
    @IBOutlet weak var imgEmoji1: UIImageView!
    @IBOutlet weak var imgEmoji2: UIImageView!
    @IBOutlet weak var imgEmoji3: UIImageView!
    @IBOutlet weak var imgEmoji4: UIImageView!
    @IBOutlet weak var imgEmoji5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      self.lblAdd.setCornerRadius(8)
        // Initialization code //730000 , FF0000
        //1C7300 , 7FFF00
    }
    
    func setEmojis(_ count : Int) {
        switch count {
        case 1:
            imgEmoji1.isHidden = false
            imgEmoji2.isHidden = true
            imgEmoji3.isHidden = true
            imgEmoji4.isHidden = true
            imgEmoji5.isHidden = true
        case 2:
            imgEmoji1.isHidden = false
            imgEmoji2.isHidden = false
            imgEmoji3.isHidden = true
            imgEmoji4.isHidden = true
            imgEmoji5.isHidden = true
        case 3:
            imgEmoji1.isHidden = false
            imgEmoji2.isHidden = false
            imgEmoji3.isHidden = false
            imgEmoji4.isHidden = true
            imgEmoji5.isHidden = true
        case 4:
            imgEmoji1.isHidden = false
            imgEmoji2.isHidden = false
            imgEmoji3.isHidden = false
            imgEmoji4.isHidden = false
            imgEmoji5.isHidden = true
        case 5:
            imgEmoji1.isHidden = false
            imgEmoji2.isHidden = false
            imgEmoji3.isHidden = false
            imgEmoji4.isHidden = false
            imgEmoji5.isHidden = false
        default:
             imgEmoji1.isHidden = true
             imgEmoji2.isHidden = true
             imgEmoji3.isHidden = true
             imgEmoji4.isHidden = true
             imgEmoji5.isHidden = true
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
class HomeTblCell2: UITableViewCell {
    
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblViewCout: UILabel!
    @IBOutlet weak var imgEmoji1: UIImageView!
    @IBOutlet weak var imgEmoji2: UIImageView!
    @IBOutlet weak var imgEmoji3: UIImageView!
    @IBOutlet weak var imgEmoji4: UIImageView!
    @IBOutlet weak var imgEmoji5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblAdd.setCornerRadius(8)
        // Initialization code //730000 , FF0000
        //1C7300 , 7FFF00
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class HomeTblCell3: UITableViewCell {
    
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblViewCout: UILabel!
    @IBOutlet weak var imgEmoji1: UIImageView!
    @IBOutlet weak var imgEmoji2: UIImageView!
    @IBOutlet weak var imgEmoji3: UIImageView!
    @IBOutlet weak var imgEmoji4: UIImageView!
    @IBOutlet weak var imgEmoji5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblAdd.setCornerRadius(8)
        // Initialization code //730000 , FF0000
        //1C7300 , 7FFF00
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class HomeTblCell5: UITableViewCell {
    
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblViewCout: UILabel!
    @IBOutlet weak var imgEmoji1: UIImageView!
    @IBOutlet weak var imgEmoji2: UIImageView!
    @IBOutlet weak var imgEmoji3: UIImageView!
    @IBOutlet weak var imgEmoji4: UIImageView!
    @IBOutlet weak var imgEmoji5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblAdd.setCornerRadius(8)
    }
}
