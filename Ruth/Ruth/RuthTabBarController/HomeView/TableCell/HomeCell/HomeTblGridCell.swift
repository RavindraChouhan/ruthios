//
//  HomeTblGridCell.swift
//  Ruth
//
//  Created by mac on 04/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import SDWebImage
class HomeTblGridCell: UITableViewCell ,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet var collectionView : UICollectionView!
    var arrHomeGridNew = [HomeGrid]()
    var imgArr: [String]!
    var controller :HomeViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgArr = ["Haunting Hour", "Walk ‘n’ Hunt", "RuthVid", "RuthStory", "Ruthknows", "Sit ‘n’ Talk", "Bible", "VIP Share"]
        self.getAllEffects()
        self.collectionView.register(UINib.init(nibName: "CollGridCell", bundle: nil), forCellWithReuseIdentifier: "CollGridCell")
    }
    //MARK:  effects/getAll 8button Api
    func getAllEffects() {
        let Url = BaseURL + "/effects/getAll"
        Util.util.showHUD()
        Service.service.get(url: Url) { (result) in
            print("getAllEffects  ==\(result)" , "Url %%%%%%%  = \(Url) ")
            Util.util.hideHUD()
            self.arrHomeGridNew.removeAll()
            if result is [String:Any] {
                let response = result as! [String:Any]
                if response["status"] as! Bool {
                    let data = response["data"] as! [[String:Any]]
                    for dict in data {
                        let homeGird = HomeGrid(info: dict)
                        self.arrHomeGridNew.append(homeGird)
                    }
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.reloadData()
                } else {
                    let strMsg = Util.util.getMsgWithDict(response)
                    self.controller.showAlertMessage(message: strMsg)
                }
                
            } else if result is Error
            {
                let error = result as! Error
                var strMsg = "Something went wrong".localized()
                strMsg = error.localizedDescription
                if strMsg == "The Internet connection appears to be offline."
                {
                    strMsg = "No internet connection".localized()
                }
                self.controller.showAlertMessage(message: strMsg)
                
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrHomeGridNew.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollGridCell", for: indexPath) as! CollGridCell
//            switch indexPath.row  {
//            case 3:
//                //cell.viewVUp.isHidden = true
//             //   cell.view.isHidden = true
//                break
//            case 4,5,6:
//    //            cell.imgHorizoU.isHidden = true
//    //            cell.imgHorizoFill.isHidden = true
//                break
//            case 7:
//    //            cell.view.isHidden = true
//    //            cell.imgVU.isHidden = true
//    //            cell.imgHorizoU.isHidden = true
//    //            cell.imgHorizoFill.isHidden = true
//                break
//            default:
//                break
//            }
        let homeGData = arrHomeGridNew[indexPath.item]
        cell.lblFillTitle.text = homeGData.chineseSimTitle
        cell.lblTitle.text = homeGData.chineseSimTitle
        cell.imgFillPoster.sd_setImage(with: URL(string: homeGData.imgSelected), placeholderImage:#imageLiteral(resourceName: "defaultProfilePicture"))
        cell.imgPoster.sd_setImage(with: URL(string: homeGData.imgUnSelected), placeholderImage:#imageLiteral(resourceName: "defaultProfilePicture"))
        let img = self.imgArr[indexPath.item]
        cell.imgTitle.image = UIImage.init(named: img)
        cell.viewSelected.isHidden = homeGData.isSelected ? true : false
        cell.viewFill.isHidden = homeGData.isSelected ? false : true
        
        return cell
    }
    
    var indeexx = -1
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0..<arrHomeGridNew.count {
            let data = arrHomeGridNew[i]
            if i == indexPath.item {
                data.isSelected = !data.isSelected
                if data.isSelected {
                    self.collectionView.reloadData()
                    switch indexPath.item {
                    case 0:
                     NotificationCenter.default.post(name:AppNotifications.notificationMap, object: nil)
                        break
                    case 1:
                        NotificationCenter.default.post(name:AppNotifications.notificationMap, object: nil)
                        break
                    case 2:
                        NotificationCenter.default.post(name:AppNotifications.notificationRuthVid, object: nil)
                        break
                    case 3:
                        NotificationCenter.default.post(name:AppNotifications.notificationRuthStory, object: nil)
                        break
                    case 4:
                        NotificationCenter.default.post(name:AppNotifications.notificationRuthKnows, object: nil)
                        break
                    case 5:
                        NotificationCenter.default.post(name:AppNotifications.notificationRuthVid, object: nil)
                        break
                    case 6:
                        NotificationCenter.default.post(name:AppNotifications.notificationBible, object: nil)
                        break
                    case 7:
                        NotificationCenter.default.post(name:AppNotifications.notificationVIPShare, object: nil)
                        break
                    default:
                        break
                    }
                }
            }else{
                data.isSelected = false
            }
        }
        self.collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(UIScreen.main.bounds.width)/2, height:103)//CGSize(width:(UIScreen.main.bounds.width)/2, height: (UIScreen.main.bounds.width)/2)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        func pushView(controller:HomeViewController) {
            self.controller = controller
        }
}
