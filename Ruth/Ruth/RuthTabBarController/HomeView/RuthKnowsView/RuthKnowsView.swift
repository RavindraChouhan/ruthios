//
//  RuthKnowsView.swift
//  Ruth
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RuthKnowsView: UIViewController,FSPagerViewDelegate,FSPagerViewDataSource ,UITableViewDataSource ,UITableViewDelegate {
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var tblView : UITableView!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
//    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var pagerControl: FSPageControl!
    var arrRuthSB = [RuthStoryBanner]()
//    var arrRuthSB = [#imageLiteral(resourceName: "ads")]
    override func viewDidLoad() {
        super.viewDidLoad()
        let ab = RuthStoryBanner(imgTitlee: "ads.png", isSelectedd: true)
        arrRuthSB.append(ab)
        
        self.RuthVidRegisterCell()
        self.RuthVidFSPagerView()
        self.setupRuthVid()
    }
    func RuthVidRegisterCell()  {
         self.setTabBarVisible(visible: false, animated: true)
        self.tblView.register(UINib(nibName: "RuthKnowsTCell",  bundle: nil)
            , forCellReuseIdentifier: "RuthKnowsTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func RuthVidFSPagerView()  {
        self.pagerView.register(UINib(nibName: "HomeFSPagerCell", bundle: nil), forCellWithReuseIdentifier: "HomeFSPagerCell")
        self.pagerView.isInfinite = true
        //        self.pagerView.automaticSlidingInterval = 3
        self.pagerControl.currentPage = 0
        self.pagerControl.setFillColor(UIColor .hexStringToColor(hex: "00FF21"), for: UIControlState.selected)
    }
    func setupRuthVid() {
        self.pagerControl.numberOfPages = self.arrRuthSB.count
        pagerView.delegate = self
        pagerView.dataSource = self
        self.pagerView.reloadData()
    }
    
    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {
        //        self.pagerControl.currentPage =
    }
    
    // MARK:- FSPagerViewDelegate, FSPagerViewDataSource METHOD ***** *** ***** **
    func numberOfItems(in pagerView:  FSPagerView) -> Int {
        return self.arrRuthSB.count
    }
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "HomeFSPagerCell", at: index) as! HomeFSPagerCell
        let data = self.arrRuthSB[index]
        cell.imgPoster.image = UIImage.init(named: data.imgTitle)

//        cell.imgPoster.sd_setImage(with: URL(string: banner), placeholderImage: nil)
        self.pagerControl.currentPage = index
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "RuthKnowsTCell", for: indexPath) as! RuthKnowsTCell
            cell.selectionStyle = .none
           cell.viewImgBg.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 1.9, scale: true)
            return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myRDetailVC = RuthKDetailViewC(nibName: "RuthKDetailViewC", bundle: nil)
        self.navigationController?.pushViewController(myRDetailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //(UIScreen.main.bounds.width)/4
        return 314
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
