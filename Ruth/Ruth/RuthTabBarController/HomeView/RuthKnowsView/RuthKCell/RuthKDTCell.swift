//
//  RuthKDTCell.swift
//  Ruth
//
//  Created by mac on 18/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RuthKDTCell: UITableViewCell {
    @IBOutlet var btnSeeAll : UIButton!
    //68C700
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @objc func addButton(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
        if button.isSelected {
            button.setImage(#imageLiteral(resourceName: "btnCheckWhite"), for: UIControlState.normal)
            button.setTitle("", for: UIControlState.normal)
//                button.backgroundColor = UIColor.hexStringToColor(hex: "00FF08")
//            button.alpha = 1.0
        }else{
            button.setImage(UIImage(), for: UIControlState.normal)
            button.setTitle("關注", for: UIControlState.normal)
            button.backgroundColor = UIColor.black
            button.alpha = 1.0
            //
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
