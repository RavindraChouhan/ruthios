//
//  RuthKnowsTCell.swift
//  Ruth
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RuthKnowsTCell: UITableViewCell{
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var viewImgBg: UIView!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var btnlike: UIButton!
    @IBOutlet weak var btnOnlie: UIButton!
    @IBOutlet weak var btnShr: UIButton!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var viewline: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblAdd.setCornerRadius(8)
        viewImgBg.setCornerRadius(7)
        imgBg.setCornerRadius(7)
        viewline.layer.borderColor = UIColor.hexStringToColor(hex:"4A4A4A").cgColor
        viewline.layer.borderWidth = 2
     
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
