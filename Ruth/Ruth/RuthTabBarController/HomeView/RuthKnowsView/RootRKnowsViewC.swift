//
//  RootRKnowsViewC.swift
//  Ruth
//
//  Created by mac on 31/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import CarbonKit

class RootRKnowsViewC: UIViewController,CarbonTabSwipeNavigationDelegate {

    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    var categoryNames = [String]()
    var categoryID = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftbuttomWithController(withCustomView: btnBack)
        self.addRightbuttomWithController(withCustomView: btnFilter)
         self.categoryNames = ["全部","圖片","文章", "圖片+文章"]
        self.setupTopBarAndNavigation()
        self.setupNavigationBar()
        self.navigationItem.title = "RuthKnows".localized()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setupTopBarAndNavigation() {
//         tabSwipe.setupCarbonPages(carbonTabSwipeNavigation, tabTitles: menuTabTitles, totalWidth: self.view.frame.width).insertIntoRootViewController(self)
        let tabSwipe = CarbonTabSwipeNavigation(items: self.categoryNames,delegate: self)
        tabSwipe.setNormalColor(UIColor.black, font: UIFont(name: ".SFUIText-Medium", size: 15)!)
        tabSwipe.setSelectedColor(UIColor.white, font: UIFont(name: ".SFUIText-Medium", size: 15)!)
        tabSwipe.carbonTabSwipeScrollView.backgroundColor = .green//UIColor.hexStr(hexStr: "f0f0f0")
        tabSwipe.setIndicatorColor(UIColor.white)
    //    tabSwipe.carbonSegmentedControl?.setWidth(4, forSegmentAt: 0)
        tabSwipe.pagesScrollView?.isScrollEnabled = true
        tabSwipe.setTabExtraWidth(40)
        tabSwipe.setTabBarHeight(26)
        tabSwipe.carbonSegmentedControl?.backgroundColor = .green
        tabSwipe.insert(intoRootViewController: self)
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
            let VC = RuthKnowsView(nibName: "RuthKnowsView", bundle: nil)
//            VC.myHomeArr = self.myHomeData
            return VC
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
