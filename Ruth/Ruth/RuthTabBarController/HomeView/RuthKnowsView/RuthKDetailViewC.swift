//
//  RuthKDetailViewC.swift
//  Ruth
//
//  Created by mac on 17/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RuthKDetailViewC: UIViewController,FSPagerViewDelegate,FSPagerViewDataSource,UITableViewDelegate ,UITableViewDataSource {
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnSharing: UIButton!
    @IBOutlet var btnMenu: UIButton!
    var actionButton: ActionButton!
     @IBOutlet var btnGift: UIButton!
     @IBOutlet var btnSms: UIButton!
    @IBOutlet var tblView : UITableView!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var pagerControl: FSPageControl!
    var isselected = false
    var indExpandable : CGFloat = 128
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.DetailRegisterCell()
        self.setupRuthKD()
        self.DetailFSPagerView()
//        self.acGift()
        //        tblView.estimatedRowHeight = 128
        self.setTabBarVisible(visible: false, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
//        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.popViewController(animated: true)
    }
    
    func DetailRegisterCell() {
        self.tblView.register(UINib(nibName: "RuthKDTCell",  bundle: nil)
            , forCellReuseIdentifier: "RuthKDTCell")
        self.tblView.register(UINib(nibName: "RuthKDetailTCell",  bundle: nil)
            , forCellReuseIdentifier: "RuthKDetailTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
    }
    @objc func actionDetail(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
        if button.isSelected {
            indExpandable = 240
        }else{
            indExpandable = 128
        }
        tblView.reloadData()
    }
    @objc func actionLike(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
    }
    @objc func actionFollow(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
    }
    func DetailFSPagerView()  {
        self.pagerView.register(UINib(nibName: "RDetailFSPagerCell", bundle: nil), forCellWithReuseIdentifier: "RDetailFSPagerCell")
        self.pagerView.isInfinite = true
        //        self.pagerView.automaticSlidingInterval = 3
        self.pagerControl.currentPage = 3
        self.pagerControl.setFillColor(UIColor .hexStringToColor(hex: "00FF21"), for: UIControlState.selected)
    }
    func setupRuthKD() {
        self.pagerControl.numberOfPages = 3
            //self.arrRuthSB.count
        pagerView.delegate = self
        pagerView.dataSource = self
        self.pagerView.reloadData()
    }
    
    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {
        //        self.pagerControl.currentPage =
    }
    
    // MARK:- FSPagerViewDelegate, FSPagerViewDataSource METHOD ***** *** ***** **
    func numberOfItems(in pagerView:  FSPagerView) -> Int {
        return 3//self.arrRuthSB.count
    }
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "RDetailFSPagerCell", at: index) as! RDetailFSPagerCell
//        let data = self.arrRuthSB[index]
//        cell.imgPoster.image = UIImage.init(named: data.imgTitle)
        
        //        cell.imgPoster.sd_setImage(with: URL(string: banner), placeholderImage: nil)
        self.pagerControl.currentPage = index
        return cell
    }
   @IBAction func actionManu(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
    if sender.isSelected {
        self.btnSms.isHidden = false
        self.btnGift.isHidden = false
    }else {
        self.btnSms.isHidden = true
        self.btnGift.isHidden = true
     }
    }
    func acGift(){
        let twitterImage = UIImage(named: "btnrkdgift")!
        let plusImage = UIImage(named: "btnrkdsms")!
        
        let twitter = ActionButtonItem(title: "", image: twitterImage)
        twitter.action = { item in print("Twitter...") }
        
        let google = ActionButtonItem(title: "", image: plusImage)
        google.action = { item in print("Google Plus...") }
        
        actionButton = ActionButton(attachedToView: self.view, items: [twitter, google])
        actionButton.action = { button in button.toggleMenu() }
        actionButton.setTitle("+", forState: UIControlState())
        
    }
    @IBAction func actionGift(btn: UIButton) {
      
    }
    @IBAction func actionSms(btn: UIButton) {
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier:
                "RuthKDetailTCell", for: indexPath) as! RuthKDetailTCell
            cell.selectionStyle = .none
            cell.btnUpAro.addTarget(self, action: #selector(actionDetail(sender:)), for: .touchUpInside)
            cell.btnLike.addTarget(self, action: #selector(actionLike(sender:)), for: .touchUpInside)
            cell.btnProfile.addTarget(self, action: #selector(actionFollow(sender:)), for: .touchUpInside)
            return cell
        } else if indexPath.row == 1 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier:
                "RuthKDTCell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return indExpandable
        }
        if indexPath.row == 1 {
            return 150
        }
        return 500
    }
    
    @IBAction func actionSharing(_ btn : UIButton) {
        self.showToast(message: "actionSharing")
    }
    
}
