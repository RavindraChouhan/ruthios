//
//  VipShareTCell.swift
//  Ruth
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class VipShareTCell: UITableViewCell {

    @IBOutlet weak var btnTee: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var btnPlya: UIButton!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblEmoji: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var viewImgBG: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewImgBG.setCornerRadius(7)
        self.imgBG.setCornerRadius(7)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
