//
//  RootVipShareVC.swift
//  Ruth
//
//  Created by mac on 31/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import CarbonKit

class RootVipShareVC: UIViewController,CarbonTabSwipeNavigationDelegate {

    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var viewRight:UIView!
    @IBOutlet weak var btnSearch: UIButton!
    var categoryNames = [String]()
    var categoryID = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftbuttomWithController(withCustomView: btnBack)
        self.addRightbuttomWithController(withCustomView: viewRight)
         self.categoryNames = ["全部","影片","錄音","圖片","文章","其他"]
        self.setupTopBarAndNavigation()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setupTopBarAndNavigation() {
        
//         tabSwipe.setupCarbonPages(carbonTabSwipeNavigation, tabTitles: menuTabTitles, totalWidth: self.view.frame.width).insertIntoRootViewController(self)
        let tabSwipe = CarbonTabSwipeNavigation(items: self.categoryNames,delegate: self)
        tabSwipe.setNormalColor(UIColor.black, font: UIFont(name: ".SFUIText-Medium", size: 13)!)
        tabSwipe.setSelectedColor(UIColor.white, font: UIFont(name: ".SFUIText-Medium", size: 13)!)
        tabSwipe.carbonTabSwipeScrollView.backgroundColor = .green//UIColor.hexStr(hexStr: "f0f0f0")
        tabSwipe.setTabExtraWidth(22)
        tabSwipe.setTabBarHeight(26)
        tabSwipe.setIndicatorColor(UIColor.white)
        tabSwipe.pagesScrollView?.isScrollEnabled = true
        tabSwipe.carbonSegmentedControl?.backgroundColor = .green
        tabSwipe.insert(intoRootViewController: self)
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
            let VC = VipShareViewC(nibName: "VipShareViewC", bundle: nil)
//            VC.myHomeArr = self.myHomeData
            return VC
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
