//
//  VipShareViewC.swift
//  Ruth
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class VipShareViewC: UIViewController ,UITableViewDataSource ,UITableViewDelegate {
    
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var tblView : UITableView!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet var imgPoster: UIImageView!
//    @IBOutlet weak var txtSearch: UITextField!
//    @IBOutlet weak var pagerView: FSPagerView!
//    @IBOutlet weak var pagerControl: FSPageControl!
    var arrRuthSB = [RuthStoryBanner]()
//    var arrRuthSB = [#imageLiteral(resourceName: "ads")]
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        let ab = RuthStoryBanner(imgTitlee: "ads.png", isSelectedd: true)
        arrRuthSB.append(ab)
       self.setupNavigationBar()
        self.title = "VipShare".localized()
        self.RuthStoryRegisterCell()
        
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    func RuthStoryRegisterCell()  {
         self.setTabBarVisible(visible: false, animated: true)
        self.tblView.register(UINib(nibName: "VipShareTCell",  bundle: nil)
            , forCellReuseIdentifier: "VipShareTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }

//        cell.imgPoster.image = UIImage.init(named: data.imgTitle)

//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "VipShareTCell", for: indexPath) as! VipShareTCell
            cell.selectionStyle = .none
            cell.viewImgBG.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 7.0, scale: true)
            return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 120
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
