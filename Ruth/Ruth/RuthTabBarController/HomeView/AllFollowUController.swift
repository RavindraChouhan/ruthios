//
//  AllFollowUController.swift
//  Ruth
//
//  Created by mac on 20/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class AllFollowUController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var collectionFollow: UICollectionView!
    var arrFollowUsers = [AllFollowU]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTabBarVisible(visible: false, animated: true)
        self.addLeftbuttomWithController(withCustomView: self.btnBack)
        self.setupNavigationBar()
        self.getAllfollowUsersHome()
        self.navigationItem.title = " 推薦主播"
       collectionFollow.register(UINib.init(nibName: "RecommendedCCell", bundle: nil), forCellWithReuseIdentifier: "RecommendedCCell")
        self.collectionFollow.delegate = self
        self.collectionFollow.dataSource = self
        self.collectionFollow.reloadData()

    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    func getAllfollowUsersHome() {
        let Url = BaseURL + followUsersHome
//        Util.util.showHUD()
        Service.service.get(url: Url) { (result) in
            print("followUsers  ==\(result) ")
//            Util.util.hideHUD()
            self.arrFollowUsers.removeAll()
            if result is [String:Any] {
                let response = result as! [String:Any]
                if response["status"] as! Bool {
                    let data = response["data"] as! [[String:Any]]
                    for dict in data {
                        let follow = AllFollowU(info: dict)
                        self.arrFollowUsers.append(follow)
                    }
                    self.collectionFollow.delegate = self
                    self.collectionFollow.dataSource = self
                    self.collectionFollow.reloadData()
                } else {
                    let strMsg = Util.util.getMsgWithDict(response)
                    self.showAlertMessage(message: strMsg)
                }
                
            } else if result is Error
            {
                let error = result as! Error
                var strMsg = "Something went wrong".localized()
                strMsg = error.localizedDescription
                if strMsg == "The Internet connection appears to be offline."
                {
                    strMsg = "No internet connection".localized()
                }
                self.showAlertMessage(message: strMsg)
            }
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFollowUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = self.collectionFollow.dequeueReusableCell(withReuseIdentifier: "RecommendedCCell", for: indexPath) as! RecommendedCCell
        
        let data = arrFollowUsers[indexPath.row]
        if data.gender == "male" {
            cell.lblMale.text = "男"
        }
        if data.gender == "female" {
            cell.lblMale.text = "女"
        }
        cell.lblName.text = data.name
        cell.img.sd_setImage(with: URL(string: data.photoUrl as String), placeholderImage:#imageLiteral(resourceName: "defaultProfilePicture"))
        cell.lblFollowers.text = "\(data.followers)"
        //        cell.lblAddess.text = data.address
        cell.dropShadow(color: .hexStringToColor(hex: "555555"), opacity: 0.45, offSet: CGSize(width: 2, height: 2), radius: 0.0, scale: true)
     
        cell.btnFollow.tag = indexPath.row
        cell.btnFollow.isSelected = data.isFollow
        cell.btnFollow.addTarget(self, action: #selector(addButton(sender:)), for: .touchUpInside)
       let strMonth = Util.util.getFormatedDateWithRequiredFormat(strFormat: "MMMM ", withDate: data.dob)
       let strDay = Util.util.getFormatedDateWithRequiredFormat(strFormat: "d", withDate: data.dob)
        cell.lblData.text = Util.util.changeChineseMonth(strMonth) + Util.util.changeChineseDay(strDay) + " 日"
       let strYear = Util.util.getFormatedDateWithRequiredFormat(strFormat: "yyyy", withDate: data.dob)
        cell.lblBorn.text = "生於" + Util.util.changeChineseYear(strYear) + "年"
        return cell
    }
    @objc func addButton(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
        if button.isSelected {
            button.setImage(#imageLiteral(resourceName: "btncheck"), for: UIControlState.normal)
            button.setTitle("", for: UIControlState.normal)
            button.backgroundColor = .white
            button.alpha = 1.0
        }else{
            //button.setImage(UIImage(named : "Unfollow"), for: UIControlState.normal)
            button.setImage(UIImage(), for: UIControlState.normal)
            button.setTitle("關注", for: UIControlState.normal)
            button.backgroundColor = .black
            button.alpha = 0.7
//
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(UIScreen.main.bounds.width - 30)/2, height: 244)
    }
}
