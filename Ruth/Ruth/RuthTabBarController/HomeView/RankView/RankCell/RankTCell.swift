//
//  RankTCell.swift
//  Ruth
//
//  Created by mac on 17/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RankTCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblViewCount: UILabel!
    @IBOutlet weak var lblViewv: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnProfile.setCornerRadius(20)
        btnProfile.layer.borderColor = UIColor.hexStringToColor(hex:"FFFFFF").cgColor
        btnProfile.layer.borderWidth = 2
        self.btnCheck.setCornerRadius(4)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
