//
//  RootRantVC.swift
//  Ruth
//
//  Created by mac on 31/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import CarbonKit

class RootRantVC: UIViewController,CarbonTabSwipeNavigationDelegate {
    @IBOutlet var btnBack : UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    var categoryNames = [String]()
    var categoryID = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftbuttomWithController(withCustomView: btnBack)
        self.addRightbuttomWithController(withCustomView: btnSearch)
         self.categoryNames = ["全部","影片","錄音","圖片","文章","其他"]
        self.setupTopBarAndNavigation()
        self.makeTransprentNavigationBar()
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    
    func setupTopBarAndNavigation() {
        // let width = self.view.frame.width
//         tabSwipe.setupCarbonPages(carbonTabSwipeNavigation, tabTitles: menuTabTitles, totalWidth: self.view.frame.width).insertIntoRootViewController(self)
        let tabSwipe = CarbonTabSwipeNavigation(items: self.categoryNames,delegate: self)
        tabSwipe.setNormalColor(UIColor.black, font: UIFont(name: ".SFUIText-Medium", size: 13)!)
        tabSwipe.setSelectedColor(UIColor.white, font: UIFont(name: ".SFUIText-Medium", size: 13)!)
        tabSwipe.carbonTabSwipeScrollView.backgroundColor = UIColor.green
        tabSwipe.setTabBarHeight(26)
        tabSwipe.setIndicatorColor(UIColor.white)
        tabSwipe.setTabExtraWidth(20)
        tabSwipe.pagesScrollView?.isScrollEnabled = true
        tabSwipe.carbonSegmentedControl?.backgroundColor = UIColor.green
        tabSwipe.insert(intoRootViewController: self)
//       carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: categoryID as [AnyObject], toolBar: self.toolBar,delegate: self)
//        tabSwipe.setupCarbonPages(carbonTabSwipeNavigation, tabTitles: categoryID, totalWidth:           self.view.frame.width).insertIntoRootViewController(self)
//        tabSwipe.preloadCarbonPages(carbonTabSwipeNavigation, tabs: 2)
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        let stroryBord = UIStoryboard(name: "Main", bundle: nil)
        let VC = stroryBord.instantiateViewController(withIdentifier: "RankViewController") as! RankViewController
        return VC
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
