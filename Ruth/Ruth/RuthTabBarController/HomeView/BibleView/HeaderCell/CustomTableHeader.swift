//
//  MyProfileCustomTHeader.swift
//  ShiraLi
//
//  Created by Mac on 17/07/17.
//  Copyright © 2017 Relibit. All rights reserved.
//

import UIKit

class CustomTableHeader: UIView {
    
    @IBOutlet weak var lblHeader:UILabel!
    @IBOutlet weak var btnViewAll:UIButton!
    @IBOutlet weak var viewClearAll:UIView!
    @IBOutlet weak var btnClearAll:UIButton!
    
    override func awakeFromNib() {
    }
    
}
class CustomTableFootar: UIView {
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblLike:UILabel!
    @IBOutlet weak var lblPhoneNumer:UILabel!
    
    override func awakeFromNib() {
    }
    
}

