//
//  BibleViewC.swift
//  Ruth
//
//  Created by mac on 02/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class BibleViewC: UIViewController,FSPagerViewDelegate,FSPagerViewDataSource ,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet var btnBack : UIButton!
    @IBOutlet var tblView : UITableView!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    //    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var pagerControl: FSPageControl!
     var arrBibleB = [RuthStoryBanner]()
    var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftbuttomWithController(withCustomView: btnBack)
        self.addRightbuttomWithController(withCustomView: btnFilter)
        let ab = RuthStoryBanner(imgTitlee: "ads.png", isSelectedd: true)
        arrBibleB.append(ab)
        self.setupNavigationBar()
        self.navigationItem.title = "捉鬼天書"//.localized()
        self.BibleRegisterCell()
        self.BibleFSPagerView()
        self.setupBible()
        self.refreshControl.tintColor = UIColor.blue
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    func BibleRegisterCell()  {
        self.setTabBarVisible(visible: false, animated: true)
        self.tblView.register(UINib(nibName: "BibleTCell",  bundle: nil)
            , forCellReuseIdentifier: "BibleTCell")
//            self.tblView.register(UINib(nibName: "RuthStoryHotTCell",  bundle: nil)
//                , forCellReuseIdentifier: "RuthStoryHotTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func BibleFSPagerView()  {
        self.pagerView.register(UINib(nibName: "HomeFSPagerCell", bundle: nil), forCellWithReuseIdentifier: "HomeFSPagerCell")
        self.pagerView.isInfinite = true
        //        self.pagerView.automaticSlidingInterval = 3
        self.pagerControl.currentPage = 0
self.pagerControl.setFillColor(UIColor .hexStringToColor(hex: "00FF21"), for: UIControlState.selected)
    }
    func setupBible() {
        self.pagerControl.numberOfPages = self.arrBibleB.count
        pagerView.delegate = self
        pagerView.dataSource = self
        self.pagerView.reloadData()
    }
    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {
        //        self.pagerControl.currentPage =
    }
    
    // MARK:- FSPagerViewDelegate, FSPagerViewDataSource METHOD ***** *** ***** **
    func numberOfItems(in pagerView:  FSPagerView) -> Int {
        return self.arrBibleB.count
    }
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "HomeFSPagerCell", at: index) as! HomeFSPagerCell
        let data = self.arrBibleB[index]
        cell.imgPoster.image = UIImage.init(named: data.imgTitle)
        
        //        cell.imgPoster.sd_setImage(with: URL(string: banner), placeholderImage: nil)
        self.pagerControl.currentPage = index
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "BibleTCell", for: indexPath) as! BibleTCell
            cell.selectionStyle = .none
            cell.viewImgBG.dropShadow(color: .hexStringToColor(hex: "7FFF00"), opacity: 1.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 7.0, scale: true)
            return cell

    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let Headerview:CustomTableHeader = Bundle.main.loadNibNamed("CustomTableHeader", owner: self, options: nil)?[0] as! CustomTableHeader
                Headerview.lblHeader.text = "Recommended Masters".localized()
             return Headerview
        } else {
            let def = UIView()
            return def
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = BibleDetailViewC(nibName: "BibleDetailViewC", bundle: nil)
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 120
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
