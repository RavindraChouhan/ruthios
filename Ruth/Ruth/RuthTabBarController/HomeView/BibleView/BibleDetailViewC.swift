//
//  BibleDetailViewC.swift
//  Ruth
//
//  Created by mac on 03/08/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class BibleDetailViewC: UIViewController,UITableViewDataSource ,UITableViewDelegate {
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var tblView : UITableView!
    @IBOutlet var btnShring: UIButton!
    @IBOutlet var imgPoster: UIImageView!
     var refreshControl: UIRefreshControl!
    let arrdetail   = ["1988年，蘇民峰獲聘往歐洲多國，包括瑞典、挪威、丹麥及西班牙勘察風水。回港後又受聘往加拿大等地勘察。同年他接受《繽紛雜誌》的訪問。1989年，他再到美國、加拿大、新加坡、日本、台灣等地勘察風水和接受《城市周刊》的訪問。1990年，蘇民峰接受台灣的《翡翠雜誌》、《生活報》等多本雜誌訪問，並於1991年推出《現代賴布衣手記之風水入門》錄影帶。","1992年，他正式開班教授風水。其後於1994年首次前往澳洲勘察風水及在1995年再度發行風水入門錄影帶。到了1999年，蘇民峰撰寫首本風水書籍《蘇民峰觀相知人》。之後也推出多部關於風水命理的著作。2000年，他成立個人的風水網站為客人及學徒解答的風水問題。除了研究風水命理之外，蘇民峰間中客串電影演出，比如《行運超人》。然而，亦有人批評他言過其實的風水知識。","2006年，蘇民峰與香港有線電視合作，舉辦玄學比賽，勝出者可成為蘇民峰入室弟子。當中被選中的學生與他及吳文忻一起於有線電視娛樂台的《「峰」生水起精讀班》(風水篇)亮相。據蘇民峰表示，這一系列的節目除了希望可以透過節目教會家庭觀眾基本的風水知識外，亦希望透過讓弟子親身實習，而讓他們有實戰的經驗。2007年年，蘇民峰再與有線電視合作與吳文忻及新一班徒弟主持《「峰」生水起精讀班》(面相篇)。（註：原本香港電台風水節目已經同有線電視的節目合併，並移至週末播出。）同年11月10日，由蘇民峰與台灣籍女朋友楊佳佳及有線電視主持黃翠如主持的旅遊節目《40日峰狂嘆世界》播映。2008年，他首次演出電視劇《今日法庭》。","發佈日期 : 2018年01月11日      標籤 : #風水, #睇相"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DetailRegisterCell()
        self.tblView.estimatedRowHeight = 200
        self.tblView.addFooterView(withFooterHight: 140)
        self.navigationController?.isNavigationBarHidden = true
    }
    @objc func pulltorefresh(){
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    func DetailRegisterCell() {
        self.setTabBarVisible(visible: false, animated: true)
        self.tblView.register(UINib(nibName: "BibleDetailTCell",  bundle: nil)
            , forCellReuseIdentifier: "BibleDetailTCell")
        self.tblView.register(UINib(nibName: "DetailTCell",  bundle: nil)
            , forCellReuseIdentifier: "DetailTCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.hexStringToColor(hex: "7FFF00")
        self.refreshControl.addTarget(self, action: #selector(pulltorefresh), for: .valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "BibleDetailTCell", for: indexPath) as! BibleDetailTCell
            cell.selectionStyle = .none
            return cell
        }
        if indexPath.row == 1 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "DetailTCell", for: indexPath) as! DetailTCell
            let data = self.arrdetail[indexPath.row]
            cell.lblDetail.text = data
            cell.selectionStyle = .none
            return cell
        }
         return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

            let footarview:CustomTableFootar = Bundle.main.loadNibNamed("CustomTableFootar", owner: self, options: nil)?[0] as! CustomTableFootar
            return footarview
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
             return 68
        }
        if indexPath.row == 1 {
            return UITableViewAutomaticDimension
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
