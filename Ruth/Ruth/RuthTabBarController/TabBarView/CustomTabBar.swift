//
//  CustomTabBar.swift
//  Ruth
//
//  Created by Mac on 12/07/17.
//  Copyright © 2017 Relibit. All rights reserved.
//

import UIKit

class CustomTabBar: UIView {

    @IBOutlet weak var viewCalendar:UIView!
    @IBOutlet weak var viewFriends:UIView!
    @IBOutlet weak var viewAdd:UIView!
    @IBOutlet weak var viewWorkOut:UIView!
    @IBOutlet weak var viewMore:UIView!
    @IBOutlet weak var viewIndicator:UIView!
    @IBOutlet weak var btnHome:UIButton!
    @IBOutlet weak var btnSeach:UIButton!
    @IBOutlet weak var btnAdd:UIButton!
    @IBOutlet weak var btnBookmark:UIButton!
    @IBOutlet weak var btnUserProfile:UIButton!
    
    let btnWidth = UIScreen.main.bounds.width / 5
  
    override func awakeFromNib() {
        self.btnHome.isSelected = true
    }
    
    func setTabViewFrame() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.viewCalendar.frame = CGRect(x: 0, y: 0, width: self.btnWidth, height: self.viewCalendar.frame.height)
            self.viewFriends.frame = CGRect(x: self.viewCalendar.frame.maxX, y: 0, width: self.btnWidth, height: self.viewFriends.frame.height)
            self.viewAdd.frame = CGRect(x: self.viewFriends.frame.maxX, y: 0, width: self.btnWidth, height: self.viewAdd.frame.height)
            self.viewWorkOut.frame = CGRect(x: self.viewAdd.frame.maxX, y: 0, width: self.btnWidth, height: self.viewWorkOut.frame.height)
            self.viewMore.frame = CGRect(x: self.viewWorkOut.frame.maxX, y: 0, width: self.btnWidth, height: self.viewMore.frame.height)
        })
    }
    
    func statusBarFrameWillChange(_ notification: Notification) {
        
//        var rect = self.frame
//        rect
//        self.frame = self.frame
        
//        let rectValue = notification.userInfo?.value(forKey: UIApplicationStatusBarFrameUserInfoKey) as? NSValue ?? NSValue()
//        var newFrame: CGRect
//        rectValue.getValue(newFrame as? UnsafeMutableRawPointer ?? UnsafeMutableRawPointer())
//        //print("statusBarFrameWillChange: newSize \(newFrame.size.width), \(newFrame.size.height)")
        // Move your view here ...
    }
    func statusBarFrameChanged(_ notification: Notification) {
//        let rectValue
//        let rectValue = notification.userInfo?.value(forKey: UIApplicationStatusBarFrameUserInfoKey) as? NSValue ?? NSValue()
//        var oldFrame: CGRect
//        rectValue.getValue(oldFrame as? UnsafeMutableRawPointer ?? UnsafeMutableRawPointer())
//        //print("statusBarFrameChanged: oldSize \(oldFrame.size.width), \(oldFrame.size.height)")
        // ... or here, whichever makes the most sense for your app.
    }
    
    @IBAction func tapTabBar(_ sender: UIButton) {
      var tagIndex:NSInteger = sender.tag
      UserNew.currentUser.isHideTabBar = false
        var x: CGFloat = 0
        self.btnHome.isSelected = false
        self.btnSeach.isSelected = false
        self.btnBookmark.isSelected = false
        self.btnUserProfile.isSelected = false
      switch (tagIndex) {
      case 1:
        x = 0
        self.btnHome.isSelected = true
//        NotificationCenter.default.post(name: AppNotifications.notificationSelectHomeTab, object:nil)
        break
      case 2:
        x = btnWidth
        self.btnSeach.isSelected = true
//      NotificationCenter.default.post(name: AppNotifications.notificationSelectRecentsTab, object:nil)
        break
      case 5:
        x = btnWidth * 2
//      NotificationCenter.default.post(name: AppNotifications.notificationSelectSearchTab, object:nil)
        break
      case 3:
        x = btnWidth * 3
        self.btnBookmark.isSelected = true
//      NotificationCenter.default.post(name: AppNotifications.notificationSelectMyMusicTab, object:nil)
        break
      case 4:
        x = btnWidth * 4
        self.btnUserProfile.isSelected = true
      //      NotificationCenter.default.post(name: AppNotifications.notificationSelectMyMusicTab, object:nil)
        break
      default: break
      }
        if tagIndex == 3 {
            UIView.animate(withDuration: 0.2, animations: {
                self.viewIndicator.frame = CGRect(x: x, y: 0, width: 0, height: 0)
            }) { (isDone) in
                if isDone {
                    
                }
            }
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.viewIndicator.frame = CGRect(x: x, y: 0, width: self.btnWidth, height: 3)
            }) { (isDone) in
                if isDone {
                    
                }
            }
        }
      tagIndex = tagIndex - 1
      let ind:NSNumber = NSNumber(value: tagIndex)
      NotificationCenter.default.post(name: AppNotifications.notificationChangeTabBar, object: ind)
    }
}
