//
//  Instagram.swift
//  Ruth
//
//  Created by mac on 19/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

struct InstagramResponse : Codable {
    let data : InstaData?
    let meta : InstaMeta?
}

struct InstaData : Codable {
    let id: String?
    let username: String?
    let profilePicture : String?
    let fullName : String?
    let bio : String?
    let website : String?
    let isBusiness : Bool?
    let counts : InstaCounts?
    let deviceTokenid : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case deviceTokenid = "deviceToken"
        case username = "username"
        case profilePicture = "profile_picture"
        case fullName = "full_name"
        case bio = "bio"
        case website = "website"
        case isBusiness = "is_business"
        case counts
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        profilePicture = try values.decodeIfPresent(String.self, forKey: .profilePicture)
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
        bio = try values.decodeIfPresent(String.self, forKey: .bio)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        isBusiness = try values.decodeIfPresent(Bool.self, forKey: .isBusiness)
        counts = try InstaCounts(from: decoder)
        deviceTokenid = try values.decodeIfPresent(String.self, forKey: .deviceTokenid)
    }
}

struct InstaCounts : Codable {
    let media : Int?
    let follows : Int?
    let followedBy : Int?
    
    enum CodingKeys: String, CodingKey {
        case media = "media"
        case follows = "follows"
        case followedBy = "followed_by"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        media = try values.decodeIfPresent(Int.self, forKey: .media)
        follows = try values.decodeIfPresent(Int.self, forKey: .follows)
        followedBy = try values.decodeIfPresent(Int.self, forKey: .followedBy)
    }
}

struct InstaMeta : Codable {
    let code : Int?
    let errorMessage: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case errorMessage = "error_message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        errorMessage = try values.decodeIfPresent(String.self, forKey: .errorMessage)
    }
}
