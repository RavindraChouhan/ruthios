    //
    //  AllModels.swift
    //  Toch-iOS
    //
    //  Created by Shivam on 17/02/18.
    //  Copyright © Linkites 2018 WeSee. All rights reserved.
    //
    
    import Foundation
    import Alamofire
    //import SVProgressHUD
    
    class UserNew {
        var id = ""
        var name = ""
        var socialID = ""
        var email = ""
        var country = ""
        var gender = ""
        var photoUrl = ""
        var isBlock = false
        var isChineseSelected = false
        var provider = ""
        var accountType = ""
        var notificationEnabled = false
        var role = ""
        var info = [String : Any]()
        //var accessToken = ""
        var otp = 0
        var mobileNumber = 0
        var showReferenceOption = false
        var referLink = ""
        var authToken:String = ""
        var isHideTabBar = false
        var isLogin = false
        var isNewUser = false

        static let currentUser:UserNew = UserNew()
        
        func initWithInfo(isAPICall:Bool) {
            let preferences = UserDefaults.standard
            self.isLogin = preferences.bool(forKey: "isLogin")
            self.isNewUser = preferences.bool(forKey: "isNewUser")
            if self.isLogin {
                self.authToken = preferences.value(forKey: "authToken") as! String
                if (preferences.value(forKey: "info") != nil) {
                    self.info = preferences.dictionary(forKey: "info")!
                    if let userID = self.info["id"] as? String {
                        self.id = userID
                    }
                    if let name = self.info["name"] as? String {
                        self.name = name
                    }
                    if let socialID = self.info["socialID"] as? String {
                        self.socialID = socialID
                    }
                    if let email = self.info["email"] as? String {
                        self.email = email
                    }
                    
                    if let  gender = self.info["gender"] as? String {
                        self.gender = gender
                    }
                    if let mobile = self.info["mobileNumber"] as? Int {
                        self.mobileNumber = mobile
                    }
                    if let country = self.info["country"] as? String {
                        self.country = country
                    }
                    if let accountType = self.info["accountType"] as? String {
                        self.accountType = accountType
                    }
                    if let role = self.info["role"] as? String {
                        self.role = role
                    }
                    if let photoUrl = self.info["photoUrl"] as? String {
                        self.photoUrl = photoUrl
                    }
                    if let isNewUser = self.info["isNewUser"] as? Bool {
                        self.isNewUser = isNewUser
                    }
                    
                }
                if isAPICall {
                     self.getInfo()
                }
            }
        }
        
        func saveUserData(_ user : User ) {
            self.id = user.id!
            self.photoUrl = user.photoUrl!
            self.gender = user.gender!
            self.country = user.country!
            self.role = user.role!
            self.mobileNumber = user.mobileNo!
            self.accountType = user.accountType!
            self.name = user.userName!
            self.email = user.email!
            self.isNewUser = user.isNewUser!
            self.socialID = user.socialID!

//            self.authToken = UserDefaults.standard.value(forKey: "authToken") as! String
            self.info["id"] = self.id
            self.info["photoUrl"] = self.photoUrl
            self.info["gender"] = self.gender
            self.info["country"] = self.country
            self.info["mobileNumber"] = self.mobileNumber
            self.info["accountType"] = self.accountType
            self.info["role"] = self.role
            self.info["name"] = self.name
            self.info["email"] = self.email
            self.info["socialID"] = self.socialID
            self.info["authToken"] = self.authToken
            self.info["isNewUser"] = self.isNewUser
            self.saveToLocal()
        }
        
        //    func SocialSignIn(parameters:[String:Any], completion:@escaping (_ success: Bool) -> Void) {
        //        let url = Service + "accounts/login"//"users/socialSignIn"
        //        Service().post(url: url, parameters: parameters) { (result) in
        //            print("FBBBBBBBBBBB result = \(result)")
        //            if result is [String : Any] {
        //                let dataInfo = result as! [String : Any]
        //                if dataInfo["success"] as! Bool {
        //                    let data = dataInfo["user"] as! [String : Any]
        //                    self.isLogin = true
        //                    self.id = data["_id"] as! String
        //                    self.getInfo()
        //                    completion(true)
        //                }
        //                else{
        //                    completion(false)
        //                }
        //            }
        //            else {
        //                completion(false)
        //            }
        //        }
        //    }
//        func addCommentOnVideoWithComment(parametes: [String:Any], completion:@escaping (_ success: Any) -> Void) {
//            let url = BaseURL + "videos/comments/create"
//            Service().post(url: url, parameters: parametes) { (result) in
//                if result is [String : Any] {
//                    let response = result as! [String : Any]
//                    if (response["success"] as! Bool) {
//                    }else{
//                        completion(response)
//                    }
//                }else {
//                    // //print("error \(result)");
//                    completion(result)
//                }
//            }
//        }
        //videoid,userid
       
        //let parameter  = ["appId": Config.AppID ,"videoId": "" ,"userId": User.currentUser.id]
//        func serverVidoesViewCount(parametes: [String:Any], completion:@escaping (_ success: Any) -> Void) {
//            let url = BaseURL + "videos/viewed"
//            Service().post(url: url, parameters: parametes) { (result) in
//                if result is [String : Any] {
//                    let response = result as! [String : Any]
//                    if ((response["msg"] as? [String: Any]) != nil) {
//
//                    }else{
//                        completion(response)
//                    }
//                }else {
//                    // //print("error \(result)");
//                    completion(result)
//                }
//            }
//        }
        
        // save user info
        func saveToLocal() {
            let preferences = UserDefaults.standard
            preferences.set(self.isLogin, forKey:"isLogin")
            preferences.set(self.info, forKey: "info")
            preferences.set(authToken, forKey:"authToken")
            preferences.set(self.photoUrl, forKey: "photoUrl")
            preferences.set(self.id, forKey: "CurrentUserID")
            preferences.synchronize()
        }
        
        func logout() {
            self.id = ""
            self.info.removeAll()
            self.isLogin = false
            let preferences = UserDefaults.standard
            preferences.set(self.isLogin, forKey:"isLogin")
            preferences.set(self.info, forKey: "info")
            preferences.synchronize()
        }
        //GET /user/mobile/me/{id
        func getInfo() {
            NetworkClient.shared.getUserProfile { (user, msg) -> (Void) in}
        }
    }
    //    func getCategoryVideos(id : String) {
    //        let Url = BaseURL  + "home/categories/videos/" + id
    ////        SVProgressHUD.show()
    //        Service().get(url: Url) { (result) in
    ////            SVProgressHUD.dismiss()
    //            print("Api home cat id ======  \(result)")
    //            if result is [String:Any] {
    //                let response = result as! [String: Any]
    //                self.categoryVideos.removeAll()
    //                let videosData = response["videos"] as! [String: Any]
    //                let homeCat = homeCategoriesVideos(info: videosData)
    //                self.categoryVideos.append(homeCat)
    //                NotificationCenter.default.post(name: AppNotifications.notificationReloadCategoryVideos, object: nil)
    //            }
    //        }
    //    }
    
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ??
    // MARK: New Model
    
    class Tutorial {
        var _id = ""
        var en = ""
        var url = ""
        var zhHans = ""
        var zhHant = ""
        var title = ""
        var description = ""
        var updatedAt = ""
        public init () {}
        public init (info : [String : Any]){
            if let _id = info["_id"] as? String {
                self._id = _id
            }
            if let en = info["en"] as? String {
                self.en = en
            }
            if let url = info["url"] as? String {
                self.url = url
            }
            if let zhHans = info["zhHans"] as? String {
                self.zhHans = zhHans
            }
            if let updatedAt = info["updatedAt"] as? String {
                self.updatedAt = updatedAt
            }
            if let description = info["description"] as? String {
                self.description = description
            }
            if let title = info["title"] as? String {
                self.title = title
            }
            if let zhHant = info["zhHant"] as? String {
                self.zhHant = zhHant
            }
        }
    }
    class RuthStoryBanner {
        var imgTitle = ""
        var isSelected = false
        
        public init(imgTitlee: String,isSelectedd : Bool) {
            imgTitle = imgTitlee
            isSelected = isSelectedd
        }
    }
    class HomeGrid {
        var id = ""
        var chineseTradiTitle = ""
        var chineseSimTitle = ""
        var englishTitle = ""
        var urls = [String]()
        var imgSelected = ""
        var imgUnSelected = ""
        var imgTitlee = ""
        var isSelected = false
        public init () {}
        public init(info : [String : Any]) {
            if let chineseTradi = info["zhHant"] as? String {
                self.chineseTradiTitle = chineseTradi
            }
            if let chineseSim = info["zhHans"] as? String {
                self.chineseSimTitle = chineseSim
            }
            if let en = info["en"] as? String {
                self.englishTitle = en
            }
            if let id = info["_id"] as? String {
                self.id = id
            }
            if let dataUrls = info["urls"] as? [String] {
                self.imgUnSelected = dataUrls[0] as String
                self.imgSelected = dataUrls[1] as String
            }
        }
    }
    class hbroadcastD {
        var id = ""
        var photoUrl = ""
        var isSelected = false
        public init () {}
        public init (info: [String :Any]) {
            if let id = info["_id"] as? String{
                self.id = id
            }
            if let photoUrl = info["photoUrl"] as? String{
                self.photoUrl = photoUrl
            }
        }
    }
    class HomeChannels {
        var chineseTradi = ""
        var chineseSim = ""
        var id = ""
        var strEndTime = ""
        var strStartTime = ""
        var endTime : Date?
        var startTime : Date?
        var profile = ""
        var cover = ""
        var description = ""
        var title = ""
        var likes = 0
        var isLike = false
        var views = 0
        var emoji = 0
        var subscribers = 0
        var isSubscribe = false
        var onAir = ""
        var broadcastData = [hbroadcastD]()
        var isSelected = false
        public init () {}
        public init (info:[String: Any]) {
            if let zh = info["zh_Hans"] as? String {
                self.chineseSim = zh
            }
            if let Hant = info["zh_Hant"] as? String {
                self.chineseTradi = Hant
            }
            if let id = info["_id"] as? String {
                self.id = id
            }
            if let pro = info["profile"] as? String {
                self.profile = pro
            }
            if let endT = info["endTime"] as? String {
                //self.strEndTime = endT
                var eTime = endT
                let indx = eTime.index(eTime.startIndex,offsetBy: 2)
                eTime.insert(":", at: indx)
                rLog("eTime = \(eTime)")
                self.strEndTime = String.chanage24To12Hrs(eTime)
                
            }
            if let startT = info["startTime"] as? String {
                //self.strStartTime = startT
                var sTime = startT
                let indx = sTime.index(sTime.startIndex,offsetBy: 2)
                sTime.insert(":", at: indx)
                self.strStartTime = String.chanage24To12Hrs(sTime)
                rLog("sTime = \(sTime)")
            }
            
            if let cover = info["cover"] as? String {
                self.cover = cover
            }
            if let descri = info["description"] as? String {
                self.description = descri
            }
            if let ti = info["title"] as? String {
                self.title = ti
            }
            if let lik = info["likes"] as? Int {
                self.likes = lik
            }
            if let isLike = info["isLike"] as? Bool {
                self.isLike = isLike
            }
            if let view = info["views"] as? Int {
                self.views = view
            }
            if let emo = info["emoji"] as? Int {
                self.emoji = emo
            }
            if let subs = info["subscribers"] as? Int {
                self.subscribers = subs
            }
            if let isSubs = info["isSubscribe"] as? Bool {
                self.isSubscribe = isSubs
            }
            if let onAir = info["onAir"] as? String {
                self.onAir = onAir
            }
            if let broadcast = info["broadcastData"] as? [[String:Any]] {
                for dict in broadcast
                {
                    let broadcast = hbroadcastD(info: dict)
                    self.broadcastData.append(broadcast)
                }
            }
             self.isSelected = false
        }
        func getLikeChannels(parametes: [String:Any], completion:@escaping (_ success: Any) -> Void) {
            let url = BaseURL + channelsMobileUpdate
            Service().post(url: url, parameters: parametes) { (result) in
                if result is [String : Any] {
                    let response = result as! [String : Any]
                    if (response["status"] as! Bool) {
                        
                    }else{
                        completion(response)
                    }
                }else {
                    // //print("error \(result)");
                    completion(result)
                }
            }
        }
        }
    class Channel {
        var chineseTradi = ""
        var chineseSim = ""
        var id = ""
        var strEndTime = ""
        var strStartTime = ""
        var endTime : Date?
        var startTime : Date?
        var profile = ""
        var cover = ""
        var description = ""
        var title = ""
        var likes = 0
        var isLike = false
        var subscribers = 0
        var isSubscribe = false
        var totalViews = 0
        var emoji = 0
        var onAir = ""
        var broadcastData = [BroadcastChannels]()
        var activeUsers = [ActiveUser]()
        var pastBroadcastArr = [BroadcastChannels]()
        var isSelected = false
        public init () {}
        public init (info:[String: Any]) {
            if let zh = info["zh_Hans"] as? String {
                self.chineseSim = zh
            }
            if let Hant = info["zh_Hant"] as? String {
                self.chineseTradi = Hant
            }
            if let id = info["_id"] as? String {
                self.id = id
            }
            if let pro = info["profile"] as? String {
                self.profile = pro
            }
            if let endT = info["endTime"] as? String {
                //self.strEndTime = endT
                var eTime = endT
                let indx = eTime.index(eTime.startIndex,offsetBy: 2)
                eTime.insert(":", at: indx)
                rLog("eTime = \(eTime)")
                self.strEndTime = String.chanage24To12Hrs(eTime)

            }
            if let startT = info["startTime"] as? String {
                //self.strStartTime = startT
                var sTime = startT
                let indx = sTime.index(sTime.startIndex,offsetBy: 2)
                sTime.insert(":", at: indx)
                self.strStartTime = String.chanage24To12Hrs(sTime)
                rLog("sTime = \(sTime)")
            }
            if let cover = info["cover"] as? String {
                self.cover = cover
            }
            if let descri = info["description"] as? String {
                self.description = descri
            }
            if let ti = info["title"] as? String {
                self.title = ti
            }
            if let lik = info["likes"] as? Int {
                self.likes = lik
            }
            if let isLike = info["isLike"] as? Bool {
                self.isLike = isLike
            }
            if let view = info["totalViews"] as? Int {
                self.totalViews = view
            }
            if let emo = info["emoji"] as? Int {
                self.emoji = emo
            }
            if let subs = info["subscribers"] as? Int {
                self.subscribers = subs
            }
            if let isSubs = info["isSubscribe"] as? Bool {
                self.isSubscribe = isSubs
            }
            if let onAir = info["onAir"] as? String {
                self.onAir = onAir
            }
            if let broadcastArr = info["broadcastData"] as? [[String:Any]] {
                for dict in broadcastArr
                {
                    let broadcastChannels = BroadcastChannels(info: dict)
                    self.broadcastData.append(broadcastChannels)
                }
            }
            if let activeArr = info["activeUserData"] as? [[String:Any]] {
                for dict in activeArr
                {
                    let activeUser = ActiveUser(info: dict)
                    self.activeUsers.append(activeUser)
                }
            }
            
            if let pastBroadcastArr = info["pastBroadcast"] as? [[String:Any]] {
                for dict in pastBroadcastArr
                {
                    let broadcastChannels = BroadcastChannels(info: dict)
                    self.pastBroadcastArr.append(broadcastChannels)
                }
            }
            self.isSelected = false
        }
    }
    
    class BroadcastChannels {
        //broadcastEndTime
        var id = ""
        var coverUrl = ""
        var broadcastTitle = ""
        var broadcastTag = ""
        var broadcastUrl = ""
        var broadcastStreamId = ""
        var isLive = false
        var isActive = false
        var broadcasterId = ""
        var broadcastLikes = 0
        var broadcastViews = 0
        var broadcastComments = 0
        var emoji = 0
        var broad_id = ""
        var broad_followers = 0
        var broad_photoUrl = ""
        var broad_userName = ""
        var broad_email = ""
        var broad_name = ""
        var broad_isFollow = false
        var activePhotoUrl = ""
        var isSelected = false
        public init () {}
        public init (info:[String:Any]) {
            if let id = info["_id"] as? String{
                self.id = id
            }
            if let broadcasterId = info["broadcasterId"] as? String{
                self.broadcasterId = broadcasterId
            }
            if let broadcastUrl = info["broadcastUrl"] as? String{
                self.broadcastUrl = broadcastUrl
            }
            if let isActive = info["isActive"] as? Bool{
                self.isActive = isActive
            }
            if let isLive = info["isLive"] as? Bool{
                self.isLive = isLive
            }
            if let Streamid = info["broadcastStreamId"] as? String{
                self.broadcastStreamId = Streamid
            }
            if let broadcastTag = info["broadcastTag"] as? String{
                self.broadcastTag = broadcastTag
            }
            if let broadcastTitle = info["broadcastTitle"] as? String{
                self.broadcastTitle = broadcastTitle
            }
            if let coverUrl = info["coverUrl"] as? String{
                self.coverUrl = coverUrl
            }
            if let lik = info["broadcastLikes"] as? Int {
                self.broadcastLikes = lik
            }
            if let view = info["broadcastViews"] as? Int {
                self.broadcastViews = view
            }
            if let emo = info["emoji"] as? Int {
                self.emoji = emo
            }
            if let broadcastC = info["broadcastComments"] as? Int {
                self.broadcastComments = broadcastC
            }
            
            if let broadcastDict = info["broadcasterData"] as? [String : Any]{
//                if let broadcastDict = broadcastArr.first{
                    if let id = broadcastDict["_id"] as? String{
                        self.broad_id = id
                    }
                    if let followers = broadcastDict["followers"] as? Int {
                        self.broad_followers = followers
                    }
                    if let photoUrl = broadcastDict["photoUrl"] as? String{
                        self.broad_photoUrl = photoUrl
                    }
                    if let userName = broadcastDict["userName"] as? String{
                        self.broad_userName = userName
                    }
                    if let email = broadcastDict["email"] as? String{
                        self.broad_email = email
                    }
                    if let name = broadcastDict["name"] as? String{
                        self.broad_name = name
                    }
                    if let isFollow = broadcastDict["isFollow"] as? Bool{
                        self.broad_isFollow = isFollow
                    }
                //}
                
            }
             self.isSelected = false
        }
        func followAndUnfollow(completion:@escaping (_ result: Bool) -> Void){
            let parameter :[String : Any] = ["type": "follow" ,"isFollow":self.broad_isFollow ? true : false ,"userId": UserNew.currentUser.id, "followingId":self.id]
            let getURL = BaseURL + userMobileUpdate
            Service.service.post(url: getURL, parameters: parameter) { (result) in
                if result is [String: Any] {
                    let response = result as! [String: Any]
                    if response["status"] as! Bool {
                        if (response["message"] as? String) == "Follow user successfully"{
                            self.broad_isFollow = true
                        } else {
                            self.broad_isFollow = false
                        }
                        completion(self.broad_isFollow)
                       // self.btnFollow.isSelected =  self.channel.broad_isFollow
                    }else{
                        completion(self.broad_isFollow)
                    }
                }
            }
        }
    }
    
    class ActiveUser {
        var id = ""
        var photoUrl = ""
        var isSelected = false
        public init () {}
        public init (info: [String :Any]) {
            if let id = info["_id"] as? String{
                self.id = id
            }
            if let photoUrl = info["photoUrl"] as? String{
                self.photoUrl = photoUrl
            }
             self.isSelected = false
        }
    }
    class LocalConversationsModel {
        var viewCount = 0
        var broadcastId = ""
        var message = ""
        var username = ""
        var photoUrl = ""
        var type = ""
        var userId = ""
        var giftCount = 0
        var giftPointsLeft = 0
        var giftType = ""
        var customOrderId = ""
        var isCompleted = false
        var isRejected = false
        public init (){}
    }
    class EliteUser {
        var photoUrl = ""
        var giftType = ""
        public init (){}
    }
    class MaskOption {
        var zhHans = ""
        var zhHant = ""
        var title = ""
        var isSelected = false
        public init () {}
        public init (info:[String : Any]) {
        }
    }
    class StickerOption {
        var zhHans = ""
        var zhHant = ""
        var title = ""
        var isSelected = false
        public init () {}
        public init (info:[String : Any]) {
        }
    }
    class Sticker {
        var id = ""
        var title = ""
        var stickerLabel = ""
        var image = ""
        var stickerType = ""
        var giftValue = 0
        var giftType = ""
        var cGift_id = ""
        var cGift_giftValue = 0
        var cGift_giftType = ""
    
        var isSelected = false
        public init () {}
        public init (info:[String : Any]) {
            if let id = info["_id"] as? String{
                self.id = id
            }
            if let title = info["title"] as? String{
                self.title = title
            }
            if let stickerL = info["stickerLabel"] as? String{
                self.stickerLabel = stickerL
            }
            if let image = info["image"] as? String{
                self.image = image
            }
            if let stickerTypev = info["stickerType"] as? String{
                self.stickerType = stickerTypev
            }
            if let giftType = info["giftType"] as? String{
                self.giftType = giftType
            }
            if let giftValue = info["giftValue"] as? Int{
                self.giftValue = giftValue
            }
             self.isSelected = false
        }
    }
    class AllFollowU {
        var _id = ""
        var email  = ""
        var userName = ""
        var mobileNo = 0
        var status = 0
        var gender = ""
        var dob = ""
        var address = ""
        var name = ""
        var photoUrl = ""
        var followers = 0
        var isFollow = false
        var isSelected = false
        
        public init (){}
        public init (info : [String:Any]) {
        if let email = info["email"] as? String {
            self.email = email
        }
        if let id = info["_id"] as? String {
            self._id = id
        }
        if let userName = info["userName"] as? String {
            self.userName = userName
        }
        if let mobileNo = info["mobileNo"] as? Int {
            self.mobileNo = mobileNo
        }
        if let status = info["status"] as? Int {
            self.status = status
        }
        if let gender = info["gender"] as? String {
            self.gender = gender
        }
        if let dob = info["dob"] as? String {
            self.dob = dob
        }
        if let address = info["address"] as? String {
            self.address = address
        }
        if let name = info["name"] as? String {
            self.name = name
        }
        if let photoUrl = info["photoUrl"] as? String {
            self.photoUrl = photoUrl
        }
        if let followers = info["followers"] as? Int {
            self.followers = followers
        }
        if let isFollow = info["isFollow"] as? Bool {
            self.isFollow = isFollow
        }
             self.isSelected = false
        }
    }
    class AllBroadcastViewsU {
        var _id = ""
        var email  = ""
        var userName = ""
        var followers = 0
        var name = ""
        var photoUrl = ""
        var isFollow = false
        var viewData = [String:Any]()
        var viewData_id = ""
        var viewData_StartTime = ""
        var viewData_EndTime = ""
        var viewData_spendingTime = 0
        var isSelected = false
        
        public init (){}
        public init (info : [String:Any]) {
            if let email = info["email"] as? String {
                self.email = email
            }
            if let id = info["_id"] as? String {
                self._id = id
            }
            if let userName = info["userName"] as? String {
                self.userName = userName
            }
            if let name = info["name"] as? String {
                self.name = name
            }
            if let photoUrl = info["photoUrl"] as? String {
                self.photoUrl = photoUrl
            }
            if let followers = info["followers"] as? Int {
                self.followers = followers
            }
            if let isFollow = info["isFollow"] as? Bool {
                self.isFollow = isFollow
            }
            if let dataView = info["viewData"] as? [String:Any] {
                
                if let viewData_id = dataView["_id"] as? String {
                    self.viewData_id = viewData_id
                }
                if let viewData_StartTime = dataView["viewStartTime"] as? String {
                    self.viewData_StartTime = viewData_StartTime
                }
                if let viewData_EndTime = dataView["viewEndTime"] as? String {
                    self.viewData_EndTime = viewData_EndTime
                }
                if let spendingTime = dataView["spendingTime"] as? Int {
                    self.viewData_spendingTime = spendingTime
                }
            }
            self.isSelected = false
        }
    }
    class UserBroadcastGifts {
        var _id = ""
        var count  = 0
        var userId = ""
        var userData_id = ""
        var userData_email  = ""
        var userData_userName = ""
        var userData_followers = 0
        var userData_name = ""
        var userData_photoUrl = ""
        var userData_isFollow = false
        var isSelected = false
        public init (){}
        public init (info : [String:Any]) {
            
            if let id = info["_id"] as? String {
                self._id = id
            }
            if let count = info["count"] as? Int {
                self.count = count
            }
            if let userId = info["userId"] as? String {
                self.userId = userId
            }
            if let dataUser = info["userData"] as? [String:Any] {
                if let id = dataUser["_id"] as? String {
                    self.userData_id = id
                }
                if let email = dataUser["email"] as? String {
                    self.userData_email = email
                }
                if let userName = dataUser["userName"] as? String {
                    self.userData_userName = userName
                }
                if let name = dataUser["name"] as? String {
                    self.userData_name = name
                }
                if let photoUrl = dataUser["photoUrl"] as? String {
                    self.userData_photoUrl = photoUrl
                }
                if let followers = dataUser["followers"] as? Int {
                    self.userData_followers = followers
                }
                if let isFollow = dataUser["isFollow"] as? Bool {
                    self.userData_isFollow = isFollow
                }
            }
            self.isSelected = false
        }
    }
    class UserBroadCustomOrder {
        var _id = ""
        var comment = ""
        var currentTime = ""
        var orderCompletionTime = ""
        var isCompleted = false
        var isRejected = false
        var userId = ""
        var userData = [String:Any]()
        var userD_id = ""
        var userD_userName = ""
        var userD_name = ""
        var userD_photoUrl = ""
        var giftData = [String:Any]()
        var giftD_id = ""
        var giftD_giftType = ""
        var giftD_giftValue = 0
        var giftD_giftImage = ""
        
        public init (){}
        public init (info : [String:Any]) {
           
            if let id = info["_id"] as? String {
                self._id = id
            }
            if let comment = info["comment"] as? String {
                self.comment = comment
            }
            if let currentTime = info["currentTime"] as? String {
                self.currentTime = currentTime
            }
            if let orderCompletionTime = info["orderCompletionTime"] as? String {
                self.orderCompletionTime = orderCompletionTime
            }
            if let isCompleted = info["isCompleted"] as? Bool {
                self.isCompleted = isCompleted
            }
            if let isRejected = info["isRejected"] as? Bool {
                self.isRejected = isRejected
            }
            if let userId = info["userId"] as? String {
                self.userId = userId
            }
            if let dataUser = info["userData"] as? [String:Any] {
                if let id = dataUser["_id"] as? String {
                    self.userD_id = id
                }
                if let userName = dataUser["userName"] as? String {
                    self.userD_userName = userName
                }
                if let name = dataUser["name"] as? String {
                    self.userD_name = name
                }
                if let photoUrl = dataUser["photoUrl"] as? String {
                    self.userD_photoUrl = photoUrl
                }
            }
            if let dataGift = info["giftData"] as? [String:Any] {
                if let id = dataGift["_id"] as? String {
                    self.giftD_id = id
                }
                if let giftType = dataGift["giftType"] as? String {
                    self.giftD_giftType = giftType
                }
                if let giftValue = dataGift["giftValue"] as? Int {
                    self.giftD_giftValue = giftValue
                }
                if let giftImage = dataGift["giftImage"] as? String {
                    self.giftD_giftImage = giftImage
                }
            }
        }
    }
    class InvitesBroadcast {
        var _id = ""
        var userName = ""
        var isLiveStatus = false
        var name = ""
        var photoUrl = ""
        var isInvite = false
        var isSelected = false
        
        public init (){}
        public init (info : [String:Any]) {
            if let id = info["_id"] as? String {
                self._id = id
            }
            if let userName = info["userName"] as? String {
                self.userName = userName
            }
            if let isLiveStatus = info["isLiveStatus"] as? Bool {
                self.isLiveStatus = isLiveStatus
            }
            if let name = info["name"] as? String {
                self.name = name
            }
            if let photoUrl = info["photoUrl"] as? String {
                self.photoUrl = photoUrl
            }
            if let isInvite = info["isInvite"] as? Bool {
                self.isInvite = isInvite
            }
                self.isSelected = false
        }
 }
    class UpdateCustomOrder {
        var comment = ""
        var currentTime = ""
        var orderCompletionTime = ""
        var isCompleted = false
         var isRejected = false
        var id = ""
        var userIId = ""
        var broadcastId = ""
        var giftId = ""
        var broadcasterId = ""
        var createdAt = ""
        var updatedAt = ""
        var __v = 0
        var isSelected = false
        public init (){}
        public init (info : [String:Any]) {
            if let comment = info["comment"] as? String {
                self.comment = comment
            }
            if let currentTime = info["currentTime"] as? String {
                self.currentTime = currentTime
            }
            if let orderCompletionTime = info["orderCompletionTime"] as? String {
                self.orderCompletionTime = orderCompletionTime
            }
            if let isCompleted = info["isCompleted"] as? Bool {
                self.isCompleted = isCompleted
            }
            if let isRejected = info["isRejected"] as? Bool {
                self.isRejected = isRejected
            }
            if let _id = info["_id"] as? String {
                self.id = _id
            }
            if let userId = info["userId"] as? String {
                self.userIId = userId
            }
            if let broadcastId = info["broadcastId"] as? String {
                self.broadcastId = broadcastId
            }
            if let giftId = info["giftId"] as? String {
                self.giftId = giftId
            }
            if let broadcasterId = info["broadcasterId"] as? String {
                self.broadcasterId = broadcasterId
            }
            if let createdAt = info["createdAt"] as? String {
                self.createdAt = createdAt
            }
            if let updatedAt = info["updatedAt"] as? String {
                self.updatedAt = updatedAt
            }
            if let v = info["__v"] as? Int {
                self.__v = v
            }
            self.isSelected = false
        }
    }
    class VerifiCode{
        var verifiCode = 0
        public init (){}
        public init (info : [String:Any]) {
            if let verificationCode = info["verificationCode"] as? Int {
                self.verifiCode = verificationCode
            }
        }
    }
    class Viewer{
        var giftCount = 0
        var likes = 0
        var spend_viewStartT = ""
        var spend_viewEndT = ""
        var customOrder = 0
        public init (){}
        public init (info : [String:Any]) {
            if let giftCount = info["giftCount"] as? Int {
                self.giftCount = giftCount
            }
            if let likes = info["likes"] as? Int {
                self.likes = likes
            }
            if let dataSpending = info["spendingTime"] as? [String:Any] {
                if let StartT = dataSpending["viewStartTime"] as? String {
                    self.spend_viewStartT = StartT
                }
                if let EndT = dataSpending["viewEndTime"] as? String {
                    self.spend_viewEndT = EndT
                }
            }
            if let customOrder = info["customOrder"] as? Int {
                self.customOrder = customOrder
            }
        }
    }
    class homeCategoriesVideos {
        var id = ""
        var name = ""
        var numberOfVideos = 0
     // var homeVideos = [HomeVideos]()
        var homeVideos = [Featuredvideos]()
        public init () {}
        public init (info:[String : Any]) {
            if let name = info["name"] as? String {
                self.name = name
            }
            if let id = info["id"] as? String {
                self.id = id
            }
            if let number = info["numberOfVideos"] as? Int {
                self.numberOfVideos = number
            }
            if let homevideo = info["videos"] as? [[String:Any]] {
                for home in homevideo {
                    let CategoriesVideo = Featuredvideos(info: home)
                    self.homeVideos.append(CategoriesVideo)
                }
            }
        }
    }
    class ObjHomeData {
        var featuredvideos = [Featuredvideos]()
        var trendingvideos = [Featuredvideos]()
        var famousvideos = [Featuredvideos]()
        
        public init (){}
        public init (info:[String : Any]) {
            if let featuredv = info["featuredvideos"] as? [[String:Any]] {
                for featu in featuredv {
                    let featuredvData = Featuredvideos(info: featu)
                    self.featuredvideos.append(featuredvData)
                }
            }
            if let dataInfo = info["recentvideos"] as? [[String:Any]] {
                for data in dataInfo {
                    let tmpData = Featuredvideos(info: data)
                    self.trendingvideos.append(tmpData)
                }
            }
            if let dataInfo = info["famousvideos"] as? [[String:Any]] {
                for data in dataInfo {
                    let tmpData = Featuredvideos(info: data)
                    self.famousvideos.append(tmpData)
                }
                
            }
        }
        
    }
    class Featuredvideos {
        var _id = ""
        var isActive = false
        var __v = 0
        var isAutoDropProducts = false
        var isFeatured = false
        var emotions = 0
        var location = 0
        var people = 0
        var products = 0
        var analysisjsonurl = ""
        var objectjsonurl = ""
        var views = 0
        var tags = ""
        var categories = [String]()
        var updatedDate = ""
        var createdDate = ""
        var isStreamable = false
        var thumbLink = ""
        var videoProvider = ""
        var videoId = ""
        var description = ""
        var shortId = ""
        var duration = ""
        var durationInSec = 0
        var link = ""
        var title = ""
        var arrayActivites = [String]()
        public init (){}
        
        public init (info:[String : Any]) {
            
            if let _id = info["_id"] as? String {
                self._id = _id
            }
            if let Active = info["isActive"] as? Bool {
                self.isActive = Active
            }
            if let V = info["__v"] as? Int {
                self.__v = V
            }
            if let isAutoDropProducts = info["isAutoDropProducts"] as? Bool {
                self.isAutoDropProducts = isAutoDropProducts
            }
            if let isFeatured = info["isFeatured"] as? Bool {
                self.isFeatured = isFeatured
            }
            if let emotions = info["emotions"] as? Int {
                self.emotions = emotions
            }
            if let locations = info["locations"] as? Int {
                self.location = locations
            }
            if let people = info["people"] as? Int {
                self.people = people
            }
            if let products = info["products"] as? Int {
                self.products = products
            }
            if let analysisjsonurl = info["analysisjsonurl"] as? String {
                self.analysisjsonurl = analysisjsonurl
            }
            if let objectjsonurl = info["objectjsonurl"] as? String {
                self.objectjsonurl = objectjsonurl
            }
            if let views = info["views"] as? Int {
                self.views = views
            }
            if let tags = info["tags"] as? String {
                self.tags = tags
            }
            if let categories = info["categories"] as? [String] {
                self.categories = categories
            }
            if let updatedDate = info["updatedDate"] as? String {
                self.updatedDate = updatedDate
            }
            if let createdDate = info["createdDate"] as? String {
                self.createdDate = createdDate
            }
            if let isStreamable = info["isStreamable"] as? Bool {
                self.isStreamable = isStreamable
            }
            if let thumbLink = info["thumbLink"] as? String {
                self.thumbLink = thumbLink
            }
            if let videoProvider = info["videoProvider"] as? String {
                self.videoProvider = videoProvider
            }
            if let videoId = info["videoId"] as? String {
                self.videoId = videoId
            }
            if let description = info["description"] as? String {
                self.description = description
            }
            if let shortId = info["shortId"] as? String {
                self.shortId = shortId
            }
            if let duration = info["duration"] as? String {
                self.duration = duration
                if self.duration.contains("M") && self.duration.contains("S")
                {
                    let strArrM = self.duration.components(separatedBy: "M")
                    if strArrM.count > 0{
                        let strSec = strArrM[1]
                        let strS = strSec.replacingOccurrences(of: "S", with: "")
                        var min = Int(strArrM[0])
                        min = min!*60
                        let sec = Int(strS)
                        self.durationInSec = sec!+min!
                    }
                    
                }else if self.duration.contains("M")
                {
                    let strM = self.duration.replacingOccurrences(of: "M", with: "")
                    var min = Int(strM)
                    min = min!*60
                    self.durationInSec = min!
                }else if self.duration.contains("S")
                {
                    let strS = self.duration.replacingOccurrences(of: "S", with: "")
                    self.durationInSec = Int(strS)!
                }
            }
            if let link = info["link"] as? String {
                self.link = link
            }
            if let title = info["title"] as? String {
                self.title = title
            }
        }
        
        func increaseVideoViewCount(completion:@escaping (_ result: Bool) -> Void) {
            //let ID = self.selectedFeaturedvideo.shortId
            let Url =  BaseURL + "home/videos/view/" + self.videoId
            Service().get(url: Url) { (result) in
                print("Api home cat id ======  \(result)")
                if result is [String : Any] {
                    let dataInfo = result as! [String : Any]
                    if dataInfo["success"] as! Bool {
                        self.views += 1
                        completion(true)
                    }
                    else{
                        completion(false)
                    }
                }
                
            }
        }
    }
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ??
    // MARK: Old Model
    class CategoryOffers {
        var id = ""
        var title = ""
        var link = ""
        var thumbLink = ""
        var offerTitle = ""
        var isVisible = false
        var isInStock = false
        var productDescription = ""
        var accountId = ""
        var price = 0
        var points = 0
        var awsUrl = ""
        var isSelected = false
        public init (){}
        public init (info:[String : Any]) {
            
            if info["_id"] != nil {
                self.id  = info["_id"] as! String
            }
            if info["price"] != nil {
                self.price  = info["price"] as! Int
            }
            if info["points"] != nil {
                self.points  = info["points"] as! Int
            }
            if info["pDescription"] != nil {
                self.productDescription  = info["pDescription"] as! String
            }
            if info["offerTitle"] != nil {
                self.offerTitle  = info["offerTitle"] as! String
            }
            if info["title"] != nil {
                self.title  = info["title"] as! String
            }
            if let link = info["link"] as? String {
                self.link = link
            }
            if let accountId = info["accountId"] as? String {
                self.accountId = accountId
            }
            if let thumbLink = info["thumbLink"] as? String {
                self.thumbLink = thumbLink
            }
            if let isVisible = info["isVisible"] as? Bool {
                self.isVisible = isVisible
            }
            if let isInStock = info["isInStock"] as? Bool {
                self.isInStock = isInStock
            }
            self.isSelected = false
        }
    }
    class FeaturedModel {
        var _id = ""
        var title = ""
        var category_title = ""
        var isVisible = false
        var isYoutube = false
        var isVimeo = false
        var shortId = ""
        var videoLink = ""
        var videoThumbLink = ""
        var tags = ""
        var viewCount = 0
        var viemoId = ""
        var appId = ""
        var subAppIds =  [String]()
        var updatedDate = ""
        var createdDate = ""
        var comments = [String: Any]()
        var date_com = ""
        var msg_com = ""
        var userid_com = ""
        var id_com = ""
        var likes = [String]()
        var pulseSize = ""
        var restrictSimilar = false
        var gif_Image = ""
        var Show_gif_Image = false
        var sdkUrl = ""
        var seekAt = 0
        var similar_product = false
        var informations = false
        var subcategories = ""
        var locations = [String:Any]()
        var name_loc = ""
        var id_loc = ""
        var _id_loc = ""
        var celebrities = ""
        var photos = ""
        var description = ""
        var contentProvider = ""
        var featured = false
        var campaignId = ""
        var watchingRewardPoints = 0
        var settingOrder = 0
        var orderid = 0
        var totalNumberOfProducts = 0
        var fashionstatements = ""
        var isAWS = false
        var follow_box_frame = false
        var category = [String]()
        var __v = 0
        var poster_image = ""
        var awsUrl = ""
        var movie_name = ""
        var json_awsUrl = ""
        var designers = [String:Any]()
        var name_des = ""
        var id_des = ""
        var _id_des = ""
        var productmessage = [[String:Any]]()
        var hangout_pro_ios = ""
        var twitter_pro_ios = ""
        var facebook_pro_ios = ""
        var whatsapp_pro_ios = ""
        var email_pro_ios = ""
        var videomessage = [[String:Any]]()
        var hangout_videome = ""
        var twitter_videome = ""
        var facebook_videome = ""
        var whatsapp_videome = ""
        var email_videome = ""
        var zipUrl = ""
        var styleDesign = ""
        var productshare_title = ""
        var videoshare_title = ""
        var cursorDesign = ""
        var featureProducts = ""
        var Autodetected = false
        var simulationFrames = ""
        var resize_Images = ""
        var stripeStatus = false
        var clickPlan = ""
        var videoStatus = ""
        var productsFiltered = ""
        var location = ""
        public init (){}
        
        public init (info:[String : Any]) {
            
            if let _id = info["_id"] as? String {
                self._id = _id
            }
            if let title = info["title"] as? String {
                self.title = title
            }
            if let category_title = info["category_title"] as? String {
                self.category_title = category_title
            }
            if let isVisible = info["isVisible"] as? Bool {
                self.isVisible = isVisible
            }
            if let isYoutube = info["isYoutube"] as? Bool {
                self.isYoutube = isYoutube
            }
            if let isVimeo = info["isVimeo"] as? Bool {
                self.isVimeo = isVimeo
            }
            if let shortId = info["shortId"] as? String {
                self.shortId = shortId
            }
            if let videoLink = info["videoLink"] as? String {
                self.videoLink = videoLink
            }
            if let videoTh = info["videoThumbLink"] as? String {
                self.videoThumbLink = videoTh
            }
            if let tags = info["tags"] as? String {
                self.tags = tags
            }
            if let viewCount = info["viewCount"] as? Int {
                self.viewCount = viewCount
            }
            if let viemoId = info["viemoId"] as? String {
                self.viemoId = viemoId
            }
            if let appId = info["appId"] as? String {
                self.appId = appId
            }
            if let updatedDate = info["updatedDate"] as? String {
                self.updatedDate = updatedDate
            }
            if let createdDate = info["createdDate"] as? String {
                self.createdDate = createdDate
            }
            if let pulseSize = info["pulseSize"] as? String {
                self.pulseSize = pulseSize
            }
            if let restrictSimilar = info["restrictSimilar"] as? Bool {
                self.restrictSimilar = restrictSimilar
            }
            if let gif_Image = info["gif_Image"] as? String {
                self.gif_Image = gif_Image
            }
            if let Show_gif_Image = info["Show_gif_Image"] as? Bool {
                self.Show_gif_Image = Show_gif_Image
            }
            if let sdkUrl = info["sdkUrl"] as? String {
                self.sdkUrl = sdkUrl
            }
            if let seekAt = info["seekAt"] as? Int {
                self.seekAt = seekAt
            }
            if let similar_product = info["similar_product"] as? Bool {
                self.similar_product = similar_product
            }
            if let informations = info["informations"] as? Bool {
                self.informations = informations
            }
            if let videoLink = info["subcategories"] as? String {
                self.videoLink = videoLink
            }
            if let videoTh = info["celebrities"] as? String {
                self.videoThumbLink = videoTh
            }
            if let photos = info["photos"] as? String {
                self.photos = photos
            }
            if let description = info["description"] as? String {
                self.description = description
            }
            if let contentProvider = info["contentProvider"] as? String {
                self.contentProvider = contentProvider
            }
            if let featured = info["featured"] as? Bool {
                self.featured = featured
            }
            if let campaignId = info["campaignId"] as? String {
                self.campaignId = campaignId
            }
            if let watchs = info["watchingRewardPoints"] as? Int {
                self.watchingRewardPoints = watchs
            }
            if let settingOrder = info["settingOrder"] as? Int {
                self.settingOrder = settingOrder
            }
            if let orderid = info["orderid"] as? Int {
                self.orderid = orderid
            }
            if let totalNu = info["totalNumberOfProducts"] as? Int {
                self.totalNumberOfProducts = totalNu
            }
            if let fashionstatements = info["fashionstatements"] as? String {
                self.fashionstatements = fashionstatements
            }
            if let isAWS = info["isAWS"] as? Bool {
                self.isAWS = isAWS
            }
            if let follow_box_frame = info["follow_box_frame"] as? Bool {
                self.follow_box_frame = follow_box_frame
            }
            if let V = info["__v"] as? Int {
                self.__v = V
            }
            if let poster_image = info["poster_image"] as? String {
                self.poster_image = poster_image
            }
            if let awsUrl = info["awsUrl"] as? String {
                self.awsUrl = awsUrl
            }
            if let movie_name = info["movie_name"] as? String {
                self.movie_name = movie_name
            }
            if let json_awsUrl = info["json_awsUrl"] as? String {
                self.json_awsUrl = json_awsUrl
            }
            if let zipUrl = info["zipUrl"] as? String {
                self.zipUrl = zipUrl
            }
            if let styleDesign = info["styleDesign"] as? String {
                self.styleDesign = styleDesign
            }
            if let productshare_title = info["productshare_title"] as? String {
                self.productshare_title = productshare_title
            }
            if let videoshare_title = info["videoshare_title"] as? String {
                self.videoshare_title = videoshare_title
            }
            if let cursorDesign = info["cursorDesign"] as? String {
                self.cursorDesign = cursorDesign
            }
            if let featureProducts = info["featureProducts"] as? String {
                self.featureProducts = featureProducts
            }
            if let Autodetected = info["Autodetected"] as? Bool {
                self.Autodetected = Autodetected
            }
            if let simulationFrames = info["simulationFrames"] as? String {
                self.simulationFrames = simulationFrames
            }
            if let resize_Images = info["resize_Images"] as? String {
                self.resize_Images = resize_Images
            }
            if let stripeStatus = info["stripeStatus"] as? Bool {
                self.stripeStatus = stripeStatus
            }
            if let clickPlan = info["clickPlan"] as? String {
                self.clickPlan = clickPlan
            }
            if let videoStatus = info["videoStatus"] as? String {
                self.videoStatus = videoStatus
            }
            if let productsFiltered = info["productsFiltered"] as? String {
                self.productsFiltered = productsFiltered
            }
            if let location = info["location"] as? String {
                self.location = location
            }
            if let subAppIds = info["subAppIds"] as? [String] {
                self.subAppIds = subAppIds
            }
            if let likes = info["likes"] as? [String] {
                self.likes = likes
            }
            if let cate = info["category"] as? [String] {
                self.category = cate
            }
            if let comments = info["comments"] as? [String:Any] {
                self.date_com = comments["date"]! as! String
                self.msg_com = comments["msg"]! as! String
                self.userid_com = comments["userid"]! as! String
                self.id_com = comments["_id"]! as! String
            }
            if let location = info["locations"] as? [String:Any] {
                self.name_loc = location["name"]! as! String
                self.id_loc = location["id"]! as! String
                self._id_loc = location["_id"]! as! String
            }
            if let desig = info["designers"] as? [String:Any] {
                self.name_des = desig["name"]! as! String
                self.id_des = desig["id"]! as! String
                self._id_des = desig["_id"]! as! String
            }
        }
    }
    class CommentModel {
        var _id = ""
        var date = ""
        var userid = ""
        var msg = ""
        public init (){}
        public init (info:[String : Any]) {
            if let _id = info["_id"] as? String {
                self._id = _id
            }
            if let date = info["date"] as? String {
                self.date = date
            }
            if let msg = info["msg"] as? String {
                self.msg = msg
            }
            if let userid = info["userid"] as? String {
                self.userid = userid
            }
        }
    }
    
    class Songs {
        var _id = ""
        var title = ""
        var category_title = ""
        var isVisible = false
        var isYoutube = false
        var isVimeo = false
        var shortId = ""
        var videoLink = ""
        var videoThumbLink = ""
        var tags = ""
        var viewCount = 0
        var viemoId = ""
        var appId = ""
        var subAppIds =  [String]()
        var updatedDate = ""
        var createdDate = ""
        var comments = [CommentModel]()
        var likes = [String]()
        var pulseSize = ""
        var restrictSimilar = false
        var gif_Image = ""
        var Show_gif_Image = false
        var sdkUrl = ""
        var seekAt = 0
        var similar_product = false
        var informations = false
        var subcategories = ""
        var locations = [[String:Any]]()
        var name_loc = ""
        var id_loc = ""
        var _id_loc = ""
        var celebrities = ""
        var photos = ""
        var description = ""
        var contentProvider = ""
        var featured = false
        var campaignId = ""
        var watchingRewardPoints = 0
        var settingOrder = 0
        var orderid = 0
        var totalNumberOfProducts = 0
        var fashionstatements = ""
        var isAWS = false
        var follow_box_frame = false
        var category = [String]()
        var poster_image = ""
        var awsUrl = ""
        var movie_name = ""
        var json_awsUrl = ""
        var designers = [String:Any]()
        var name_des = ""
        var id_des = ""
        var _id_des = ""
        var productmessage = [String:Any]()
        var hangoutProMsg = ""
        var twitterProMsg = ""
        var facebookProMsg = ""
        var whatsappProMsg = ""
        var emailProMsg = ""
        var videoMessage = [String:Any]()
        var hangoutVideoMsg = ""
        var twitterVideoMsg = ""
        var facebookVideoMsg = ""
        var whatsappVideoMsg = ""
        var emailVideoMsg = ""
        var zipUrl = ""
        var styleDesign = ""
        var productshare_title = ""
        var videoshare_title = ""
        var cursorDesign = ""
        var featureProducts = [Any]()
        var Autodetected = false
        var simulationFrames = [Any]()
        var resize_Images = [Any]()
        var stripeStatus = false
        var clickPlan = ""
        var videoStatus = ""
        var productsFiltered = [Any]()
        var location = ""
        public init (){}
        public init (info:[String : Any]) {
            if let _id = info["_id"] as? String {
                self._id = _id
            }
            if let title = info["title"] as? String {
                self.title = title
            }
            if let category_title = info["category_title"] as? String {
                self.category_title = category_title
            }
            if let isVisible = info["isVisible"] as? Bool {
                self.isVisible = isVisible
            }
            if let isYoutube = info["isYoutube"] as? Bool {
                self.isYoutube = isYoutube
            }
            if let isVimeo = info["isVimeo"] as? Bool {
                self.isVimeo = isVimeo
            }
            if let shortId = info["shortId"] as? String {
                self.shortId = shortId
            }
            if let videoLink = info["videoLink"] as? String {
                self.videoLink = videoLink
            }
            if let videoTh = info["videoThumbLink"] as? String {
                self.videoThumbLink = videoTh
            }
            if let tags = info["tags"] as? String {
                self.tags = tags
            }
            if let viewCount = info["viewCount"] as? Int {
                self.viewCount = viewCount
            }
            if let viemoId = info["viemoId"] as? String {
                self.viemoId = viemoId
            }
            if let appId = info["appId"] as? String {
                self.appId = appId
            }
            if let updatedDate = info["updatedDate"] as? String {
                self.updatedDate = updatedDate
            }
            if let createdDate = info["createdDate"] as? String {
                self.createdDate = createdDate
            }
            if let pulseSize = info["pulseSize"] as? String {
                self.pulseSize = pulseSize
            }
            if let restrictSimilar = info["restrictSimilar"] as? Bool {
                self.restrictSimilar = restrictSimilar
            }
            if let gif_Image = info["gif_Image"] as? String {
                self.gif_Image = gif_Image
            }
            if let Show_gif_Image = info["Show_gif_Image"] as? Bool {
                self.Show_gif_Image = Show_gif_Image
            }
            if let sdkUrl = info["sdkUrl"] as? String {
                self.sdkUrl = sdkUrl
            }
            if let seekAt = info["seekAt"] as? Int {
                self.seekAt = seekAt
            }
            if let similar_product = info["similar_product"] as? Bool {
                self.similar_product = similar_product
            }
            if let informations = info["informations"] as? Bool {
                self.informations = informations
            }
            if let videoLink = info["subcategories"] as? String {
                self.videoLink = videoLink
            }
            if let videoTh = info["celebrities"] as? String {
                self.videoThumbLink = videoTh
            }
            if let photos = info["photos"] as? String {
                self.photos = photos
            }
            if let description = info["description"] as? String {
                self.description = description
            }
            if let contentProvider = info["contentProvider"] as? String {
                self.contentProvider = contentProvider
            }
            if let featured = info["featured"] as? Bool {
                self.featured = featured
            }
            if let campaignId = info["campaignId"] as? String {
                self.campaignId = campaignId
            }
            if let watchs = info["watchingRewardPoints"] as? Int {
                self.watchingRewardPoints = watchs
            }
            if let settingOrder = info["settingOrder"] as? Int {
                self.settingOrder = settingOrder
            }
            if let orderid = info["orderid"] as? Int {
                self.orderid = orderid
            }
            if let totalNu = info["totalNumberOfProducts"] as? Int {
                self.totalNumberOfProducts = totalNu
            }
            if let fashionstatements = info["fashionstatements"] as? String {
                self.fashionstatements = fashionstatements
            }
            if let isAWS = info["isAWS"] as? Bool {
                self.isAWS = isAWS
            }
            if let follow_box_frame = info["follow_box_frame"] as? Bool {
                self.follow_box_frame = follow_box_frame
            }
            if let poster_image = info["poster_image"] as? String {
                self.poster_image = poster_image
            }
            if let awsUrl = info["awsUrl"] as? String {
                self.awsUrl = awsUrl
            }
            if let movie_name = info["movie_name"] as? String {
                self.movie_name = movie_name
            }
            if let json_awsUrl = info["json_awsUrl"] as? String {
                self.json_awsUrl = json_awsUrl
            }
            if let zipUrl = info["zipUrl"] as? String {
                self.zipUrl = zipUrl
            }
            if let styleDesign = info["styleDesign"] as? String {
                self.styleDesign = styleDesign
            }
            if let productshare_title = info["productshare_title"] as? String {
                self.productshare_title = productshare_title
            }
            if let videoshare_title = info["videoshare_title"] as? String {
                self.videoshare_title = videoshare_title
            }
            if let cursorDesign = info["cursorDesign"] as? String {
                self.cursorDesign = cursorDesign
            }
            if let featureProducts = info["featureProducts"] as? [Any] {
                self.featureProducts = featureProducts
            }
            if let Autodetected = info["Autodetected"] as? Bool {
                self.Autodetected = Autodetected
            }
            if let simulationFrames = info["simulationFrames"] as? [Any] {
                self.simulationFrames = simulationFrames
            }
            if let resize_Images = info["resize_Images"] as? [Any] {
                self.resize_Images = resize_Images
            }
            if let stripeStatus = info["stripeStatus"] as? Bool {
                self.stripeStatus = stripeStatus
            }
            if let clickPlan = info["clickPlan"] as? String {
                self.clickPlan = clickPlan
            }
            if let videoStatus = info["videoStatus"] as? String {
                self.videoStatus = videoStatus
            }
            if let productsFiltered = info["productsFiltered"] as? [Any] {
                self.productsFiltered = productsFiltered
            }
            if let location = info["location"] as? String {
                self.location = location
            }
            if let subAppIds = info["subAppIds"] as? [String] {
                self.subAppIds = subAppIds
            }
            if let likes = info["likes"] as? [String] {
                self.likes = likes
            }
            if let cate = info["category"] as? [String] {
                self.category = cate
            }
            if let comm = info["comments"] as? [[String:Any]] {
                for commments in comm {
                    let commData = CommentModel(info: commments)
                    self.comments.append(commData)
                }
            }
            if let messagePro = info["productmessage"] as? [String:Any] {
                let iosMsg = messagePro["ios"] as! [String:Any]
                self.hangoutProMsg = iosMsg["hangout"] as! String
                self.twitterProMsg = iosMsg["twitter"] as! String
                self.facebookProMsg = iosMsg["facebook"] as! String
                self.whatsappProMsg = iosMsg["whatsapp"] as! String
                self.emailProMsg = iosMsg["email"] as! String
            }
            if let message = info["videomessage"] as? [String:Any] {
                let iosMsg = message["ios"] as! [String:Any]
                self.hangoutVideoMsg = iosMsg["hangout"] as! String
                self.twitterVideoMsg = iosMsg["twitter"] as! String
                self.facebookVideoMsg = iosMsg["facebook"] as! String
                self.whatsappVideoMsg = iosMsg["whatsapp"] as! String
                self.emailVideoMsg = iosMsg["email"] as! String
            }
            
            if let location = info["locations"] as? [String:Any] {
                self.name_loc = location["name"]! as! String
                self.id_loc = location["id"]! as! String
                self._id_loc = location["_id"]! as! String
            }
            if let desig = info["designers"] as? [String:Any] {
                self.name_des = desig["name"]! as! String
                self.id_des = desig["id"]! as! String
                self._id_des = desig["_id"]! as! String
            }
        }
    }
    
    class Fashiondesigner {
        var imageUrl = ""
        var _id = ""
        var name = ""
        var locationAddress = ""
        var categoryid = ""
        var categoryname = ""
        var type = ""
        var description = ""
        var isVisible = false
        var appId = ""
        var dob = ""
        var profession = ""
        var wikipediaurl = ""
        var updatedDate =  ""
        var __v = 0
        var tags = ""
        var viewsCount = 0
        var photos = [String]()
        var companies = [[String:Any]]()
        var settingOrder = 0
        var orderid = 0
        var brands = ""
        var days = 0
        var awsUrl = ""
        public init (){}
        public init (info:[String : Any]) {
            if let styleDesign = info["imageUrl"] as? String {
                self.imageUrl = styleDesign
            }
            if let productshare_title = info["_id"] as? String {
                self._id = productshare_title
            }
            if let videoshare_title = info["name"] as? String {
                self.name = videoshare_title
            }
            if let cursorDesign = info["locationAddress"] as? String {
                self.locationAddress = cursorDesign
            }
            if let featureProducts = info["categoryid"] as? String {
                self.categoryid = featureProducts
            }
            if let type = info["type"] as? String {
                self.type = type
            }
            if let description = info["description"] as? String {
                self.description = description
            }
            if let isVisible = info["isVisible"] as? Bool {
                self.isVisible = isVisible
            }
            if let appId = info["appId"] as? String {
                self.appId = appId
            }
            if let dob = info["dob"] as? String {
                self.dob = dob
            }
            if let videoStatus = info["profession"] as? String {
                self.profession = videoStatus
            }
            if let productsFiltered = info["wikipediaurl"] as? String {
                self.wikipediaurl = productsFiltered
            }
            if let update = info["updatedDate"] as? String {
                self.updatedDate = update
            }
            if let v = info["__v"] as? Int {
                self.__v = v
            }
            if let likes = info["tags"] as? String {
                self.tags = likes
            }
            if let viewsCount = info["viewsCount"] as? Int {
                self.viewsCount = viewsCount
            }
            if let companies = info["companies"] as? [[String:Any]] {
                self.companies = companies
            }
            if let settingOrder = info["settingOrder"] as? Int {
                self.settingOrder = settingOrder
            }
            if let brands = info["brands"] as? String {
                self.brands = brands
            }
            if let orderid = info["orderid"] as? Int {
                self.orderid = orderid
            }
            if let days = info["days"] as? Int {
                self.days = days
            }
            if let awsUrl = info["awsUrl"] as? String {
                self.awsUrl = awsUrl
            }
        }
    }
