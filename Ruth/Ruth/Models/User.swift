//
//  User.swift
//  Ruth
//
//  Created by mac on 16/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

var mainUser: User?

class User: Decodable {
    var updatedAt: String?
    let id: String?
    var photoUrl: String?
    var gender: String?
    var country: String?
    var role: String?
    var mobileNo: Int?
    var accountType: String?
    var userName: String?
    var email: String?
    var socialID: String?
    var isNewUser: Bool?
    var status: Int?
//    var followers: Int?
    var giftCount: Int?
    var isVip: Bool?
    var pointEarn:[Point]?
    var live:[otherUser]?
    var videos:[otherUser]?
    
    private enum CodingKeys: String, CodingKey {
        case updatedDate = "updatedAt"
        case id = "_id"
        case photoUrl = "photoUrl"
        case gender = "gender"
        case country = "country"
        case role = "role"
        case mobileNo = "mobileNo"
        case accountType = "accountType"
        case userName = "userName"
        case email = "email"
        case socialID = "socialID"
        case isNewUser = "isNewUser"
        case status = "status"
//        case followers = "followers"
        case giftCount = "giftCount"
        case isVip = "isVip"
        case pointEarn = "pointEarn"
//        case live = "live"
//        case videos = "videos"
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        updatedAt = try values.decodeIfPresent(String.self, forKey: CodingKeys.updatedDate)
        id = try values.decodeIfPresent(String.self, forKey: CodingKeys.id)
        photoUrl = try values.decodeIfPresent(String.self, forKey: CodingKeys.photoUrl)
        gender = try values.decodeIfPresent(String.self, forKey: CodingKeys.gender)
        country = try values.decodeIfPresent(String.self, forKey: CodingKeys.country)
        role = try values.decodeIfPresent(String.self, forKey: CodingKeys.role)
        mobileNo = try values.decodeIfPresent(Int.self, forKey: CodingKeys.mobileNo)
        accountType = try values.decodeIfPresent(String.self, forKey: CodingKeys.accountType)
        userName = try values.decodeIfPresent(String.self, forKey: CodingKeys.userName)
        email = try values.decodeIfPresent(String.self, forKey: CodingKeys.email)
        socialID = try values.decodeIfPresent(String.self, forKey: CodingKeys.socialID)
        isNewUser = try values.decodeIfPresent(Bool.self, forKey: CodingKeys.isNewUser)
        status = try values.decodeIfPresent(Int.self, forKey: CodingKeys.status)
        giftCount = try values.decodeIfPresent(Int.self, forKey: CodingKeys.giftCount)
        isVip = try values.decodeIfPresent(Bool.self, forKey: CodingKeys.isVip)
        pointEarn = try values.decodeIfPresent([Point].self, forKey: CodingKeys.pointEarn)
//        live = try values.decodeIfPresent([otherUser].self, forKey: CodingKeys.live)
//        videos = try values.decodeIfPresent([otherUser].self, forKey: CodingKeys.videos)
    }
}
class SignupResponse: Decodable {
    let status: Bool?
    let message: String?
    let zh_Hans: String?
    let zh_Hant: String?
    let token: String?
    let data: User?
}

class OtpResponse: Decodable {
    let status: Bool?
    let message: String?
    let zh_Hans: String?
    let zh_Hant: String?
    let token: String?
}
class Point: Decodable {
    var _id: String?
    var total: Int?
    
    private enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case total = "total"
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: CodingKeys._id)
        total = try values.decodeIfPresent(Int.self, forKey: CodingKeys.total)
        rLog("Point === \(String(describing: _id)) TOTAL ==== \(String(describing: total))")
    }

}

class otherUser: Decodable {
    var id: String?
    var coverUrl: String?
    var broadcastTitle: String?
    var broadcastTag: String?
    var broadcastViews: Int?
    var broadcastUrl: String?
    var broadcastStreamId: String?
    let isLive: Bool?
    let isActive: Bool?
    var broadcasterId: String?
    var createdAt: String?
    var viewCount: Int?
    var broadcastLikes: Int?
    var broadcastComments: Int?
    var emoji: Int?
    var viewsUsers: [ViewsUsers]?
    
    
    private enum CodingKeys: String, CodingKey {
        case id = "_id"
        case coverUrl = "coverUrl"
        case broadcastTitle = "broadcastTitle"
        case broadcastTag = "broadcastTag"
        case broadcastViews = "broadcastViews"
        case broadcastUrl = "broadcastUrl"
        case broadcastStreamId = "broadcastStreamId"
        case isLive = "isLive"
        case isActive = "isActive"
        case broadcasterId = "broadcasterId"
        case createdAt = "createdAt"
        case viewCount = "viewCount"
        case broadcastLikes = "broadcastLikes"
        case broadcastComments = "broadcastComments"
        case emoji = "emoji"
        case viewsUsers = "viewsUsers"
        
        
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: CodingKeys.id)
        coverUrl = try values.decodeIfPresent(String.self, forKey: CodingKeys.coverUrl)
        broadcastTitle = try values.decodeIfPresent(String.self, forKey: CodingKeys.broadcastTitle)
        broadcastTag = try values.decodeIfPresent(String.self, forKey: CodingKeys.broadcastTag)
        broadcastViews = try values.decodeIfPresent(Int.self, forKey: CodingKeys.broadcastViews)
        broadcastUrl = try values.decodeIfPresent(String.self, forKey: CodingKeys.broadcastUrl)
        broadcastStreamId = try values.decodeIfPresent(String.self, forKey: CodingKeys.broadcastStreamId)
        isLive = try values.decodeIfPresent(Bool.self, forKey: CodingKeys.isLive)
        isActive = try values.decodeIfPresent(Bool.self, forKey: CodingKeys.isActive)
        broadcasterId = try values.decodeIfPresent(String.self, forKey: CodingKeys.broadcasterId)
        createdAt = try values.decodeIfPresent(String.self, forKey: CodingKeys.createdAt)
        viewCount = try values.decodeIfPresent(Int.self, forKey: CodingKeys.viewCount)
        broadcastLikes = try values.decodeIfPresent(Int.self, forKey: CodingKeys.broadcastLikes)
        broadcastComments = try values.decodeIfPresent(Int.self, forKey: CodingKeys.broadcastComments)
        emoji = try values.decodeIfPresent(Int.self, forKey: CodingKeys.emoji)
        viewsUsers = try values.decodeIfPresent([ViewsUsers].self, forKey: CodingKeys.viewsUsers)
    }
    
}
class ViewsUsers: Decodable {
    var id: String?
    var photoUrl: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id = "_id"
        case photoUrl = "photoUrl"
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: CodingKeys.id)
        photoUrl = try values.decodeIfPresent(Int.self, forKey: CodingKeys.photoUrl)
    }
    
}


struct AllFollowUser : Codable {
    let status : Bool?
    let message : String?
    let data : [FollowUsers]?
    let zh_Hans : String?
    let zh_Hant : String?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
        case zh_Hans = "zh_Hans"
        case zh_Hant = "zh_Hant"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([FollowUsers].self, forKey: .data)
        zh_Hans = try values.decodeIfPresent(String.self, forKey: .zh_Hans)
        zh_Hant = try values.decodeIfPresent(String.self, forKey: .zh_Hant)
    }
    
}

struct FollowUsers : Codable {
    let _id : String?
    let email : String?
    let userName : String?
    let mobileNo : Int?
    let status : Int?
    let gender : String?
    let dob : String?
    let address : String?
    let name : String?
    let photoUrl : String?
    let followers : Int?
    let isFollow : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case _id = "_id"
        case email = "email"
        case userName = "userName"
        case mobileNo = "mobileNo"
        case status = "status"
        case gender = "gender"
        case dob = "dob"
        case address = "address"
        case name = "name"
        case photoUrl = "photoUrl"
        case followers = "followers"
        case isFollow = "isFollow"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        mobileNo = try values.decodeIfPresent(Int.self, forKey: .mobileNo)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        photoUrl = try values.decodeIfPresent(String.self, forKey: .photoUrl)
        followers = try values.decodeIfPresent(Int.self, forKey: .followers)
        isFollow = try values.decodeIfPresent(Bool.self, forKey: .isFollow)
    }
    
}


