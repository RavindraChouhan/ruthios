//
//  SignupDetailController.swift
//  Ruth
//
//  Created by mac on 19/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class SignupDetailController: UIViewController {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lblRegi: UILabel!
    @IBOutlet weak var lblAppN: UILabel!
    @IBOutlet weak var btnShowPass: UIButton!
    
    var selectedUsername: String?
    var selectedPhone: String?
    var loginService: LoginService = NetworkClient.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblRegi.text =  "Register using Phone Number".localized()
        self.lblAppN.text =  "Ruth".localized()
          self.txtEmail.placeholder = "Enter Email".localized()
          self.txtUsername.placeholder = "Username".localized()
          self.txtPassword.placeholder = "Password".localized()
          self.txtUsername.text = self.selectedUsername
        self.addTapGestureToView()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func actionShowPassword(_ sender: Any) {
        self.btnShowPass.isSelected = !self.btnShowPass.isSelected
        self.txtPassword.toggleSecureEntry()
    }
    @IBAction func actionNext(_ sender: Any) {
        self.view.endEditing(true)
        guard let email = self.txtEmail.text, let pwd = self.txtPassword.text else { return }
        if email.count == 0 {
            self.showAlertMessage(message: "Please enter email address".localized())
            return
        }
        if !email.isValidEmail() {
            self.showAlertMessage(message: "Please enter vaild email address".localized())
            return
        }
        if pwd.count == 0 {
            self.showAlertMessage(message: "Please enter password".localized())
            return
        }
        if pwd.count < 7 || pwd.count > 20 {
            self.showAlertMessage(message: "Password must contain 6-20 characters".localized())
            return
        }
        if !pwd.isValidPassword() {
            self.showAlertMessage(message: "Password must contain characters and numbers both".localized())
            return
        }
        else {
            if (self.txtPassword.text?.trim().count)! > 0 {
                self.btnNext.isSelected = true
                self.btnNext.isUserInteractionEnabled = true
            }else {
                self.btnNext.isSelected = false
                self.btnNext.isUserInteractionEnabled = false
            }
            
            Util.util.showHUD()
            loginService.registerByMobile(username: self.selectedUsername ?? "", mobile: self.selectedPhone ?? "", email: email, password: pwd) { (user, error) -> (Void) in
                Util.util.hideHUD()
                guard error == nil else {
                    self.showAlertMessage(message: error ?? "")
                    return
                }
                guard let tutorial = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.inviteView.rawValue) as? InviteController else { return }
                let navControl = UINavigationController(rootViewController: tutorial)
                self.present(navControl, animated: true, completion: nil)
            }
        }
    }
}

extension SignupDetailController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtUsername {
            self.txtEmail.becomeFirstResponder()
        }
        else if textField == self.txtEmail {
            self.txtPassword.becomeFirstResponder()
        }
        else if textField == self.txtPassword {
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //  self.txtBirthday.text = birthDate
        //        print("self.txtPwd.text?.count)\(self.txtCode.text?.count)")
        if (self.txtPassword.text?.count)! >= 5 {
            self.btnNext.isSelected = true
            self.btnNext.isUserInteractionEnabled = true
        }else {
            self.btnNext.isSelected = false
            self.btnNext.isUserInteractionEnabled = false
        }
        return true
    }
}
