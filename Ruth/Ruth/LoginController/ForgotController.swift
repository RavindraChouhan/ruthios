//
//  ForgotController.swift
//  Ruth
//
//  Created by mac on 15/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ForgotController: UIViewController {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSendCode: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtCode: UITextField!
     @IBOutlet weak var lblAN: UILabel!
     @IBOutlet weak var lblForG: UILabel!
    
    var seconds = 60
    var timerr: Timer!
    var skipOTPTime = Date()
    var loginService: LoginService = NetworkClient.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
          txtMobile.resignFirstResponder()
        txtCode.resignFirstResponder()
        self.txtMobile.addToolbarOnTextfield()
        self.addTapGestureToView()
        self.txtMobile.placeholder = "Mobile Number".localized()
        self.txtCode.placeholder = "Verification Code".localized()
        
        self.lblAN.text = "Ruth".localized()
        self.lblForG.text = "ForgotPassword".localized()
        self.btnSendCode.setTitle("Send".localized(), for: .normal)
       
    }
    @IBAction func actionBack(_ sender: Any) {
      self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSendCode(_ sender: Any) {
        self.view.endEditing(true)
        guard let mobile = self.txtMobile.text?.trim() else { return }
        if mobile.count == 0 {
            self.showAlertMessage(message: "Please enter mobile number".localized())
            return
        }
        if mobile.count > 0 {
            if (mobile.count < 8) {
                self.showAlertMessage(message: "Please enter vaild mobile number".localized())
            }
            else {
                self.btnSendCode.isHidden = true
                self.lblTimer.isHidden = false
                self.btnSendCode.setTitle("Resend".localized(), for: .normal)
                self.runTimer()
                Util.util.showHUD()
                let phoneNumber = self.getNumberWithCode(mobile: mobile)
                loginService.sendOtp(mobile: phoneNumber, username: nil, type: OTPType.forgot.rawValue, onResult: { (isSend, message) -> (Void) in
                    Util.util.hideHUD()
                    if isSend! == false {
//                        let msg = Util.util.getMsgWithKey(message!)
                        self.showAlertMessage(message: message!)
                        self.resetTimer()
                    }
                })
            }
        }
    }
    
    @objc func timerSendCode() {
        self.lblTimer.text = ""
    }
    
    @IBAction func actionNext(_ sender: Any) {
        self.view.endEditing(true)
        guard let mobile = self.txtMobile.text?.trim(), let code = self.txtCode.text?.trim() else { return }
        if mobile.count == 0 {
            self.showAlertMessage(message: "Please enter mobile number".localized())
            return
        }
        if (mobile.count < 8) {
            self.showAlertMessage(message: "Please enter vaild mobile number".localized())
            return
        }
        if code.count == 0 {
            self.showAlertMessage(message: "Please enter verification code".localized())
            return
        }
        if code.count < 6  {
            self.showAlertMessage(message: "Please enter valid verification code".localized())
            return
        }
        
        if mobile.count > 0 && code.count > 0 {
            Util.util.showHUD()
            let phoneNumber = self.getNumberWithCode(mobile: mobile)
            loginService.verifyOtp(mobile: phoneNumber, otpCode: code, onResult: { (isVerify, message) -> (Void) in
                if isVerify! {
                    self.forgotPassword(mobile: phoneNumber)
                }
                else {
                    Util.util.hideHUD()
//                    let msg = Util.util.getMsgWithKey(message!)
                    self.showAlertMessage(message: message!)
                   
                    self.resetTimer()
                }
            })
        }
    }
    
    func forgotPassword(mobile: String) {
        loginService.forgotPassword(mobile: mobile) { (isVerify, token) -> (Void) in
            Util.util.hideHUD()
            if isVerify! {
                guard let reset = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.resetView.rawValue) as? ResetPasswordController else { return }
                reset.tokenPwd = token ?? ""
                self.navigationController?.pushViewController(reset, animated: true)
            }
            else {
                self.showAlertMessage(message: token ?? "")
            }
        }
    }
}

extension ForgotController {
    func runTimer() {
        let skipDuretion = 1
        skipOTPTime = Date.addComponent(component: .minute, n: skipDuretion, date: Date())
        UserDefaults.standard.set(skipOTPTime, forKey: "OTPCodeTime")
        UserDefaults.standard.synchronize()
        
        self.timerr = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        let currentDate = Date()
        let str = currentDate.offsetFrom(date: skipOTPTime)
        
        self.lblTimer.text = "\(str) s"
        if str == "00" || str.hasPrefix("-") {
            self.resetTimer()
        }
    }
    
    @objc func comesInForeground() {
        if let skipSongTimeTmp = UserDefaults.standard.value(forKey: "OTPCodeTime") as? Date
        {
            skipOTPTime = skipSongTimeTmp
            if timerr == nil {
                self.timerr = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            }else{
                if timerr.isValid{
                    timerr.invalidate()
                    
                    self.timerr = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
                }
            }
        }
    }
    func resetTimer() {
        self.seconds = 60
        self.lblTimer.text = "\(self.seconds) s"
        self.btnSendCode.isHidden = false
        self.lblTimer.isHidden = true
        if self.timerr != nil {
            self.timerr.invalidate()
        }
        
    }
}

extension ForgotController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtMobile {
            self.txtCode.becomeFirstResponder()
        }
        else if textField == self.txtCode {
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //  self.txtBirthday.text = birthDate
        //        print("self.txtPwd.text?.count)\(self.txtCode.text?.count)")
        if (self.txtCode.text?.count)! >= 3 {
            self.btnNext.isSelected = true
            self.btnNext.isUserInteractionEnabled = true
        }else {
            self.btnNext.isSelected = false
            self.btnNext.isUserInteractionEnabled = false
        }
        return true
    }
}
