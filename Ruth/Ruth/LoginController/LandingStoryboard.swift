//
//  LandingStoryboard.swift
//  Ruth
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import Foundation
import UIKit

enum LandingControllers: String {
    case landingView = "LandingViewController"
    case termsView = "TermsPresentController"
    case ageView = "AgeModelController"
    case loginView = "LoginController"
    case signupView = "SignUpController"
    case forgotView = "ForgotController"
    case instagramView = "InstagramController"
    case signupDetail = "SignupDetailController"
    case resetView = "ResetPasswordController"
    case inviteView = "InviteController"
    case tutorialView = "TutorialController"
    case recommendedView = "RecommendedController"
}

enum LandingSegues: String {
    case google = "googleLogin"
    case instagram = "instagramLogin"
    case facebook = "facebookLogin"
}

extension UIStoryboard {
    class var landingStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}

class LandingStoryboard {
    
    class func showLandingScreen(over controller: UIViewController) {
        showInitialScreen(of: "Main", over: controller)
    }
    
    class func showTermsModalView(controller: UIViewController) {
        guard let aVC = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.termsView.rawValue) as? TermsPresentController else { return }
        aVC.modalPresentationStyle = .overCurrentContext
        aVC.modalTransitionStyle = .crossDissolve
        controller.present(aVC, animated: true, completion: nil)
    }
    
    class func showAgeModalView(controller: UIViewController) {
        guard let aVC = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.ageView.rawValue) as? AgeModelController else { return }
        aVC.modalPresentationStyle = .overCurrentContext
        aVC.modalTransitionStyle = .crossDissolve
        controller.present(aVC, animated: true, completion: nil)
    }    
    
    class private func showInitialScreen(of storyboardName: String, over controller: UIViewController) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        guard let signupViewController = storyboard.instantiateInitialViewController() else { return }
        guard let window = UIApplication.shared.keyWindow else { return }
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
        }, completion: { completed in
            for subview in window.subviews {
                subview.removeFromSuperview()
            }
            window.rootViewController = signupViewController
        })
    }
}
