
//
//  InstagramController.swift
//  Ruth
//
//  Created by mac on 18/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

protocol InstagramProtocol {
    func didGetInstaInformation(data: InstaData)
}

class InstagramController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
    
    var delegate: InstagramProtocol?
    var loginService: LoginService = NetworkClient.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Instagram"
        self.setUpNavigation(navControl: self.navigationController!)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.btnBack)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.unSignedRequest()
    }
    func setUpNavigation(navControl: UINavigationController)  {
        navControl.navigationBar.barTintColor = UIColor.black
        navControl.navigationBar.tintColor = UIColor.white
        navControl.navigationBar.barStyle = .default
        navControl.navigationBar.isTranslucent = true
        navControl.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18), NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.webView.frame = CGRect(x: 0, y: DeviceType.iPhoneX ? 88 : 64, width: ScreenSize.SCREEN_WIDTH, height: DeviceType.iPhoneX ? ScreenSize.SCREEN_HEIGHT - 88 : ScreenSize.SCREEN_HEIGHT - 64)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func unSignedRequest () {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=public_content", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL, INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID, INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        self.webView.loadRequest(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            self.handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        print("Instagram authentication token ==", authToken)
        var devicetoken = ""
        if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
            devicetoken = temp as! String
        }
        Util.util.showHUD()
        loginService.loginWithInstagram(authToken: authToken, deviceToken:devicetoken ) { (data, error) -> (Void) in
            Util.util.hideHUD()
            guard error == nil else {
                self.showAlertMessage(message: error ?? "")
                return
            }
            self.delegate?.didGetInstaInformation(data: data!)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension InstagramController : UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = false
        loginIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = true
        loginIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }

}
