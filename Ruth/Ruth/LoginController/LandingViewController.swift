//
//  LandingViewController.swift
//  Ruth
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class LandingViewController: UIViewController {
    @IBOutlet weak var lblAName: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var lblTerms: UILabel!
    var socialStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblAName.text = "Ruth".localized()
        self.lblLogin.text = "Login".localized()
        //Login to agree to the Terms & Conditions
        let strApp = "Login to agree to the ".localized()
        let strTerms = "Terms and conditions.".localized()
        //SFUIText-Medium
        let attributedTermsString = NSMutableAttributedString(attributedString: NSAttributedString(string: strApp, attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size: 14)!]))
        attributedTermsString.append(NSAttributedString(string: strTerms, attributes: [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.underlineColor: UIColor.lightGray, NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size: 14)!]))
        //cell.btnTerms.setAttributedTitle(attributedTermsString, for: .normal)
        self.lblTerms.attributedText = attributedTermsString
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func actionTermsCondition(_ sender: Any) {
        LandingStoryboard.showTermsModalView(controller: self)
    }
    @IBAction func actionLoginPhone(_ sender: Any) {
        let myLoginController = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
        self.navigationController?.pushViewController(myLoginController!, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let birthday = segue.destination as? SelectBirthdayController else { return }
        if segue.identifier == LandingSegues.facebook.rawValue {
            birthday.socialStr = LandingSegues.facebook.rawValue
        }
        else if segue.identifier == LandingSegues.google.rawValue {
            birthday.socialStr = LandingSegues.google.rawValue
        }
        else if segue.identifier == LandingSegues.instagram.rawValue {
            birthday.socialStr = LandingSegues.instagram.rawValue
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if UserDefaults.standard.bool(forKey: "isSelectedDOB"){
            self.socialStr = identifier
            self.performSocialLogin()
            return false
        }
        return true
    }
    
    func performSocialLogin() {
        switch self.socialStr {
        case LandingSegues.facebook.rawValue:
            self.loginFromFacebook()
            break
        case LandingSegues.google.rawValue:
            self.loginFromGoogle()
            break
        case LandingSegues.instagram.rawValue:
            self.loginFromInstagram()
            break
        default:
            print("Nope**")
        }
    }
}

// MARK:- Google Login Method
extension LandingViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    func loginFromGoogle() {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("GOOGLE **** \(error.localizedDescription)")
        }
        else {
            var deviceToken = ""
            if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
                deviceToken = temp as! String
            }
            guard let socailId = user.userID else { return }
            guard let emailId =  user.profile.email else { return }
            guard let username = user.profile.name else { return }
//            guard let devicetoken = user.profile.name else { return }
            var image = ""
            if user.profile.hasImage {
                let pic = user.profile.imageURL(withDimension: UInt(round(500 * 500)))
                image = String(describing: pic!)
            }
            //emailID: emailId
            
            self.socialLoginWith(socailID: socailId, userName: username, imageURL: image, deviceToken: deviceToken, deviceType: DeviceiOS, accountType: LoginType.google.rawValue, email: emailId)
        }
    }
}

// MARK:- Facebook Login Method
extension LandingViewController {
    func loginFromFacebook() {
        if FBSDKAccessToken.current() != nil { // ALREADY LOGGED USER
            self.getDetailsFromFacebook()
        }
        else {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            loginManager.logIn(withReadPermissions: ["email","public_profile"], from: self) { (result, error) in
                if (error != nil) {
                    self.showAlertMessage(message: error?.localizedDescription)
                }
                else {
                    self.getDetailsFromFacebook()
                }
            }
        }
    }
    
    func getDetailsFromFacebook() {
        let dictPermi = ["fields": "id, name, first_name, last_name, email, gender, picture.type(large)"]
        
        Util.util.showHUD()
        FBSDKGraphRequest(graphPath: "me", parameters: dictPermi).start(completionHandler: { (connection, result, error) in
            Util.util.hideHUD()
            if error != nil {
                //error?.localizedDescription
                self.showAlertMessage(message:"Please try again.".localized())
            }
            else if result == nil {
                self.showAlertMessage(message: "Sorry not getting proper data from facebook".localized())
            }
            else {
                var deviceToken = ""
                if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
                    deviceToken = temp as! String
                }
                if let dict = result as? [String:Any] {
                    let socialId = dict["id"] as! String
                    let firstName = dict["first_name"] as! String
                    let lastName = dict["last_name"] as! String
                    let username = firstName + " " + lastName
                    let devicetoken = dict["deviceToken"] as! String
                    let deviceType = dict["deviceType"] as! String
                    
                    let picture = dict["picture"] as! [String:Any]
                    let dataPic = picture["data"] as! [String:Any]
                    let image = dataPic["url"] as! String
                    
                    Util.util.showHUD()
                    self.socialLoginWith(socailID: socialId, userName: username, imageURL: image, deviceToken: deviceToken, deviceType: DeviceiOS,accountType: LoginType.facebook.rawValue)
                }
                else {
                    self.showAlertMessage(message: "Sorry not getting proper data from facebook".localized())
                }
            }
        })
    }
}

// MARK:- Instagram Login Method
extension LandingViewController: InstagramProtocol {
    func loginFromInstagram() {
        guard let instaVC = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.instagramView.rawValue) as? InstagramController else { return }
        instaVC.delegate = self
        self.navigationController?.pushViewController(instaVC, animated: true)
    }
    
    func didGetInstaInformation(data: InstaData) {
        var deviceToken = ""
        if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
            deviceToken = temp as! String
        }
        guard let socailId = data.id else { return }
        guard let username = data.fullName else { return }
        guard let image = data.profilePicture else { return }
//        guard let devicetoken = data.profilePicture else { return }
        
        self.socialLoginWith(socailID: socailId, userName: username, imageURL: image, deviceToken: deviceToken, deviceType: DeviceiOS, accountType: LoginType.instagram.rawValue)
    }
}

