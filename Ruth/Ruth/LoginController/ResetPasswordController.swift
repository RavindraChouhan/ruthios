//
//  ResetPasswordController.swift
//  Ruth
//
//  Created by mac on 20/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ResetPasswordController: UIViewController {

    @IBOutlet weak var btnPwd: UIButton!
    @IBOutlet weak var btnConfirmPwd: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPwd: UITextField!
    
    @IBOutlet weak var lblrest: UILabel!
     @IBOutlet weak var lblAppN: UILabel!
    var tokenPwd = ""
    var loginService: LoginService = NetworkClient.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTapGestureToView()
        lblAppN.text = "Ruth".localized()
        self.lblrest.text = "Reset Password".localized()
        self.txtPassword.placeholder = "Password".localized()
        self.txtConfirmPwd.placeholder = "Re-enter Passwod".localized()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPassword(_ sender: Any) {
        self.btnPwd.isSelected = !self.btnPwd.isSelected
        self.txtPassword.toggleSecureEntry()
    }
    
    @IBAction func actionConfirmPassword(_ sender: Any) {
        self.btnConfirmPwd.isSelected = !self.btnConfirmPwd.isSelected
        self.txtConfirmPwd.toggleSecureEntry()
    }
    
    @IBAction func actionNext(_ sender: Any) {
        self.view.endEditing(true)
        guard let pwd = self.txtPassword.text?.trim(), let confirm = self.txtConfirmPwd.text?.trim() else { return }
        if pwd.count == 0 {
            self.showAlertMessage(message: "Please enter password".localized())
            return
        }
        if pwd.count < 6 || pwd.count > 20 {
            self.showAlertMessage(message: "Password must contain 6- 20 characters".localized())
            return
        }
        if !pwd.isValidPassword() {
            self.showAlertMessage(message: "Password must contain characters and numbers both".localized())
            return
        }
        if confirm.count == 0 {
            self.showAlertMessage(message: "Please enter confirm password".localized())
            return
        }
        if pwd != confirm {
            self.showAlertMessage(message: "Passwords must be same".localized())
            return
        }
        if pwd.count > 0 && confirm.count > 0 {
            Util.util.showHUD()
            loginService.resetPassword(token: self.tokenPwd, password: pwd, onResult: { (isReset, message) -> (Void) in
                Util.util.hideHUD()
                if isReset! {
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller is LoginController {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
                else {
//                    let msg = Util.util.getMsgWithKey(message!)
                    self.showAlertMessage(message: message!)
                }
            })
        }
    }
}

extension ResetPasswordController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtPassword {
            self.txtConfirmPwd.becomeFirstResponder()
        }
        else if textField == self.txtConfirmPwd {
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //  self.txtBirthday.text = birthDate
        //        print("self.txtPwd.text?.count)\(self.txtCode.text?.count)")
        if (self.txtConfirmPwd.text?.count)! >= 5 {
            self.btnNext.isSelected = true
            self.btnNext.isUserInteractionEnabled = true
        }else {
            self.btnNext.isSelected = false
            self.btnNext.isUserInteractionEnabled = false
        }
        return true
    }
}
