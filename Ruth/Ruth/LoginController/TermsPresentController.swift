//
//  TermsPresentController.swift
//  Ruth
//
//  Created by mac on 08/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class TermsPresentController: UIViewController {

    @IBOutlet weak var txtTerms: UITextView!
      @IBOutlet weak var lblAName: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
         lblAName.text = "Terms & Conditions".localized()
    }
    
    @IBAction func actionCloseView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension TermsPresentController: UIScrollViewDelegate, UITextViewDelegate {
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        txtTerms.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.backgroundColor = UIColor.hexStringToColor(hex: "2EF809")
    }
}
