//
//  SelectBirthdayController.swift
//  Ruth
//9669306253
//pass 123456asdf
//  Created by mac on 11/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class SelectBirthdayController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtBirthday: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblAName: UILabel!
    @IBOutlet weak var lblEnterB: UILabel!
    @IBOutlet weak var lblYour: UILabel!
    var tabControl : UITabBarController!
    var datePicker = UIDatePicker()
    var selectedAge = 0
    var socialStr = ""
    var isSelected = true
    var loginService: LoginService = NetworkClient.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtBirthday.placeholder =  "Pleaseselectbirthday".localized()
        self.lblAName.text = "Ruth".localized()
        self.lblEnterB.text = "Enter Birthday".localized()
        self.lblYour.text = "Your birthday won’t be shared publicly".localized()
        //self.txtBirthday.becomeFirstResponder()
        self.txtBirthday.addDoneButtonPickerView()
        self.addTapGestureToView()
        var components = DateComponents()
        components.year = -100
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        components.year = -15
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateDate), name: AppNotifications.notificationAgeSelected, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if self.txtBirthday.text?.trim().count == 0 {
            self.showAlertMessage(message: "Please select date of birth".localized())
            return
        }
        if (self.txtBirthday.text?.trim().count)! > 12 {
            self.showAlertMessage(message: "Please select valid date of birth".localized())
            return
        }
        
        self.view.endEditing(true)
        if self.socialStr != "" {
            print("SOCAIL ** \(self.socialStr)")
            if self.selectedAge < 18 {
                LandingStoryboard.showAgeModalView(controller: self)
            }
            else {
                UserDefaults.standard.set(true, forKey:"isSelectedDOB")
                UserDefaults.standard.synchronize()
                self.performSocialLogin()
            }
        }
        else {
            self.datePickerValueChanged(sender: self.datePicker)
            if self.selectedAge < 18 {
                LandingStoryboard.showAgeModalView(controller: self)
            }
            else {
                if isSelected ==  true {
                    let mySignUpController = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: "SignUpController") as? SignUpController
                    self.navigationController?.pushViewController(mySignUpController!, animated: true)
                } else {
                guard let loginVC = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.loginView.rawValue) as? LoginController else { return }
                self.navigationController?.pushViewController(loginVC, animated: true)
             }
            }
        }
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionTextField(_ sender: UITextField) {
        datePicker.datePickerMode = .date
        sender.inputView = datePicker
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy" //"MMM, dd yyyy"
        let birthDate = formatter.string(from: sender.date)
        let dob = self.calculateAge(dob: birthDate)
        self.selectedAge = dob.year
        if dob.year >= 18{
            self.txtBirthday.text = birthDate
            if (self.txtBirthday.text?.trim().count)! > 0 {
                self.btnNext.isSelected = true
                self.btnNext.isUserInteractionEnabled = true
            }else {
                self.btnNext.isSelected = false
                self.btnNext.isUserInteractionEnabled = false
            }
        }else{
            self.txtBirthday.text = birthDate
            self.btnNext.isSelected = false
//            self.btnNext.isUserInteractionEnabled = false
        }
    }
    
    @objc func updateDate() {
        self.datePickerValueChanged(sender: self.datePicker)
    }
}

// MARK:- Socail Login API
extension SelectBirthdayController {
    func performSocialLogin() {
        switch self.socialStr {
        case LandingSegues.facebook.rawValue:
            self.loginFromFacebook()
            break
        case LandingSegues.google.rawValue:
            self.loginFromGoogle()
            break
        case LandingSegues.instagram.rawValue:
            self.loginFromInstagram()
            break
        default:
            print("Nope**")
        }
    }
}

// MARK:- Google Login Method
extension SelectBirthdayController: GIDSignInDelegate, GIDSignInUIDelegate {
    func loginFromGoogle() {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("GOOGLE **** \(error.localizedDescription)")
        }
        else {
            var deviceToken = ""
            if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
                deviceToken = temp as! String
            }
            guard let socailId = user.userID else { return }
            guard let emailId =  user.profile.email else { return }
            guard let username = user.profile.name else { return }
            //guard let deviceTokenid = deviceToken else { return }
            var image = ""
            if user.profile.hasImage {
                let pic = user.profile.imageURL(withDimension: UInt(round(500 * 500)))
                image = String(describing: pic!)
            }
            //emailID: emailId
            self.socialLoginWith(socailID: socailId, userName: username, imageURL: image, deviceToken: deviceToken, deviceType:DeviceiOS, accountType: LoginType.google.rawValue, email: emailId)
        }
    }
}

// MARK:- Facebook Login Method
extension SelectBirthdayController {
    func loginFromFacebook() {
        if FBSDKAccessToken.current() != nil { // ALREADY LOGGED USER
            self.getDetailsFromFacebook()
        }
        else {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            loginManager.logIn(withReadPermissions: ["email","public_profile"], from: self) { (result, error) in
                if (error != nil) {
                    self.showAlertMessage(message: error?.localizedDescription)
                }
                else {
                    self.getDetailsFromFacebook()
                }
            }
        }
    }
    
    func getDetailsFromFacebook() {
       
        
        let dictPermi = ["fields": "id, name, first_name, last_name, email, gender, picture.type(large)"]
        
        Util.util.showHUD()
        FBSDKGraphRequest(graphPath: "me", parameters: dictPermi).start(completionHandler: { (connection, result, error) in
            Util.util.hideHUD()
            if error != nil {
                //error?.localizedDescription
                self.showAlertMessage(message:"Please try again.".localized())
            }
            else if result == nil {
                self.showAlertMessage(message: "Sorry not getting proper data from facebook".localized())
            }
            else {
                var token = ""
                if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
                    token = temp as! String
                }
                
                if let dict = result as? [String:Any] {
                    let socialId = dict["id"] as! String
                    let firstName = dict["first_name"] as! String
                    let lastName = dict["last_name"] as! String
                    let username = firstName + " " + lastName
                    token = dict["deviceToken"] as! String
//                    DeviceiOS = dict["deviceType"] as! String
                    let picture = dict["picture"] as! [String:Any]
                    let dataPic = picture["data"] as! [String:Any]
                    let image = dataPic["url"] as! String
                    
                    Util.util.showHUD()
                    self.socialLoginWith(socailID: socialId, userName: username, imageURL: image, deviceToken: token, deviceType: DeviceiOS, accountType: LoginType.facebook.rawValue)
                }
                else {
                    self.showAlertMessage(message: "Sorry not getting proper data from facebook".localized())
                }
            }
        })
    }
}

// MARK:- Instagram Login Method
extension SelectBirthdayController: InstagramProtocol {
    func loginFromInstagram() {
        guard let instaVC = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.instagramView.rawValue) as? InstagramController else { return }
        instaVC.delegate = self
        self.navigationController?.pushViewController(instaVC, animated: true)
    }
    
    func didGetInstaInformation(data: InstaData) {
        guard let socailId = data.id else { return }
        guard let username = data.fullName else { return }
        guard let image = data.profilePicture else { return }
        guard let deviceTokenid = data.deviceTokenid else { return }
        
        self.socialLoginWith(socailID: socailId, userName: username, imageURL: image, deviceToken: deviceTokenid, deviceType: DeviceiOS, accountType: LoginType.instagram.rawValue)
    }
}
