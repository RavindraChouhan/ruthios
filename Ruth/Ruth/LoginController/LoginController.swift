//
//  LoginController.swift
//  Ruth
//
//  Created by mac on 15/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    @IBOutlet weak var btnRemember: UIButton!
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtPwd: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblAppName: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var btnRemem: UIButton!
    @IBOutlet weak var btnForgot: UIButton!
    @IBOutlet weak var btnCreate: UIButton!
    
    var loginService: LoginService = NetworkClient.shared
    var errorCount = 0
   var tabControl: UITabBarController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblLogin.text = "Phone login".localized()
        self.lblAppName.text = "Ruth".localized()
        self.txtMobile.placeholder = "Mobile Number".localized()
        self.txtPwd.placeholder = "Password".localized()
        self.btnCreate.setTitle("Create a new account".localized(), for: .normal)
         self.btnRemem.setTitle(" Remember me".localized(), for: .normal)
         self.btnForgot.setTitle("ForgotPassword ?".localized(), for: .normal)
        self.txtMobile.addToolbarOnTextfield()
        self.addTapGestureToView()
    }
    
    @IBAction func actionBack(_ sender: Any) {
       self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func actionRememberAccount(_ sender: Any) {
        self.btnRemember.isSelected = !self.btnRemember.isSelected
    }
    
    @IBAction func actionShowPassword(_ sender: Any) {
        self.btnShowPass.isSelected = !self.btnShowPass.isSelected
        self.txtPwd.toggleSecureEntry()
    }
    @IBAction func actionCreateNewAccount(_ sender: Any) {
        let mySignUpController = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: "SelectBirthdayController") as? SelectBirthdayController
        self.navigationController?.pushViewController(mySignUpController!, animated: true)
    }
    @IBAction func actionNext(_ sender: Any) {
        self.view.endEditing(true)
        Util.util.hideHUD()
        guard let mobile = self.txtMobile.text?.trim(), let pwd = self.txtPwd.text?.trim() else { return }
        if mobile.count == 0 {
            self.showAlertMessage(message: "Please enter mobile number".localized())
            return
        }
        if (mobile.count < 6) {
            self.showAlertMessage(message: "Please enter vaild mobile number".localized())
            return
        }
//        if !self.containsOnlyLetters(input: mobile) {
//            self.showAlertMessage(message: "Please enter vaild mobile number")
//            return
//        }
        if pwd.count == 0 {
            self.showAlertMessage(message: "Please enter password".localized())
            return
        }
        if mobile.count > 0 && pwd.count > 0 {
            Util.util.showHUD()
            var deviceToken = ""
            if let temp = UserDefaults.standard.value(forKey: "deviceToken") {
                deviceToken = temp as! String
            }
            let phoneNumber = self.getNumberWithCode(mobile: mobile)
            loginService.loginWithMobileNumber(mobile: phoneNumber, password: pwd, deviceToken: deviceToken) { (user, error) -> (Void) in
                Util.util.hideHUD()
                guard error == nil else {
                    self.errorCount = self.errorCount + 1
                    if self.errorCount == 5 {
                        self.errorCount = 0
                        self.showAlertMessage(message: "It seems you have forgotten your password. Please reset your password".localized())
                        return
                    }
                    self.showAlertMessage(message: "Please enter vaild mobile number".localized())
                    return
                }
                print(" Go TO HOME *** ***")
                if !UserNew.currentUser.isNewUser {
                    
                    objAppDelegate.checkLoginInAppLaunch()
                    //self.loadHomeController()
                } else {
                    guard let tutorial = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.inviteView.rawValue) as? InviteController else { return }
                    let navControl = UINavigationController(rootViewController: tutorial)
                    self.present(navControl, animated: true, completion: nil)
                }
//                UserDefaults.standard.set(true, forKey: "isLogin")
//                UserDefaults.standard.synchronize()
                
            }
        }
    }
//    // MARK: Load HomeController
//    func loadHomeController() {
//        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        self.tabControl = storyboard.instantiateViewController(withIdentifier: "tabControl") as! UITabBarController
//        self.navigationController?.pushViewController(self.tabControl, animated: true)
//        self.setUpTabBar()
//        UINavigationBar.appearance().tintColor = UIColor.white
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size:  18.0)!]
//        NotificationCenter.default.addObserver(self, selector: #selector(self.ChangeTabBar(notification:)), name: AppNotifications.notificationChangeTabBar, object: nil)
//    }
//
//    // MARK: Change TabBar notification
//    @objc func ChangeTabBar(notification:Notification)  {
//        let num:NSNumber = notification.object as! NSNumber
//        let index:NSInteger = num.intValue
//        //log(msg: "index == \(index)")
//        self.tabControl.selectedIndex = index
//        Util.util.tabControl = self.tabControl
//        //        UserDefaults.standard.set(false, forKey: "isAlbumLoaded")
//        //        UserDefaults.standard.synchronize()
//    }
//
//    // MARK: Setup TabBar UI
//    func setUpTabBar() {
//        let customtab:CustomTabBar = Bundle.main.loadNibNamed("CustomTabBar", owner: self, options: nil)?[0] as! CustomTabBar
//        if(DeviceType.iPhoneX){
//            customtab.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 116, width: UIScreen.main.bounds.width, height: 116)
//        }else{
//            customtab.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - customtab.frame.size.height, width: UIScreen.main.bounds.width, height: customtab.frame.size.height)
//        }
//        customtab.tag = 999
//        //        customtab.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleWidth]
//        self.tabControl.view.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleWidth]
//        self.tabControl.view.addSubview(customtab)
//        Util.util.tabControl = self.tabControl
//        Util.util.tabControl.selectedIndex = 0
//    }
}
extension LoginController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtMobile {
            self.txtPwd.becomeFirstResponder()
        }
        else if textField == self.txtPwd {
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (self.txtPwd.text?.count)! >= 5 {
            self.btnNext.isSelected = true
            self.btnNext.isUserInteractionEnabled = true
        }else {
            self.btnNext.isSelected = false
            self.btnNext.isUserInteractionEnabled = false
        }
        return true
    }
}
