//
//  SignUpController.swift
//  Ruth
//
//  Created by mac on 15/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class SignUpController: UIViewController {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSendCode: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var lblregist: UILabel!
    @IBOutlet weak var lblAppName: UILabel!
    @IBOutlet weak var lblTrems: UILabel!
    @IBOutlet weak var btnAlread: UIButton!
   
    
    var seconds = 60
    var timerr: Timer!
    var loginService: LoginService = NetworkClient.shared
    
    var skipOTPTime = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblAppName.text = "Ruth".localized()
        self.lblregist.text = "Register using Phone Number".localized()
        
        let strApp = "Press \" > \" button below to register and agree to the ".localized()
        let strTerms = "Terms and conditions.".localized()
        //SFUIText-Medium
        let attributedTermsString = NSMutableAttributedString(attributedString: NSAttributedString(string: strApp, attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size: 14)!]))
        attributedTermsString.append(NSAttributedString(string: strTerms, attributes: [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.underlineColor: UIColor.lightGray, NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size: 14)!]))
        //cell.btnTerms.setAttributedTitle(attributedTermsString, for: .normal)
        
        self.lblTrems.attributedText = attributedTermsString
        
        self.txtMobile.placeholder = "Enter Phone Number".localized()
        self.txtUsername.placeholder = "Username".localized()
        self.txtCode.placeholder = "Verification Code".localized()
        self.btnSendCode.setTitle("Send" .localized(), for: .normal)
        btnAlread.setTitle("Already registered ?".localized(), for: .normal)
        self.txtMobile.addToolbarOnTextfield()
        self.txtCode.addToolbarOnTextfield()
        self.addTapGestureToView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.comesInForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    @IBAction func actionBack(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
 
    @IBAction func actionSendCode(_ sender: Any) {
        self.view.endEditing(true)
        guard let username = self.txtUsername.text?.trim(), let mobile = self.txtMobile.text?.trim() else { return }
        if username.count == 0 {
            self.showAlertMessage(message: "Please enter username".localized())
            return
        }
        let dict = username.checkStringCharCount()
        let letterCount = dict["letter"] as? Int
//        let digitCount = dict["digit"] as? Int
        
        
        if (letterCount! < 3) {
            self.showAlertMessage(message: "Username must contain 3 characters".localized())
            return
        }
        if mobile.count == 0 {
            self.showAlertMessage(message: "Please enter mobile number".localized())
            return
        }
        
        
        
//        if ((self.txtMobile.text?.trim().count)! > 10) {
//            self.showAlertMessage(message: "Please enter vaild mobile number")
//            return
//        }
        if username.count > 0 && mobile.count > 0 {
            if (mobile.count < 8) {
                self.showAlertMessage(message: "Please enter vaild mobile number".localized())
            }
            else {
                self.btnSendCode.isHidden = true
                self.lblTimer.isHidden = false
                self.btnSendCode.setTitle("Resend" .localized(), for: .normal)
                self.runTimer()
                
                Util.util.showHUD()
                let phoneNumber = self.getNumberWithCode(mobile: mobile)
                loginService.sendOtp(mobile: phoneNumber, username: username, type: OTPType.registration.rawValue, onResult: { (isSend, message) -> (Void) in
                    Util.util.hideHUD()
                    if isSend == false {
//                        let msg = Util.util.getMsgWithKey(message!)
                        self.showAlertMessage(message: message)
                        self.resetTimer()
                        return
                    }
                })
            }
        }
        else {
            self.showAlertMessage(message: "Please enter username and mobile Number".localized())
        }
    }
    
    @IBAction func actionTermsCondition(_ sender: Any) {
        LandingStoryboard.showTermsModalView(controller: self)
    }
    
    @IBAction func actionNext(_ sender: Any) {
        self.view.endEditing(true)
//        let srt = "852" + (self.txtMobile.text?.trim())!
     
        guard let username = self.txtUsername.text?.trim(), let mobile = self.txtMobile.text?.trim(), let code = self.txtCode.text?.trim() else { return }
        if username.count == 0 {
            self.showAlertMessage(message: "Please enter username".localized())
            return
        }
        if mobile.count == 0 {
            self.showAlertMessage(message: "Please enter mobile number".localized())
            return
        }
        if (mobile.count < 6) {
            self.showAlertMessage(message: "Please enter vaild mobile number".localized())
            return
        }
//        if (mobile.count) > 12 {
//            self.showAlertMessage(message: "Please enter vaild mobile number")
//            return 852
//        }
        
        if username.count > 0 && mobile.count > 0 && code.count > 0 {
            
            Util.util.showHUD()
            let phoneNumber = self.getNumberWithCode(mobile: mobile)
            loginService.verifyOtp(mobile: phoneNumber, otpCode: code, onResult: { (isVerify, message) -> (Void) in
                Util.util.hideHUD()
                if isVerify! {
                    guard let signup = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: LandingControllers.signupDetail.rawValue) as? SignupDetailController else { return }
                    signup.selectedUsername = username
                    signup.selectedPhone = phoneNumber
                    self.navigationController?.pushViewController(signup, animated: true)
                }
                else {
//                    let msg = Util.util.getMsgWithKey(message!)
                    self.showAlertMessage(message: message!)
                    
                    self.resetTimer()
                }
            })
        }
        else {
            self.showAlertMessage(message: "Please enter verification code".localized())
        }
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        let myLoginController = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
        self.navigationController?.pushViewController(myLoginController!, animated: true)
    }
}
extension SignUpController {
    func runTimer() {
        let skipDuretion = 1
        skipOTPTime = Date.addComponent(component: .minute, n: skipDuretion, date: Date())
        UserDefaults.standard.set(skipOTPTime, forKey: "OTPCodeTime")
        UserDefaults.standard.synchronize()
        
        self.timerr = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        let currentDate = Date()
        let str = currentDate.offsetFrom(date: skipOTPTime)
        
        self.lblTimer.text = "\(str) s"
        if str == "00" || str.hasPrefix("-") {
            self.resetTimer()
        }
    }
    
    @objc func comesInForeground() {
        if let skipSongTimeTmp = UserDefaults.standard.value(forKey: "OTPCodeTime") as? Date
        {
            skipOTPTime = skipSongTimeTmp
            if timerr == nil {
                self.timerr = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            }else{
                if timerr.isValid{
                   timerr.invalidate()
                    
                    self.timerr = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
                }
            }
        }
    }
    
    func resetTimer() {
        self.seconds = 60
        self.lblTimer.text = "\(self.seconds) s"
        self.btnSendCode.isHidden = false
        self.lblTimer.isHidden = true
        if self.timerr != nil {
            self.timerr.invalidate()
        }
        
    }
}

extension SignUpController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtUsername {
            self.txtMobile.becomeFirstResponder()
        }
        else if textField == self.txtMobile {
            self.txtCode.becomeFirstResponder()
        }
        else if textField == self.txtCode {
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //  self.txtBirthday.text = birthDate
//        print("self.txtPwd.text?.count)\(self.txtCode.text?.count)")
        if (self.txtCode.text?.count)! >= 5 {
            self.btnNext.isSelected = true
            self.btnNext.isUserInteractionEnabled = true
        }else {
            self.btnNext.isSelected = false
            self.btnNext.isUserInteractionEnabled = false
        }
        return true
    }
}
