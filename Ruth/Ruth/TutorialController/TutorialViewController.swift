//
//  TutorialViewController.swift
//  Raut
//  Copyright © 2017 Relibit Labs, LLC. All rights reserved.
//

import UIKit
import SDWebImage

class TutorialViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionViewTutorial: UICollectionView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
     var tabControl: UITabBarController!
    var currentIdx = 0
    
//    var mArrayData = [[String : Any]]()
    var mArrayData = [Tutorial]()
    override func viewDidLoad() {
//        self.addInvaildDeviceNotificationObserver()
        super.viewDidLoad()
        self.showTutorial()
        self.btnBack.alpha = 0
        self.btnBack.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.btnSkip.setTitle("Skipt".localized(), for: .normal)
        
//        self.lblTitle.font = UIFont(name: "Avenir-Black", size: DeviceType.IS_IPHONE_5 ? 28 : 32)//Avenir Heavy 24.0
        self.collectionViewTutorial.register(UINib(nibName: "TutorialCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TutorialCollectionCell")
        self.collectionViewTutorial.dataSource = self
        self.collectionViewTutorial.delegate = self
        self.collectionViewTutorial.reloadData()
        self.pageControl.currentPage = 0
        self.btnLeft.alpha = 0.0
        self.btnLeft.isHidden = true
        self.pageControl.numberOfPages = self.mArrayData.count
//            if DeviceType.iPhoneX
//            {
//                self.lblTitle.frame = CGRect(x: self.lblTitle.frame.minX, y: self.lblTitle.frame.minY + 30, width: self.lblTitle.frame.width, height: self.lblTitle.frame.height)
//                //        }else
//                //        {
//                //            self.lblTitle.frame = CGRect(x: self.lblTitle.frame.minX, y: self.lblTitle.frame.minY , width: self.lblTitle.frame.width, height: self.lblTitle.frame.height)
//            }
//
//            if DeviceType.iPhone4 || DeviceType.iPhone5
//            {
//                self.collectionViewTutorial.frame = CGRect(x: self.collectionViewTutorial.frame.minX, y: self.lblTitle.frame.minY + 20, width: self.collectionViewTutorial.frame.width, height:  UIScreen.main.bounds.height - (self.lblTitle.frame.maxY))
//            }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func moveLeft(_ btn : UIButton){
        if self.currentIdx >= 0{
            self.currentIdx -= 1
            self.collectionViewTutorial.scrollToItem(at: IndexPath(item: self.currentIdx, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            self.updatePageControl()
        }
    }
    
    @IBAction func moveRight(_ btn : UIButton){
        if self.currentIdx <= (self.mArrayData.count-1){
            self.currentIdx += 1
            self.collectionViewTutorial.scrollToItem(at: IndexPath(item: self.currentIdx, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            self.updatePageControl()
        }
    }
    
    func showTutorial() {
        let Url = BaseURL + "/tutorials/getAll"
        Util.util.showHUD()
        Service.service.get(url: Url) { (result) in
            print("showTutorial  ==\(result)" , "tutorialsUrl  = \(Url) ")
            Util.util.hideHUD()
            if result is [String : Any] {
                let response = result as! [String:Any]
                let arrdata = response ["data"] as! [[String:Any]]
                for dict in arrdata {
                    let tutorial = Tutorial(info: dict)
                    self.mArrayData.append(tutorial)
                }
                self.pageControl.currentPage = 0
                self.pageControl.numberOfPages = self.mArrayData.count
                self.pageControl.isHidden = false
                let tutorial = self.mArrayData[0]
                self.btnNext.setTitle(tutorial.zhHans, for: .normal)
                self.collectionViewTutorial.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mArrayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TutorialCollectionCell", for: indexPath) as! TutorialCollectionCell
        let tutorial = self.mArrayData[indexPath.item]
        cell.imgTutorial.sd_setImage(with: URL(string: tutorial.url)!, placeholderImage: #imageLiteral(resourceName: "bgLogin"))
        cell.lblDes.text = tutorial.description
        cell.lblTitle.text = tutorial.title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if DeviceType.iPhone5 || DeviceType.iPhone4 {
            var size = collectionView.frame.size
            size.height = 436
                size.width = UIScreen.main.bounds.width
            return size
        }
        return CGSize(width:UIScreen.main.bounds.width, height:UIScreen.main.bounds.height - (DeviceType.iPhoneX ? 224 : 112))
    }
    
    func updatePageControl() {
        self.pageControl.currentPage = self.currentIdx
        let tutorial = self.mArrayData[self.currentIdx]
        self.btnNext.setTitle(tutorial.zhHans, for: .normal)
        if self.pageControl.currentPage == 0
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.btnLeft.alpha = 0.0
            }, completion: { (isDone) in
                if isDone{
                    self.btnLeft.isHidden = true
                }
            })
        }else{
            self.btnLeft.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.btnLeft.alpha = 1.0
            }, completion: { (isDone) in
                if isDone{
                    
                }
            })
        }
        
        if self.pageControl.currentPage == self.mArrayData.count-1
        {
            //self.btnBack.isHidden = false
            //            self.btnNext.setTitle("Get Started", for: .normal)
            UIView.animate(withDuration: 0.3, animations: {
                self.btnRight.alpha = 0.0
            }, completion: { (isDone) in
                if isDone{
                    self.btnRight.isHidden = true
                }
            })
        }else{
            // self.btnBack.isHidden = false
            // self.btnNext.setTitle("Next", for: .normal)
            self.btnRight.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.btnRight.alpha = 1.0
            }, completion: { (isDone) in
                if isDone{
                }
            })
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.currentIdx = Int(scrollView.contentOffset.x/UIScreen.main.bounds.width)
        self.updatePageControl()
    }
    
    @IBAction func actionSkip(_ btn : UIButton)
    {
        UserDefaults.standard.set(true, forKey: "isTutorialViewed")
        UserDefaults.standard.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionNext(_ btn : UIButton)
    {
        if self.currentIdx < self.mArrayData.count-1{
            self.moveRight(self.btnRight)
        }else{
            self.actionSkip(self.btnSkip)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

