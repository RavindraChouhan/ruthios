
//
//  TutorialViewController.swift
//  Ruth
//
//  Created by mac on 02/07/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import SDWebImage

class TutorialViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var collectionTutorial: UICollectionView!
    
    var dataAuthToken = [AuthToken]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        self.showAuthToken()
        self.setupCollectionView()
        // Do any additional setup after loading the view.
    }
    func setupCollectionView() {
        self.collectionTutorial.delegate = self
        self.collectionTutorial.dataSource = self
        self.collectionTutorial.register(UINib(nibName: "TutorialCCell", bundle: nil), forCellWithReuseIdentifier: "TutorialCCell")
        self.collectionTutorial.reloadData()
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSkip(_ sender: Any) {
        self.showAlertMessage(message: "actionSkip")
    }
    @IBAction func actionNext(_ sender: Any) {
        self.showAlertMessage(message: "actionNext")
    }

    func showAuthToken() {
        let Url = BaseURL + "/tutorials/getAll"
//        NetworkClient.
        Service.service.get(url: Url) { (result) in
            print("showAuthToken%%%%%%%%  ==\(result)" , "Url $$$$  = \(Url) ")
            if result is [String : Any] {
                let response = result as! [String:Any]
                let arrdata = response ["data"] as! [[String:Any]]
                for dict in arrdata {
                    let auth = AuthToken(info: dict)
                    self.dataAuthToken.append(auth)
                    self.collectionTutorial.reloadData()
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataAuthToken.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TutorialCCell", for: indexPath) as! TutorialCCell
        let dataimg = dataAuthToken[indexPath.item]
         cell.imgPoster.sd_setImage(with: URL(string: dataimg.url), placeholderImage: nil)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:UIScreen.main.bounds.width, height: 190
        )
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
