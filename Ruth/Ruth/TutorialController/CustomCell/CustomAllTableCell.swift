//
//  ContactTableCell.swift
//  Ruth
//
//  Created by mac on 20/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class ContactTableCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblName.text = "All".localized()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func loadDefaultCell(contact: ContactModel, index: Int) {
        self.lblName.text = contact.username
        self.btnCheck.tag = index
        self.btnCheck.isSelected = contact.isSelected
    }
    
    func loadContactsData(contact: ContactModel, index: Int) {
        self.lblName.text = contact.username
        self.lblPhone.text = contact.mobile
        self.btnCheck.tag = index
        self.btnCheck.isSelected = !contact.isSelected
    }
}

class FacebookInstaTableCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
         lblName.text = "All".localized()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class GoogleTableCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
         lblName.text = "All".localized()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

