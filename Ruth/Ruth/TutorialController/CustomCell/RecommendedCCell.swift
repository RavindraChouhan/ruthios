//
//  RecommendedCCell.swift
//  Ruth
//
//  Created by mac on 26/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RecommendedCCell: UICollectionViewCell {
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblAddess: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lbl123: UILabel!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var lblBorn: UILabel!
    @IBOutlet weak var lblFan: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
     
        viewLine.layer.borderColor = UIColor.hexStringToColor(hex:"D0021B").cgColor
        viewLine.layer.borderWidth = 2
    }
    func loadDefaultCell(index: Int) {
//        self.lblName.text = index.username TutorialCCell
        self.btnFollow.tag = index
        self.btnFollow.isSelected = !btnFollow.isSelected
    }

}

class LastRecommendedCCell: UICollectionViewCell {
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewLine.layer.borderColor = UIColor.hexStringToColor(hex:"D0021B").cgColor
        viewLine.layer.borderWidth = 2
        //#C91215
        // Initialization code
    }
    

}

class TutorialCCell: UICollectionViewCell {
  @IBOutlet weak var imgPoster : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }
}
