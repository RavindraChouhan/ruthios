//
//  InviteTableController.swift
//  Ruth
//
//  Created by mac on 20/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class InviteTableController: UITableViewController {
    
    var contacts = [ContactModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UITableView DataSource & Delegate Method
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InviteAllCell", for: indexPath) as! ContactTableCell
            cell.selectionStyle = .none
            let contact = self.contacts[indexPath.row]
            cell.loadDefaultCell(contact: contact, index: indexPath.row)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableCell", for: indexPath) as! ContactTableCell
            cell.selectionStyle = .none
            let indexx = indexPath.row
            let contact = self.contacts[indexx]
            cell.loadContactsData(contact: contact, index: indexPath.row)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            for contact in self.contacts {
                if (self.contacts.first?.isSelected)! {
                    contact.isSelected = false
                } else {
                    contact.isSelected = true
                }
            }
        }
        else {
            let indexx = indexPath.row
            let contact = self.contacts[indexx]
            contact.isSelected = !contact.isSelected
            
            // Reset all button toogle
            self.contacts.first?.isSelected = false
        }
        self.tableView.reloadData()
    }
}
