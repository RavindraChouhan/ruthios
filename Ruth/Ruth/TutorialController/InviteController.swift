
//  InviteController.swift
//  Ruth
//
//  Created by mac on 28/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit
import Contacts
import FBSDKCoreKit

class InviteController: UIViewController {
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblYourFriend: UILabel!
    @IBOutlet weak var btnBNext: UIBarButtonItem!
    @IBOutlet weak var btnSkip: UIButton!
    
    var table : InviteTableController?
    
    var contactsArr = [ContactModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.btnBNext.title = "Next".localized()
         self.btnSkip.setTitle("Skip".localized(), for: .normal)
        let strSearchTitle = " "+"Search".localized()
        self.btnSearch.setTitle(strSearchTitle, for: .normal)
        self.lblYourFriend.text = "Your Friend".localized()
        self.setupNavigationBar()
        self.navigationItem.title = "Invite Friends".localized()
        
        self.fetchContacts()
        self.txtSearch.addToolbarOnTextfield()
        self.txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.setupNavigationBar()
        getFriends()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let tableController = segue.destination as? InviteTableController else {
            return
        }
        self.table = tableController
        
    }
    
    func getFriends()  {
        
        let params = ["fields": "id, first_name, last_name, name, email, picture"]
        
        let graphRequest = FBSDKGraphRequest(graphPath: "me/taggable_friends", parameters: params)
        // (Shivam) :- "me/taggable_friends" for all friends
        let connection = FBSDKGraphRequestConnection()
        connection.add(graphRequest, completionHandler: { (connection, result, error) in
            if error == nil {
                if let userData = result as? [String:Any] {
                    print(userData)
                }
            } else {
                print("Error Getting Friends \(String(describing: error))");
            }
            
        })
        
        connection.start()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        LandingStoryboard.showLandingScreen(over: self)
    }
    @IBAction func actionShowPopView(_ sender: Any) {
        LandingStoryboard.showLandingScreen(over: self)
    }
    @IBAction func actionSkip(_ sender: Any) {
        let myRecommendedController = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: "RecommendedController") as? RecommendedController
        self.navigationController?.pushViewController(myRecommendedController!, animated: true)
//        self.showAlertMessage(message: "Recommended view In-Progress".localized())
    }
    
    @IBAction func actionNext(_ sender: Any) {
        let myRecommendedController = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: "RecommendedController") as? RecommendedController
        self.navigationController?.pushViewController(myRecommendedController!, animated: true)
//        self.showAlertMessage(message: "Recommended view In-Progress")
    }
}

extension InviteController: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.btnSearch.isHidden = (textField.text?.count)! > 0 ? true : false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

extension InviteController {
    
    func fetchContacts()  {
        Util.util.showHUD()
        let keys = [CNContactPhoneNumbersKey, CNContactOrganizationNameKey, CNContactNicknameKey, CNContactGivenNameKey, CNContactFamilyNameKey, CNContactMiddleNameKey, CNContactNamePrefixKey]
        
        var message: String!
        self.contactsArr.removeAll()
        let contactsStore = CNContactStore()
        do {
            try contactsStore.enumerateContacts(with: CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])) {
                (contact, cursor) -> Void in
                if (!contact.phoneNumbers.isEmpty) {
                    for phoneNumber in contact.phoneNumbers {
                        let contactModel = ContactModel()
                        
                        let firstName = contact.givenName
                        let lastName = contact.familyName
                        
                        var fName = ""
                        var lName = ""
                        if !firstName.trim().isEmpty {
                            fName = contact.givenName
                        }
                        if !lastName.trim().isEmpty {
                            lName = contact.familyName
                        }
                        contactModel.username = fName + " " + lName
                        
                        if let phoneNumberStruct = phoneNumber.value as? CNPhoneNumber {
                            let phoneNumber = phoneNumberStruct.stringValue
                            contactModel.mobile = phoneNumber
                        }
                        if !contactModel.username.trim().isEmpty {
                            self.contactsArr.append(contactModel)
                        }
                        break
                    }
                }
            }
            
            self.contactsArr = self.contactsArr.sorted {
                $0.username.localizedCaseInsensitiveCompare($1.username) == .orderedAscending
            }
            
            let tmpContact = ContactModel()
            tmpContact.username = "All".localized()
            tmpContact.isSelected = true
            self.contactsArr.insert(tmpContact, at: 0)
            
            Util.util.hideHUD()
            self.table?.contacts = self.contactsArr
            self.table?.tableView.reloadData()
            
            if contactsArr.count == 0 {
                message = "No contacts were found matching the given phone number.".localized()
            }
            else {
                // Send contact to server..
            }
        }
        catch {
            message = "Unable to fetch contacts.".localized()
        }
        Util.util.hideHUD()
        if message != nil {
            self.showAlertMessage(message: message)
            let alert = UIAlertController(title: "Unable to fetch contacts".localized(), message: "Please allow contact permission from setting".localized(), preferredStyle: .alert)
            let setting = UIAlertAction(title: "Settings".localized(), style: .default) { (isDD) in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Setting opened: \(success)") // Prints true
                    })
                }
            }
            let cancel = UIAlertAction(title: "Cancel".localized(), style: .default) { (isDD) in
            }
            alert.addAction(cancel)
            alert.addAction(setting)
            self.present(alert, animated: true, completion: nil)
        }
        self.table?.contacts = self.contactsArr
        self.table?.tableView.reloadData()
    }
}

class ContactModel {
    var username = ""
    var mobile = ""
    var status = 0
    var isSelected = false
    public init() {}
    
    public init(info: [String:Any]) {
        self.username = info["username"] as! String
        self.mobile = info["mobile"] as! String
        self.status = info["status"] as! Int
        self.isSelected = info["isSelected"] as! Bool
    }
}
