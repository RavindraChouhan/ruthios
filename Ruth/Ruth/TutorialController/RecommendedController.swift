//
//  RecommendedController.swift
//  Ruth
//
//  Created by mac on 20/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//

import UIKit

class RecommendedController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var btnok: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionRecommended: UICollectionView!
    @IBOutlet weak var viewTransperent: UIView!
    @IBOutlet weak var viewShow: UIView!
    
    var arrFollowUsers = [AllFollowU]()
    var tabControl : UITabBarController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllfollowUsersHome()
        viewShow.layer.cornerRadius = 13
        viewShow.clipsToBounds = true
        self.setupNavigationBar()
        self.navigationItem.title = " 推薦主播"
       collectionRecommended.register(UINib.init(nibName: "RecommendedCCell", bundle: nil), forCellWithReuseIdentifier: "RecommendedCCell")
        //NewRecommendedCCell
        collectionRecommended.register(UINib.init(nibName: "LastRecommendedCCell", bundle: nil), forCellWithReuseIdentifier: "LastRecommendedCCell")
        self.btnCancel.setTitle("Cancel".localized(), for: .normal)
        self.btnok.setTitle("OK".localized(), for: .normal)
        self.lblTitle.text = "Do you really want to exist ?".localized()
        self.collectionRecommended.delegate = self
        self.collectionRecommended.dataSource = self
        self.collectionRecommended.reloadData()

    }
        @IBAction func actionShowPop(_ sender: Any) {
             self.showView(isshow: true)
        }
    
    func showView(isshow :Bool){
        self.showAlertViewWithAlertView(viewAlert:viewShow , WithBlackTransperentView: viewTransperent)
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.showView(isshow: false)
    }
    @IBAction func actionOk(_ sender: Any) {
        let stroryBord = UIStoryboard(name: "Main", bundle: nil)
        let landingViewController = stroryBord.instantiateViewController(withIdentifier: "LandingViewController") as! LandingViewController
        self.navigationController?.pushViewController(landingViewController, animated: true)
    }
    @IBAction func actionNext(_ sender: Any) {
        objAppDelegate.checkLoginInAppLaunch()
    }
    func getAllfollowUsersHome() {
        let Url = BaseURL + followUsersHome
        //        Util.util.showHUD()
        Service.service.get(url: Url) { (result) in
            // print("followUsers  ==\(result)" , "Url followUsers  = \(Url) ")
            //            Util.util.hideHUD()
            self.arrFollowUsers.removeAll()
            if result is [String:Any] {
                let response = result as! [String:Any]
                if response["status"] as! Bool {
                    let data = response["data"] as! [[String:Any]]
                    for dict in data {
                        let follow = AllFollowU(info: dict)
                        self.arrFollowUsers.append(follow)
                    }
                    self.collectionRecommended.delegate = self
                    self.collectionRecommended.dataSource = self
                    self.collectionRecommended.reloadData()
                } else {
                    let strMsg = Util.util.getMsgWithDict(response)
                    self.showAlertMessage(message: strMsg)
                }
                
            } else if result is Error
            {
                let error = result as! Error
                var strMsg = "Something went wrong".localized()
                strMsg = error.localizedDescription
                if strMsg == "The Internet connection appears to be offline."
                {
                    strMsg = "No internet connection".localized()
                }
                self.showAlertMessage(message: strMsg)
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFollowUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.last == 9 {
            let cell = self.collectionRecommended.dequeueReusableCell(withReuseIdentifier: "LastRecommendedCCell", for: indexPath) as! LastRecommendedCCell
            cell.btnFollow.addTarget(self, action: #selector(addButton(sender:)), for: .touchUpInside)
//            cell.loadDefaultCell(index: indexPath.row)
            return cell
        }
        else {
            let cell = self.collectionRecommended.dequeueReusableCell(withReuseIdentifier: "RecommendedCCell", for: indexPath) as! RecommendedCCell
            let data = arrFollowUsers[indexPath.row]
            if data.gender == "male" {
                cell.lblMale.text = "男"
            }
            if data.gender == "female" {
                cell.lblMale.text = "女"
            }
            cell.lblName.text = data.name
            cell.img.sd_setImage(with: URL(string: data.photoUrl as String), placeholderImage:#imageLiteral(resourceName: "icon"))
            cell.lblFollowers.text = "\(data.followers)"
            //        cell.lblAddess.text = data.address
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.isSelected = data.isFollow
            cell.btnFollow.addTarget(self, action: #selector(addButton(sender:)), for: .touchUpInside)
            let strMonth = Util.util.getFormatedDateWithRequiredFormat(strFormat: "MMMM ", withDate: data.dob)
            let strDay = Util.util.getFormatedDateWithRequiredFormat(strFormat: "d", withDate: data.dob)
            cell.lblData.text = Util.util.changeChineseMonth(strMonth) + Util.util.changeChineseDay(strDay) + " 日"
            let strYear = Util.util.getFormatedDateWithRequiredFormat(strFormat: "yyyy", withDate: data.dob)
            cell.lblBorn.text = "生於" + Util.util.changeChineseYear(strYear) + "年"
            return cell
        }
    }
    @objc func addButton(sender: UIButton) {
        let button = sender
        button.isSelected = !button.isSelected
        if button.isSelected {
            button.setImage(#imageLiteral(resourceName: "btncheck"), for: UIControlState.normal)
            button.setTitle("", for: UIControlState.normal)
            button.backgroundColor = .white
            button.alpha = 1.0
        }else{
            //button.setImage(UIImage(named : "Unfollow"), for: UIControlState.normal)
            button.setImage(UIImage(), for: UIControlState.normal)
            button.setTitle("關注", for: UIControlState.normal)
            button.backgroundColor = .black
            button.alpha = 0.7
//
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(UIScreen.main.bounds.width - 30)/2, height: 244)
    }
    
//    // MARK: Load HomeController
//    func loadHomeController() {
//        UserDefaults.standard.set(true, forKey: "isLoadDOB")
//        UserDefaults.standard.synchronize()
//        
//        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        self.tabControl = storyboard.instantiateViewController(withIdentifier: "tabControl") as! UITabBarController
//        self.navigationController?.pushViewController(self.tabControl, animated: true)
//        self.setUpTabBar()
//        UINavigationBar.appearance().tintColor = UIColor.white
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font: UIFont(name: ".SFUIText-Medium", size:  18.0)!]
//        NotificationCenter.default.addObserver(self, selector: #selector(self.ChangeTabBar(notification:)), name: AppNotifications.notificationChangeTabBar, object: nil)
//    }
//    
//    // MARK: Change TabBar notification
//    @objc func ChangeTabBar(notification:Notification)  {
//        let num:NSNumber = notification.object as! NSNumber
//        let index:NSInteger = num.intValue
//        //log(msg: "index == \(index)")
//        self.tabControl.selectedIndex = index
//        Util.util.tabControl = self.tabControl
////        UserDefaults.standard.set(false, forKey: "isAlbumLoaded")
////        UserDefaults.standard.synchronize()
//    }
//    
//    // MARK: Setup TabBar UI
//    func setUpTabBar() {
//        let customtab:CustomTabBar = Bundle.main.loadNibNamed("CustomTabBar", owner: self, options: nil)?[0] as! CustomTabBar
//        if(DeviceType.iPhoneX){
//            customtab.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 116, width: UIScreen.main.bounds.width, height: 116)
//        }else{
//            customtab.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - customtab.frame.size.height, width: UIScreen.main.bounds.width, height: customtab.frame.size.height)
//        }
//        customtab.tag = 999
//        //        customtab.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleWidth]
//        self.tabControl.view.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleWidth]
//        self.tabControl.view.addSubview(customtab)
//        Util.util.tabControl = self.tabControl
//        Util.util.tabControl.selectedIndex = 0
//    }
    

}
