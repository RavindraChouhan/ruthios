//
//  TutorialCollectionCell.swift
//  Florish
//
//  Created by mac on 14/12/17.
//  Copyright © 2017 Relibit Labs, LLC. All rights reserved.
//

import UIKit

class TutorialCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var txtStep: UITextView!
    @IBOutlet weak var imgTutorial: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        //Avenir Heavy 24.0
//        if DeviceType.iPhone5 || DeviceType.iPhone4{
//            self.txtStep.font = UIFont(name: "Avenir-Heavy", size:  22.0)
//        }
        // Initialization code
    }

}
