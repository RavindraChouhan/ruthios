
//
//  AppDelegate.swift
//  Ruth
//
//  Created by mac on 06/06/18.
//  Copyright © 2018 Vogueme. All rights reserved.
//
import UIKit
import CoreLocation
import FBSDKCoreKit
import GoogleSignIn
import GoogleMaps
import AWSS3
import UserNotifications
import UserNotificationsUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, LiveStreamingViewDelegate, UNUserNotificationCenterDelegate {
    var window: UIWindow?
    var tabControl: UITabBarController!
    var locationManager = CLLocationManager()
    var liveNaviController :  UINavigationController!
    var mapStyle: GMSMapStyle!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //zhHans
        // iOS 10 support
        UIApplication.shared.applicationIconBadgeNumber = 0
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9.0, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8.0, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        GMSServices.provideAPIKey("AIzaSyCvYipuB0B0kkwskcH7d5TJDp0WRz0-5AQ")
        UserDefaults.standard.set("zh-Hant", forKey:"SelectedLanguage")
        UserDefaults.standard.synchronize()
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: AWS.accessKey.base64Decoded()!, secretKey: AWS.secretKey.base64Decoded()!)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast2, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        //self.setLanguage()
        if let strLang = UserDefaults.standard.value(forKey: "SelectedLanguage") as? String
        {
            if strLang == "zh-Hant"
            {
                UserNew.currentUser.isChineseSelected = true
                let localString = "zh-Hant"
                Localize.setCurrentLanguage(localString)
            }else{
                UserNew.currentUser.isChineseSelected = false
            }
        }
        if UserDefaults.standard.bool(forKey: "isLogin")
        {
            UserNew.currentUser.initWithInfo(isAPICall: true)
        }else
        {
            UserNew.currentUser.initWithInfo(isAPICall: false)
        }
        
        self.checkLoginInAppLaunch()
        Util.util.readJsonFile()
        UserNew.currentUser.isChineseSelected = true
        // Setup Facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Setup Google
        GIDSignIn.sharedInstance().clientID = GoogleClientId
        
        // Get user location
        self.getUserCurrentLocation()
        self.LoadMapStyle()
        return true
    }
    func LoadMapStyle() {
        do {
            if let styleURL = Bundle.main.url(forResource: "gmapstyle", withExtension: "json") {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find gmapstyle.json")
            }
        }catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    func setLanguage() {
        // Change Localization
        var localString = ""
        let lanTitle = "Chinese_Tr"
        if lanTitle == "Chinese_Tr" {
            localString = "zh-Hant"
            Localize.setCurrentLanguage(localString)
        }
        else {
            localString = "zh-Hans"
            Localize.setCurrentLanguage(localString)
        }
        NotificationCenter.default.post(name: AppNotifications.notificationChangeLangugage, object: nil)
    }
    func checkLoginInAppLaunch() {
        if let str = UserDefaults.standard.value(forKey: "selectedLanguage") as? String {
            UserNew.currentUser.isChineseSelected = str == "zhHans" ? true : false
        }
        if UserDefaults.standard.bool(forKey: "isLogin")
        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.tabControl = UIStoryboard.landingStoryboard.instantiateViewController(withIdentifier: "tabControl") as! UITabBarController
            self.window?.rootViewController =  self.tabControl
            self.window?.makeKeyAndVisible()
            self.setUpTabBar()
            NotificationCenter.default.addObserver(self, selector: #selector(self.ChangeTabBar(notification:)), name: AppNotifications.notificationChangeTabBar, object: nil)
            //  STPPaymentConfiguration.shared().publishableKey = Config.stripe_Private_Key
        }
    }
    //     MARK:- Receive observer on change bottom tab
    @objc func ChangeTabBar(notification:Notification)  {
        let num:NSNumber = notification.object as! NSNumber
        let index:NSInteger = num.intValue
        if index == 4{
            if liveNaviController == nil {
                let liveController = LiveStreamingViewC.create().config(self.tabControl)
                liveController.delegate = self
                liveController.show(into: self.tabControl)
            }
        } else{
            self.tabControl.selectedIndex = index
            Util.util.tabControl = self.tabControl
        }
    }
    
    func showPinView() {
        let createLivePopupVC = CreateLivePopupVC.create().config(self.tabControl)
        createLivePopupVC.show(into: self.tabControl)
    }
    
    func showCreateLiveView() {
        let createLiveVC = CreateLiveStreamingViewC.create().config(self.tabControl)
        createLiveVC.show(into: self.tabControl)
    }
    
    func setUpTabBar() {
        let customtab:CustomTabBar = Bundle.main.loadNibNamed("CustomTabBar", owner: self, options: nil)?[0] as! CustomTabBar
        if(DeviceType.iPhoneX){
            customtab.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 95, width: UIScreen.main.bounds.width, height: 115)
        }else{
            customtab.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - customtab.frame.size.height, width: UIScreen.main.bounds.width, height: customtab.frame.size.height)
        }
        customtab.tag = 999
        customtab.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleWidth]
        self.tabControl.view.autoresizingMask = [.flexibleLeftMargin,.flexibleRightMargin,.flexibleWidth]
        self.tabControl.view.addSubview(customtab)
        Util.util.tabControl = self.tabControl
        Util.util.tabControl.selectedIndex = 0
    }
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //Facebook app activation
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let strDeviceToken = deviceToken.reduce("", {$0 + String(format: "%02.2hhx", $1)})
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var strDeviceToken = ""
        for i in 0..<deviceToken.count {
            strDeviceToken += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        rLog("DeviceDeviceDeviceToken===\(strDeviceToken)")
        if strDeviceToken.trim().isEmpty {
             UserDefaults.standard.set(strDeviceToken, forKey: "deviceToken")
            //UserDefaults.standard.set("", forKey: UserDefaults.keys.deviceToken)
            UserDefaults.standard.synchronize()
        }
        else {
             UserDefaults.standard.set(strDeviceToken, forKey: "deviceToken")
           // UserDefaults.standard.set(strDeviceToken, forKey: UserDefaults.keys.deviceToken)
            UserDefaults.standard.synchronize()
        }
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
    }
  
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        rLog("didReceiveRemoteNotification userInfo == \(userInfo)")
        if ((application.applicationState == UIApplicationState.background) || (application.applicationState == UIApplicationState.inactive))
        {
            application.applicationIconBadgeNumber += 1
            if(userInfo["id"] != nil) {
                let id = userInfo["id"] as! String
                let type = userInfo["type"] as! String
                
                if let notificationType = userInfo["notificationType"] as? String{
                    UserDefaults.standard.set(notificationType, forKey: "notificationType")
                }
                UserDefaults.standard.set(true, forKey: "Deeplink")
                UserDefaults.standard.set(type, forKey: "type")
                UserDefaults.standard.set(id, forKey: "Deeplink_id")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
                    //                    NotificationCenter.default.post(name: AppNotifications.notificationShowDeeplinkInfo, object: nil)
                })
            }
        }
    }
    //MARK:- Facbook setup and login Mananger
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    func requestNotificationAuthorization(application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.absoluteString.contains("google") {
            let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String
            let annotationKey = options[UIApplicationOpenURLOptionsKey.annotation]
            return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotationKey)
        }
        else {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }
    }
}

// MARK:- CLLocationManagerDelegate Method
extension AppDelegate: CLLocationManagerDelegate {
    func getUserCurrentLocation() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        // TODO: Remove after testing
        //        currentLatitude = userLocation.coordinate.latitude
        //        currentLongitude = userLocation.coordinate.longitude
        
        let location = CLLocation(latitude: currentLatitude, longitude: currentLongitude)
        self.fetchCityAndCountry(location: location) { country, error in
            guard let country = country, error == nil else { return }
            //            print("Address :: \(country)")
            isVaildCountry = country == defaultCountry ? true : false
            currentCountry = country
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Error :: Location denied by user")
        isVaildCountry = false
        currentLatitude = 22.28552
        currentLongitude = 114.15769
    }
    func fetchCityAndCountry(location: CLLocation, completion: @escaping (_ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.country, error)
        }
    }
}
